# data-sync-agent

此工具用于将数据从IntarkDB同步到openGauss数据库中，支持时序数据表的全量同步和增量同步。

#### 一、工程说明

##### 1、编程语言：Java

##### 2、编译工具：Maven

#### 二、使用说明

##### 1、数据表准备
云端数据库的数据表，第一列必须是node_id varchar，其余字段与边端IntarkDB数据表完全一致。

##### 2、配置文件修改
修改src/main/resources文件夹下conf.yaml文件，其中
-   centerDb为云端openGauss数据库的对应配置信息，[ip, port, user, password]为openGauss数据库的ip地址、端口、用户名、密码，
-   localDb为边端intarkDB数据库的对应配置信息，[ip, port, user, password, nodeId]为intarkDB数据库的ip地址、端口、用户名、密码、节点id
-   syncTable为需要同步的数据表的配置信息，[tableName, syncColumn]为数据表名，数据表中的时间索引列
-   loopWait为任务队列处理线程空闲等待时长，单位为毫秒

##### 3、程序运行
启动云端openGauss数据库

启动边端IntarkDB数据库的local-db程序

使用maven打包data-sync-agent，可以得到cloud-edge-0.0.1-SNAPSHOT.jar，将修改好的配置文件放在同一目录下，输入指令执行
```
java -Dconfig_file='conf.yaml' -jar .\cloud-edge-0.0.1-SNAPSHOT.jar
```
