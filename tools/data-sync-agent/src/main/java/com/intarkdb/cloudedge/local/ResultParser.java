/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.local;

import org.opengauss.util.csv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ResultParser {

    private static final Logger log = LoggerFactory.getLogger(ResultParser.class);

    /**
     * cache the trans methods in a map
     */
    private static HashMap<String, LinkedList<Method>> parserMap = new HashMap<>();
    private static HashMap<String, Integer> dateMap = new HashMap<>();

    /**
     * get the trans method of each column, based on the msg of metadata
     *
     * @param resultSet
     * @return
     */
    private static LinkedList<Method> initParserChain(String tableName, String columnName, ResultSet resultSet) throws SQLException, NoSuchMethodException {
        LinkedList<Method> result = new LinkedList<>();
        Class<?> resultSetClass = ResultSet.class;
        int columnIndex = -1;
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            if (columnIndex == -1 && columnName.equals(metaData.getColumnName(i))) {
                columnIndex = i;
                dateMap.put(tableName, columnIndex);
            }
            switch (metaData.getColumnType(i)) {
                case Types.INTEGER:
                    result.add(resultSetClass.getDeclaredMethod("getInt", int.class));
                    break;
                case Types.BIGINT:
                    result.add(resultSetClass.getDeclaredMethod("getLong", int.class));
                    break;
                case Types.CHAR:
                case Types.VARCHAR:
                case Types.LONGNVARCHAR:
                    result.add(resultSetClass.getDeclaredMethod("getString", int.class));
                    break;
//                    case Types.TINYINT:
//                    case Types.SMALLINT:
//                        result.add(resultSetClass.getDeclaredMethod("getShort", int.class));
//                        break;
//                    case Types.FLOAT:
//                        result.add(resultSetClass.getDeclaredMethod("getFloat", int.class));
//                        break;
//                    case Types.DECIMAL:
//                        result.add(resultSetClass.getDeclaredMethod("getBigDecimal", int.class));
//                        break;
//                    case Types.DATE:
//                        result.add(resultSetClass.getDeclaredMethod("getTime", int.class));
//                        break;
//                    case Types.TIMESTAMP:
//                        result.add(resultSetClass.getDeclaredMethod("getTimestamp", int.class));
//                        break;
//                    case Types.BOOLEAN:
//                        result.add(resultSetClass.getDeclaredMethod("getBoolean", int.class));
//                        break;
                default:
//                        System.out.println("unknown type");
                    result.add(resultSetClass.getDeclaredMethod("getString", int.class));
            }
        }
        parserMap.put(tableName, result);
        return result;
    }

    public static int countParse(ResultSet resultSet) {
        try {
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            log.error("count sql parse error!");
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 读取时间戳正好等于右闭区间的情况
     * 不需要考虑读取时间戳右闭区间没读完的情况
     * @param tableName
     * @param columnName
     * @param resultSet
     * @return
     */
    public static List<List<String>> simpleParse(String tableName, String columnName, ResultSet resultSet) {
        // get the trans method from the cache
        LinkedList<Method> parserChain = parserMap.get(tableName);
        // build it if it doesn't exist
        if (parserChain == null) {
            try {
                parserChain = initParserChain(tableName, columnName, resultSet);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        int colCnt = parserChain.size();
        int rowCnt = 0;
        List<List<String>> listList = new ArrayList<>();
        try {
            while (resultSet.next()) {
                List<String> line = new ArrayList<>(colCnt);
                int column = 1;
                for (Method m : parserChain) {
                    Object o = m.invoke(resultSet, column);
                    if (o == null) {
                        line.add("");
                    } else {
                        line.add(o + "");
                    }
                    column++;
                }
                listList.add(line);
                rowCnt++;
            }
            return listList;
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *  结果转成列表格式待进一步处理
     * @param tableName
     * @param columnName
     * @param resultSet
     * @return
     */
    public static LocalReadResult commonParse(String tableName, String columnName, ResultSet resultSet)
            throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // get the trans method from the cache
        LinkedList<Method> parserChain = parserMap.get(tableName);
        // build it if it doesn't exist
        if (parserChain == null) {
            parserChain = initParserChain(tableName, columnName, resultSet);
        }
        int colCnt = parserChain.size();
        int rowCnt = 0;
        int dateIndex = dateMap.get(tableName);
        LastDateInfo lastDateInfo = new LastDateInfo();
        LocalReadResult result = new LocalReadResult();
        List<List<String>> listList = new ArrayList<>();
        while (resultSet.next()) {
            List<String> line = new ArrayList<>(colCnt);
            int column = 1;
            for (Method m : parserChain) {
                Object o = m.invoke(resultSet, column);
                if (o == null) {
                    line.add("");
                } else {
                    line.add(o + "");
                }
                check(column, dateIndex, line, lastDateInfo);
                column++;
            }
            listList.add(line);
            rowCnt++;
        }
        result.setResultStringList(listList);
        result.setDate(lastDateInfo.getLastDate());
        result.setSameDateCount(lastDateInfo.getLastDateCount());
        result.setCount(rowCnt);
        return result;
    }

    private static void check(int column, int dateIndex, List<String> line, LastDateInfo lastDateInfo) {
        if (column == dateIndex) {
            String thisDate = line.get(column - 1);
            if (thisDate.equals(lastDateInfo.getLastDate())) {
                int lastDateCount = lastDateInfo.getLastDateCount();
                lastDateInfo.setLastDateCount(lastDateCount + 1);
            } else {
                lastDateInfo.setLastDate(thisDate);
                lastDateInfo.setLastDateCount(1);
            }
        }
    }

    /**
     * 将结果转成csv格式
     * @return
     */
    public static String listParseToCsv(List<List<String>> list, String nodeId) {
        StringWriter stringWriter = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(stringWriter);
        for (List<String> l : list) {
            l.add(0, nodeId);
            csvWriter.writeNext(l.toArray(new String[l.size()]));
        }
        return stringWriter.toString();
    }

    /**
     * 结果直接转成csv格式
     * @param tableName
     * @param columnName
     * @param resultSet
     * @return
     */
    @Deprecated
    public static CsvResult csvParse(String tableName, String columnName, ResultSet resultSet) {
        // get the trans method from the cache
        LinkedList<Method> parserChain = parserMap.get(tableName);
        // build it if it doesn't exist
        if (parserChain == null) {
            try {
                parserChain = initParserChain(tableName, columnName, resultSet);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        StringWriter stringWriter = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(stringWriter);
        int colCnt = parserChain.size();
        int rowCnt = 0;
        int dateIndex = dateMap.get(tableName);
        String lastDate = null;
        try {
            while (resultSet.next()) {
                String[] line = new String[colCnt];
                lastDate = runParser(parserChain, line, resultSet, dateIndex, lastDate);
                csvWriter.writeNext(line);
                rowCnt++;
            }
            return new CsvResult(rowCnt, lastDate, stringWriter.toString());
        } catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            try {
                csvWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static String runParser(LinkedList<Method> parserChain, String[] line, ResultSet resultSet, int dateIndex, String lastDate) throws InvocationTargetException, IllegalAccessException {
        int column = 1;
        for (Method m : parserChain) {
            line[column - 1] = m.invoke(resultSet, column) + "";
            if (column == dateIndex) {
                lastDate = line[column - 1];
            }
            column++;
        }
        return lastDate;
    }
}
