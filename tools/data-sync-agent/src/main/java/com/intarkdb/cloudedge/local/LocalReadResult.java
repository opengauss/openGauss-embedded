/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.local;

import java.util.List;

public class LocalReadResult {
    /**
     * 结果值
     */
    private List<List<String>> resultStringList;

    /**
     * 本次读取最后一个时间戳
     */
    private String date;

    /**
     * 本次读取跟最后一个时间戳一致的数据行数
     */
    private int sameDateCount;

    /**
     * 本次读取结果数
     */
    private int count;

    public List<List<String>> getResultStringList() {
        return resultStringList;
    }

    public void setResultStringList(List<List<String>> resultStringList) {
        this.resultStringList = resultStringList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSameDateCount() {
        return sameDateCount;
    }

    public void setSameDateCount(int sameDateCount) {
        this.sameDateCount = sameDateCount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
