/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.center;

import com.intarkdb.cloudedge.config.DbMsg;
import org.opengauss.copy.CopyManager;
import org.opengauss.core.BaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 中心数据库相关的方法
 */

public class CopyManagerHelper {

    private static final Logger log = LoggerFactory.getLogger(CopyManagerHelper.class);

    private CopyManagerHelper() {
    }

    private static CopyManagerHelper singleton = new CopyManagerHelper();

    public static CopyManagerHelper getCopyManagerHelper() {
        return singleton;
    }

    private static CopyManager copyManager;
    private static Connection connection;
    private static ReentrantLock lock = new ReentrantLock();

    public static void connectToCenterDB(DbMsg dbMsg) {
        try {
            Class.forName("org.opengauss.Driver");
            String url = MessageFormat.format("jdbc:opengauss://{0}:{1}/postgres", dbMsg.getIp(), dbMsg.getPort());
            connection = DriverManager.getConnection(url, dbMsg.getUser(), dbMsg.getPassword());
            copyManager = new CopyManager((BaseConnection) connection);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void releaseConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean uploadData(String tableName, String data) {
        if (data == null || "".equals(data)) {
            return true;
        }
        lock.lock();
        try {
            copyIn(tableName, data);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable t) {
            // todo handle duplicate upload error
            t.printStackTrace();
        } finally {
            lock.unlock();
        }
        return false;
    }

    private void copyIn(String tableName, String data) throws SQLException, IOException {
        long t = System.currentTimeMillis();
        copyManager.copyIn("COPY " + tableName + " FROM STDIN WITH (FORMAT CSV)", new StringReader(data));
        log.debug("copy manager cost {} ms", System.currentTimeMillis() - t);
    }

    public boolean uploadDataNoLock(String tableName, String data) {
        if (data == null || "".equals(data)) {
            return true;
        }
        try {
            copyIn(tableName, data);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable t) {
            // todo handle duplicate upload error
            t.printStackTrace();
        }
        return false;
    }


}
