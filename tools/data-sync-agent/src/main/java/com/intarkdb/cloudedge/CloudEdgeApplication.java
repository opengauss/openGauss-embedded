/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge;

import com.intarkdb.cloudedge.config.ConfigProperties;
import com.intarkdb.cloudedge.config.ConfigReader;
import com.intarkdb.cloudedge.mission.SimpleMissionUploadManagerImpl;
import com.intarkdb.cloudedge.mission.UploadMissionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudEdgeApplication {

	private static final Logger log = LoggerFactory.getLogger(CloudEdgeApplication.class);

	public static void main(String[] args) {
//		SpringApplication.run(CloudEdgeApplication.class, args);

		// read config
		ConfigProperties conf = ConfigReader.readConfigFromYaml();

		UploadMissionManager syncManager = new SimpleMissionUploadManagerImpl();
		// UploadMissionManager syncManager = new AsyncMissionUploadManagerImpl();
		syncManager.init(conf);
		log.info("init finished");
		syncManager.start();
	}

}
