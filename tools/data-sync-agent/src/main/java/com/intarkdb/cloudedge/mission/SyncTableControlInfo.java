/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.mission;

import com.intarkdb.cloudedge.common.Constant;

public class SyncTableControlInfo {
    /**
     * 表名
     */
    private String tableName;

    /**
     * 任务类型
     */
    private int type;

    /**
     * 时序表的时间列，由用户指定
     */
    private String syncColumn;

    /**
     * 同步任务扫描左区间（左开）
     */
    private Long beginTime;

    /**
     * 同步任务扫描右区间（右闭）
     */
    private Long endTime;

    /**
     * 节点id
     */
    private String nodeId;

    /**
     * 唯一列
     */
    private String uniqueColumn;

    public SyncTableControlInfo(String tableName, String syncColumn, Long beginTime) {
        this.tableName = tableName;
        this.syncColumn = syncColumn;
        this.beginTime = beginTime;
        this.endTime = beginTime + Constant.SCAN_WINDOW_RANGE;
    }

    public SyncTableControlInfo(String tableName, String syncColumn, Long beginTime, Long endTime, String nodeId) {
        this.tableName = tableName;
        this.syncColumn = syncColumn;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.nodeId = nodeId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSyncColumn() {
        return syncColumn;
    }

    public void setSyncColumn(String syncColumn) {
        this.syncColumn = syncColumn;
    }

    public Long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Long beginTime) {
        this.beginTime = beginTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getUniqueColumn() {
        return uniqueColumn;
    }

    public void setUniqueColumn(String uniqueColumn) {
        this.uniqueColumn = uniqueColumn;
    }

}
