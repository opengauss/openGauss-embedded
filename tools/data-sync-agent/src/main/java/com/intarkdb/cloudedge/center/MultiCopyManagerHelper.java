/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.center;

import com.intarkdb.cloudedge.config.DbMsg;
import com.intarkdb.cloudedge.config.SyncTableInfo;
import org.opengauss.copy.CopyManager;
import org.opengauss.core.BaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MultiCopyManagerHelper {
    private static final Logger log = LoggerFactory.getLogger(MultiCopyManagerHelper.class);

    private static String url;
    private static String user;
    private static String password;
    private static Map<String, Connection> connectionMap;

    public static void init(DbMsg dbMsg, List<SyncTableInfo> syncTableInfos) {
        try {
            Class.forName("org.opengauss.Driver");

            String target = "jdbc:opengauss://center_ip:center_port/postgres"
                    .replaceFirst("center_ip", dbMsg.getIp())
                    .replaceFirst("center_port", dbMsg.getPort());
            url = target;
            user = dbMsg.getUser();
            password = dbMsg.getPassword();
            connectionMap = new ConcurrentHashMap<>();

            for (SyncTableInfo info : syncTableInfos) {
                connectionMap.put(info.getTableName(), DriverManager.getConnection(url, user, password));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private final static int DUPLICATE_ERR_CODE = 16153;

    public static boolean uploadData(String tableName, String data) {
        if (data == null || "".equals(data)) {
            return true;
        }
        long t = System.currentTimeMillis();
        Connection connection = connectionMap.get(tableName);
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                return false;
            }
            connectionMap.put(tableName, connection);
        }
        try {
            CopyManager copyManager = new CopyManager((BaseConnection) connection);
            copyManager.copyIn("COPY " + tableName + " FROM STDIN WITH (FORMAT CSV)", new StringReader(data));
//            log.debug("copy manager cost {} ms", System.currentTimeMillis() - t);
            return true;
        } catch (SQLException e) {
            if ("Database connection failed when starting copy".equals(e.getMessage())) {
                connectionMap.remove(tableName);
                try {
                    connection.close();
                } catch (SQLException e1) {
                }
                return false;
            } else if (e.getErrorCode() == DUPLICATE_ERR_CODE) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable t1) {
            // todo handle duplicate upload error
            t1.printStackTrace();
        }
        return false;
    }

}
