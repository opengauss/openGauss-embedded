/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.mission;

import com.intarkdb.cloudedge.center.MultiCopyManagerHelper;
import com.intarkdb.cloudedge.common.Constant;
import com.intarkdb.cloudedge.local.LocalDBReader;
import com.intarkdb.cloudedge.local.LocalReadResult;
import com.intarkdb.cloudedge.local.ResultParser;
import com.intarkdb.cloudedge.tool.TimeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class SimpleUploadMission extends UploadMission {

    private static final Logger log = LoggerFactory.getLogger(SimpleUploadMission.class);

    public SimpleUploadMission(SyncTableControlInfo controlMsg) {
        super(controlMsg);
    }

    /**
     *
     * @param reader
     * @param controlMsg
     * @return 返回本次已处理数据
     */
    @Override
    public Integer readAndUpload(LocalDBReader reader, SyncTableControlInfo controlMsg, AtomicLong uploadCounter) {
        // default value of next read begin timestamp
        long updateBeginTimestamp = controlMsg.getEndTime();
        String nowDate = TimeParser.microsecondToString(updateBeginTimestamp);

//        CopyManagerHelper copyManagerHelper = CopyManagerHelper.getCopyManagerHelper();

        String lastSyncDate = TimeParser.microsecondToString(controlMsg.getBeginTime());

        // read data from table
        ResultSet dataRaw = reader.readData(controlMsg.getTableName(), controlMsg.getSyncColumn(), lastSyncDate, nowDate, Constant.BATCH_SIZE);

        LocalReadResult localReadResult = null;
        try {
            localReadResult = ResultParser.commonParse(controlMsg.getTableName(), controlMsg.getSyncColumn(), dataRaw);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        List<List<String>> resultStringList = localReadResult.getResultStringList();

        // 右闭区间是否已经读完？
        if (localReadResult.getCount() == Constant.BATCH_SIZE) {
            List<List<String>> lastDateResult = reader.readData(controlMsg.getTableName(), controlMsg.getSyncColumn(),
                    localReadResult.getDate(), localReadResult.getSameDateCount(), Constant.BATCH_SIZE);
            resultStringList.addAll(lastDateResult);
            updateBeginTimestamp = TimeParser.stringToMicrosecond(localReadResult.getDate());
        }

        String csvString = ResultParser.listParseToCsv(resultStringList, controlMsg.getNodeId());
        // upload to center db
        if (!MultiCopyManagerHelper.uploadData(controlMsg.getTableName(), csvString)) {
            log.warn("upload failed! table : " + controlMsg.getTableName() + " time: " + nowDate);
            // todo 区分不同的返回异常
            return 0;
        }

        // update control message
        long scanEndTimestamp = Instant.now().toEpochMilli() - Constant.SCAN_WAIT_RANGE;
        if (controlMsg.getEndTime() < scanEndTimestamp) {
            controlMsg.setEndTime(scanEndTimestamp);
        }
        controlMsg.setBeginTime(updateBeginTimestamp);
        reader.updateSyncTime(controlMsg.getTableName(), controlMsg.getBeginTime(), controlMsg.getEndTime());

        if (this.failCount > 0) {
            this.failCount = 0;
        }

        return resultStringList.size();
    }

}
