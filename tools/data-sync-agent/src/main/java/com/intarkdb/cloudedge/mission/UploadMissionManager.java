/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.mission;

import com.intarkdb.cloudedge.config.ConfigProperties;

public interface UploadMissionManager {

    /**
     * mission initialize
     * @param conf
     */
    void init(ConfigProperties conf);

    /**
     * refresh config
     * @param conf
     */
    void refreshConfig(ConfigProperties conf);

    /**
     * add mission to the running loop
     * @param missionMsg
     * @return
     */
    boolean addMission(SimpleUploadMission missionMsg);

    /**
     * begin to run the mission loop
     */
    void start();

    /**
     * stop the mission loop and close the resource
     */
    void close();
}
