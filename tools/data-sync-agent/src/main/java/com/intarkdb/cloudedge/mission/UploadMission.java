/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.mission;

import com.intarkdb.cloudedge.local.LocalDBReader;

import java.util.concurrent.atomic.AtomicLong;

public abstract class UploadMission {
    protected SyncTableControlInfo controlMsg;

    protected int failCount = 0;

    public UploadMission(SyncTableControlInfo controlMsg) {
        this.controlMsg = controlMsg;
    }

    public SyncTableControlInfo getControlMsg() {
        return controlMsg;
    }

    public void setControlMsg(SyncTableControlInfo controlMsg) {
        this.controlMsg = controlMsg;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }

    public void addFailCount() {
        if (this.failCount < Integer.MAX_VALUE) {
            this.failCount++;
        }
    }

    public abstract Integer readAndUpload(LocalDBReader reader, SyncTableControlInfo controlMsg, AtomicLong uploadCounter);
}
