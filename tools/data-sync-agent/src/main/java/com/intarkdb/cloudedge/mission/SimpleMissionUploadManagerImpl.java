/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.mission;

import com.intarkdb.cloudedge.center.MultiCopyManagerHelper;
import com.intarkdb.cloudedge.common.Constant;
import com.intarkdb.cloudedge.common.ThreadUncaughtExceptionHandler;
import com.intarkdb.cloudedge.config.ConfigProperties;
import com.intarkdb.cloudedge.config.LocalDbMsg;
import com.intarkdb.cloudedge.local.LocalDBReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class SimpleMissionUploadManagerImpl implements UploadMissionManager {

    private static final Logger log = LoggerFactory.getLogger(SimpleMissionUploadManagerImpl.class);

    private ConcurrentLinkedQueue<UploadMission> missionQueue;

    private ConcurrentHashMap<String, LocalDBReader> readerMap;

    public AtomicLong uploadCount = new AtomicLong(0);

    private ScheduledExecutorService executorService;

    private boolean closeFlag = false;

    private Integer loopWait;


    /**
     * init the mission
     */
    public void init(ConfigProperties conf) {
        readerMap = new ConcurrentHashMap<>();
        // init mission
        missionQueue = new ConcurrentLinkedQueue<>();

        int coreSize = 0;

        // init copyManager
//        CopyManagerHelper.connectToCenterDB(conf.getCenterDb());
        MultiCopyManagerHelper.init(conf.getCenterDb(), conf.getSyncTable());

        // get access to local instarDB
        for (LocalDbMsg localDbMsg: conf.getLocalDb()) {
            LocalDBReader reader = new LocalDBReader(localDbMsg);
            // check if the sync manage table exists, if not, build it.
            reader.initManageTable();
            readerMap.put(localDbMsg.getNodeId(), reader);
            reader.initSimpleMissionQueue(missionQueue, conf.getSyncTable(), localDbMsg.getNodeId());
            coreSize += conf.getSyncTable().size();
        }

        // init thread pool
        executorService = Executors.newScheduledThreadPool(coreSize);

        loopWait = conf.getLoopWait();
    }

    @Override
    public void refreshConfig(ConfigProperties conf) {
        // todo
        // shut down old ScheduledExecutorService

        // initialize and start new ScheduledExecutorService

    }

    /**
     * add mission to list
     * @return
     */
    @Override
    public boolean addMission(SimpleUploadMission missionMsg) {
        return missionQueue.offer(missionMsg);
    }

    @Override
    public void start() {
        speedPrint();
        uploadLoop();
    }

    @Override
    public void close() {
        closeFlag = true;
    }

    public void uploadLoop() {
        while (!closeFlag) {
            if (missionQueue.isEmpty()) {
                try {
                    Thread.sleep(loopWait);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                UploadMission msg;
                while ((msg = missionQueue.poll()) != null) {
                    handleMission(msg, missionQueue);
                }
            }
        }
        executorService.shutdown();
    }

    private void handleMission(UploadMission mission, ConcurrentLinkedQueue<UploadMission> missionQueue) {

        Long now = Instant.now().toEpochMilli();
        Long delayTime = Math.max(mission.getControlMsg().getEndTime() / 1000 + Constant.SCAN_WAIT_RANGE - now, 0);
        delayTime = Math.max(delayTime, Math.min(mission.getFailCount(), Constant.FAIL_LIMIT) * 5 * 1000);
        // put the mission to delay executorService
        executorService.schedule(() -> {
            long begin = 0;
            try {
                begin = Instant.now().toEpochMilli();
                int handleNum = mission.readAndUpload(readerMap.get(mission.getControlMsg().getNodeId()), mission.getControlMsg(), null);
                uploadCount.getAndAdd(handleNum);
            } catch (Throwable t) {
                log.error("handle mission error!" + mission.toString());
                t.printStackTrace();
                mission.addFailCount();
            } finally {
                // 将更新完的任务放回列表，下次处理
                long end = Instant.now().toEpochMilli();
//                log.debug("mission update " + mission.getControlMsg().getNodeId() + " " + mission.getControlMsg().getTableName() + " cost " + (end - begin));
                missionQueue.offer(mission);
            }
        }, delayTime, TimeUnit.MILLISECONDS);
    }


    private void speedPrint() {
        Thread thread = new Thread(() -> {
            long lastCount = 0;
            int secCount = 1;
            while (!closeFlag) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long thisCount = uploadCount.get();
                log.info("upload count: " + thisCount + " upload speed: " + (thisCount - lastCount) + " avg speed: " + (thisCount / secCount));
                lastCount = thisCount;
                secCount++;
            }
        });
        thread.setUncaughtExceptionHandler(new ThreadUncaughtExceptionHandler(log));
        thread.start();
    }
}
