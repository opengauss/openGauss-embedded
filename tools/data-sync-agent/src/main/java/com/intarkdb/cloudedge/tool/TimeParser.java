/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.tool;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class TimeParser {

    private static final Logger log = LoggerFactory.getLogger(TimeParser.class);

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    private static DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SS");
    private static DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
    private static DateTimeFormatter formatter5 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");

    private static final int FORMATTER_LENGTH = 19;
    private static final int FORMATTER_LENGTH1 = 23;
    private static final int FORMATTER_LENGTH2 = 10;
    private static final int FORMATTER_LENGTH3 = 22;
    private static final int FORMATTER_LENGTH4 = 21;
    private static final int FORMATTER_LENGTH5 = 26;

    public static LocalDateTime toLocalDatetime(String dateTimeString) {
        LocalDateTime localDateTime = null;

        try {
            switch (dateTimeString.length()) {
                case FORMATTER_LENGTH:
                    localDateTime = LocalDateTime.parse(dateTimeString, formatter);
                    break;
                case FORMATTER_LENGTH1:
                    localDateTime = LocalDateTime.parse(dateTimeString, formatter1);
                    break;
                case FORMATTER_LENGTH2:
                    localDateTime = LocalDateTime.parse(dateTimeString, formatter2);
                    break;
                case FORMATTER_LENGTH3:
                    localDateTime = LocalDateTime.parse(dateTimeString, formatter3);
                    break;
                case FORMATTER_LENGTH4:
                    localDateTime = LocalDateTime.parse(dateTimeString, formatter4);
                    break;
                case FORMATTER_LENGTH5:
                    localDateTime = LocalDateTime.parse(dateTimeString, formatter5);
                    break;
                default:
                    System.out.println("unsupported datetime string: " + dateTimeString);
                    return localDateTime;
            }
            return localDateTime;
        } catch (Exception e) {
            log.error("date time trans failed, String: " + dateTimeString);
            return localDateTime;
        }
    }

    public static long toTimestamp(String dateTimeString) {
        LocalDateTime localDateTime = toLocalDatetime(dateTimeString);

        if (localDateTime != null) {
            return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } else {
            return 0;
        }
    }

    public static String toString(long timestamp) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault());

        return localDateTime.format(formatter1);
    }

    public static String microsecondToString(long microsecondTimestamp) {
        long milli = microsecondTimestamp / 1000;
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(milli), ZoneId.systemDefault());
        return localDateTime.format(formatter1) + String.format("%03d", microsecondTimestamp % 1000);
    }

    public static long stringToMicrosecond(String dateTimeString) {
        long milli = toTimestamp(dateTimeString);

        return milli * 1000 + Integer.valueOf(dateTimeString.substring(23));
    }

    public static String roundUpTimestamp(String dateTimeString) {
        LocalDateTime localDateTime = toLocalDatetime(dateTimeString);

        if (localDateTime != null) {

            if (localDateTime.getNano() != 0) {
                LocalDateTime ceilTimestamp = localDateTime.plus(1, ChronoUnit.SECONDS);
                return ceilTimestamp.format(formatter5);
            } else {
                return localDateTime.format(formatter5);
            }
        } else {
            return null;
        }

    }

    public static void main(String... args) {
//        System.out.println(roundUpTimestamp("2020-01-01 11:11:11.231"));
        System.out.println(microsecondToString(1702006505063167L));
        System.out.println(stringToMicrosecond("2023-12-08 11:35:05.063167"));
    }
}
