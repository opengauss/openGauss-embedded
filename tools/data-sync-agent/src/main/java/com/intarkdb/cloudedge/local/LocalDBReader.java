/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.local;

import com.intarkdb.cloudedge.common.Constant;
import com.intarkdb.cloudedge.config.LocalDbMsg;
import com.intarkdb.cloudedge.config.SyncTableInfo;
import com.intarkdb.cloudedge.mission.AsyncUploadMission;
import com.intarkdb.cloudedge.mission.SimpleUploadMission;
import com.intarkdb.cloudedge.mission.SyncTableControlInfo;
import com.intarkdb.cloudedge.mission.UploadMission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

public class LocalDBReader {

    private static final Logger log = LoggerFactory.getLogger(LocalDBReader.class);

    public LocalDBReader(LocalDbMsg localDbMsg) {
        this.localDbMsg = localDbMsg;
        String projectRootPath = System.getProperty("user.dir");
        try {
            Class.forName("org.instardb.jdbc.InstarDriver");
            String url = "jdbc:instardb://local_ip:local_port"
                    .replaceAll("local_ip", localDbMsg.getIp())
                    .replaceAll("local_port", localDbMsg.getPort());
            connection = DriverManager.getConnection(url, localDbMsg.getUser(), localDbMsg.getPassword());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected Connection connection;

    protected LocalDbMsg localDbMsg;


    /**
     * init the sync manage table
     */
    public void initManageTable() {
        try (Statement statement = connection.createStatement();
             ResultSet i = statement.executeQuery(Constant.DESCRIBE_SYNC_TABLE_SQL)) {
                
            return;
        } catch (SQLException e) {
            log.info("Manage table not existed, build manage table");
            try (Statement statement = connection.createStatement();
                 ResultSet j = statement.executeQuery(Constant.CREATE_SYNC_TABLE_SQL)) {
                log.debug("create manage table");
            } catch (SQLException e1) {
                log.error("create Manage table error");
                e1.printStackTrace();
            }
        }

    }

    public void initSimpleMissionQueue(ConcurrentLinkedQueue<UploadMission> missionQueue, List<SyncTableInfo> syncTableInfo, String nodeId) {

        Map<String, SyncTableInfo> tableMap = syncTableInfo.stream().collect(Collectors.toMap(SyncTableInfo::getTableName, a -> a));

        try (
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery("SELECT table_name, sync_column, begin_timestamp, end_timestamp from SYS_CLOUD_SYNC");
            while (resultSet.next()) {
                String tableName = resultSet.getString(1);
                String syncColumn = resultSet.getString(2);
                Long beginTime = resultSet.getLong(3);
                Long endTime = resultSet.getLong(4);

                SyncTableControlInfo controlMsg = new SyncTableControlInfo(tableName, syncColumn, beginTime, endTime, nodeId);
                missionQueue.offer(new SimpleUploadMission(controlMsg));
                tableMap.remove(tableName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // new table config to upload
        int id = missionQueue.size();
        Long endTimeStamp = (Instant.now().toEpochMilli() - Constant.SCAN_WAIT_RANGE) * 1000;
        for (Map.Entry<String, SyncTableInfo> entry : tableMap.entrySet()) {
            try (PreparedStatement prepareStatement = connection.prepareStatement("insert into SYS_CLOUD_SYNC(id, type, table_name, sync_column, begin_timestamp, end_timestamp, node_id) values (?, ?, ?, ?, ?, ?, ?)")) {
                prepareStatement.setInt(1, id++);
                prepareStatement.setInt(2, 1);
                prepareStatement.setString(3, entry.getKey());
                prepareStatement.setString(4, entry.getValue().getSyncColumn());
                prepareStatement.setLong(5, 0);
                prepareStatement.setLong(6, endTimeStamp);
                prepareStatement.setString(7, "");      // todo node id
                int i = prepareStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            // add to mission queue
            SyncTableControlInfo controlMsg = new SyncTableControlInfo(entry.getKey(), entry.getValue().getSyncColumn(), 0L, endTimeStamp, nodeId);
            missionQueue.offer(new SimpleUploadMission(controlMsg));
        }

    }

    public void initAsyncMissionQueue(ConcurrentLinkedQueue<UploadMission> missionQueue, List<SyncTableInfo> syncTableInfo, String nodeId) {

        Map<String, SyncTableInfo> tableMap = syncTableInfo.stream().collect(Collectors.toMap(SyncTableInfo::getTableName, a -> a));

        try (
                Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery("SELECT table_name, sync_column, begin_timestamp, end_timestamp from SYS_CLOUD_SYNC");
            while (resultSet.next()) {
                String tableName = resultSet.getString(1);
                String syncColumn = resultSet.getString(2);
                Long beginTime = resultSet.getLong(3);
                Long endTime = resultSet.getLong(4);

                SyncTableControlInfo controlMsg = new SyncTableControlInfo(tableName, syncColumn, beginTime, endTime, nodeId);
                missionQueue.offer(new AsyncUploadMission(controlMsg));
                tableMap.remove(tableName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // new table config to upload
        int id = missionQueue.size();
        Long endTimeStamp = Instant.now().toEpochMilli() - Constant.SCAN_WAIT_RANGE;
        for (Map.Entry<String, SyncTableInfo> entry : tableMap.entrySet()) {
            try (PreparedStatement prepareStatement = connection.prepareStatement("insert into SYS_CLOUD_SYNC(id, type, table_name, sync_column, begin_timestamp, end_timestamp, node_id) values (?, ?, ?, ?, ?, ?, ?)")) {
                prepareStatement.setInt(1, id++);
                prepareStatement.setInt(2, 1);
                prepareStatement.setString(3, entry.getKey());
                prepareStatement.setString(4, entry.getValue().getSyncColumn());
                prepareStatement.setLong(5, 0);
                prepareStatement.setLong(6, endTimeStamp);
                prepareStatement.setString(7, "");      // todo node id
                int i = prepareStatement.executeUpdate();
                // add to mission queue
                SyncTableControlInfo controlMsg = new SyncTableControlInfo(entry.getKey(), entry.getValue().getSyncColumn(), 0L, endTimeStamp, nodeId);
                missionQueue.offer(new AsyncUploadMission(controlMsg));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public ResultSet readData(String tableName, String columnName, String lastSyncTime, String nowDate, int batchSize) {
        String sql = MessageFormat.format(Constant.SQL_SELECT_1, tableName, columnName, columnName, batchSize);
        try (PreparedStatement prepareStatement = connection.prepareStatement(sql)) {

            prepareStatement.setString(1, lastSyncTime);
            prepareStatement.setString(2, nowDate);

            return prepareStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<List<String>> readData(String tableName, String columnName, String timeStamp, int beginIndex, int batchSize) {
        String countSql = MessageFormat.format(Constant.SQL_SELECT_2, tableName, columnName);
        String querySql = MessageFormat.format(Constant.SQL_SELECT_3, tableName, columnName);
        try (PreparedStatement countStatement = connection.prepareStatement(countSql);
             PreparedStatement queryStatement = connection.prepareStatement(querySql)) {
            countStatement.setString(1, timeStamp);
            ResultSet resultSet = countStatement.executeQuery();
            int count = ResultParser.countParse(resultSet);

            List<List<String>> result = new ArrayList<>();

            // 分页将等于右闭区间的数据全读取
            for (int i = beginIndex; i < count; i += batchSize) {
                queryStatement.setString(1, timeStamp);
                queryStatement.setInt(2, i);
                queryStatement.setInt(3, batchSize);
                ResultSet rs = queryStatement.executeQuery();
                result.addAll(ResultParser.simpleParse(tableName, columnName, rs));
            }
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateSyncTime(String tableName, long beginTime, long endTime) {
        try (PreparedStatement prepareStatement = connection.prepareStatement("UPDATE SYS_CLOUD_SYNC set begin_timestamp = ?, end_timestamp = ? where table_name = ?")) {
            prepareStatement.setLong(1, beginTime);
            prepareStatement.setLong(2, endTime);
            prepareStatement.setString(3, tableName);
            int i = prepareStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void main(String[] args) throws InterruptedException {
        int total = 100; // 总进度
        int progress = 0; // 当前进度

        while (progress <= total) {
            System.out.print("\r进度: " + progress + "%"); // \r 会将光标移动到行首，从而实现覆盖打印的效果
            progress += 10;
            Thread.sleep(500); // 暂停 500 毫秒以模拟进度更新
        }
    }

}
