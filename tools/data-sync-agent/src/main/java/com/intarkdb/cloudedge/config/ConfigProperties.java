/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.config;

import java.util.List;

public class ConfigProperties {

    // center database setting
    private DbMsg centerDb;

    // local database setting
    private List<LocalDbMsg> localDb;

    // sync table msg
    private List<SyncTableInfo> syncTable;

    // loop wait time (ms)
    private Integer loopWait;

    public DbMsg getCenterDb() {
        return centerDb;
    }

    public void setCenterDb(DbMsg centerDb) {
        this.centerDb = centerDb;
    }

    public List<LocalDbMsg> getLocalDb() {
        return localDb;
    }

    public void setLocalDb(List<LocalDbMsg> localDb) {
        this.localDb = localDb;
    }

    public List<SyncTableInfo> getSyncTable() {
        return syncTable;
    }

    public void setSyncTable(List<SyncTableInfo> syncTable) {
        this.syncTable = syncTable;
    }

    public Integer getLoopWait() {
        return loopWait;
    }

    public void setLoopWait(Integer loopWait) {
        this.loopWait = loopWait;
    }
}
