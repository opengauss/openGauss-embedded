/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.common;

public class Constant {

    /**
     * 单次读取数量
     */
    public static final int BATCH_SIZE = 500;

    /**
     * 扫描窗口宽度
     */
    public static final long SCAN_WINDOW_RANGE = 15 * 1000;

    /**
     * 乱序插入等待时间，读取时将只读取到当前时间戳 - 本值
     */
    public static final long SCAN_WAIT_RANGE = 15 * 1000;

    public static final int FAIL_LIMIT = 10;

    public static final String CREATE_SYNC_TABLE_SQL = "CREATE TABLE SYS_CLOUD_SYNC " +
            "(id INTEGER, " +
            "table_name varchar(100), " +
            "sync_column varchar(100), " +
            "type int, " +
            "begin_timestamp long, " +
            "end_timestamp long, " +
            "node_id varchar(100), " +
            "unique_column varchar(100), " +
            "update_time long, " +
            "PRIMARY KEY (id), " +
            "unique (table_name))";

    public static final String DESCRIBE_SYNC_TABLE_SQL = "describe SYS_CLOUD_SYNC";

    public static final String SQL_SELECT_1 = "SELECT * FROM {0} where {1} > ? and {2} <= ? limit {3}";
    public static final String SQL_SELECT_2 = "SELECT count(*) FROM {0} where {1} = ?";
    public static final String SQL_SELECT_3 = "SELECT * FROM {0} where {1} = ? offset ? limit ?";

    public static final String CENTER_IP = "center_ip";
    public static final String CENTER_PORT = "center_port";
    public static final String CENTER_USER = "center_user";
    public static final String CENTER_PASSWORD = "center_password";

    public static final String LOCAL_IP = "local_ip";
    public static final String LOCAL_PORT = "local_port";
    public static final String LOCAL_USER = "local_user";
    public static final String LOCAL_PASSWORD = "local_password";
}
