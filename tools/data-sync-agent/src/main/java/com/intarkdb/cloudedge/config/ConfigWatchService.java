/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.config;

import com.intarkdb.cloudedge.tool.StringUtil;

import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class ConfigWatchService {

    public static void configWatch() {
        String configFile = System.getProperty("config.file");
        if (StringUtil.isBlank(configFile)) {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            URL resourceUrl = classLoader.getResource("conf.yaml");
            configFile = resourceUrl.getPath();
        }
        Path filePath = Paths.get(configFile);
        try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
            Path directory = filePath.getParent();
            directory.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);

            WatchKey watchKey;
            while ((watchKey = watchService.take()) != null) {
                checkWatch(watchKey, filePath);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void checkWatch(WatchKey watchKey, Path filePath) {
        for (WatchEvent<?> event : watchKey.pollEvents()) {
            if (event.context().equals(filePath.getFileName())) {
                // 文件被修改，执行相应的操作
                ConfigProperties newProperties = ConfigReader.readConfigFromYaml();
            }
        }
        watchKey.reset();
    }
}
