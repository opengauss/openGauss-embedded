/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN 
 BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*/
package com.intarkdb.cloudedge.config;

import com.intarkdb.cloudedge.common.SyncException;
import com.intarkdb.cloudedge.tool.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


public class ConfigReader {

    private static final Logger log = LoggerFactory.getLogger(ConfigReader.class);

    /**
     * Deprecated
     * @param path
     * @return
     */
    @Deprecated
    public static Map<String, String> readConfiguration(String path) {
        if (path == null || "".equals(path)) {
            String currentPath = System.getProperty("user.dir");
            path = currentPath + "/conf.properties";
        }
        Map<String, String> result = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line;

            while ((line = reader.readLine()) != null) {
                handle(line.trim(), result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param line
     * @param result
     */
    @Deprecated
    private static void handle(String line, Map<String, String> result) {
        int commentIndex = line.indexOf("#");
        if (commentIndex > -1) {
            line = line.substring(0, commentIndex);
        }
        if (line.length() == 0) {
            return;
        }
        String[] kvs = line.split("=");
        result.put(kvs[0].trim(), kvs[1].trim());
    }

    /**
     * read configurations from yaml file
     * @return
     */
    public static ConfigProperties readConfigFromYaml() {
        ConfigProperties configProperties = new ConfigProperties();

        String configFile = System.getProperty("config_file");
        if (StringUtil.isBlank(configFile)) {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            URL resourceUrl = classLoader.getResource("conf.yaml");
            if (resourceUrl != null) {
                configFile = resourceUrl.getPath();
            } else {
                configFile = System.getProperty("user.dir") + "\\src\\main\\resources\\conf.yaml";
            }
        }
        Yaml yaml = new Yaml();
        try (FileInputStream fileInputStream = new FileInputStream(configFile)) {
            ConfigProperties config = yaml.loadAs(fileInputStream, ConfigProperties.class);

            checkAndFixConfig(config);

            return config;
        } catch (IOException | SyncException e) {
            // config file read failed
            log.error("property file read failed!");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * check if there are any required configurations not set
     * apply the default value if there are available rules
     * @param config
     */
    private static void checkAndFixConfig(ConfigProperties config) throws SyncException {
        // check local db setting
        List<LocalDbMsg> msgList = config.getLocalDb();
        for (LocalDbMsg msg: msgList) {
            checkDbMsg(msg);
            checkNodeId(msg);
        }

        // check center db setting
        checkDbMsg(config.getCenterDb());

        // check sync table setting
        List<SyncTableInfo> syncTableInfos = config.getSyncTable();
        for (SyncTableInfo info : syncTableInfos) {
            checkTableInfo(info);
        }
    }

    private static void checkDbMsg(DbMsg msg) throws SyncException {
        String ip = msg.getIp();
        if (StringUtil.isBlank(ip) || !isValidIP(ip)) {
            throw new SyncException("invalid ip: " + msg.getIp());
        }
        String port = msg.getPort();
        if (StringUtil.isBlank(port) || !isValidPort(port)) {
            throw new SyncException("invalid port: " + msg.getPort());
        }
    }

    private static void checkNodeId(LocalDbMsg msg) {
        String nodeId = msg.getNodeId();
        if (StringUtil.isBlank(nodeId)) {
            // set default node id using ip and port
            msg.setNodeId(msg.getIp() + "_" + msg.getPort());
        }
    }

    private static void checkTableInfo(SyncTableInfo info) throws SyncException {
        String tableName = info.getTableName();
        if (StringUtil.isBlank(tableName)) {
            throw new SyncException("table name or sync column name missing");
        }
        String syncColumn = info.getSyncColumn();
        if (StringUtil.isBlank(syncColumn)) {
            throw new SyncException("table name or sync column name missing");
        }
    }

    // ip and port pattern
    private static final String IP_REGEX = "^((\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])\\.){3}(\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])$";
    private static final String PORT_REGEX = "^\\d{1,5}$";

    public static boolean isValidIP(String ip) {
        return Pattern.matches(IP_REGEX, ip);
    }

    public static boolean isValidPort(String port) {
        return Pattern.matches(PORT_REGEX, port);
    }
}
