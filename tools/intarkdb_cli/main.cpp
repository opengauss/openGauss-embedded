/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* main.cpp
*
* IDENTIFICATION
* openGauss-embedded/tools/intarkdb_cli/main.cpp
*
* -------------------------------------------------------------------------
*/
#include <fmt/core.h>
#include <fmt/format.h>

#include <iostream>
#include <memory>
#include <set>

#include "linenoise.h"

#include "binder/binder.h"
#include "catalog/catalog.h"
#include "main/connection.h"
#include "main/database.h"
#include "kv_operator.h"
#include "cmd.h"
#include "assist.h"
#include "function/version/pragma_version.h"

#include "compute/kv/kv_connection.h"

auto main(int argc, char** argv) -> int {
    std::string path = "./";
    if (argc >= 2) {
        if (strcmp(argv[1], "--version")==0){
            std::cout << intarkdb::LibraryVersion() << " " << intarkdb::SourceID() << std::endl;
            return 0;
        }
        else{
            path = argv[1];
            path += "/";
        }
    }
    try {
        auto db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance(path.c_str()));
        // 启动db
        db_instance->Init();

        Connection conn(db_instance);
        conn.Init();

        auto kv_conn = std::make_unique<KvConnection>(db_instance);
        kv_conn->Init();
        KvOperator kv_operator(std::move(kv_conn));
        ClassCmd cmd(path, &conn, &kv_operator);

        char *zHome;
        int nHistory;
        char *zHistory = getenv("INTARKDB_HISTORY");
        if( zHistory ){
            zHistory = strdup(zHistory);
        }else if( (zHome = find_home_dir(0))!=nullptr ){
            nHistory = strlen30(zHome) + 20;
            if( (zHistory = (char*)malloc(nHistory))!=nullptr ){
                sqlite3_snprintf(nHistory, zHistory,"%s/.intarkdb_history", zHome);
            }
        }
        if( zHistory ){
            linenoiseHistoryLoad(zHistory);
        }
        cmd.main();
        if( zHistory ){
            linenoiseHistorySetMaxLen(2000);
            linenoiseHistorySave(zHistory);
            free(zHistory);
        }
    } catch (const std::exception& e) {
        std::cout<<" ERROR MSG:"<<e.what()<<std::endl;
    }
    
    return 0;
}
