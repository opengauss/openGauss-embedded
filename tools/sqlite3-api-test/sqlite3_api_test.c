#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>

#include "interface/c/intarkdb_sql.h"
#include "interface/sqlite3_api_wrapper/include/sqlite3.h"


int NUM_Threads[20] = {5, 10, 15, 20, 25, 30};
char result[100] = {'\0'}; // 初始化为全零
int NUM_Insert = 10;

int *thread_index[100];
void *handle_multi[100];

int exec_handle(void *data, int argc, char **argv, char **colname)
{
    /* 计数器*/
    int i = *(int *)(data);
    *(int *)(data) = i + 1;

    /* 取出结果*/
    printf("[%s] is [%s], [%s] is [%s]...\n", colname[0], argv[0], colname[1], argv[1]);
    return 0;
}

int main(int argc, char *argv[])
{
    sqlite3 *db;
    int h = 0;
    int rc;

    float time_use = 0;
    float task_tps = 0;
    struct timeval start;
    struct timeval end;
    const char *data = "Callback function called";

    char str[100];
    char *err_msg = NULL;

    rc = sqlite3_open("sqlites.db", &db);
    if (rc)
    {
        printf("Can't open database: %s\n", "sqlites.db");
        exit(0);
    }
    printf("open database success\n");
    /********************第二步，创建数据库表*************************/
    char *sql_create_table;
    char buf[120];

    sql_create_table = "CREATE TABLE PLC_DATA(ID INTEGER, ACCTID INTEGER, CHGAMT VARCHAR(20), PRIMARY KEY (ID,ACCTID));";

    rc = sqlite3_exec(db, sql_create_table, NULL, 0, &err_msg);
    if (rc != SQLITE_OK)
    {
        fprintf(stdout, "SQL error: %s\n", err_msg);
    } else {
        fprintf(stdout, "Table created successfully\n");
    }

    sql_create_table = "INSERT INTO PLC_DATA(ID,ACCTID,CHGAMT) VALUES (1,11,'AAA'), (2,22,'BBB');";
    rc = sqlite3_exec(db, sql_create_table, NULL, 0, &err_msg);
    if (rc != SQLITE_OK){
        printf("Can't insert data \n");
    } else {
        printf("insert data success\n");
    }
   
    sql_create_table = "SELECT ID,ACCTID,CHGAMT FROM PLC_DATA;";
    rc = sqlite3_exec(db, sql_create_table, exec_handle, &data, &err_msg);
    if (rc != SQLITE_OK){
        printf("Can't select data \n");
    } else {
        printf("select data success\n");
    }


    sqlite3_stmt *pstmt;
    const char *sql = "INSERT INTO PLC_DATA(ID,ACCTID,CHGAMT) VALUES(?,?,?);";
    int nRet = sqlite3_prepare_v2(db, sql, strlen(sql), &pstmt, NULL);
    rc = sqlite3_bind_int(pstmt, 1, 3);
    printf("sqlite3_bind_int rc = %d \n", rc);
    rc = sqlite3_bind_int(pstmt, 2, 33);
    printf("sqlite3_bind_int rc = %d \n", rc);
    rc = sqlite3_bind_text(pstmt, 3,  "Jonn", strlen("Jonn"), NULL);
    printf("sqlite3_bind_text rc = %d \n", rc);
    
    sqlite3_step(pstmt);
    sqlite3_reset(pstmt);

    sql_create_table = "SELECT ID,ACCTID,CHGAMT FROM PLC_DATA;";
    nRet = sqlite3_prepare_v2(db, sql_create_table, strlen(sql_create_table), &pstmt, NULL);
     
    while (SQLITE_ROW == sqlite3_step(pstmt)) {
        printf("ID = %s\n", (char *)sqlite3_column_text(pstmt, 0));
        printf("ACCTID = %s\n", (char *)sqlite3_column_text(pstmt, 1));
        printf("CHGAMT = %s\n", (char *)sqlite3_column_text(pstmt, 2));
    }
    sqlite3_reset(pstmt);


    sql_create_table = "SELECT ID,ACCTID,CHGAMT FROM PLC_DATA;";
    rc = sqlite3_exec(db, sql_create_table, exec_handle, &data, &err_msg);
    if (rc != SQLITE_OK){
        printf("Can't select data \n");
    } else {
        printf("select data success\n");
    }
    sqlite3_close(db);
    printf("sqlite3_close success\n");

    return 0;
}