/*
 * Copyright (c) 2022 GC.
 *
 * openGauss is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *          http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * intarkdb_kv.h
 *
 *
 * IDENTIFICATION
 *    src/interface/c/intarkdb_kv.h
 *
 * -------------------------------------------------------------------------
 */
#ifndef __C_API_INTARKDB_KV_H__
#define __C_API_INTARKDB_KV_H__

#include "stdio.h"

#include "storage/db_handle.h"
#include "storage/storage.h"
#include "compute/sql/include/common/winapi.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void * g_kv_instance;

/* This is the reply object returned by command() */
typedef struct intarkdbReply_t {
    int type;   /* return type */
    size_t len; /* Length of string */
    char *str;  /* err or value*/
} intarkdbReply;

/* Context for alloc on Gstor */
typedef struct intarkdbContext_t {
    int err; /* Error flags, 0 when there is no error */
    char errstr[128]; /* String representation of error when applicable */
} intarkdbContext;

EXPORT_API status_t intarkdb_startup_db(int dbtype, char *path);
EXPORT_API void intarkdb_shutdown_db(void);

EXPORT_API status_t alloc_kv_handle(void **handle);
EXPORT_API void free_kv_handle(void *handle);

EXPORT_API status_t create_or_open_kv_table(void *handle, const char *table_name);

EXPORT_API void *intarkdb_command_set(void *handle, const char *key, const char *val);
EXPORT_API void *intarkdb_command_get(void *handle, const char *key);
EXPORT_API void *intarkdb_command_del(void *handle, const char *key, int prefix, int *count);

EXPORT_API status_t intarkdb_kv_begin(void *handle);
EXPORT_API status_t intarkdb_kv_commit(void *handle);
EXPORT_API status_t intarkdb_kv_rollback(void *handle);

EXPORT_API void intarkdb_freeReplyObject(intarkdbReply *reply);

#ifdef __cplusplus
}
#endif
#endif
