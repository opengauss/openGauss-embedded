/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* intarkdb_kv.c
*
* IDENTIFICATION
* src/interface/c/intarkdb_kv.c
*
* -------------------------------------------------------------------------
*/
#include "intarkdb_kv.h"

#ifdef __cplusplus
extern "C" {
#endif

void * g_kv_instance = NULL;

status_t intarkdb_startup_db(int dbtype, char *path)
{
    if(dbtype != DB_TYPE_GSTOR && dbtype != DB_TYPE_CEIL) {
        printf("dbtype(%d:GSTOR,%d:CEIL) error!! input dbtype = %d\n", DB_TYPE_GSTOR, DB_TYPE_CEIL, dbtype);
        if(dbtype == DB_TYPE_CEIL) {
            printf("dbtype = %d, CEIL db, not supported now!\n", dbtype);
        }
        return GS_ERROR;
    }

    uint32 len = (uint32)strlen(path);
    if (len <= 1) {
        memset(path, 0, strlen(path)+1);
        sprintf(path, "./");
    } else if (len >= (GS_MAX_PATH_LEN - 1)) {
        printf("path too long(max length is %d): path=%s\n", GS_MAX_PATH_LEN, path);
        return GS_ERROR;
    }

    if (kv_startup(&g_kv_instance, dbtype, path) != GS_SUCCESS) {
        printf("[API] db_startup failed");
        return GS_ERROR;
    }

    return GS_SUCCESS;
}

void intarkdb_shutdown_db(void)
{
    kv_shutdown(g_kv_instance);
}

status_t alloc_kv_handle(void **handle)
{
    if (kv_alloc(g_kv_instance, handle) != GS_SUCCESS) {
        printf("get handle failed");
        return GS_ERROR;
    }

    return GS_SUCCESS;
}

void free_kv_handle(void *handle)
{
    kv_free(handle);
}

status_t create_or_open_kv_table(void *handle, const char *table_name)
{
    return kv_open_table(handle, table_name);
}

void *intarkdb_command_set(void *handle, const char *key, const char *val)
{
    text_t text_t_key, text_t_val;
    text_t_key.len = strlen(key) + 1;
    text_t_key.str = (char*)key;
    text_t_val.len = strlen(val) + 1;
    text_t_val.str = (char*)val;
    status_t ret = gstor_put(((db_handle_t*)handle)->handle, text_t_key.str, text_t_key.len, text_t_val.str, text_t_val.len);

    intarkdbReply *reply = (intarkdbReply*)malloc(sizeof(intarkdbReply));
    reply->type = ret;
    if(ret==GS_SUCCESS) {
        reply->len = 2;
        reply->str = (char*)malloc(reply->len + 1);
        sprintf(reply->str, "%s", "ok");
    } else {
        reply->len = 6;
        reply->str = (char*)malloc(reply->len + 1);
        sprintf(reply->str, "%s", "failed");
    }

    return reply;
}

void *intarkdb_command_get(void *handle, const char *key)
{
    text_t text_t_key;
    text_t_key.len = strlen(key) + 1;
    text_t_key.str = (char*)key;

    bool32 eof;
    text_t stg_value;
    stg_value.str = NULL;
    stg_value.len = 0;
    status_t ret = gstor_get(((db_handle_t*)handle)->handle, text_t_key.str, text_t_key.len, &stg_value.str, &stg_value.len, &eof);
    
    intarkdbReply *reply = (intarkdbReply*)malloc(sizeof(intarkdbReply));
    reply->type = ret;
    reply->len = stg_value.len;
    reply->str = stg_value.str;

    return reply;
}

void *intarkdb_command_del(void *handle, const char *key, int prefix, int *count)
{
    text_t text_t_key;
    text_t_key.len = strlen(key) + 1;
    text_t_key.str = (char*)key;
    status_t ret = gstor_del(((db_handle_t*)handle)->handle,  text_t_key.str, text_t_key.len, (bool32)prefix, (uint32*)count);

    intarkdbReply *reply = (intarkdbReply*)malloc(sizeof(intarkdbReply));
    reply->type = ret;
    if(ret==GS_SUCCESS) {
        reply->len = 2;
        reply->str = (char*)malloc(reply->len + 1);
        sprintf(reply->str, "%s", "ok");
    } else {
        reply->len = 6;
        reply->str = (char*)malloc(reply->len + 1);
        sprintf(reply->str, "%s", "failed");
    }

    return reply;
}

status_t intarkdb_kv_begin(void *handle)
{
    return kv_begin(handle);
}

status_t intarkdb_kv_commit(void *handle)
{
    return kv_commit(handle);
}

status_t intarkdb_kv_rollback(void *handle)
{
    return kv_rollback(handle);
}

void intarkdb_freeReplyObject(intarkdbReply *reply)
{
    if (reply) {
        free(reply);
    }
    // 由外部调用设置NULL
}

#ifdef __cplusplus
}
#endif
