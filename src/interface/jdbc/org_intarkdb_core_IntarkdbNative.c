/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * org_intarkdb_core_IntarkdbNative.c
 *
 * IDENTIFICATION
 * openGauss-embedded/src/interface/jdbc/org_intarkdb_core_IntarkdbNative.c
 *
 * -------------------------------------------------------------------------
 */
/* DO NOT EDIT THIS FILE - it is machine generated */
#include "org_intarkdb_core_IntarkdbNative.h"

#include <string.h>

#include "interface/c/intarkdb_sql.h"
#include "network/server/srv_interface.h"

/* Header for class org_intarkdb_core_IntarkdbNative */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_open
 * Signature: (Ljava/lang/String;Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1open(JNIEnv *env, jclass cls, jstring path,
                                                                            jobject db) {
    const char *c_path = (*env)->GetStringUTFChars(env, path, 0);
    intarkdb_database *c_db = (intarkdb_database *)(*env)->GetDirectBufferAddress(env, db);
    int result = intarkdb_open(c_path, c_db);
    (*env)->ReleaseStringUTFChars(env, path, c_path);
    return result;
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_close
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1close(JNIEnv *env, jclass cls, jobject db) {
    intarkdb_database *c_db = (intarkdb_database *)(*env)->GetDirectBufferAddress(env, db);
    intarkdb_close(c_db);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_connect
 * Signature: (Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1connect(JNIEnv *env, jclass cls,
                                                                               jobject database, jobject conn) {
    intarkdb_database *c_database = (intarkdb_database *)(*env)->GetDirectBufferAddress(env, database);
    intarkdb_connection *c_conn = (intarkdb_connection *)(*env)->GetDirectBufferAddress(env, conn);
    return intarkdb_connect(*c_database, c_conn);
}
/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_disconnect
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1disconnect(JNIEnv *env, jclass cls,
                                                                                  jobject conn) {
    intarkdb_connection *c_conn = (intarkdb_connection *)(*env)->GetDirectBufferAddress(env, conn);
    intarkdb_disconnect(c_conn);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_query
 * Signature: (Ljava/nio/ByteBuffer;Ljava/lang/String;Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1query(JNIEnv *env, jclass cls,
                                                                             jobject connection, jstring query,
                                                                             jobject result) {
    const char *c_query = (*env)->GetStringUTFChars(env, query, 0);
    intarkdb_connection *c_connection = (intarkdb_connection *)(*env)->GetDirectBufferAddress(env, connection);
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);

    int res = intarkdb_query(*c_connection, c_query, c_result);

    (*env)->ReleaseStringUTFChars(env, query, c_query);

    return res;
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_init_result
 * Signature: ()Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1init_1result(JNIEnv *env, jclass cls) {
    return (*env)->NewDirectByteBuffer(env, (void *)intarkdb_init_result(), sizeof(intarkdb_res_def));
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_row_count
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1row_1count(JNIEnv *env, jclass cls,
                                                                                   jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    return (jlong)intarkdb_row_count(c_result);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_row_count
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1result_1effect_1row(JNIEnv *env, jclass cls,
                                                                                            jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    return (jlong)intarkdb_result_effect_row(c_result);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_column_count
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1column_1count(JNIEnv *env, jclass cls,
                                                                                      jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    return (jlong)intarkdb_column_count(c_result);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_column_name
 * Signature: (Ljava/nio/ByteBuffer;J)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jstring JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1column_1name(JNIEnv *env, jclass cls,
                                                                                       jobject result, jlong col) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    const char *column_name = intarkdb_column_name(c_result, col);
    return (*env)->NewStringUTF(env, column_name);
}
/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_value_varchar
 * Signature: (Ljava/nio/ByteBuffer;JJ)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jstring JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1value_1varchar(JNIEnv *env, jclass cls,
                                                                                         jobject result, jlong row,
                                                                                         jlong col) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    const char *value_varchar = intarkdb_value_varchar(c_result, row, col);
    if (value_varchar == NULL) {
        return NULL;  // 返回一个null的jstring对象
    }
    return (*env)->NewStringUTF(env, value_varchar);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_value_timestamp
 * Signature: (Ljava/nio/ByteBuffer;JJ)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jlong JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1value_1timestamp(JNIEnv *env, jclass cls,
                                                                                         jobject result, jlong row,
                                                                                         jlong col) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    return (jlong)intarkdb_value_timestamp_ms(c_result, row, col);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_value_date
 * Signature: (Ljava/nio/ByteBuffer;JJ)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jstring JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1value_1date(JNIEnv *env, jclass cls,
                                                                                      jobject result, jlong row,
                                                                                      jlong col) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    const char *value_varchar = intarkdb_value_date(c_result, row, col);
    if (value_varchar == NULL) {
        return NULL;  // 返回一个null的jstring对象
    }
    return (*env)->NewStringUTF(env, value_varchar);
}

JNIEXPORT jbyteArray JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1value_1blob(JNIEnv *env, jclass cls,
                                                                                       jobject result, jlong row,
                                                                                       jlong col) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    int32_t val_len;
    intarkdb_blob_len(c_result, row, col, &val_len);
    jbyteArray result_array= (*env)->NewByteArray(env, val_len);
    void *blob_ptr = intarkdb_value_blob(c_result, row, col, &val_len);
    if (blob_ptr != NULL) {
        (*env)->SetByteArrayRegion(env, result_array, 0, val_len, (jbyte *)blob_ptr);
        return result_array;
    } else {
      (*env)->DeleteLocalRef(env, result_array);
      result_array = NULL;
    }
}
/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_result_msg
 * Signature: (Ljava/nio/ByteBuffer;)
 */
JNIEXPORT jstring JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1result_1msg(JNIEnv *env, jclass cls,
                                                                                      jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    const char *value_varchar = intarkdb_result_msg(c_result);
    return (*env)->NewStringUTF(env, value_varchar);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_free_row
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1free_1row(JNIEnv *env, jclass cls,
                                                                                 jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    intarkdb_free_row(c_result);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_destroy_result
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1destroy_1result(JNIEnv *env, jclass cls,
                                                                                       jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    intarkdb_destroy_result(c_result);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_prepare
 * Signature: (Ljava/nio/ByteBuffer;Ljava/lang/String;Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1prepare(JNIEnv *env, jclass cls, jobject conn,
                                                                               jstring query, jobject prepStmt) {
    intarkdb_connection *c_conn = (intarkdb_connection *)(*env)->GetDirectBufferAddress(env, conn);
    const char *c_query = (*env)->GetStringUTFChars(env, query, 0);
    intarkdb_prepared_statement *c_prepStmt =
        (intarkdb_prepared_statement *)(*env)->GetDirectBufferAddress(env, prepStmt);
    int result = intarkdb_prepare(*c_conn, c_query, c_prepStmt);
    (*env)->ReleaseStringUTFChars(env, query, c_query);
    return result;
}
/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_execute_prepared
 * Signature: (Ljava/nio/ByteBuffer;Ljava/lang/String;Ljava/nio/ByteBuffer;Array;)I
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1execute_1prepared(JNIEnv *env, jclass cls,
                                                                                         jobject prepStmt,
                                                                                         jobject result,
                                                                                         jobjectArray params) {
    intarkdb_prepared_statement *c_prepStmt =
        (intarkdb_prepared_statement *)(*env)->GetDirectBufferAddress(env, prepStmt);
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    int64_t length = 0;
    if (params != NULL) {
        length = (*env)->GetArrayLength(env, params);
    }
    // 遍历params数组
    for (jsize i = 0; i < length; i++) {
        // 获取params数组中的元素
        jobject param = (*env)->GetObjectArrayElement(env, params, i);
        if (param == NULL) {
            intarkdb_bind_null(*c_prepStmt, i + 1);
            continue;
        }
        // 判断param的类型
        jclass paramClass = (*env)->GetObjectClass(env, param);
        jmethodID getClassMethod = (*env)->GetMethodID(env, paramClass, "getClass", "()Ljava/lang/Class;");
        jobject paramClassObj = (*env)->CallObjectMethod(env, param, getClassMethod);
        jclass paramClassClass = (*env)->GetObjectClass(env, paramClassObj);
        jmethodID getNameMethod = (*env)->GetMethodID(env, paramClassClass, "getName", "()Ljava/lang/String;");
        jstring paramClassName = (jstring)(*env)->CallObjectMethod(env, paramClassObj, getNameMethod);
        const char *className = (*env)->GetStringUTFChars(env, paramClassName, 0);

        // 调用相应的intarkdb_state_方法
        intarkdb_state_t state;
        if (strcmp(className, "java.lang.Boolean") == 0) {
            jmethodID booleanValueMethod = (*env)->GetMethodID(env, paramClass, "booleanValue", "()Z");
            jboolean value = (*env)->CallBooleanMethod(env, param, booleanValueMethod);
            state = intarkdb_bind_boolean(*c_prepStmt, i + 1, value);
        } else if (strcmp(className, "java.lang.Integer") == 0) {
            jmethodID intValueMethod = (*env)->GetMethodID(env, paramClass, "intValue", "()I");
            jint value = (*env)->CallIntMethod(env, param, intValueMethod);
            state = intarkdb_bind_int32(*c_prepStmt, i + 1, value);
        } else if (strcmp(className, "java.lang.Long") == 0) {
            jmethodID longValueMethod = (*env)->GetMethodID(env, paramClass, "longValue", "()J");
            jlong value = (*env)->CallLongMethod(env, param, longValueMethod);
            state = intarkdb_bind_int64(*c_prepStmt, i + 1, value);
        } else if (strcmp(className, "java.lang.Float") == 0) {
            jmethodID floatValueMethod = (*env)->GetMethodID(env, paramClass, "floatValue", "()F");
            jfloat value = (*env)->CallFloatMethod(env, param, floatValueMethod);
            state = intarkdb_bind_float(*c_prepStmt, i + 1, value);
        } else if (strcmp(className, "java.lang.Double") == 0) {
            jmethodID doubleValueMethod = (*env)->GetMethodID(env, paramClass, "doubleValue", "()D");
            jdouble value = (*env)->CallDoubleMethod(env, param, doubleValueMethod);
            state = intarkdb_bind_double(*c_prepStmt, i + 1, value);
        } else if (strcmp(className, "java.lang.String") == 0) {
            jstring strValue = (jstring)param;
            const char *value = (*env)->GetStringUTFChars(env, strValue, 0);
            state = intarkdb_bind_varchar(*c_prepStmt, i + 1, value);
            (*env)->ReleaseStringUTFChars(env, strValue, value);
        } else if (strcmp(className, "java.math.BigDecimal") == 0) {
            jmethodID bigDecimalValueMethod = (*env)->GetMethodID(env, paramClass, "toString", "()Ljava/lang/String;");
            jstring value = (jstring)(*env)->CallObjectMethod(env, param, bigDecimalValueMethod);
            const char *strValue = (*env)->GetStringUTFChars(env, value, 0);
            state = intarkdb_bind_decimal(*c_prepStmt, i + 1, strValue);
            (*env)->ReleaseStringUTFChars(env, value, strValue);
        }

        else if (strcmp(className, "java.sql.Timestamp") == 0) {
            jmethodID timestampValueMethod = (*env)->GetMethodID(env, paramClass, "getTime", "()J");
            jlong milliseconds = (*env)->CallLongMethod(env, param, timestampValueMethod);
            int64_t intValue = (int64_t)milliseconds;
            state = intarkdb_bind_timestamp_ms(*c_prepStmt, i + 1, intValue);
        } else if (strcmp(className, "[B") == 0) {  // 字节数组类型
            jbyteArray byteArray = (jbyteArray)param;
            jsize byteLength = (*env)->GetArrayLength(env, byteArray);
            jbyte *byteData = (*env)->GetByteArrayElements(env, byteArray, NULL);

            state = intarkdb_bind_blob(*c_prepStmt, i + 1, (const void *)byteData, byteLength);

        }

        else {
            // 其他类型处理
            state = SQL_ERROR;
        }
        // 释放资源
        (*env)->ReleaseStringUTFChars(env, paramClassName, className);
        (*env)->DeleteLocalRef(env, paramClassName);
        (*env)->DeleteLocalRef(env, paramClassClass);
        (*env)->DeleteLocalRef(env, paramClassObj);
        (*env)->DeleteLocalRef(env, paramClass);
        (*env)->DeleteLocalRef(env, param);

        // 检查状态
        if (state != SQL_SUCCESS) {
            printf("intarkdb_bind_param error, in the %d parameter ,the type: %s is not support\n", i, className);
            return state;
        }
    }
    intarkdb_state_t ret = intarkdb_execute_prepared(*c_prepStmt, c_result);
    return ret;
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_destroy_prepare
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1destroy_1prepare(JNIEnv *env, jclass cls,
                                                                                        jobject prepStmt) {
    intarkdb_prepared_statement *c_prepStmt =
        (intarkdb_prepared_statement *)(*env)->GetDirectBufferAddress(env, prepStmt);
    intarkdb_destroy_prepare(c_prepStmt);
}

/*
 * Class:     intarkdb_prepare_nparam
 * Method:    intarkdb_row_count
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jlong JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1prepare_1nparam(JNIEnv *env, jclass cls,
                                                                                        jobject prepStmt) {
    intarkdb_prepared_statement *c_prepStmt =
        (intarkdb_prepared_statement *)(*env)->GetDirectBufferAddress(env, prepStmt);
    return (jlong)intarkdb_prepare_nparam(*c_prepStmt);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_column_type
 * Signature: (Ljava/nio/ByteBuffer;L)I;
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1column_1type(JNIEnv *env, jclass cls,
                                                                                    jobject result, jlong col) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    return intarkdb_column_type(c_result, col);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative_intarkdb_prepare_errmsg
 * Method:    intarkdb_prepare_errmsg
 * Signature: (Ljava/nio/ByteBuffer;)J
 */
JNIEXPORT jstring JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1prepare_1errmsg(JNIEnv *env, jclass cls,
                                                                                          jobject prepStmt) {
    intarkdb_prepared_statement *c_prepStmt =
        (intarkdb_prepared_statement *)(*env)->GetDirectBufferAddress(env, prepStmt);
    const char *value_varchar = intarkdb_prepare_errmsg(*c_prepStmt);
    return (*env)->NewStringUTF(env, value_varchar);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    is_select
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT jboolean JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1is_1select(JNIEnv *env, jclass cls,
                                                                                      jobject result) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    return (jboolean)c_result->is_select;
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_need_result_ex
 * Signature: (Ljava/nio/ByteBuffer;Z)V
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1need_1result_1ex(JNIEnv *env, jclass cls,
                                                                                        jobject result, jboolean need) {
    intarkdb_result c_result = (intarkdb_result)(*env)->GetDirectBufferAddress(env, result);
    intarkdb_need_result_ex(c_result, need);
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_start_remote
 * Signature: (Ljava/nio/ByteBuffer;J)I
 */
JNIEXPORT jint JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1start_1remote(JNIEnv *env, jclass cls,
                                                                                     jstring dbname, jstring location,
                                                                                     jlong port) {
    printf("srv_start_server ....");

    const char *c_dbname = (*env)->GetStringUTFChars(env, dbname, 0);
    const char *c_location = (*env)->GetStringUTFChars(env, location, 0);

    const char *ip = "0.0.0.0";
    server_config config;
    strcpy(config.host[0], ip);
    for (int i = 1; i < 8; i++) {
        strcpy(config.host[i], "\0");
    }

    int db_i = 0;
    while (db_i < MAX_DB_NUM) {
        config.db_items[db_i].isUsed = false;
        strcpy(config.db_items[db_i].name, "\0");
        strcpy(config.db_items[db_i].location, "\0");
        db_i++;
    }
    strcpy(config.db_items[0].name, c_dbname);
    strcpy(config.db_items[0].location, c_location);
    config.db_items[0].isUsed = true;

    config.port = port;
    // todo:add db config
    int result = startup_server(&config);
    printf("srv_start_server result=%d, ip:%s , dbname:%s , location:%s \n", result, config.host[0], c_dbname,
           c_location);
    if (result != 0) {
        int re = cm_get_error_code();
        printf("cm_get_error_code re=%d  \n", re);
        if (re == 2800) {
            result = server_add_database(c_dbname, c_location);
            printf("server_add_database re=%d  \n", result);
        }
    }

    return result;
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_stop_remote
 * Signature: (Ljava/nio/ByteBuffer;J)I
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1stop_1remote(JNIEnv *env, jclass cls) {
    stop_server();
}

/*
 * Class:     org_intarkdb_core_IntarkdbNative
 * Method:    intarkdb_delete_databse
 * Signature: (Ljava/nio/ByteBuffer;J)I
 */
JNIEXPORT void JNICALL Java_org_intarkdb_core_IntarkdbNative_intarkdb_1delete_1databse(JNIEnv *env, jclass cls,
                                                                                       jstring dbname) {
    const char *c_dbname = (*env)->GetStringUTFChars(env, dbname, 0);
    server_delete_database(c_dbname);
}

#ifdef __cplusplus
}
#endif
