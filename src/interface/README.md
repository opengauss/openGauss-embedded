## interface说明
## 编译步骤 & 执行步骤
1. 在3rd/gstor/目录直接 make 即可编译（会自动编译存储引擎和SQL引擎的代码）
   - make/make debug: 编译生成debug版本
   - make test: 编译生成debug版本，同时会编译test目录
   - make release: 编译生成release版本
2. 生成的SQL引擎相关的库文件和测试程序在gstor/output/<span style="color: red;"><debug|release></span>/lib目录下
3. 生成的SQL引擎的动态库是gstor/output/<span style="color: red;"><debug|release></span>/lib目录下的libintarkdb.so
4. 在 gstor/src/interface/目录执行
   - make/make debug: 编译生成debug版本
   - make release: 编译生成release版本
   生成的java jni的动态库是gstor/output/<span style="color: red;"><debug|release></span>/lib目录下的对应动态库文件， 如libgstor_jni.so
5. 将数据库相关so : libsecurec.so,libgstor.so,libstorage.so, libintarkdb.so, libgstor_jni.so
   文件引入到JDBC项目 3rd/gstor/src/interface/java/jdbc/src/main/resources/org/instardb/native/Linux 对应的系统文件中，
   JDBC就可以引用动态库，进行数据库相关操作。

例如：运行系统是X86_64位，则将libsecurec.so,libgstor.so,libstorage.so, libintarkdb.so, libgstor_jni.so
   放在3rd/gstor/src/interface/java/jdbc/src/main/resources/org/instardb/native/Linux/x86_64 文件夹中。

   aarch 64位，则放在对应的aarch64文件下。

6. 在JDBC项目中执行mvn相关命令可以打包jdbc jar