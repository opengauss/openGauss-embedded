# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * intarkdb.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/intarkdb.py
# *
# * -------------------------------------------------------------------------
#  */

from intarkdb.module.connection import Connection

# 用户建立与数据库的连接
def connect(database = ".", isolation_level = "auto"):

    conn = Connection(path=database, isolation=isolation_level)
    conn.open()

    return conn
