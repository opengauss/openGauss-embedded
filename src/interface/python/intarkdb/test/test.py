# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * test.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/test/test.py
# *
# * -------------------------------------------------------------------------
#  */


from test_connection import connection_test
from test_cursor import cursor_test
from test_multi_connection import multi_connection_test

if __name__ == "__main__":
    print("-----connection连接对象测试-----")
    connection_test()
    print()

    print("-----建立多个connection连接对象测试-----")
    multi_connection_test()
    print()

    print("-----cursor游标对象测试-----")
    cursor_test()
    print()