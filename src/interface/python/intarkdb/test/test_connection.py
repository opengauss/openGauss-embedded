# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * test_connection.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/test/test_connection.py
# *
# * -------------------------------------------------------------------------
#  */

import sys
sys.path.append(r"../..")

from intarkdb.module.StandardError import InterfaceError, Error, DatabaseError, ProgrammingError
from intarkdb import intarkdb



def connection_test():
    print("1. 连接intarkdb, 设置isolation_level为None, 使得事务手动开启")
    try:
        conn = intarkdb.connect(database='.', isolation_level = None)
    except InterfaceError as e:
        print("InterfaceError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("2. 测试connection对象的cursor方法")
    try:
        conn.cursor()
    except Error as e:
        print("Error!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("3. 测试开启事务")
    try:
        conn.begin()
    except DatabaseError as e:
        print("DatabaseError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("4. 测试connection对象的execute方法")
    print("A. 测试 DROP")
    sql_str1 = "DROP TABLE IF EXISTS test_table;"
    print("执行:",sql_str1)
    try:
        conn.execute(sql_str1)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)
        conn.close()
    print("*****************")

    print("B. 测试 CREATE")
    sql_str2 = "CREATE TABLE test_table (ID INTEGER, ACCTID INTEGER, CHGAMT VARCHAR(20), BAK1 VARCHAR(20), BAK2 VARCHAR(20), PRIMARY KEY (ID,ACCTID));"
    print("执行:",sql_str2)
    try:
        conn.execute(sql_str2)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)
        conn.close()
    print("*****************")

    print("C. 测试 INSERT")
    sql_str3 = "INSERT INTO test_table VALUES (1, 11, 'AAA', 'b1', 'b2'), (2, 22, 'BBB', 'b1', 'b2');"
    print("执行:",sql_str3)
    try:
        c = conn.execute(sql_str3)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)
        conn.close()
    print("*****************")

    print("D. 测试 SELECT")
    sql_str4 = "SELECT ID,ACCTID,CHGAMT,BAK1,BAK2 FROM test_table;"
    print("执行:",sql_str4)
    try:
        c = conn.execute(sql_str4)
        print("验证结果集的行数为2")
        assert c.rowcount == 2
        print("断言验证成功")
        print("验证结果集的列数为5")
        assert c.colcount == 5
        print("断言验证成功")
        print("调用fetch方法, 得到的结果集为:",c.fetchall())
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)
        conn.close()  
    print("*****************")

    print("E. 测试 UPDATE")
    sql_str5 = "UPDATE test_table SET CHGAMT = 'ABC' WHERE ID = 1;"
    print("执行:",sql_str5)
    try:
        conn.execute(sql_str5)
        print("接着执行:",sql_str4)
        c = conn.execute(sql_str4)
        print("验证结果集的行数为2")
        assert c.rowcount == 2
        print("断言验证成功")
        print("验证结果集的列数为5")
        assert c.colcount == 5
        print("断言验证成功")
        print("调用fetch方法, 得到的结果集为:",c.fetchall())      
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)   
        conn.close()
    print("*****************")   

    print("F. 测试 DELETE")
    sql_str6 = "DELETE FROM test_table WHERE ID = 2;"
    print("执行:",sql_str6)
    try:
        conn.execute(sql_str6)
        print("接着执行:",sql_str4)
        c = conn.execute(sql_str4)
        print("验证结果集的行数为1")
        assert c.rowcount == 1
        print("断言验证成功")
        print("验证结果集的列数为5")
        assert c.colcount == 5
        print("断言验证成功")
        print("调用fetch方法, 得到的结果集为:",c.fetchall())      
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)   
        conn.close()
    print("*****************")   

    print("G. 测试 executemany 方法")
    sql_str7 = "INSERT INTO test_table VALUES(?,?,?,?,?);"
    rows = [(3,33,'CCC','b1','b2'), (4,44,'DDD','b1','b2'),(5,55,'EEE','b1','b2'), (6,66,'FFF','b1','b2')]
    print("执行:",sql_str7,"参数为",rows)
    try:
        conn.executemany(sql_str7,rows)
        print("接着执行:",sql_str4)
        c = conn.execute(sql_str4)
        print("验证结果集的行数为5")
        assert c.rowcount == 5
        print("断言验证成功")
        print("验证结果集的列数为5")
        assert c.colcount == 5
        print("断言验证成功")
        print("调用fetch方法, 得到的结果集为:",c.fetchall())        
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)     
        conn.close()
    print("-------------------------------")

    print("5. 测试提交事务")
    try:
        conn.commit()
    except DatabaseError as e:
        print("DatabaseError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("6. 测试回滚事务")
    try:
        conn.rollback()
    except DatabaseError as e:
        print("DatabaseError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("7. 关闭连接，关闭数据库")
    try:
        conn.close()
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")


if __name__ == "__main__":
    print("-----connection连接对象测试-----")
    connection_test()
    