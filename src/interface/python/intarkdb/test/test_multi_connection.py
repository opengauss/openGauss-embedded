# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * test_multi_connection.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/test/test_multi_connection.py
# *
# * -------------------------------------------------------------------------
#  */

import sys
sys.path.append(r"../..")

import threading
from intarkdb.module.StandardError import InterfaceError, Error, DatabaseError
from intarkdb import intarkdb
import time


def multi_connection_test():
    print("建立第一个连接")
    try:
        conn = intarkdb.connect(database='.')
        print("验证连接的数量为1")
        assert conn.CONNECTION_NUM == 1
        print("断言验证成功")
        print("连接对象总数为",conn.CONNECTION_NUM)
    except InterfaceError as e:
        print("InterfaceError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("建立第二个连接")
    try:
        conn2 = intarkdb.connect(database='.')
        print("验证连接的数量为2")
        assert conn2.CONNECTION_NUM == 2
        print("断言验证成功")
        print("连接对象总数为",conn2.CONNECTION_NUM)
    except InterfaceError as e:
        print("InterfaceError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("测试第二个连接的事务操作")
    print("A. 开启事务")
    try:
        conn2.begin()
    except DatabaseError as e:
        print("DatabaseError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("*****************")
    print("B. 提交事务")
    try:
        conn2.commit()
    except DatabaseError as e:
        print("DatabaseError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("*****************")
    print("C. 回滚事务")
    try:
        conn2.rollback()
    except DatabaseError as e:
        print("DatabaseError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("建立第三个连接")
    try:
        conn3 = intarkdb.connect(database='.')
        print("验证连接的数量为3")
        assert conn.CONNECTION_NUM == 3
        print("断言验证成功")
        print("连接对象总数为",conn.CONNECTION_NUM)
    except InterfaceError as e:
        print("InterfaceError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("测试第三个连接的游标方法")
    try:
        conn3.cursor()
    except Error as e:
        print("Error!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("关闭第二个连接")
    try:
        conn2.close()
        print("验证连接的数量为2")
        assert conn.CONNECTION_NUM == 2
        print("断言验证成功")
        print("连接对象总数为",conn.CONNECTION_NUM)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("关闭第三个连接")
    try:
        conn3.close()
        print("验证连接的数量为1")
        assert conn.CONNECTION_NUM == 1
        print("断言验证成功")
        print("连接对象总数为",conn.CONNECTION_NUM)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("关闭第一个连接")
    try:
        conn.close()
        print("验证连接的数量为0")
        assert conn.CONNECTION_NUM == 0
        print("断言验证成功")
        print("连接对象总数为",conn.CONNECTION_NUM)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")


def multi_thread_test(thread_num = 5):
    def func(id):
        conn = intarkdb.connect(database='.')
        print("第",id+1,"个连接建立成功，连接对象总数为",conn.CONNECTION_NUM)
        print("-------------------------------")
        
        time.sleep(0.01)

        conn.close()  
        print("第",id+1,"个连接关闭成功，连接对象总数为",conn.CONNECTION_NUM)
        print("-------------------------------")

    t = []
    for i in range(thread_num):
        try:
            t.append(threading.Thread(target=func, args=(i,)))
        except Exception as e:
            print("error message:",e)        
    for i in range(thread_num):
        try:
            t[i].start()
        except Exception as e:
            print("error message:",e)
    for i in range(thread_num):
        try:
            t[i].join()
        except Exception as e:
            print("error message:",e)

if __name__ == "__main__":
    # print("-----建立多个connection连接对象测试-----")
    # multi_connection_test()
    # print()
    print("-----多线程建立建立connection连接对象测试-----")
    multi_thread_test()