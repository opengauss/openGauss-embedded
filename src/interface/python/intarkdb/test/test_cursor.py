# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * test_cursor.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/test/test_cursor.py
# *
# * -------------------------------------------------------------------------
#  */

import sys
sys.path.append(r"../..")

from intarkdb.module.StandardError import InterfaceError, Error, ProgrammingError
from intarkdb import intarkdb



def cursor_test():
    print("1. 连接intarkdb, 设置isolation_level为'auto', 使得事务自动开启")
    try:
        conn = intarkdb.connect(database='.', isolation_level='auto')
    except InterfaceError as e:
        print("InterfaceError!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("2. 创建 cursor 对象")
    try:
        c = conn.cursor()
    except Error as e:
        print("Error!","error message:",e)
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

    print("3. 测试execute方法")
    print("A. 测试 DROP")
    sql_str1 = "DROP TABLE IF EXISTS TEST_TABLE;"
    print("执行:",sql_str1)
    try:
        c.execute(sql_str1)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)
        c.close()
    print("*****************")

    print("B. 测试 CREATE")
    sql_str2 = "CREATE TABLE TEST_TABLE (ID INTEGER, NAME VARCHAR(20), AGE FLOAT, GENDER BOOLEAN, PRIMARY KEY (ID));"
    print("执行:",sql_str2)
    try:
        c.execute(sql_str2)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)
        c.close()
    print("*****************")

    print("C. 测试 INSERT")
    sql_str3 = "INSERT INTO TEST_TABLE VALUES (1, 'TOM', 23.0, True), (2, 'AMY', 25.0, False);"
    print("执行:",sql_str3)
    try:
        c.execute(sql_str3)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)
        c.close()
    print("*****************")

    print("D. 测试 SELECT")
    sql_str4 = "SELECT ID,NAME,AGE,GENDER FROM TEST_TABLE;"
    print("执行:",sql_str4)
    try:
        c.execute(sql_str4)
        print("验证结果集的行数为2")
        assert c.rowcount == 2
        print("断言验证成功")
        print("验证结果集的列数为4")
        assert c.colcount == 4
        print("断言验证成功")
        print("调用fetchall方法, 得到的结果集为:",c.fetchall())
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)  
        c.close()
    print("*****************")

    print("E. 测试 UPDATE")
    sql_str5 = "UPDATE TEST_TABLE SET AGE = 30 WHERE ID = 1;"
    print("执行:",sql_str5)
    try:
        c.execute(sql_str5)
        print("接着执行:",sql_str4)
        c.execute(sql_str4)
        print("验证结果集的行数为2")
        assert c.rowcount == 2
        print("断言验证成功")
        print("验证结果集的列数为4")
        assert c.colcount == 4
        print("断言验证成功")
        print("调用fetchall方法, 得到的结果集为:",c.fetchall())    
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)   
        c.close()
    print("*****************")

    print("F. 测试 DELETE")
    sql_str6 = "DELETE FROM TEST_TABLE WHERE ID = 1;"
    print("执行:",sql_str6)
    try:
        c.execute(sql_str6)
        print("接着执行:",sql_str4)
        c.execute(sql_str4)
        print("验证结果集的行数为1")
        assert c.rowcount == 1
        print("断言验证成功")
        print("验证结果集的列数为4")
        assert c.colcount == 4
        print("断言验证成功")
        print("调用fetchall方法, 得到的结果集为:",c.fetchall())       
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)   
        c.close()
    print("*****************")   

    print("G. 测试 executemany 方法")
    sql_str7 = "INSERT INTO TEST_TABLE VALUES(?,?,?,?);"
    rows = [(3, 'MIKE', 18.0, 1), (4, 'lily', 35.3, 0),(5, 'JACK', 60.1, 1), (6, 'BOBO', 19.0, 0),(7, 'lee', 55.0, 1)]
    print("执行:",sql_str7,"参数为",rows)
    try:
        c.executemany(sql_str7,rows)
        print("接着执行:",sql_str4)
        c.execute(sql_str4)
        print("验证结果集的行数为6")
        assert c.rowcount == 6
        print("断言验证成功")
        print("验证结果集的列数为4")
        assert c.colcount == 4
        print("断言验证成功")
        print("调用fetchall方法, 得到的结果集为:",c.fetchall())     
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)     
        c.close()
    print("-------------------------------")

    print("4. 测试fetch方法")
    print("首先执行SELETE语句, 使得rownumner归零")
    print("执行:",sql_str4)
    try:
        c.execute(sql_str4)
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)
        c.close()
    print("*****************")

    print("A. 测试fetchone方法")
    try:
        result = c.fetchone()
        print("结果为",result)
        print("测试切片, 验证名字为'MIKE'")
        assert result[1] == "MIKE"
        print("断言验证成功")
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)   
        c.close()
    print("*****************")  

    print("B. 测试fetchmany方法")
    try:
        print("取arraysize为2")
        result2 = c.fetchmany(2)
        print("结果为",result2)
        print("测试切片,验证第2个人的年龄为35.3")
        assert result2[1][2] == 35.3
        print("断言验证成功")  
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)   
        c.close()
    print("*****************")  

    print("C. 测试fetchall方法")
    try:
        result3 = c.fetchall()
        print("结果为",result3)
        print("测试切片,输出最后一个人的性别为男性")
        assert result3[len(result3)-1][3] == True
        print("断言验证成功")  
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        c.close()
    except Exception as e:
        print("error message:",e)   
        c.close()
    print("-------------------------------")

    print("5. 关闭游标")
    try:
        c.close()
    except ProgrammingError as e:
        print("ProgrammingError!","error message:",e)
        conn.close()
    except Exception as e:
        print("error message:",e)   
        conn.close()
    print("-------------------------------")   

    print("6. 关闭连接，关闭数据库")
    try:
        conn.close()
    except Exception as e:
        print("error message:",e)
    print("-------------------------------")

if __name__ == "__main__":
    print("-----cursor游标对象测试-----")
    cursor_test()