# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * connection.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/module/connection.py
# *
# * -------------------------------------------------------------------------
#  */

from intarkdb.module.StandardError import InterfaceError, DatabaseError, Error
from intarkdb.module.model import INTARKDB_DATABASE, INTARKDB_CONNECTION, GS_SUCCESS
from intarkdb.module.sql import SQL
from intarkdb.module.cursor import Cursor
import threading


class Connection:

    DATABASE = INTARKDB_DATABASE()
    CONNECTION_NUM = 0
    CONNECTION_LOCK = threading.Lock()


    def __init__(self, path=".", isolation = "auto"):
        self._path = path   
        self._conn = INTARKDB_CONNECTION()
        self._isolation = isolation
        self.__sql = SQL(Connection.DATABASE)
    

    def __del__(self):
        pass


    def open(self):
        with Connection.CONNECTION_LOCK:
            if (Connection.CONNECTION_NUM == 0):
                # 打开数据库
                ret = self.__sql.open_db(self._path)
                if GS_SUCCESS == ret:
                    print("open database success, path = {}".format(self._path))
                else:
                    raise InterfaceError(ret)
            
            # 建立连接
            ret = self.__sql.intarkdb_connect(self._conn)
            if GS_SUCCESS == ret:
                Connection.CONNECTION_NUM += 1
                print("intarkdb connect success")
            else:
                print("intarkdb connect fail")
                raise InterfaceError()
        

    def cursor(self):
        cur = Cursor(conn = self, database = Connection.DATABASE)
        if cur:
            print("create cursor success")
            return cur  
        else:
            print("create cursor fail")
            raise Error(-1)

            
    # 启动事务
    def begin(self):
        ret = self.__sql.intarkdb_query(self._conn, "begin", None)
        if GS_SUCCESS == ret:
            print("transaction begin success")
            return ret
        else:
            print("transaction begin fail")
            raise DatabaseError(ret)

        
    # 提交事务
    def commit(self):
        ret = self.__sql.intarkdb_query(self._conn, "commit", None)
        if GS_SUCCESS == ret:
            print("intarkdb commit success")
            return ret
        else:
            print("intarkdb commit fail")
            raise DatabaseError(ret)
 

    # 回滚事务
    def rollback(self):
        ret = self.__sql.intarkdb_query(self._conn, "rollback", None)
        if GS_SUCCESS == ret:
            print("intarkdb rollback success")
            return ret
        else:
            print("intarkdb rollback fail")
            raise DatabaseError(ret)


    def close(self):
        # 关闭连接
        with Connection.CONNECTION_LOCK:
            self.__sql.intarkdb_disconnect(self._conn)
            Connection.CONNECTION_NUM -= 1
            print("intarkdb disconnect success")

            # 关闭数据库
            # 数据库开启比较快，连接数量为0的时候应该关闭
            if (Connection.CONNECTION_NUM == 0):
                self.__sql.close_db()  
                print("close db success!")  
                


    def execute(self, operation, parameters = None):
        cur = Cursor(conn = self, database = Connection.DATABASE)  
        cur.execute(operation, parameters)   
        return cur  


    def executemany(self, operation, seq_of_parameters):  
        cur = Cursor(conn = self, database = Connection.DATABASE)   
        cur.executemany(operation, seq_of_parameters)
        return cur  
        
