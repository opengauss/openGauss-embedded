# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * sql.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/module/sql.py
# *
# * -------------------------------------------------------------------------
#  */

from ctypes import *
from intarkdb.module.model import *
from intarkdb.dbapi2 import Date,Timestamp
import decimal


class Dynamic:
    def __init__(self):
        self._intarkdb_sql_clib = CDLL("libintarkdb.so")


    def get_intarkdb_sql_clib(self):
        return self._intarkdb_sql_clib


class SQL:
    def __init__(self, database):
        self._dynamic = Dynamic()
        self._db = database


    def open_db(self, path):
        ret = self._dynamic.get_intarkdb_sql_clib().intarkdb_open(path.encode(), byref(self._db))
        return ret


    def close_db(self):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_close(byref(self._db))


    def intarkdb_query(self, conn, sql_str, intarkdb_result):
        ret = self._dynamic.get_intarkdb_sql_clib().intarkdb_query(
            conn, sql_str.encode(), intarkdb_result)
        return ret


    def intarkdb_row_count(self, intarkdb_result):
        rowcount = self._dynamic.get_intarkdb_sql_clib().intarkdb_row_count(intarkdb_result)
        return rowcount


    def intarkdb_column_count(self, intarkdb_result):
        return self._dynamic.get_intarkdb_sql_clib().intarkdb_column_count(intarkdb_result)

    def intarkdb_column_name(self, intarkdb_result, col_index) -> str:
        self._dynamic.get_intarkdb_sql_clib().intarkdb_column_name.restype = c_char_p
        if (self._dynamic.get_intarkdb_sql_clib().intarkdb_column_name(intarkdb_result, col_index) == None):
            return "None"
        return self._dynamic.get_intarkdb_sql_clib().intarkdb_column_name(intarkdb_result, col_index).decode()


    def intarkdb_column_type(self, intarkdb_result, col_index):
        return self._dynamic.get_intarkdb_sql_clib().intarkdb_column_type(intarkdb_result, col_index)


    def intarkdb_value(self, intarkdb_result, row_index, col_index, code_type):
        # 转化为字符串类型
        if code_type != 0:
            self._dynamic.get_intarkdb_sql_clib().intarkdb_value_varchar.restype = c_char_p
            res = self._dynamic.get_intarkdb_sql_clib().intarkdb_value_varchar(intarkdb_result, row_index, col_index)
            if(res != None):
                res = res.decode()
            else:
                return None
        else:
            return None

        # 根据字段类型，转化到对应类型
        if(res in ['-infinity', 'infinity','-nan']):
            pass
        elif (res == ""):
            return None
        elif code_type in [GS_TYPE_UTINYINT, GS_TYPE_USMALLINT, GS_TYPE_UINT32]:
            self._dynamic.get_intarkdb_sql_clib().intarkdb_value_uint32.restype = c_uint32
            res = self._dynamic.get_intarkdb_sql_clib().intarkdb_value_uint32(intarkdb_result, row_index, col_index)
        elif code_type == GS_TYPE_UINT64:
            self._dynamic.get_intarkdb_sql_clib().intarkdb_value_uint64.restype = c_uint64
            res = self._dynamic.get_intarkdb_sql_clib().intarkdb_value_uint64(intarkdb_result, row_index, col_index)
        elif(code_type in [GS_TYPE_REAL, GS_TYPE_FLOAT]):
            self._dynamic.get_intarkdb_sql_clib().intarkdb_value_double.restype = c_double
            res = self._dynamic.get_intarkdb_sql_clib().intarkdb_value_double(intarkdb_result, row_index, col_index)
        elif(code_type in [GS_TYPE_INTEGER, GS_TYPE_TINYINT, GS_TYPE_SMALLINT, GS_TYPE_BIGINT]):
            res = int(res)
        elif(code_type == GS_TYPE_BOOLEAN):
            res = int(res)
        elif(code_type == GS_TYPE_DECIMAL):
            decimal.getcontext().prec = 3
            res = decimal.Decimal(res)
        elif(code_type == GS_TYPE_DATE):
            # 字符串为日期形式 "YYYY-MM-DD"
            date_parts = res.split("-")
            year = int(date_parts[0])
            month = int(date_parts[1])
            day = int(date_parts[2])
            res = Date(year, month, day)  
            res = str(res)
        elif(code_type == GS_TYPE_TIMESTAMP):    
            # 字符串为日期形式 '%Y-%m-%d %H:%M:%S.%f'
            parts = res.split()     
            # %Y-%m-%d
            date_parts = parts[0].split("-")
            year = int(date_parts[0])
            month = int(date_parts[1])
            day = int(date_parts[2])
            # %H:%M:%S.%f
            time_parts = parts[1].split(":")
            hour = int(time_parts[0])
            minute = int(time_parts[1])
            second_part = time_parts[2].split(".")
            second = int(second_part[0])
            microsecond = 0
            if(len(second_part)>1):
                microsecond = int(second_part[1])
            res = Timestamp(year, month, day, hour, minute, second, microsecond)
            res = res.strftime('%Y-%m-%d %H:%M:%S.%f')
            
        # 返回结果
        return res


    def intarkdb_init_result(self):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_init_result.restype = POINTER(INTARKDB_RES_DEF)
        result = self._dynamic.get_intarkdb_sql_clib().intarkdb_init_result()
        return result


    def intarkdb_free_result(self, intarkdb_result):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_free_row(intarkdb_result)


    def intarkdb_destory_result(self, intarkdb_result):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_destroy_result(intarkdb_result)


    def intarkdb_result_msg(self, intarkdb_result):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_result_msg.restype = c_char_p
        return self._dynamic.get_intarkdb_sql_clib().intarkdb_result_msg(intarkdb_result)


    def intarkdb_connect(self, conn):
        ret = self._dynamic.get_intarkdb_sql_clib().intarkdb_connect(self._db, byref(conn))
        return ret


    def intarkdb_disconnect(self, conn):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_disconnect(byref(conn))


    def intarkdb_prepare(self, conn, sql_str, prepared_statement):
        ret = self._dynamic.get_intarkdb_sql_clib().intarkdb_prepare(
            conn, sql_str.encode(), byref(prepared_statement))
        return ret


    def intarkdb_execute_prepared(self, prepared_statement, intarkdb_result):
        ret = self._dynamic.get_intarkdb_sql_clib().intarkdb_execute_prepared(
            prepared_statement, intarkdb_result)
        return ret


    def intarkdb_prepare_errmsg(self, prepared_statement):
        return self._dynamic.get_intarkdb_sql_clib().intarkdb_prepare_errmsg(prepared_statement)


    def intarkdb_destroy_prepare(self, prepared_statement):
        self._dynamic.get_intarkdb_sql_clib().intarkdb_destroy_prepare(byref(prepared_statement))


    def intarkdb_bind_value(self, prepared_statement, param_idx, val, code_type):
        # prepared_statement = byref(prepared_statement)
        if(code_type == GS_TYPE_INTEGER):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_int32(prepared_statement, param_idx, val)
        elif(code_type == GS_TYPE_BIGINT):
            self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_int64.argtypes = [
                INTARKDB_PREPARED_STATEMENT, c_uint, c_int64]
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_int64(prepared_statement, param_idx, val)
        elif(code_type == GS_TYPE_TINYINT):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_int8(prepared_statement, param_idx, val)
        elif(code_type == GS_TYPE_SMALLINT):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_int16(prepared_statement, param_idx, val)
        elif(code_type == GS_TYPE_BOOLEAN):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_boolean(prepared_statement, param_idx, val)
        elif(code_type in [GS_TYPE_REAL, GS_TYPE_FLOAT]):
            self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_double.argtypes = [
                INTARKDB_PREPARED_STATEMENT, c_uint, c_double]
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_double(prepared_statement, param_idx, c_double(val).value)
        elif(code_type in [GS_TYPE_DATE, GS_TYPE_TIMESTAMP]):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_date(prepared_statement, param_idx, val)
        elif(code_type == GS_TYPE_VARCHAR):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_varchar(prepared_statement, param_idx, val.encode())
        elif(code_type == None):
            return self._dynamic.get_intarkdb_sql_clib().intarkdb_bind_null(prepared_statement, param_idx)
        else:
            return GS_ERROR
