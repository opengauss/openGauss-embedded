# /*
# * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
# *
# * openGauss embedded is licensed under Mulan PSL v2.
# * You can use this software according to the terms and conditions of the Mulan PSL v2.
# * You may obtain a copy of Mulan PSL v2 at:
# *
# * http://license.coscl.org.cn/MulanPSL2
# *
# * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
# * See the Mulan PSL v2 for more details.
# * -------------------------------------------------------------------------
# *
# * model.py
# *
# * IDENTIFICATION
# * openGauss-embedded/src/interface/python/intarkdb/module/model.py
# *
# * -------------------------------------------------------------------------
#  */

from ctypes import *

GS_FALSE = 0
GS_TRUE = 1

# class EN_STATUS(Enum):
GS_ERROR = -1
GS_SUCCESS = 0
GS_TIMEDOUT = 1

# class EN_ASSIGN_TYPE(Enum):
ASSIGN_TYPE_EQUAL = 0  # 等于
ASSIGN_TYPE_LESS = 0 + 1  # 小于
ASSIGN_TYPE_MORE = 0 + 2  # 大于
ASSIGN_TYPE_LESS_EQUAL = 0 + 3  # 小于等于
ASSIGN_TYPE_MORE_EQUAL = 0 + 4  # 大于等于
ASSIGN_TYPE_UNEQUAL = 0 + 5  # 不等于

# class EN_GS_TYPE(Enum):
GS_TYPE_UNKNOWN = -1
GS_TYPE_BASE = 20000
GS_TYPE_INTEGER = 20000 + 1  # /# * native 32 bits integer # */
GS_TYPE_BIGINT = 20000 + 2  # /# * native 64 bits integer # */
GS_TYPE_REAL = 20000 + 3  # /# * 8-byte native double # */
GS_TYPE_NUMBER = 20000 + 4  # /# * number # */
GS_TYPE_DECIMAL = 20000 + 5  # /# * decimal  internal used # */
GS_TYPE_DATE = 20000 + 6  # /# * datetime # */
GS_TYPE_TIMESTAMP = 20000 + 7  # /# * timestamp # */
GS_TYPE_CHAR = 20000 + 8  # /# * char(n) # */
GS_TYPE_VARCHAR = 20000 + 9  # /# * varchar  varchar2 # */
GS_TYPE_STRING = 20000 + 10  # /# * native char # * # */
GS_TYPE_BINARY = 20000 + 11  # /# * binary # */
GS_TYPE_VARBINARY = 20000 + 12  # /# * varbinary # */
GS_TYPE_CLOB = 20000 + 13  # /# * clob # */
GS_TYPE_BLOB = 20000 + 14  # /# * blob # */
GS_TYPE_CURSOR = 20000 + 15  # /# * resultset  for stored procedure # */
GS_TYPE_COLUMN = 20000 + 16  # /# * column type  internal used # */
GS_TYPE_BOOLEAN = 20000 + 17
GS_TYPE_TIMESTAMP_TZ_FAKE = 20000 + 18
GS_TYPE_TIMESTAMP_LTZ = 20000 + 19
GS_TYPE_INTERVAL = 20000 + 20  # /# * interval of Postgre style  no use # */
GS_TYPE_INTERVAL_YM = 20000 + 21  # /# * interval YEAR TO MONTH # */
GS_TYPE_INTERVAL_DS = 20000 + 22  # /# * interval DAY TO SECOND # */
GS_TYPE_RAW = 20000 + 23  # /# * raw # */
GS_TYPE_IMAGE = 20000 + 24  # /# * image  equals to longblob # */
GS_TYPE_UINT32 = 20000 + 25  # /# * unsigned integer # */
GS_TYPE_UINT64 = 20000 + 26  # /# * unsigned bigint # */
GS_TYPE_SMALLINT = 20000 + 27  # /# * 16-bit integer # */
GS_TYPE_USMALLINT = 20000 + 28  # /# * unsigned 16-bit integer # */
GS_TYPE_TINYINT = 20000 + 29  # /# * 8-bit integer # */
GS_TYPE_UTINYINT = 20000 + 30  # /# * unsigned 8-bit integer # */
GS_TYPE_FLOAT = 20000 + 31  # /# * 4-byte float # */
GS_TYPE_TIMESTAMP_TZ = 20000 + 32  # /# * timestamp with time zone # */
GS_TYPE_ARRAY = 20000 + 33  # /# * array # */

GS_TYPE_OPERAND_CEIL = 20000 + 40    # ceil of operand type
GS_TYPE_RECORD = 20000 + 41
GS_TYPE_COLLECTION = 20000 + 42
GS_TYPE_OBJECT = 20000 + 43
GS_TYPE__DO_NOT_USE = 20000 + 44

GS_TYPE_FUNC_BASE = 20000 + 200
GS_TYPE_TYPMODE = 20000 + 200 + 1
GS_TYPE_VM_ROWID = 20000 + 200 + 2
GS_TYPE_ITVL_UNIT = 20000 + 200 + 3
GS_TYPE_UNINITIALIZED = 20000 + 200 + 4
GS_TYPE_NATIVE_DATE = 20000 + 200 + 5       # native datetime  internal used
GS_TYPE_NATIVE_TIMESTAMP = 20000 + 200 + 6  # native timestamp  internal used
GS_TYPE_LOGIC_TRUE = 20000 + 200 + 7       # native true  internal


class COL_TEXT(Structure):
    _fields_ = [
        ("str", c_char_p),
        ("len", c_uint),
        ("assign", c_int),
    ]


class EXP_COLUMN_DEF(Structure):
    _fields_ = [
        ("name", COL_TEXT),
        ("col_type", c_int),
        ("col_slot", c_ushort),
        ("size", c_ushort),
        ("nullable", c_uint),
        ("is_primary", c_uint),
        ("is_default", c_uint),
        ("default_val", COL_TEXT),
        ("crud_value", COL_TEXT),
        ("precision", c_ushort),
        ("comment", COL_TEXT),
    ]


class EXP_INDEX_DEF(Structure):
    _fields_ = [
        ("name", COL_TEXT),
        ("cols", POINTER(COL_TEXT)),
        ("col_count", c_uint),
        ("is_unique", c_uint),
        ("is_primary", c_uint),
    ]


class RES_ROW_DEF(Structure):
    _fields_ = [
        ("column_count", c_int),
        ("row_column_list", POINTER(EXP_COLUMN_DEF))
    ]


class INTARKDB_RES_DEF(Structure):
    _fields_ = [
        ("row_count", c_int),
        ("res_row", c_void_p),
    ]


class INTARKDBREPLY(Structure):
    _fields_ = [
        ("type", c_int),
        ("len", c_size_t),
        ("str", c_char_p),
    ]


class INTARKDB_DATABASE(Structure):
    _fields_ = [
        ("db", c_void_p),
    ]


class INTARKDB_CONNECTION(Structure):
    _fields_ = [
        ("conn", c_void_p),
    ]


class INTARKDB_PREPARED_STATEMENT(Structure):
    _fields_ = [
        ("prep_stmt", c_void_p),
    ]


class API_TEXT_T(Structure):
    _fields_ = [
        ("str", c_char_p),
        ("len", c_longlong),
        ("data_type", c_longlong),
    ]


class INTARKDB_RES_DEF(Structure):
    _fields_ = [
        ("row_count", c_longlong),
        ("is_select", c_bool),
        ("res_row", c_void_p),
        ("column_count", c_longlong),
        ("column_names", POINTER(API_TEXT_T)),
        ("msg", c_char_p),
        ("value_ptr", c_char_p),
        ("row_idx", c_longlong),
    ]
