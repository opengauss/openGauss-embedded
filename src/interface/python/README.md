# IntarkDB Python DB API

#### 一、运行环境配置

如果数据库动态库已经安装到系统下，该步骤可以忽略。如果未安装，则需要配置LD_LIBRARY_PATH，指定数据库动态库文件地址：

```shell
export LD_LIBRARY_PATH=~/openGauss-embedded/output/debug/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=~/openGauss-embedded/output/release/lib:$LD_LIBRARY_PATH
```

如果不想每次打开终端执行该命令，则可以将该命令添加到~/.bashrc或者/etc/profile文件中，例如：

```shell
# 打开 ~/.bashrc 文件
sudo vim ~/.bashrc

# 复制下面语句，插入到 ~/.bashrc 文件的最后一行，保存并退出
export LD_LIBRARY_PATH=~/openGauss-embedded/output/debug/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=~/openGauss-embedded/output/release/lib:$LD_LIBRARY_PATH

# 激活 ~/.bashrc 文件
source ~/.bashrc
```

#### 二、使用说明

1. 导入 intarkdb 模块

```shell
# 可使用 pip 安装 intarkdb 的安装包
from intarkdb import intarkdb
```

2. 创建连接对象

```shell
conn = intarkdb.connect()
```

3. 创建游标对象

```shell
c = conn.cursor()
```

4. 调用 execute 方法执行 SQL 语句

```shell
# DROP
sql_str1 = "DROP TABLE IF EXISTS TEST_TABLE;"
c.execute(sql_str1)

# CREATE
sql_str2 = "CREATE TABLE TEST_TABLE (ID INTEGER, NAME VARCHAR(20), AGE FLOAT, GENDER BOOLEAN, PRIMARY KEY (ID));"
c.execute(sql_str2)

# INSERT
sql_str3 = "INSERT INTO TEST_TABLE VALUES (1, 'TOM', 23.0, True), (2, 'AMY', 25.0, False);"
c.execute(sql_str3)

# SELECT
sql_str4 = "SELECT ID,NAME,AGE,GENDER FROM TEST_TABLE;"
c.execute(sql_str4)

# UPDATE
sql_str5 = "UPDATE TEST_TABLE SET AGE = 30 WHERE ID = 1;"
c.execute(sql_str5)

# DELETE
sql_str6 = "DELETE FROM TEST_TABLE WHERE ID = 1;"
c.execute(sql_str6)
```

5. 调用 fetch* 方法查询

```shell
# 执行 SELETE 语句, 使得 rownumner 归零
sql_str4 = "SELECT ID,NAME,AGE,GENDER FROM TEST_TABLE;"
c.execute(sql_str4)

# 取查询结果的一行
result = c.fetchone()
print("fetchone:",result)

# 取查询结果的多行
result2 = c.fetchmany(2)
print("fetchmany:",result2)

# 取查询结果的剩余所有行
result3 = c.fetchall()
print("fetchall",result3)
```

6. 关闭游标

```shell
c.close()
```

7. 关闭连接

```shell
# 关闭连接后，当连接数量为0时，关闭数据库
conn.close()
```

#### 三、测试代码

```shell
可直接执行 test.py, 或者分别执行 

python test_connection.py 

python test_multi_connection.py 

python test_cursor.py
```
