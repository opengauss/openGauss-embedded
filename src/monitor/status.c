/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* status.c
*
* IDENTIFICATION
* openGauss-embedded/src/monitor/status.c
*
* -------------------------------------------------------------------------
*/

#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/utsname.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>
#include "cm_types.h"
#include "cm_defs.h"
#include "cm_log.h"
#include "status.h"

unsigned long get_cpu_total_occupy() {
	FILE *fd;
	long int temp[10];
	char buff[1024]={0};
	fd = fopen("/proc/stat","r");
		
	fgets(buff,sizeof(buff),fd);
	char name[64]={0};
	sscanf(buff,"%16s %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld",name,&temp[0],&temp[1],&temp[2],&temp[3],&temp[4],&temp[5],&temp[6],&temp[7],&temp[8],&temp[9]);
	fclose(fd);
	return temp[0]+temp[1]+temp[2]+temp[3];
}

double get_self_cpu(unsigned long long interval) {
    struct rusage self_ru, self_ru2;
    getrusage(RUSAGE_SELF, &self_ru);
    uint64 cpu_time1 = get_cpu_total_occupy();
    uint32 time_internal = 100;
    cm_sleep(time_internal);
    getrusage(RUSAGE_SELF, &self_ru2);
    uint64 cpu_time2 = get_cpu_total_occupy();
    long proc_cpu_time = (self_ru2.ru_stime.tv_sec + self_ru2.ru_utime.tv_sec - self_ru.ru_stime.tv_sec - self_ru.ru_utime.tv_sec) * MS_PER_SEC + (self_ru2.ru_stime.tv_usec + self_ru2.ru_utime.tv_usec - self_ru.ru_stime.tv_usec - self_ru.ru_utime.tv_usec);
    return (double)proc_cpu_time/((cpu_time2 - cpu_time1) * GS_TIME_THOUSAND_UN);
}

int get_memory_by_pid(pid_t pid) {
  FILE* fd;
  char line[1024] = {0};
  char virtual_filename[32] = {0};
  char vmrss_name[32] = {0};
  int vmrss_num = 0;
  sprintf(virtual_filename, "/proc/%d/status", pid);
  fd = fopen(virtual_filename, "r");
  if (fd == NULL) {
    GS_LOG_DEBUG_INF("open %s failed", virtual_filename);
    return 0;
  }

  for(int i = 0 ; i < GS_SEC_PER_MIN; i++) {
    fgets(line, sizeof(line), fd);
    GS_LOG_DEBUG_INF("get line:%s", line);
    if (strstr(line, "VmRSS:") != NULL) {
      sscanf(line, "%16s %d", vmrss_name, &vmrss_num);
      break;
    }
  }
  fclose(fd);
  return vmrss_num;
}

uint64 get_dir_size(const char* filename) {
    //printf("%s dir:%s\n", __func__, filename);
    DIR *dir;
    struct dirent *entry;
    struct stat statbuf;
    long long int totalSize=0;

    lstat(filename, &statbuf);
    totalSize+=statbuf.st_size;
    if ((dir = opendir(filename)) == NULL) {
        fprintf(stderr, "Cannot open dir: %s", filename);
        return totalSize;
    }
    while ((entry = readdir(dir)) != NULL) {
        char subdir[256];
        sprintf(subdir, "%s/%s", filename, entry->d_name);
        lstat(subdir, &statbuf);
        if (S_ISDIR(statbuf.st_mode)) {
            if (strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0)
                continue;
            uint64 subDirSize = get_dir_size(subdir);
            totalSize+=subDirSize;
        }
        else {
            totalSize+=statbuf.st_size;
        }
    }
    closedir(dir);    
    return totalSize;
}

void get_os_info(char* osInfo, unsigned long size) {
    static struct utsname name;
    uname(&name);
    snprintf(osInfo, size, "%s %s %s", name.sysname, name.release, name.machine);
    return;
}

uint64 get_start_time(uint64 pid)
{
    FILE *fd;
    char *p;
	unsigned long btime, run_time;
    char line[1024] = {0};
    char btime_name[8] = {0};
	fd = fopen("/proc/stat", "r");
	if (fd == NULL)
    {
        GS_LOG_DEBUG_INF("open /proc/stat failed");
        return 0;
    }
    while (fgets(line, sizeof(line), fd) != NULL )
    {
        if (strstr(line, "btime") != NULL)
        {
            sscanf(line, "%5s %lu", btime_name, &btime);
            break;
        }
    }
	close(fd);

    GS_LOG_DEBUG_INF("btime:%ld", btime);
    char pid_stat[255];
    sprintf(pid_stat, "/proc/%d/stat", pid);
    fd = fopen(pid_stat, "r");
    if (fd == NULL) {
        GS_LOG_DEBUG_INF("open %s failed", pid_stat);
        return 0;
    }
    char char_stat[1024] = {0};
    for(int i = 0; i < 22; i++)
    {
        fscanf(fd, "%s", char_stat);
    }
    run_time = strtoul(char_stat, &p, 10);
	close(fd);
    return run_time/sysconf(_SC_CLK_TCK) + btime;
}