/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * thread_pool.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/include/stream_agg/thread_pool.h
 *
 * -------------------------------------------------------------------------
  */
#pragma once
#include "compute/sql/include/main/database.h"
#include "compute/ts/include/stream_agg/scheduler.h"
#include "compute/ts/include/stream_agg/ts_stream_agg_job_manage.h"
#include "compute/ts/include/table/ts_table.h"
#include "main/connection.h"

class TsCleaner {
public:
    TsCleaner(std::shared_ptr<IntarkDB> db);
    ~TsCleaner();   

    void TsCleanInvalidInfo();
    void TsCleanProc();

private:
    std::shared_ptr<IntarkDB> db_;
};

void* TsClean(void *dbstoragewrap); 