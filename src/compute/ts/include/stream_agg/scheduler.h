/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * scheduler.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/include/stream_agg/scheduler.h
 *
 * -------------------------------------------------------------------------
  */
#pragma once

#include "job.h"
#include "thread_pool.h"
#include "compute/sql/include/main/database.h"

const uint64_t DEFAULT_SCHEDULE_INTERVAL = 3; // 单位： 秒
const std::string DEFAULT_DATA_MIN_TIME = "\'1970-01-01 00:00:00\'"; 
const uint64_t MAX_QUERY_SINTERVAL_NUMS = 10; // 最大查询范围不超过schedule interval 的整数倍
const uint32_t DEFAULT_MAX_RUNTIME = 5; // 单位： 秒
const uint32_t INVALID_MAX_RUNTIME = 0; // 单位： 秒
const int64_t MAX_WAITTIME_SINGLE_TIME = 5; // 单位： 秒

#define MACRO_START_TIME    ("__START_TIME__")
#define MACRO_END_TIME      ("__END_TIME__")

struct DbStorageWrapper {
    std::shared_ptr<BaseStorage> db_storage;
};

class TsScheduler {
public:
    TsScheduler(std::shared_ptr<IntarkDB> db, uint32_t thread_pool_num); // thread_pool_num: session->kernel->attr.exec_agg_thread_num
    ~TsScheduler();
    
    void TsStreamAggSchedulerProc();

    void TsGetScheduledJobList();

    void TsStartScheduledJobs();

    void TsScheduledJobStart(TsScheduledJob *sjob);

    void TsScheduledJobTransitionStateTo(TsScheduledJob *sjob, ScheduledJobState new_state);

    void TsTimeWait();
    timestamp_stor_t TsEarliestWakeupToStartNextJob();
    timestamp_stor_t TsEarliestJobTimeout();

    void TsCheckForTimedOutScheduledJobs();

    void TsSaveSJobStatInfo(Connection *conn, TsScheduledJob *sjob, bool is_succ_finished);

private:
    void TsAggSqlReplaceStartEndTime(Connection *conn, TsScheduledJob *sjob, 
        std::string &agg_sql, const timestamp_stor_t &start_time, const timestamp_stor_t &end_time);
    void TsSJobFailedStatSet(TsScheduledJob* sjob, timestamp_stor_t finish_at, std::string err_msg);
    
    void TsCheckANdDeleteDstTableOldData(Connection *conn, TsScheduledJob *sjob);
    void TsTimedOutScheduledJobProc(TsScheduledJob *sjob);

    bool32 TsScheduledJobCheckIfValid(TsScheduledJob *sjob);

    void TsScheduledJobStartEndTimeSet(TsScheduledJob *sjob);

    void TsScheduledJobNoneBucketExec(Connection *conn, std::string agg_sql, TsScheduledJob *sjob);
    void TsScheduledJobTimeBucketExec(Connection *conn, std::string &agg_sql, TsScheduledJob *sjob);
private:
    std::shared_ptr<IntarkDB> db_;

    std::vector<TsJobInfo> valid_joblist_;
    std::unordered_map<int64_t, TsScheduledJob> scheduled_job_map_;

    ThreadPool thread_pool_;
};

void * TsStreamAggSchedulerMain(void *dbstoragewrap);