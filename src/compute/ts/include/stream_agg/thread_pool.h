/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * thread_pool.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/include/stream_agg/thread_pool.h
 *
 * -------------------------------------------------------------------------
  */
#pragma once

#include <functional>
#include <future>
#include <thread>
#include <vector>
#include <deque>
#include <mutex>
#include <condition_variable>

#include "job.h"

class ThreadPool {
public:
    ThreadPool(size_t num_threads);
    ~ThreadPool();

    void ThreadFunction(); // 线程执行函数

    void AddJob(TsScheduledJob* sjob);

private:
    std::vector<std::thread> workers_;
    std::deque<TsScheduledJob*> job_queue_;
    std::mutex queue_mutex_;
    std::condition_variable condition_;
    std::atomic<bool> stop_{false};
};
