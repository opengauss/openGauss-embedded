/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * job.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/include/stream_agg/job.h
 *
 * -------------------------------------------------------------------------
  */
#pragma once

#include <functional>

#include "storage/gstor/zekernel/common/cm_date.h"
#include "ts_stream_agg_job_manage.h"

class TsScheduledJob {
public:
    TsScheduledJob() {}
    TsScheduledJob(TsJobInfo vjob): job(vjob) {}
    ~TsScheduledJob() {}
public:
    TsJobInfo job;

    // 任务状态信息
    timestamp_stor_t dst_start_timestamp; // 本次目标表插入的数据代表的源数据表的起始时间戳
    timestamp_stor_t dst_end_timestamp; // 本次目标表插入的数据代表的源数据表的结束时间戳
    std::string dst_start_datetime; // 本次目标表插入的数据代表的源数据表的起始时间
    std::string dst_end_datetime; // 本次目标表插入的数据代表的源数据表的结束时间
    timestamp_stor_t timeout_at; // 下次启动后执行超时时间
    ScheduledJobState state{SCHEDULED_JOB_STATE_SCHEDULED}; // 被调度任务状态

    std::function<void(TsScheduledJob*)> task;  // Task or function to be executed by the job
};

