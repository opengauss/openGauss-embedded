/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * ts_stream_agg_job_manage.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/include/stream_agg/ts_stream_agg_job_manage.h
 *
 * -------------------------------------------------------------------------
 */

#pragma once
#include <vector>
#include <string>
#include "storage/gstor/zekernel/common/cm_date.h"
#include "storage/gstor/zekernel/common/cm_defs.h"
#include "compute/sql/include/type/timestamp_t.h"
#include "common/record_batch.h"

typedef enum JobState
{
    JOB_STATE_NULL = -1,
	JOB_STATE_INVALID, // 无效
	JOB_STATE_VALID, // 有效, 被调度的任务只能是有效任务
	JOB_STATE_PAUSE // 暂停
}JobState;

typedef enum BucketType
{
    BUCKET_TYPE_NULL = -1,
	NONE_BUCKET, // 无分桶
	TIME_BUCKET, // 时间分桶
}BucketType;

typedef enum ScheduleTimeType
{
    SCHEDULE_TIME_ONE = 0,      // 一次性任务调度
	SCHEDULE_TIME_CYCLE = 1     // 周期调度
}ScheduleTimeType;

typedef enum ScheduledJobState
{
	/* terminal state for now. Later we may have path to SCHEDULED_JOB_STATE_SCHEDULED */
	SCHEDULED_JOB_STATE_DISABLED,
	/*
	 * This is the initial state. next states: SCHEDULED_JOB_STATE_STARTED,
	 * SCHEDULED_JOB_STATE_DISABLED. This job is not running and has been scheduled to
	 * be started at a later time.
	 */
	SCHEDULED_JOB_STATE_SCHEDULED,
	/*
	 * next states: SCHEDULED_JOB_STATE_TERMINATING, SCHEDULED_JOB_STATE_SCHEDULED. This job has
	 * been started by the scheduler and is either running or finished (and
	 * the finish has not yet been detected by the scheduler).
	 */
	SCHEDULED_JOB_STATE_STARTED,
	/*
	 * next states: SCHEDULED_JOB_STATE_SCHEDULED. The scheduler has explicitly sent a
	 * terminate to this job but has not yet detected that it has stopped.
	 */
	SCHEDULED_JOB_STATE_TERMINATING,
    SCHEDULED_JOB_STATE_EXEC_TIMEOUT
} ScheduledJobState;

const uint32_t uint32_null = 0xFFFFFFFF;
const std::string string_null = "";
const timestamp_stor_t timestamp_stor_null = { 0xFFFFFFFF };
const uint64_t uint64_null = 0xFFFFFFFF;
const BucketType bucket_type_null = BUCKET_TYPE_NULL;
const JobState job_status_null = JOB_STATE_NULL;

typedef struct TsJob {
    // 任务表
    uint32_t job_id; // 流计算任务id
    std::string name; // 流计算任务名
    JobState state; // 流计算任务状态， 只有该状态为 JOB_STATE_VALID 时，才能被调度到任务队列
    uint32_t schedule_time; // 任务调度周期, 默认0 为一次性任务
    uint32_t schedule_interval; // 调度周期, 默认300 单位：秒
    uint32_t max_runtime; // 任务单次最长执行时间, 默认600 单位：秒
    uint32_t failed_retry_num; // 失败重试次数
    uint32_t retry_period; // 失败重试间隔, 单位：秒
    timestamp_stor_t initial_start; // 首次启动时间，默认立刻
    uint32_t partiton_type; // 时序表分区字段类型, 参考 gs_type_t 
    uint32_t offset_start; // 任务执行起始时间，默认0为数据最小时间, 单位：秒
    uint32_t offset_end; // 任务执行结束时间，默认0为数据最大时间, 单位：秒

    TsJob() 
        : job_id(uint32_null), name(string_null), state(job_status_null),schedule_time(uint32_null), 
        schedule_interval(uint32_null), max_runtime(uint32_null), failed_retry_num(uint32_null), 
        retry_period(uint32_null), initial_start(timestamp_stor_null), partiton_type(uint32_null), 
        offset_start(uint32_null), offset_end(uint32_null) {}
}TsJob;

typedef struct TsJobStat {
    uint32_t job_id; // 流计算任务id
    timestamp_stor_t last_start; // 上一次启动时间
    timestamp_stor_t last_finish; // 上一次结束时间
    timestamp_stor_t next_start; // 下一次启动时间
    timestamp_stor_t last_succuss_finish; // 上一次成功时间点，即上次成功的结束时间点
    uint64_t total_runs; // 任务执行次数
    uint64_t total_failed; // 任务执行失败次数
    uint32_t consecutive_failures; // 任务连续失败次数

    TsJobStat()
        : job_id(uint32_null), last_start(timestamp_stor_null), last_finish(timestamp_stor_null), 
        next_start(timestamp_stor_null), last_succuss_finish(timestamp_stor_null), total_runs(uint64_null),
        total_failed(uint64_null), consecutive_failures(uint32_null) {}
}TsJobStat;

typedef struct TsJobErr {
    uint32_t job_id; // 流计算任务id
    uint64_t pid; // 执行任务线程ID
    std::string start_time; // 任务开始时间
    std::string finish_time; // 任务结束时间
    std::string err_data; // 任务失败原因

    TsJobErr()
        : job_id(uint32_null), pid(uint64_null), start_time(string_null), 
        finish_time(string_null), err_data(string_null) {}
}TsJobErr;

typedef struct TsCaggPolicy {
    uint32_t job_id; // 流计算任务id
    std::string db_name; // 存储数据库名
    std::string table_name; // 存储表名
    std::string exec_sql; // 聚合sql语句
    BucketType bucket_type; // 分桶类型: 0无分桶; 1时间分桶
    std::string bucket_field_name; // 分桶字段名
    uint64_t bucket_width; // 分桶宽度, 单位：秒

    TsCaggPolicy() 
        : job_id(uint32_null), db_name(string_null), table_name(string_null),exec_sql(string_null), 
        bucket_type(bucket_type_null), bucket_field_name(string_null), bucket_width(uint64_null) {}
}TsCaggPolicy;

typedef struct TsJobInfo  {
    TsJob         ts_job;
    TsCaggPolicy  ts_cagg_policy;
    TsJobStat     ts_job_stat;
    TsJobErr      ts_job_err;
}TsJobInfo;

class Connection;

class TsStreamAggJobManage {
public:
    TsStreamAggJobManage() {}
    ~TsStreamAggJobManage() {}

public: 
    std::vector<TsJobInfo> GetAllValidJob(Connection *connection);
    int32_t StartJob(Connection *connection, std::string job_name, uint32_t offset_start, uint32_t offset_end);
    int32_t PauseJob(Connection *connection, std::string job_name);
    int32_t UpdateTsJobStat(Connection *connection, TsJobStat ts_job_stat);
    int32_t UpdateTsJobErr(Connection *connection, TsJobErr ts_job_err);
    int32_t CreateTsjob(Connection *connection, TsJobInfo ts_job_info, std::string des_sql);
    int32_t GetJobId(Connection *connection, std::string job_name, uint32_t& job_id);
    int32_t DeletetTsJobInfo(Connection *connection, uint32_t job_id);
    std::vector<std::string> GetAllJobName(Connection *connection);

    int32_t CountTsJob(Connection *connection);
    int32_t GetJob(Connection *connection, std::string job_name, TsJobInfo& result);
    int32_t SelectTsJobStat(Connection *connection, uint32_t job_id, RecordBatch& result);
    int32_t SelectTsJobErr(Connection *connection, uint32_t job_id, RecordBatch& result);
};