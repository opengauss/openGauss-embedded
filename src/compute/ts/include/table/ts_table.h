/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * ts_table.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/include/table/ts_table.h
 *
 * -------------------------------------------------------------------------
 */

#pragma once
#include "compute/ts/include/stream_agg/ts_stream_agg_job_manage.h"
#include "compute/sql/include/main/connection.h"

class TsTable {
public:
    TsTable() {}
    ~TsTable() {}

public: 
    // TsJob
    int32_t Insert(Connection *connection, const TsJob& ts_job);
    int32_t Delete(Connection *connection, const TsJob& condition);
    int32_t Update(Connection *connection, const TsJob& from, const TsJob& to);
    int32_t Select(Connection *connection, uint32_t job_id, TsJob& result);
    int32_t Select(Connection *connection, const TsJob& condition, std::vector<TsJob>& results);
    int32_t Select(Connection *connection, std::vector<TsJob>& results);
    // TsJobStat
    int32_t Insert(Connection *connection, const TsJobStat& ts_job_stat);
    int32_t Delete(Connection *connection, const TsJobStat&  condition);
    int32_t Update(Connection *connection, const TsJobStat&  from, const TsJobStat&  to);
    int32_t Select(Connection *connection, uint32_t job_id, TsJobStat& result);
    int32_t Select(Connection *connection, const TsJobStat&  condition, std::vector<TsJobStat>& results);
    int32_t Select(Connection *connection, const std::vector<uint32_t>& job_ids, std::vector<TsJobStat>& results);
    // TsJobErr
    int32_t Insert(Connection *connection, const TsJobErr& ts_job_err);
    int32_t Delete(Connection *connection, const TsJobErr& condition);
    int32_t Update(Connection *connection, const TsJobErr& from, const TsJobErr& to);
    int32_t Select(Connection *connection, uint32_t job_id, TsJobErr& result);
    int32_t Select(Connection *connection, const TsJobErr& condition, std::vector<TsJobErr>& results);
    int32_t Select(Connection *connection, const std::vector<uint32_t>& job_ids, std::vector<TsJobErr>& results);
    // TsCaggPolicy
    int32_t Insert(Connection *connection, const TsCaggPolicy& ts_cagg_policy);
    int32_t Delete(Connection *connection, const TsCaggPolicy& condition);
    int32_t Update(Connection *connection, const TsCaggPolicy& from, const TsCaggPolicy& to);
    int32_t Select(Connection *connection, uint32_t job_id, TsCaggPolicy& result);
    int32_t Select(Connection *connection, const TsCaggPolicy& condition, std::vector<TsCaggPolicy>& results);
    int32_t Select(Connection *connection, const std::vector<uint32_t>& job_ids, std::vector<TsCaggPolicy>& results);

private:
    void BuildFieldAndValueSql(const std::vector<std::string> fields, const std::vector<std::string> values, std::string& field_sql, std::string& value_sql);
    void BuildWhereSql(const std::vector<std::string> fields, const std::vector<std::string> values, std::string& where);
    void BuildSetSql(const std::vector<std::string> fields, const std::vector<std::string> values, std::string& set);
    void BuildJobIdSql(const std::vector<std::uint32_t> job_ids, std::string& job_ids_sql);
    // TsJob
    void GetFieldAndValues(const TsJob& ts_job, std::vector<std::string>& fields, std::vector<std::string>& values);
    void DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsJob>& results);
    // TsJobStat
    void GetFieldAndValues(const TsJobStat& ts_job_stat, std::vector<std::string>& fields, std::vector<std::string>& values);
    void DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsJobStat>& results);
    // TsJobErr
    void GetFieldAndValues(const TsJobErr& ts_job_err, std::vector<std::string>& fields, std::vector<std::string>& values);
    void DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsJobErr>& results);
    // TsCaggPolicy
    void GetFieldAndValues(const TsCaggPolicy& ts_cagg_policy, std::vector<std::string>& fields, std::vector<std::string>& values);
    void DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsCaggPolicy>& results);
};

/* COLUMN ID in ts_job */
#define TS_JOB_COLUMN_ID_JOB_ID             0
#define TS_JOB_COLUMN_ID_NAME               1
#define TS_JOB_COLUMN_ID_STATE              2
#define TS_JOB_COLUMN_ID_SCHEDULE_TIME      3
#define TS_JOB_COLUMN_ID_SCHEDULE_INTERVAL  4
#define TS_JOB_COLUMN_ID_MAX_RUNTIME        5
#define TS_JOB_COLUMN_ID_FAILED_RETRY_NUM   6
#define TS_JOB_COLUMN_ID_RETRY_PERIOD       7
#define TS_JOB_COLUMN_ID_INITIAL_START      8
#define TS_JOB_COLUMN_ID_PARTITON_TYPE      9
#define TS_JOB_COLUMN_ID_OFFSET_START       10
#define TS_JOB_COLUMN_ID_OFFSET_END         11

/* COLUMN ID in ts_job_stat */
#define TS_JOB_STAT_COLUMN_ID_JOB_ID                0
#define TS_JOB_STAT_COLUMN_ID_LAST_START            1
#define TS_JOB_STAT_COLUMN_ID_LAST_FINISH           2
#define TS_JOB_STAT_COLUMN_ID_NEXT_START            3
#define TS_JOB_STAT_COLUMN_ID_LAST_SUCCUSS_FINISH   4
#define TS_JOB_STAT_COLUMN_ID_TOTAL_RUNS            5
#define TS_JOB_STAT_COLUMN_ID_TOTAL_FAILED          6
#define TS_JOB_STAT_COLUMN_ID_CONSECUTIVE_FAILURES  7

/* COLUMN ID in ts_job_err */
#define TS_JOB_ERR_COLUMN_ID_JOB_ID         1
#define TS_JOB_ERR_COLUMN_ID_PID            2
#define TS_JOB_ERR_COLUMN_ID_START_TIME     3
#define TS_JOB_ERR_COLUMN_ID_FINISH_TIME    4
#define TS_JOB_ERR_COLUMN_ID_ERR_DATA       5

/* COLUMN ID in ts_cagg_policy */
#define TS_CAGG_POLICY_COLUMN_ID_JOB_ID             0
#define TS_CAGG_POLICY_COLUMN_ID_DB_NAME            1
#define TS_CAGG_POLICY_COLUMN_ID_TABLE_NAME         2
#define TS_CAGG_POLICY_COLUMN_ID_EXEC_SQL           3
#define TS_CAGG_POLICY_COLUMN_ID_BUCKET_TYPE        4
#define TS_CAGG_POLICY_COLUMN_ID_BUCKET_FIELD_NAME  5
#define TS_CAGG_POLICY_COLUMN_ID_BUCKET_WIDTH       6
