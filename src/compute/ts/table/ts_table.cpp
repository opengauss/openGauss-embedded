/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * ts_table.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/table/ts_table.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "compute/ts/include/table/ts_table.h"

void TsTable::BuildFieldAndValueSql(const std::vector<std::string> fields, const std::vector<std::string> values, std::string& field_sql, std::string& value_sql) {
    for (auto field = fields.begin(), value = values.begin(); field != fields.end(); ++field, ++value) {
        if (std::next(field) == fields.end()) {
            field_sql += *field;
            value_sql += *value;
        } else {
            field_sql += (*field + ", ");
            value_sql += (*value + ", ");
        }
    }
}

void TsTable::BuildWhereSql(const std::vector<std::string> fields, const std::vector<std::string> values, std::string& where) {
    for (auto field = fields.begin(), value = values.begin(); field != fields.end(); ++field, ++value) {
        if (std::next(field) == fields.end()) {
            where += (*field + " = " + *value);
        } else {
            where += (*field + " = " + *value + " and ");
        }
    }
}

void TsTable::BuildSetSql(const std::vector<std::string> fields, const std::vector<std::string> values, std::string& set) {
    for (auto field = fields.begin(), value = values.begin(); field != fields.end(); ++field, ++value) {
        if (std::next(field) == fields.end()) {
            set += (*field + " = " + *value);
        } else {
            set += (*field + " = " + *value + ",");
        }
    }
}

void TsTable::BuildJobIdSql(const std::vector<std::uint32_t> job_ids, std::string& job_ids_sql) {
    for (auto job_id = job_ids.begin(); job_id != job_ids.end(); ++job_id) {
        if (std::next(job_id) == job_ids.end()) {
            job_ids_sql += std::to_string(*job_id);
        } else {
            job_ids_sql += (std::to_string(*job_id) + ", ");
        }
    }
}

 // TsJob
int32_t TsTable::Insert(Connection *connection, const TsJob& ts_job) {
    std::vector<std::string> fields, values;
    std::string sql, field_sql, value_sql;

    GetFieldAndValues(ts_job, fields, values);
    fields.push_back("create_time");
    fields.push_back("update_time");
    values.push_back("now()");
    values.push_back("now()");

    BuildFieldAndValueSql(fields, values, field_sql, value_sql);
    sql = "insert into ts_job (" + field_sql + ") " + "values(" + value_sql + ");";
    GS_LOG_DEBUG_INF("insert ts_job sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to insert ts_job tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to insert ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Delete(Connection *connection, const TsJob& condition) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "delete from ts_job where " + where + ";";
    GS_LOG_DEBUG_INF("delete ts_job sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to delete ts_job tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to delete ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Update(Connection *connection, const TsJob& from, const TsJob& to) {
    std::vector<std::string> to_fields, to_values, from_fields, from_values;
    std::string sql, set, where;

    GetFieldAndValues(to, to_fields, to_values);
    to_fields.push_back("update_time");
    to_values.push_back("now()");

    BuildSetSql(to_fields, to_values, set);

    GetFieldAndValues(from, from_fields, from_values);

    BuildWhereSql(from_fields, from_values, where);

    sql = "update ts_job set " + set + " where " + where + ";";
    GS_LOG_DEBUG_INF("update ts_job sql=%s", sql.c_str()); 
    
    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to update ts_job tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to update ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Select(Connection *connection, uint32_t job_id, TsJob& result) {
    std::string sql;
    std::vector<TsJob> results;

    sql = "select * from ts_job where job_id = " + std::to_string(job_id) + ";";
    GS_LOG_DEBUG_INF("select ts_job sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);
    if (results.size() != 1) {
        GS_LOG_RUN_ERR("The amount of ts_job obtained was incorrect!");
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    result = results[0];

    return status;
}

int32_t TsTable::Select(Connection *connection, const TsJob& condition, std::vector<TsJob>& results) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "select * from ts_job where " + where + " order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_job sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

int32_t TsTable::Select(Connection *connection, std::vector<TsJob>& results) {
    std::string sql;
    sql = "select * from ts_job order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_job sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

void TsTable::GetFieldAndValues(const TsJob& ts_job, std::vector<std::string>& fields, std::vector<std::string>& values) {
    if (ts_job.job_id != uint32_null) {
        fields.push_back("job_id");
        values.push_back(std::to_string(ts_job.job_id));
    }

    if (ts_job.name != string_null) {
        fields.push_back("job_name");
        values.push_back("'" + ts_job.name + "'");
    }

    if (ts_job.state != job_status_null) {
        fields.push_back("state");
        values.push_back(std::to_string(ts_job.state));
    }

    if (ts_job.schedule_time != uint32_null) {
        fields.push_back("schedule_time");
        values.push_back(std::to_string(ts_job.schedule_time));
    }

    if (ts_job.schedule_interval != uint32_null) {
        fields.push_back("schedule_interval");
        values.push_back(std::to_string(ts_job.schedule_interval));
    }

    if (ts_job.max_runtime != uint32_null) {
        fields.push_back("max_runtime");
        values.push_back(std::to_string(ts_job.max_runtime));
    }

    if (ts_job.failed_retry_num != uint32_null) {
        fields.push_back("failed_retry_num");
        values.push_back(std::to_string(ts_job.failed_retry_num));
    }

    if (ts_job.retry_period != uint32_null) {
        fields.push_back("retry_period");
        values.push_back(std::to_string(ts_job.retry_period));
    }

    if (ts_job.initial_start != timestamp_stor_null) {
        fields.push_back("initial_start");
        values.push_back(std::to_string(ts_job.initial_start.ts));
    }

    if (ts_job.partiton_type != uint32_null) {
        fields.push_back("partiton_type");
        values.push_back(std::to_string(ts_job.partiton_type));
    }

    if (ts_job.offset_start != uint32_null) {
        fields.push_back("offset_start");
        values.push_back(std::to_string(ts_job.offset_start));
    }

    if (ts_job.offset_end != uint32_null) {
        fields.push_back("offset_end");
        values.push_back(std::to_string(ts_job.offset_end));
    }
}

void TsTable::DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsJob>& results) {
    size_t row_count = rb->RowCount();

    for (int i = 0; i < row_count; i++) {
        TsJob result;
        result.job_id = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_JOB_ID).GetCastAs<uint32_t>();
        result.name = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_NAME).GetCastAs<std::string>();
        result.state = static_cast<JobState>(rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_STATE).GetCastAs<uint32_t>());
        result.schedule_time = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_SCHEDULE_TIME).GetCastAs<uint32_t>();
        result.schedule_interval = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_SCHEDULE_INTERVAL).GetCastAs<uint32_t>();
        result.max_runtime = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_MAX_RUNTIME).GetCastAs<uint32_t>();
        result.failed_retry_num = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_FAILED_RETRY_NUM).GetCastAs<uint32_t>();
        result.retry_period = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_RETRY_PERIOD).GetCastAs<uint32_t>();
        result.initial_start = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_INITIAL_START).GetCastAs<timestamp_stor_t>();
        result.partiton_type = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_PARTITON_TYPE).GetCastAs<uint32_t>();
        result.offset_start = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_OFFSET_START).GetCastAs<uint32_t>();
        result.offset_end = rb->Row(i).FieldRef(TS_JOB_COLUMN_ID_OFFSET_END).GetCastAs<uint32_t>();

        results.push_back(result);
    }
}

// TsJobStat
int32_t TsTable::Insert(Connection *connection, const TsJobStat& ts_job_stat) {
    std::vector<std::string> fields, values;
    std::string sql, field_sql, value_sql;

    GetFieldAndValues(ts_job_stat, fields, values);
    fields.push_back("create_time");
    fields.push_back("update_time");
    values.push_back("now()");
    values.push_back("now()");

    BuildFieldAndValueSql(fields, values, field_sql, value_sql);
    sql = "insert into ts_job_stat (" + field_sql + ") " + "values(" + value_sql + ");";
    GS_LOG_DEBUG_INF("insert ts_job_stat sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to insert ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to insert ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Delete(Connection *connection, const TsJobStat& condition) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "delete from ts_job_stat where " + where + ";";
    GS_LOG_DEBUG_INF("delete ts_job_stat sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to delete ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to delete ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Update(Connection *connection, const TsJobStat& from, const TsJobStat& to) {
    std::vector<std::string> to_fields, to_values, from_fields, from_values;
    std::string sql, set, where;

    GetFieldAndValues(to, to_fields, to_values);
    to_fields.push_back("update_time");
    to_values.push_back("now()");

    BuildSetSql(to_fields, to_values, set);

    GetFieldAndValues(from, from_fields, from_values);

    BuildWhereSql(from_fields, from_values, where);

    sql = "update ts_job_stat set " + set + " where " + where + ";";
    GS_LOG_DEBUG_INF("update ts_job_stat sql=%s", sql.c_str()); 
    
    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to update ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to update ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Select(Connection *connection, uint32_t job_id, TsJobStat& result) {
    std::string sql;
    std::vector<TsJobStat> results;

    sql = "select * from ts_job_stat where job_id = " + std::to_string(job_id) + ";";
    GS_LOG_DEBUG_INF("select ts_job_stat sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);
    if (results.size() != 1) {
        GS_LOG_RUN_ERR("The amount of ts_job_stat obtained was incorrect!");
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    result = results[0];

    return status;
}

int32_t TsTable::Select(Connection *connection, const TsJobStat& condition, std::vector<TsJobStat>& results) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "select * from ts_job_stat where " + where + " order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_job_stat sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

int32_t TsTable::Select(Connection *connection, const std::vector<uint32_t>& job_ids, std::vector<TsJobStat>& results) {
    std::string sql, job_ids_sql;

    if (job_ids.size() == 0) {
        return GS_SUCCESS;
    }
    
    BuildJobIdSql(job_ids, job_ids_sql);
    sql = "select * from ts_job_stat where job_id in (" + job_ids_sql + ") order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_job_stat sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

void TsTable::GetFieldAndValues(const TsJobStat& ts_job_stat, std::vector<std::string>& fields, std::vector<std::string>& values) {
    if (ts_job_stat.job_id != uint32_null) {
        fields.push_back("job_id");
        values.push_back(std::to_string(ts_job_stat.job_id));
    }

    if (ts_job_stat.last_start != timestamp_stor_null) {
        fields.push_back("last_start");
        values.push_back(std::to_string(ts_job_stat.last_start.ts));
    }

    if (ts_job_stat.last_finish != timestamp_stor_null) {
        fields.push_back("last_finish");
        values.push_back(std::to_string(ts_job_stat.last_finish.ts));
    }

    if (ts_job_stat.next_start != timestamp_stor_null) {
        fields.push_back("next_start");
        values.push_back(std::to_string(ts_job_stat.next_start.ts));
    }

    if (ts_job_stat.last_succuss_finish != timestamp_stor_null) {
        fields.push_back("last_succuss_finish");
        values.push_back(std::to_string(ts_job_stat.last_succuss_finish.ts));
    }

    if (ts_job_stat.total_runs != uint64_null) {
        fields.push_back("total_runs");
        values.push_back(std::to_string(ts_job_stat.total_runs));
    }

    if (ts_job_stat.total_failed != uint64_null) {
        fields.push_back("total_failed");
        values.push_back(std::to_string(ts_job_stat.total_failed));
    }

    if (ts_job_stat.consecutive_failures != uint32_null) {
        fields.push_back("consecutive_failures");
        values.push_back(std::to_string(ts_job_stat.consecutive_failures));
    }
}

void TsTable::DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsJobStat>& results) {
    size_t row_count = rb->RowCount();

    for (int i = 0; i < row_count; i++) {
        TsJobStat result;
        result.job_id = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_JOB_ID).GetCastAs<uint32_t>();
        result.last_start = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_LAST_START).GetCastAs<timestamp_stor_t>();
        result.last_finish = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_LAST_FINISH).GetCastAs<timestamp_stor_t>();
        result.next_start = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_NEXT_START).GetCastAs<timestamp_stor_t>();
        result.last_succuss_finish = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_LAST_SUCCUSS_FINISH).GetCastAs<timestamp_stor_t>();
        result.total_runs = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_TOTAL_RUNS).GetCastAs<uint64_t>();
        result.total_failed = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_TOTAL_FAILED).GetCastAs<uint64_t>();
        result.consecutive_failures = rb->Row(i).FieldRef(TS_JOB_STAT_COLUMN_ID_CONSECUTIVE_FAILURES).GetCastAs<uint32_t>();

        results.push_back(result);
    }
}

// TsJobErr
int32_t TsTable::Insert(Connection *connection, const TsJobErr& ts_job_err) {
    std::vector<std::string> fields, values;
    std::string sql, field_sql, value_sql;

    GetFieldAndValues(ts_job_err, fields, values);
    fields.push_back("create_time");
    fields.push_back("update_time");
    values.push_back("now()");
    values.push_back("now()");

    BuildFieldAndValueSql(fields, values, field_sql, value_sql);
    sql = "insert into ts_job_err (" + field_sql + ") " + "values(" + value_sql + ");";
    GS_LOG_DEBUG_INF("insert ts_job_err sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to insert ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to insert ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Delete(Connection *connection, const TsJobErr& condition) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "delete from ts_job_err where " + where + ";";
    GS_LOG_DEBUG_INF("delete ts_job_err sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to delete ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to delete ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Update(Connection *connection, const TsJobErr& from, const TsJobErr& to) {
    std::vector<std::string> to_fields, to_values, from_fields, from_values;
    std::string sql, set, where;

    GetFieldAndValues(to, to_fields, to_values);
    to_fields.push_back("update_time");
    to_values.push_back("now()");

    BuildSetSql(to_fields, to_values, set);

    GetFieldAndValues(from, from_fields, from_values);

    BuildWhereSql(from_fields, from_values, where);

    sql = "update ts_job_err set " + set + " where " + where + ";";
    GS_LOG_DEBUG_INF("update ts_job_err sql=%s", sql.c_str()); 
    
    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to update ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to update ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Select(Connection *connection, uint32_t job_id, TsJobErr& result) {
    std::string sql;
    std::vector<TsJobErr> results;

    sql = "select * from ts_job_err where job_id = " + std::to_string(job_id) + ";";
    GS_LOG_DEBUG_INF("select ts_job_err sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);
    if (results.size() != 1) {
        GS_LOG_RUN_ERR("The amount of ts_job_err obtained was incorrect!");
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    result = results[0];

    return status;
}

int32_t TsTable::Select(Connection *connection, const TsJobErr& condition, std::vector<TsJobErr>& results) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "select * from ts_job_err where " + where + " order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_job_err sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

int32_t TsTable::Select(Connection *connection, const std::vector<uint32_t>& job_ids, std::vector<TsJobErr>& results) {
    std::string sql, job_ids_sql;

    if (job_ids.size() == 0) {
        return GS_SUCCESS;
    }
    
    BuildJobIdSql(job_ids, job_ids_sql);
    sql = "select * from ts_job_err where job_id in (" + job_ids_sql + ") order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_job_err sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

void TsTable::GetFieldAndValues(const TsJobErr& ts_job_err, std::vector<std::string>& fields, std::vector<std::string>& values) {
    if (ts_job_err.job_id != uint32_null) {
        fields.push_back("job_id");
        values.push_back(std::to_string(ts_job_err.job_id));
    }

    if (ts_job_err.pid != uint64_null) {
        fields.push_back("pid");
        values.push_back(std::to_string(ts_job_err.pid));
    }

    if (ts_job_err.start_time != string_null) {
        fields.push_back("start_time");
        values.push_back("'" + ts_job_err.start_time + "'");
    }

    if (ts_job_err.finish_time != string_null) {
        fields.push_back("finish_time");
        values.push_back("'" + ts_job_err.finish_time + "'");
    }

    if (ts_job_err.err_data != string_null) {
        fields.push_back("err_data");

        size_t pos = 0;
        bool replaced = false;
        std::string str = ts_job_err.err_data;
        while ((pos = str.find("'", pos)) != std::string::npos) {
            str.replace(pos, 1, "\"");
            pos += 1;
            replaced = true;
        }

        if (replaced) {
            values.push_back("'" + str + "'");
        } else {
            values.push_back("'" + ts_job_err.err_data + "'");
        }
    }
}

void TsTable::DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsJobErr>& results) {
    size_t row_count = rb->RowCount();

    for (int i = 0; i < row_count; i++) {
        TsJobErr result;
        result.job_id = rb->Row(i).FieldRef(TS_JOB_ERR_COLUMN_ID_JOB_ID).GetCastAs<uint32_t>();
        result.pid = rb->Row(i).FieldRef(TS_JOB_ERR_COLUMN_ID_PID).GetCastAs<uint64_t>();
        result.start_time = rb->Row(i).FieldRef(TS_JOB_ERR_COLUMN_ID_START_TIME).GetCastAs<std::string>();
        result.finish_time = rb->Row(i).FieldRef(TS_JOB_ERR_COLUMN_ID_FINISH_TIME).GetCastAs<std::string>();
        result.err_data = rb->Row(i).FieldRef(TS_JOB_ERR_COLUMN_ID_ERR_DATA).GetCastAs<std::string>();

        results.push_back(result);
    }
}

// TsCaggPolicy
int32_t TsTable::Insert(Connection *connection, const TsCaggPolicy& ts_cagg_policy) {
    std::vector<std::string> fields, values;
    std::string sql, field_sql, value_sql;

    GetFieldAndValues(ts_cagg_policy, fields, values);
    fields.push_back("create_time");
    fields.push_back("update_time");
    values.push_back("now()");
    values.push_back("now()");

    BuildFieldAndValueSql(fields, values, field_sql, value_sql);
    sql = "insert into ts_cagg_policy (" + field_sql + ") " + "values(" + value_sql + ");";
    GS_LOG_DEBUG_INF("insert ts_cagg_policy sql=%s", sql.c_str());
    
    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to insert ts_cagg_policy tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to insert ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Delete(Connection *connection, const TsCaggPolicy& condition) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "delete from ts_cagg_policy where " + where + ";";
    GS_LOG_DEBUG_INF("delete ts_cagg_policy sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to delete ts_cagg_policy tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to delete ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Update(Connection *connection, const TsCaggPolicy& from, const TsCaggPolicy& to) {
    std::string sql;
    
    std::vector<std::string> to_fields, to_values, from_fields, from_values;
    std::string set, where;

    GetFieldAndValues(to, to_fields, to_values);
    to_fields.push_back("update_time");
    to_values.push_back("now()");

    BuildSetSql(to_fields, to_values, set);

    GetFieldAndValues(from, from_fields, from_values);

    BuildWhereSql(from_fields, from_values, where);

    sql = "update ts_cagg_policy set " + set + " where " + where + ";";
    GS_LOG_DEBUG_INF("update ts_cagg_policy sql=%s", sql.c_str()); 
    
    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to update ts_cagg_policy tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to update ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    return status;
}

int32_t TsTable::Select(Connection *connection, uint32_t job_id, TsCaggPolicy& result) {
    std::string sql;
    std::vector<TsCaggPolicy> results;

    sql = "select * from ts_cagg_policy where job_id = " + std::to_string(job_id) + ";";
    GS_LOG_DEBUG_INF("select ts_cagg_policy sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);
    if (results.size() != 1) {
        GS_LOG_RUN_ERR("The amount of ts_cagg_policy obtained was incorrect!");
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    result = results[0];

    return status;
}

int32_t TsTable::Select(Connection *connection, const TsCaggPolicy& condition, std::vector<TsCaggPolicy>& results) {
    std::string sql, where;
    std::vector<std::string> fields, values;

    GetFieldAndValues(condition, fields, values);

    BuildWhereSql(fields, values, where);
    
    sql = "select * from ts_cagg_policy where " + where + " order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_cagg_policy sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

int32_t TsTable::Select(Connection *connection, const std::vector<uint32_t>& job_ids, std::vector<TsCaggPolicy>& results) {
    std::string sql, job_ids_sql;

    if (job_ids.size() == 0) {
        return GS_SUCCESS;
    }
    
    BuildJobIdSql(job_ids, job_ids_sql);
    sql = "select * from ts_cagg_policy where job_id in (" + job_ids_sql + ") order by job_id asc;";
    GS_LOG_DEBUG_INF("select ts_cagg_policy sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_ERR("Failed to select ts_cagg_policy tables, ignoring...(sql=%s)", sql.c_str());
        return GS_ERROR;
    }

    DataMapping(std::move(rb), results);

    return status;
}

void TsTable::GetFieldAndValues(const TsCaggPolicy& ts_cagg_policy, std::vector<std::string>& fields, std::vector<std::string>& values) {
    if (ts_cagg_policy.job_id != uint32_null) {
        fields.push_back("job_id");
        values.push_back(std::to_string(ts_cagg_policy.job_id));
    }

    if (ts_cagg_policy.db_name != string_null) {
        fields.push_back("db_name");
        values.push_back("'" + ts_cagg_policy.db_name + "'");
    }

    if (ts_cagg_policy.table_name != string_null) {
        fields.push_back("table_name");
        values.push_back("'" + ts_cagg_policy.table_name + "'");
    }

    if (ts_cagg_policy.exec_sql != string_null) {
        fields.push_back("exec_sql");
        values.push_back("'" + ts_cagg_policy.exec_sql + "'");
    }

    if (ts_cagg_policy.bucket_type != bucket_type_null) {
        fields.push_back("bucket_type");
        values.push_back(std::to_string(ts_cagg_policy.bucket_type));
    }

    if (ts_cagg_policy.bucket_field_name != string_null) {
        fields.push_back("bucket_field_name");
        values.push_back("'" + ts_cagg_policy.bucket_field_name + "'");
    }

    if (ts_cagg_policy.bucket_width != uint64_null) {
        fields.push_back("bucket_width");
        values.push_back(std::to_string(ts_cagg_policy.bucket_width));
    }
}

void TsTable::DataMapping(std::unique_ptr<RecordBatch> rb, std::vector<TsCaggPolicy>& results) {
    size_t row_count = rb->RowCount();

    for (int i = 0; i < row_count; i++) {
        TsCaggPolicy result;
        result.job_id = rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_JOB_ID).GetCastAs<uint32_t>();
        result.db_name = rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_DB_NAME).GetCastAs<std::string>();
        result.table_name = rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_TABLE_NAME).GetCastAs<std::string>();
        result.exec_sql = rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_EXEC_SQL).GetCastAs<std::string>();
        result.bucket_type = static_cast<BucketType>(rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_BUCKET_TYPE).GetCastAs<uint32_t>());
        result.bucket_field_name = rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_BUCKET_FIELD_NAME).GetCastAs<std::string>();
        result.bucket_width = rb->Row(i).FieldRef(TS_CAGG_POLICY_COLUMN_ID_BUCKET_WIDTH).GetCastAs<uint64_t>();

        results.push_back(result);
    }
}