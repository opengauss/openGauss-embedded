# 流计算任务新增、暂停、删除、手动执行

## 流计算任务新增

`CALL TS_ADD_CAGG_POLICY('policy_name', 'des_table_name', schedule_time, 'agg_sql', refreash_interval, start_offset, end_offset, 'bucket_field','bucket_width')`

参数说明  
- policy_name       策略名, 全局唯一
- des_table_name    数据保存表
- schedule_time     任务调度是否周期执行, 0表示不周期执行, 1表示周期执行
- agg_sql           数据查询语句
- refreash_interval 任务执行周期
- start_offset      查询开始偏移时间
- end_offset        查询结束偏移时间
- bucket_field      目标表分桶字段名
- bucket_width      分桶宽度, h/d, 如: '1d' '1h', ''表示不使用时间分桶

eg: `CALL TS_ADD_CAGG_POLICY('ts_job', 'des_table', 10, 'select sum(a) as sum_a, sum(b) as sum_b from test where date >= __START_TIME__ and date <= __END_TIME__', 1, 0, 0, 'date_time', '');`

## 流计算任务暂停

`CALL TS_PAUSE_CAGG_POLICY('policy_name')`

参数说明  
- policy_name       策略名

eg: `CALL TS_PAUSE_CAGG_POLICY('ts_job');`

## 流计算任务删除

`CALL TS_DEL_CAGG_POLICY('policy_name')`

参数说明  
- policy_name       策略名

eg: `CALL TS_PAUSE_CAGG_POLICY('ts_job');`

## 流计算任务手动执行

`CALL TS_REFRESH_CAGG_POLICY('policy_name', start_offset, end_offset)`

参数说明  
- policy_name       策略名
- start_offset      查询开始偏移时间
- end_offset        查询结束偏移时间

eg: `CALL TS_REFRESH_CAGG_POLICY('ts_job', 0, 0);`

