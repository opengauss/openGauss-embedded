/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * scheduler.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/stream_agg/scheduler.cpp
 *
 * -------------------------------------------------------------------------
 */
#include <algorithm>
#include <stdexcept>

#include "scheduler.h"
#include "storage/db_handle.h"
#include "gstor_executor.h"
#include "main/connection.h"

TsScheduler::TsScheduler(std::shared_ptr<IntarkDB> db, uint32_t thread_pool_num) 
        : db_(db), thread_pool_(thread_pool_num) {

}

TsScheduler::~TsScheduler() {

}

void TsScheduler::TsStreamAggSchedulerProc() {
    GS_LOG_RUN_INF("Start to Stream Agg.");
    while(1){
        // 获取任务列表 TsScheduledJob
        TsGetScheduledJobList();
        // 执行
        TsStartScheduledJobs();
        // 等待
        TsTimeWait();

        TsCheckForTimedOutScheduledJobs();
    }
    TsCheckForTimedOutScheduledJobs();
}

static timestamp_stor_t least_date(timestamp_stor_t left, timestamp_stor_t right)
{
	return (left < right ? left : right);
}

void TsScheduler::TsTimeWait() {
    timestamp_stor_t next_wakeup;
    next_wakeup.ts = NumericLimits<int64_t>::Maximum();
    next_wakeup = least_date(TsEarliestWakeupToStartNextJob(), next_wakeup);
    next_wakeup = least_date(TsEarliestJobTimeout(), next_wakeup);
    
    int64_t interval = next_wakeup==NumericLimits<int64_t>::Maximum() ? DEFAULT_SCHEDULE_INTERVAL*MICROSECS_PER_SECOND : (next_wakeup-TimestampNow()).ts;
    interval = std::min(interval, MAX_WAITTIME_SINGLE_TIME*MICROSECS_PER_SECOND);
    GS_LOG_DEBUG_INF("[StreamAgg] TsTimeWait %ld us.", interval);
    if (interval > 0)
        std::this_thread::sleep_for(std::chrono::microseconds(interval));
}

timestamp_stor_t TsScheduler::TsEarliestWakeupToStartNextJob() {
    timestamp_stor_t earliest;
    earliest.ts = NumericLimits<int64_t>::Maximum();
    if (scheduled_job_map_.empty()){
        GS_LOG_DEBUG_INF("[StreamAgg]Has no scheduled job now!");
        return earliest;
    }

    for (auto& mjob : scheduled_job_map_){
        auto &sjob = mjob.second;
        if (sjob.state == SCHEDULED_JOB_STATE_SCHEDULED) {
            earliest = least_date(sjob.job.ts_job_stat.next_start, earliest);
        }
    }
    return earliest;
}

timestamp_stor_t TsScheduler::TsEarliestJobTimeout() {
    timestamp_stor_t earliest_timeout;
    earliest_timeout.ts = NumericLimits<int64_t>::Maximum();
    if (scheduled_job_map_.empty()){
        GS_LOG_DEBUG_INF("[StreamAgg]Has no scheduled job now!");
        return earliest_timeout;
    }

    for (auto& mjob : scheduled_job_map_){
        auto &sjob = mjob.second;
        if (sjob.state == SCHEDULED_JOB_STATE_STARTED) {
            earliest_timeout = least_date(sjob.timeout_at, earliest_timeout);
        }
    }
    return earliest_timeout;
}

void TsScheduler::TsGetScheduledJobList() {
    // step 1: 获取任务列表
    valid_joblist_.clear();

    Connection conn(db_);
    conn.Init();
    TsStreamAggJobManage job_mng;
    valid_joblist_ = job_mng.GetAllValidJob(&conn);

    std::unordered_set<uint32_t> valid_job_set;
    // step 2: 遍历任务列表，将符合条件的任务添加到 scheduled_job_list_ /  中, 即ts_job表中的state为 1-有效的任务 
    for (auto &vjob : valid_joblist_) {
        if (vjob.ts_job.state == JOB_STATE_VALID) {
            valid_job_set.insert(vjob.ts_job.job_id);

            if (scheduled_job_map_.find(vjob.ts_job.job_id) == scheduled_job_map_.end()){
                auto sjob = TsScheduledJob(vjob);
                scheduled_job_map_[vjob.ts_job.job_id] = sjob;
            } else {
                scheduled_job_map_[vjob.ts_job.job_id].job = vjob;
            }
            
        }
    }
    // 清理被删除或被暂停的任务
    std::vector<uint32_t> wait_deleted_vec;
    for (auto &mjob : scheduled_job_map_) {
        auto &sjob = mjob.second;
        if (valid_job_set.find(sjob.job.ts_job.job_id) == valid_job_set.end()) {
            if (sjob.state == SCHEDULED_JOB_STATE_STARTED) {
                sjob.state = SCHEDULED_JOB_STATE_DISABLED;
            } else if (sjob.state == SCHEDULED_JOB_STATE_SCHEDULED || sjob.state == SCHEDULED_JOB_STATE_DISABLED) {
                wait_deleted_vec.push_back(sjob.job.ts_job.job_id);
            }
        }
    }
    for (auto &job_id : wait_deleted_vec) {
        scheduled_job_map_.erase(job_id);
    }
}

void TsScheduler::TsScheduledJobTransitionStateTo(TsScheduledJob *sjob, ScheduledJobState new_state) {
    ScheduledJobState prev_state = sjob->state;
	switch (new_state)
	{
		case SCHEDULED_JOB_STATE_DISABLED:
			if (prev_state != SCHEDULED_JOB_STATE_EXEC_TIMEOUT && prev_state != SCHEDULED_JOB_STATE_STARTED && prev_state != SCHEDULED_JOB_STATE_TERMINATING) {
                GS_LOG_RUN_ERR("[StreamAgg] job %d-%s state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from %d to %d",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), prev_state, new_state);
                throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from {} to {}",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name, static_cast<uint32_t>(prev_state), static_cast<uint32_t>(new_state)));
            }
            GS_LOG_DEBUG_INF("[StreamAgg] scheduledjob(id:%d) state to SCHEDULED_JOB_STATE_DISABLED!", sjob->job.ts_job.job_id);
			break;
		case SCHEDULED_JOB_STATE_SCHEDULED:
            GS_LOG_DEBUG_INF("[StreamAgg] scheduledjob(id:%d) state to SCHEDULED_JOB_STATE_SCHEDULED!", sjob->job.ts_job.job_id);
			break;
		case SCHEDULED_JOB_STATE_STARTED:
			if (prev_state != SCHEDULED_JOB_STATE_SCHEDULED) {
                GS_LOG_RUN_ERR("[StreamAgg] job %d-%s state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from %d to %d",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), prev_state, new_state);
                throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from {} to {}",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name, static_cast<uint32_t>(prev_state), static_cast<uint32_t>(new_state)));
            }

			if (sjob->job.ts_job.max_runtime > 0)
				sjob->timeout_at = sjob->job.ts_job_stat.next_start + sjob->job.ts_job.max_runtime*MICROSECS_PER_SECOND;
			else
				sjob->timeout_at.ts = 0;
            GS_LOG_DEBUG_INF("[StreamAgg] scheduledjob(id:%d) state to SCHEDULED_JOB_STATE_STARTED!", sjob->job.ts_job.job_id);
			break;
		case SCHEDULED_JOB_STATE_TERMINATING:
			if (prev_state != SCHEDULED_JOB_STATE_STARTED) {
                GS_LOG_RUN_ERR("[StreamAgg] job %d-%s state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from %d to %d",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), prev_state, new_state);
                throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from {} to {}",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name, static_cast<uint32_t>(prev_state), static_cast<uint32_t>(new_state)));
            }
            GS_LOG_DEBUG_INF("[StreamAgg] scheduledjob(id:%d) state to SCHEDULED_JOB_STATE_TERMINATING!", sjob->job.ts_job.job_id);
			break;
        case SCHEDULED_JOB_STATE_EXEC_TIMEOUT:
			if (prev_state != SCHEDULED_JOB_STATE_STARTED && prev_state != SCHEDULED_JOB_STATE_EXEC_TIMEOUT) {
                GS_LOG_RUN_ERR("[StreamAgg] job %d-%s state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from %d to %d",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), prev_state, new_state);
                throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} state(0-disabled, 1-scheduled, 2-started, 3-terminating, 4-timeout) transition error from {} to {}",
                                sjob->job.ts_job.job_id, sjob->job.ts_job.name, static_cast<uint32_t>(prev_state), static_cast<uint32_t>(new_state)));
            }
            GS_LOG_DEBUG_INF("[StreamAgg] scheduledjob(id:%d) state to SCHEDULED_JOB_STATE_EXEC_TIMEOUT!", sjob->job.ts_job.job_id);
			break;
	}

	sjob->state = new_state;
}

void TsScheduler::TsStartScheduledJobs() {
    if (scheduled_job_map_.empty()){
        GS_LOG_DEBUG_INF("[StartingSjobs]Has no scheduled job now!");
        return;
    }
    for (auto& mjob : scheduled_job_map_) {
        auto &sjob = mjob.second;
        int64_t job_start_diff = (sjob.job.ts_job_stat.next_start - TimestampNow()).ts;
        if (sjob.state == SCHEDULED_JOB_STATE_SCHEDULED && job_start_diff <= 0)
		{
			GS_LOG_DEBUG_INF("[StartingSjobs]starting scheduled job %d", sjob.job.ts_job.job_id);
			TsScheduledJobStart(&sjob);
            // 当前任务超时时间
            if (sjob.job.ts_job.max_runtime == INVALID_MAX_RUNTIME) {
                sjob.timeout_at = sjob.job.ts_job_stat.last_start + DEFAULT_MAX_RUNTIME*MICROSECS_PER_SECOND;
            }
            else {
                sjob.timeout_at = sjob.job.ts_job_stat.last_start + sjob.job.ts_job.max_runtime*MICROSECS_PER_SECOND;
            }
            GS_LOG_DEBUG_INF("[StartingSjobs]sjob %d started and will time out at %ld", sjob.job.ts_job.job_id, sjob.timeout_at.ts);
		}
		else
		{
			GS_LOG_DEBUG_INF("[StartingSjobs]starting scheduled job %d in %ld seconds",
				                         sjob.job.ts_job.job_id, job_start_diff);
		}
    }
}

static void ReplaceSubstring(std::string& str, const std::string& oldSubstr, const std::string& newSubstr, bool &replaced) {
    size_t pos = 0;
    replaced = false;
    while ((pos = str.find(oldSubstr, pos)) != std::string::npos) {
        str.replace(pos, oldSubstr.length(), newSubstr);
        pos += newSubstr.length();
        replaced = true;
    }
    GS_LOG_DEBUG_INF("after ReplaceSubstring: %s", str.c_str());
}

void TsScheduler::TsScheduledJobStartEndTimeSet(TsScheduledJob *sjob) {
    timestamp_stor_t now_time = TimestampNow();
    std::string start_date;
    std::string end_date;
    timestamp_stor_t end_time = now_time - sjob->job.ts_job.offset_end*MICROSECS_PER_SECOND;
    timestamp_stor_t start_time;

    if (sjob->job.ts_job_stat.last_succuss_finish == 0) {
        // 首次启动
        //  首次起始时间策略:  
        //      起始时间 = now(当前时间) - 开始时间偏移(start_offset)；开始时间偏移为空着0时，取最小时间;
        //  首次结束时间策略:  
        //      结束始时间 = now(当前时间) - 结束时间偏移(end_offset)  ；结束时间偏移为空着0时，取最大时
        if (sjob->job.ts_job.offset_start == 0){
            start_time.ts = 0;
        } else {
            start_time = now_time - sjob->job.ts_job.offset_start*MICROSECS_PER_SECOND;
        }
    } else {
        // 非首次启动时间策略:
        //  起始时间策略:  
        //      起始时间 = 上一次执行成功时间点
        //  结束时间策略:  
        //      结束时间 = now(当前时间) - 结束时间偏移(end_offset)  ；结束时间偏移为空着0时，取最大时间
        start_time = sjob->job.ts_job_stat.last_succuss_finish;
    }
    
    // 查询时间范围不能大于10个 refreash_interval， 大于后取最新的10个分区，旧分区填补由手动执行任务操作修复。
    if (end_time - start_time > MAX_QUERY_SINTERVAL_NUMS * sjob->job.ts_job.schedule_interval*MICROSECS_PER_SECOND) {
        start_time = end_time - MAX_QUERY_SINTERVAL_NUMS * sjob->job.ts_job.schedule_interval*MICROSECS_PER_SECOND;
    }

    if (start_time >= end_time) {
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s start_time >= end_time! please check job schedule_interval(%d) must be greater than 0!", 
            sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), sjob->job.ts_job.schedule_interval);
        throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} start time is greater than end time! please check job schedule_interval({}) must be greater than 0!", 
            sjob->job.ts_job.job_id, sjob->job.ts_job.name, 
            sjob->job.ts_job.schedule_interval));
    }
    
    if (!TryCast::Operation<timestamp_stor_t, std::string>(start_time, start_date)) {
        throw std::runtime_error("[StreamAgg] timestamp_stor_t convert to string failed!");
    }
    GS_LOG_DEBUG_INF("[StreamAgg] job:%d, start_time: %s\n", sjob->job.ts_job.job_id, start_date.c_str());
    sjob->dst_start_datetime = start_date;
    sjob->dst_start_timestamp = start_time;

    if (!TryCast::Operation<timestamp_stor_t, std::string>(end_time, end_date)) {
        throw std::runtime_error("[StreamAgg] end_time convert to string failed!");
    }
    GS_LOG_DEBUG_INF("[StreamAgg] job:%d, end_time: %s\n", sjob->job.ts_job.job_id, end_date.c_str());
    sjob->dst_end_datetime = end_date;
    sjob->dst_end_timestamp = end_time;
}

void TsScheduler::TsAggSqlReplaceStartEndTime(Connection *conn, TsScheduledJob *sjob, 
        std::string &agg_sql, const timestamp_stor_t &start_time, const timestamp_stor_t &end_time) {
    std::string start_date;
    std::string end_date;
    // 替换开始时间
    if (!TryCast::Operation<timestamp_stor_t, std::string>(start_time, start_date)) {
        throw std::runtime_error("[StreamAgg] timestamp_stor_t convert to string failed!");
    }
    GS_LOG_DEBUG_INF("[StreamAgg] job:%d, start_time: %s\n", sjob->job.ts_job.job_id, start_date.c_str());
    bool replaced = false;
    ReplaceSubstring(agg_sql, MACRO_START_TIME, "'"+start_date+"'", replaced);
    if (!replaced){
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s agg_sql replace %s failed!", sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), MACRO_START_TIME);
        throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} agg_sql replace {} failed!", sjob->job.ts_job.job_id, sjob->job.ts_job.name, MACRO_START_TIME));
    }

    // 替换结束时间
    if (!TryCast::Operation<timestamp_stor_t, std::string>(end_time, end_date)) {
        throw std::runtime_error("[StreamAgg] end_time convert to string failed!");
    }
    GS_LOG_DEBUG_INF("[StreamAgg] job:%d, end_time: %s\n", sjob->job.ts_job.job_id, end_date.c_str());
    ReplaceSubstring(agg_sql, MACRO_END_TIME, "'"+end_date+"'", replaced);
    if (!replaced){
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s agg_sql replace %s failed!", sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), MACRO_END_TIME);
        throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} agg_sql replace {} failed!", sjob->job.ts_job.job_id, sjob->job.ts_job.name, MACRO_END_TIME));
    }
}

void TsScheduler::TsSaveSJobStatInfo(Connection *conn, TsScheduledJob *sjob, bool is_succ_finished) {
    TsStreamAggJobManage mng;
    mng.UpdateTsJobStat(conn, sjob->job.ts_job_stat);
    if (!is_succ_finished) {
        mng.UpdateTsJobErr(conn, sjob->job.ts_job_err);
    }
}

void TsScheduler::TsCheckANdDeleteDstTableOldData(Connection *conn, TsScheduledJob *sjob) {
    std::stringstream ss;
    if (sjob->job.ts_cagg_policy.table_name.empty() || sjob->job.ts_cagg_policy.bucket_field_name.empty()) {
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s dst table_name<%s> or bucket_field_name<%s> is null!", 
                    sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str(), sjob->job.ts_cagg_policy.table_name.c_str(), sjob->job.ts_cagg_policy.bucket_field_name.c_str());
        throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} dst table_name<{}> or bucket_field_name<{}> is null!", 
                    sjob->job.ts_job.job_id, sjob->job.ts_job.name, sjob->job.ts_cagg_policy.table_name, sjob->job.ts_cagg_policy.bucket_field_name));
    }
    ss << "DELETE FROM " << sjob->job.ts_cagg_policy.table_name ;
    ss << " WHERE " << sjob->job.ts_cagg_policy.bucket_field_name << " >= '" << sjob->dst_start_datetime << "' and ";
    ss << sjob->job.ts_cagg_policy.bucket_field_name << " < '" << sjob->dst_end_datetime << "';";

    auto ret = conn->Query(ss.str().c_str());
    if (ret->GetRetCode() != GS_SUCCESS) {
        throw std::runtime_error(ret->GetRetMsg());
    }
}

bool32 TsScheduler::TsScheduledJobCheckIfValid(TsScheduledJob *sjob) {
    auto &ts_job = sjob->job.ts_job;
    if (ts_job.name.empty() || ts_job.state != JOB_STATE_VALID) {
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s is not valid!", ts_job.job_id, ts_job.name.c_str());
        return GS_FALSE;
    }

    auto &policy = sjob->job.ts_cagg_policy;
    if (policy.table_name.empty() || policy.db_name.empty() || policy.exec_sql.empty() || policy.bucket_field_name.empty()) {
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s cagg policy is invalid!", ts_job.job_id, ts_job.name.c_str());
        return GS_FALSE;
    }

    if (policy.bucket_type == TIME_BUCKET && (policy.bucket_field_name.empty() || policy.bucket_width == 0)) {
        GS_LOG_RUN_ERR("[StreamAgg] job %d-%s bucket_type is TIME_BUCKET but bucket_field_name or bucket_width is null!", 
                    ts_job.job_id, ts_job.name.c_str());
        return GS_FALSE;
    }
    return GS_TRUE;
}

void TsScheduler::TsScheduledJobNoneBucketExec(Connection *conn, std::string agg_sql, TsScheduledJob *sjob) {
    // step1.1 替换数据查询语句中的 __START_TIME__， __END_TIME__ 内置宏
    TsAggSqlReplaceStartEndTime(conn, sjob, agg_sql, sjob->dst_start_timestamp, sjob->dst_end_timestamp);

    agg_sql = "INSERT INTO " + sjob->job.ts_cagg_policy.table_name + " " + agg_sql;

    // step1.2 检测删除旧数据
    TsCheckANdDeleteDstTableOldData(conn, sjob);
    
    // 目标sql： INSERT INTO dst_table_name 
    //         SELECT sum(a) AS sum_a, sum(b) AS sum(b), min(date_time) AS start_time, max(date_time) AS end_time 
    //         FROM src_table_name WHERE date_time >= __START_TIME__ AND date_time < __END_TIME__;
    // step2 计算
    auto result = conn->Query(agg_sql.c_str());
    if (result->GetRetCode()!= GS_SUCCESS){
        GS_LOG_RUN_ERR("[NoneBucket] agg_sql exec failed!,job id: %d, agg_sql:%s", sjob->job.ts_job.job_id, agg_sql.c_str());
        throw std::runtime_error(fmt::format("[NoneBucket] job id:{}, agg_sql(start_time-{},end_time-{}) exec failed!", 
            sjob->job.ts_job.job_id, sjob->dst_start_timestamp.ts, sjob->dst_end_timestamp.ts));
    }
}

void TsScheduler::TsScheduledJobTimeBucketExec(Connection *conn, std::string &agg_sql, TsScheduledJob *sjob) {
    timestamp_stor_t start_time = sjob->dst_start_timestamp;
    timestamp_stor_t end_time = start_time + sjob->job.ts_cagg_policy.bucket_width * MICROSECS_PER_SECOND;

    TsCheckANdDeleteDstTableOldData(conn, sjob);

    while (start_time < sjob->dst_end_timestamp && start_time < end_time) {
        // 第二个条件是为了处理最后一个bucket过小导致没有数据引起的问题
        if (end_time > sjob->dst_end_timestamp 
            || (sjob->dst_end_timestamp - end_time) < sjob->job.ts_cagg_policy.bucket_width * MICROSECS_PER_SECOND) {
            end_time = sjob->dst_end_timestamp;
        }
        std::string agg_sql_bucket = agg_sql;
        TsAggSqlReplaceStartEndTime(conn, sjob, agg_sql_bucket, start_time, end_time);

        agg_sql_bucket = "INSERT INTO " + sjob->job.ts_cagg_policy.table_name + " " + agg_sql_bucket;

        GS_LOG_DEBUG_INF("[TimeBucket]sjob %d bucket sql:%s", sjob->job.ts_job.job_id, agg_sql_bucket.c_str());

        auto result = conn->Query(agg_sql_bucket.c_str());
        if (result->GetRetCode()!= GS_SUCCESS){
            GS_LOG_RUN_ERR("[TimeBucket] agg_sql_bucket exec failed!,job id: %d, agg_sql_bucket:%s, err:%s\n", 
                                sjob->job.ts_job.job_id, agg_sql_bucket.c_str(), result->GetRetMsg().c_str());
            throw std::runtime_error(fmt::format("[TimeBucket] job id:{}, agg_sql_bucket(start_time-{},end_time-{}) exec failed!, err:{}", 
                                        sjob->job.ts_job.job_id, start_time.ts, end_time.ts, result->GetRetMsg()));
        }

        // 被主线程检测到任务执行超时 或 任务执行中自己检测到执行超时
        if ((sjob->state == SCHEDULED_JOB_STATE_EXEC_TIMEOUT) || (sjob->state == SCHEDULED_JOB_STATE_STARTED && (sjob->timeout_at > 0 && TimestampNow() > sjob->timeout_at))) {
            TsScheduledJobTransitionStateTo(sjob, SCHEDULED_JOB_STATE_EXEC_TIMEOUT);
            throw std::runtime_error(fmt::format("[TimeBucket]job {}-{}(start_time-{},end_time-{}) had timed out at {}!!", 
                sjob->job.ts_job.job_id, sjob->job.ts_job.name, sjob->dst_start_datetime, sjob->dst_end_datetime, sjob->timeout_at.ts));
        }

        start_time = end_time;
        end_time = end_time + sjob->job.ts_cagg_policy.bucket_width * MICROSECS_PER_SECOND;
    }
}

void TsScheduler::TsScheduledJobStart(TsScheduledJob *sjob) {

	TsScheduledJobTransitionStateTo(sjob, SCHEDULED_JOB_STATE_STARTED);

	if (sjob->state != SCHEDULED_JOB_STATE_STARTED)
		return;

    sjob->job.ts_job_stat.last_start = TimestampNow();

    // Create a function that encapsulates the job logic
    auto jobFunction = [&](TsScheduledJob* sjob) {
        timestamp_stor_t finish_at;
        finish_at.ts = 0;
        bool is_succ_finished = false;
        
        Connection conn(db_);
        conn.Init();
        try {
            if (!TsScheduledJobCheckIfValid(sjob)) {
                throw std::runtime_error(fmt::format("[StreamAgg] job {}-{} is invalid!", sjob->job.ts_job.job_id, sjob->job.ts_job.name));
            }
            // Execute the job logic here, using the sjob parameter
            GS_LOG_DEBUG_INF("processing!!!");
            // 设置开始本次任务处理的数据的时间范围
            TsScheduledJobStartEndTimeSet(sjob);
            // step1 读取源数据表数据
            GS_LOG_DEBUG_INF("exec_sql:%s", sjob->job.ts_cagg_policy.exec_sql.c_str());
            if (sjob->job.ts_cagg_policy.bucket_type == NONE_BUCKET) {
                // 无分桶
                TsScheduledJobNoneBucketExec(&conn, sjob->job.ts_cagg_policy.exec_sql, sjob);
            } else if (sjob->job.ts_cagg_policy.bucket_type == TIME_BUCKET) {
                // 时间分桶
                TsScheduledJobTimeBucketExec(&conn, sjob->job.ts_cagg_policy.exec_sql, sjob);
            } else {
                GS_LOG_RUN_ERR("[jobFunction] job %d-%s bucket_type is invalid!", sjob->job.ts_job.job_id, sjob->job.ts_job.name.c_str());
                throw std::runtime_error(fmt::format("[jobFunction] job {}-{} bucket_type is invalid!", sjob->job.ts_job.job_id, sjob->job.ts_job.name));
            }

            // 被主线程检测到任务执行超时 或 任务执行中自己检测到执行超时
            if ((sjob->state == SCHEDULED_JOB_STATE_EXEC_TIMEOUT) 
                || (sjob->state == SCHEDULED_JOB_STATE_STARTED && (sjob->timeout_at > 0 && TimestampNow() > sjob->timeout_at))) {
                TsScheduledJobTransitionStateTo(sjob, SCHEDULED_JOB_STATE_TERMINATING);
                throw std::runtime_error(fmt::format("[jobFunction]job {}-{}(start_time-{},end_time-{}) had timed out at {}!!", 
                    sjob->job.ts_job.job_id, sjob->job.ts_job.name, sjob->dst_start_datetime, sjob->dst_end_datetime, sjob->timeout_at.ts));
            }

            // step4 修改任务状态表 或修改任务错误信息表
            // 任务处理结束，更新状态信息
            finish_at = TimestampNow();
            sjob->job.ts_job_stat.last_succuss_finish = sjob->dst_end_timestamp;
            sjob->job.ts_job_stat.next_start = finish_at + sjob->job.ts_job.schedule_interval * MICROSECS_PER_SECOND;
            is_succ_finished = true;
        } catch (const std::exception& e) {
            // 非超时失败 和 在任务线程中检测到的超时失败
            finish_at = TimestampNow();
            TsSJobFailedStatSet(sjob, finish_at, e.what());
            
            GS_LOG_RUN_WAR("[jobFunction] stream agg job exec failed!");
        }
        sjob->job.ts_job_stat.total_runs++;
        sjob->job.ts_job_stat.last_finish = finish_at;
        
        // 如果是一次性任务，则首次执行后将状态置为无效
        if (sjob->job.ts_job.schedule_time == SCHEDULE_TIME_ONE) {
            sjob->job.ts_job.state = JOB_STATE_INVALID;
        }
        
        // step5 更新任务状态表，注：*************超时失败情况不在此处更新*************
        TsSaveSJobStatInfo(&conn, sjob, is_succ_finished);
        if (sjob->job.ts_job.state == JOB_STATE_INVALID) {
            TsScheduledJobTransitionStateTo(sjob, SCHEDULED_JOB_STATE_DISABLED);
        } else {
            TsScheduledJobTransitionStateTo(sjob, SCHEDULED_JOB_STATE_SCHEDULED);
        }
    };

    // Assign the job function to the task member variable of the TsScheduledJob object
    sjob->task = jobFunction;
    // Add the TsScheduledJob object to the task queue of the thread pool
    thread_pool_.AddJob(sjob);
}

void TsScheduler::TsSJobFailedStatSet(TsScheduledJob* sjob, timestamp_stor_t finish_at, std::string err_msg) {
    // ts_job_err
    sjob->job.ts_job_err.job_id = sjob->job.ts_job.job_id;
    sjob->job.ts_job_err.pid = pthread_self();
    if (!TryCast::Operation<timestamp_stor_t, std::string>(sjob->job.ts_job_stat.last_start, sjob->job.ts_job_err.start_time)) {
        throw std::runtime_error("[StreamAgg] end_time convert to string failed!");
    }
    if (!TryCast::Operation<timestamp_stor_t, std::string>(finish_at, sjob->job.ts_job_err.finish_time)) {
        throw std::runtime_error("[StreamAgg] end_time convert to string failed!");
    }
    sjob->job.ts_job_err.err_data = err_msg;

    // ts_job_stat
    sjob->job.ts_job_stat.job_id = sjob->job.ts_job.job_id;
    sjob->job.ts_job_stat.total_failed++;
    if (sjob->job.ts_job_stat.consecutive_failures < sjob->job.ts_job.failed_retry_num) {
        sjob->job.ts_job_stat.consecutive_failures++;
        sjob->job.ts_job_stat.next_start = finish_at + sjob->job.ts_job.retry_period*MICROSECS_PER_SECOND;
    } else {
        sjob->job.ts_job_stat.consecutive_failures = 0;
        sjob->job.ts_job_stat.next_start = finish_at + sjob->job.ts_job.schedule_interval*MICROSECS_PER_SECOND;
    }
}

void TsScheduler::TsTimedOutScheduledJobProc(TsScheduledJob *sjob) {
    timestamp_stor_t job_timeout_at = TimestampNow();
    GS_LOG_DEBUG_INF("[TimeOutProc]starting job %d time out!!", sjob->job.ts_job.job_id);
    // to do  结束该任务：需要给执行任务的线程发送中止信号
    std::string timeout_datetime;
    if (!TryCast::Operation<timestamp_stor_t, std::string>(sjob->timeout_at, timeout_datetime)) {
        throw std::runtime_error("[TimeOutProc] end_time convert to string failed!");
    }
    TsSJobFailedStatSet(sjob, job_timeout_at, fmt::format("[TimeOutProc]starting job {}-{} time out at {}!!", sjob->job.ts_job.job_id, sjob->job.ts_job.name, timeout_datetime));
    sjob->job.ts_job_stat.total_runs++;
    sjob->job.ts_job_stat.last_finish = job_timeout_at;
    TsScheduledJobTransitionStateTo(sjob, SCHEDULED_JOB_STATE_SCHEDULED);
    // 当前任务超时时间
    sjob->timeout_at = sjob->job.ts_job_stat.next_start + sjob->job.ts_job.max_runtime * MICROSECS_PER_SECOND;

    Connection conn(db_);
    conn.Init();
    TsSaveSJobStatInfo(&conn, sjob, false);
    TsCheckANdDeleteDstTableOldData(&conn, sjob);
}

void TsScheduler::TsCheckForTimedOutScheduledJobs() {
    if (scheduled_job_map_.empty()){
        GS_LOG_DEBUG_INF("Has no scheduled job now!");
        return;
    }
    for (auto& mjob : scheduled_job_map_){
        auto &sjob = mjob.second;
        if (sjob.state == SCHEDULED_JOB_STATE_STARTED && sjob.timeout_at > 0 && TimestampNow() >= sjob.timeout_at)
		{
            try{
                // TsTimedOutScheduledJobProc(&sjob);
                TsScheduledJobTransitionStateTo(&sjob, SCHEDULED_JOB_STATE_EXEC_TIMEOUT);
            } catch (const std::exception& e) {
                GS_LOG_RUN_ERR("[TimeOutProc] job %d-%s time out proc failed!,err:%s", sjob.job.ts_job.job_id, sjob.job.ts_job.name.c_str(), e.what());
            }
		}
    }
}

void * TsStreamAggSchedulerMain(void *dbstoragewrap) {
    int *result = nullptr;
    // instance_t storage_instance_
    DbStorageWrapper* instance = static_cast<DbStorageWrapper*>(dbstoragewrap);
    auto db = std::make_shared<IntarkDB>(instance->db_storage);
    TsScheduler ts_scheduler(db, db->db_get_streamagg_threadpool_num());

    ts_scheduler.TsStreamAggSchedulerProc();

    delete instance;

    return result;
}


