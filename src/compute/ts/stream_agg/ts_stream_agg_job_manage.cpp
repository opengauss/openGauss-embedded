/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * ts_stream_agg_job_manage.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/stream_agg/ts_stream_agg_job_manage.cpp
 *
 * -------------------------------------------------------------------------
 */

#include "compute/ts/include/stream_agg/ts_stream_agg_job_manage.h"
#include "compute/ts/include/table/ts_table.h"
#include "compute/sql/include/main/connection.h"

TsTable ts_table;

std::vector<TsJobInfo> TsStreamAggJobManage::GetAllValidJob(Connection *connection) {
    std::vector<TsJobInfo> results;
    std::vector<TsJob> ts_jobs;
    std::vector<TsJobStat> ts_job_stats;
    std::vector<TsCaggPolicy> ts_cagg_policys;
    std::vector<TsJobErr> ts_job_errs;
    std::vector<uint32_t> job_ids;
    std::size_t len;
    TsJob condition;
    condition.state = JOB_STATE_VALID;
    ts_table.Select(connection, condition, ts_jobs);
    len = ts_jobs.size();
    if (len == 0) {
        return results;
    }

    for (const auto& ts_job : ts_jobs) {
        job_ids.push_back(ts_job.job_id);
    }

    ts_table.Select(connection, job_ids, ts_job_stats);
    if (len != ts_job_stats.size()) {
        GS_LOG_RUN_ERR("The number of ts_jobs and ts_job_stats are different!");
        return results;
    }

    ts_table.Select(connection, job_ids, ts_cagg_policys);
    if (len != ts_cagg_policys.size()) {
        GS_LOG_RUN_ERR("The number of ts_jobs and ts_cagg_policys are different!");
        return results;
    }
    
    ts_table.Select(connection, job_ids, ts_job_errs);
    if (len != ts_job_errs.size()) {
        GS_LOG_RUN_ERR("The number of ts_jobs and ts_job_errs are different!");
        return results;
    }

    for (size_t i = 0; i < len; ++i) {
        TsJobInfo result;
        result.ts_job = ts_jobs[i];
        result.ts_job_err = ts_job_errs[i];
        result.ts_job_stat = ts_job_stats[i];
        result.ts_cagg_policy = ts_cagg_policys[i];
        results.push_back(result);
    }

    return results;
}

int32_t TsStreamAggJobManage::StartJob(Connection *connection, std::string job_name, 
                            uint32_t offset_start, uint32_t offset_end) {
    TsJob from, to;
    from.name = job_name;
    from.state = JOB_STATE_VALID;
    to.offset_start = offset_start;
    to.offset_end = offset_end;

    return ts_table.Update(connection, from, to);
}

int32_t TsStreamAggJobManage::PauseJob(Connection *connection, std::string job_name) {
    TsJob from, to;
    from.name = job_name;
    to.state = JOB_STATE_PAUSE;

    return ts_table.Update(connection, from, to);
}

int32_t TsStreamAggJobManage::UpdateTsJobStat(Connection *connection, TsJobStat ts_job_stat) {
    TsJobStat from;
    from.job_id = ts_job_stat.job_id;

    return ts_table.Update(connection, from, ts_job_stat);
}

int32_t TsStreamAggJobManage::UpdateTsJobErr(Connection *connection, TsJobErr ts_job_err) {
    TsJobErr from;
    from.job_id = ts_job_err.job_id;

    return ts_table.Update(connection, from, ts_job_err);
}

int32_t TsStreamAggJobManage::CreateTsjob(Connection *connection, TsJobInfo ts_job_info, std::string des_sql) {
    uint32_t job_id;

    connection->Query("begin;");
    auto status = ts_table.Insert(connection, ts_job_info.ts_job);
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("insert ts_job err");
        connection->Query("rollback;");
        return GS_ERROR;
    }

    status = GetJobId(connection, ts_job_info.ts_job.name, job_id);
    if (status != GS_SUCCESS || job_id == 0) {
        GS_LOG_RUN_ERR("can't query ts_job record, ts_job.name = %s, job_id = %d", ts_job_info.ts_job.name.c_str(), job_id);
        connection->Query("rollback;");
        return GS_ERROR;
    }

    ts_job_info.ts_job_err.job_id = job_id;
    ts_job_info.ts_job_stat.job_id = job_id;
    ts_job_info.ts_cagg_policy.job_id = job_id;

    status = ts_table.Insert(connection, ts_job_info.ts_job_err);
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("insert ts_job_err err");
        connection->Query("rollback;");
        return GS_ERROR;
    }

    status = ts_table.Insert(connection, ts_job_info.ts_job_stat);
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("insert ts_job_stat err");
        connection->Query("rollback;");
        return GS_ERROR;
    }

    status = ts_table.Insert(connection, ts_job_info.ts_cagg_policy);
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_ERR("insert ts_cagg_policy err");
        connection->Query("rollback;");
        return GS_ERROR;
    }

    auto ret = connection->Query(des_sql.c_str());
    if (ret->GetRetCode() != GS_SUCCESS) {
        GS_LOG_RUN_ERR("create destination table failed!");
        connection->Query("rollback;");
        return GS_ERROR;
    }

    connection->Query("commit;");
    return GS_SUCCESS;
}

int32_t TsStreamAggJobManage::GetJobId(Connection *connection, std::string job_name, uint32_t& job_id) {
    TsJob condition;
    std::vector<TsJob> results;
    condition.name = job_name;
    ts_table.Select(connection, condition, results);

    if (results.size() != 1) {
        return GS_ERROR;
    }

    job_id = results[0].job_id;

    return GS_SUCCESS;
}

int32_t TsStreamAggJobManage::DeletetTsJobInfo(Connection *connection, uint32_t job_id) { 
    TsJob ts_job_condition;
    TsJobErr ts_job_err_condition;
    TsJobStat ts_job_stat_condition;
    TsCaggPolicy ts_cagg_policy_condition;

    ts_job_condition.job_id = job_id;
    ts_job_err_condition.job_id = job_id;
    ts_job_stat_condition.job_id = job_id;
    ts_cagg_policy_condition.job_id = job_id;

    auto status = ts_table.Delete(connection, ts_job_condition);
    if (status != GS_SUCCESS) {
        return GS_ERROR;
    }

    status = ts_table.Delete(connection, ts_job_stat_condition);
    if (status != GS_SUCCESS) {
        return GS_ERROR;
    }

    status = ts_table.Delete(connection, ts_job_err_condition);
    if (status != GS_SUCCESS) {
        return GS_ERROR;
    }

    status = ts_table.Delete(connection, ts_cagg_policy_condition);
    if (status != GS_SUCCESS) {
        return GS_ERROR;
    }

    return GS_SUCCESS;
}

std::vector<std::string> TsStreamAggJobManage::GetAllJobName(Connection *connection) {
    std::vector<TsJob> ts_jobs;
    std::vector<std::string> job_names;
    std::size_t len;

    ts_table.Select(connection, ts_jobs);
    len = ts_jobs.size();
    if (len == 0) {
        return job_names;
    }

    for (size_t i = 0; i < len; ++i) {
        job_names.push_back(ts_jobs[i].name);
    }

    return job_names;
}

int32_t TsStreamAggJobManage::CountTsJob(Connection *connection) {
    int32_t result;
    std::string sql = "select count(*) from ts_job where state=1";

    auto rb = connection->Query(sql.c_str());
    result = rb->Row(0).FieldRef(0).GetCastAs<int32_t>();
    return result;
}

int32_t TsStreamAggJobManage::GetJob(Connection *connection, std::string job_name, TsJobInfo& result) {
    uint32_t job_id;

    auto r = GetJobId(connection, job_name, job_id);
    if (r != GS_SUCCESS) {
        throw std::runtime_error("get jod id failed!");
    }

    r = ts_table.Select(connection, job_id, result.ts_job);
    if (r != GS_SUCCESS) {
        throw std::runtime_error("select ts_job failed!");
    }
    r = ts_table.Select(connection, job_id, result.ts_cagg_policy);
    if (r != GS_SUCCESS) {
        throw std::runtime_error("select ts_cagg_policy failed!");
    }

    return GS_SUCCESS;
}

int32_t TsStreamAggJobManage::SelectTsJobStat(Connection *connection, uint32_t job_id, RecordBatch& result) {
    std::string sql = "select * from ts_job_stat where job_id = " + std::to_string(job_id);
    auto rb = connection->Query(sql.c_str());
    if (rb->GetRetCode() != GS_SUCCESS) {
        return GS_ERROR;
    }

    result = *rb;
    return GS_SUCCESS;
}

int32_t TsStreamAggJobManage::SelectTsJobErr(Connection *connection, uint32_t job_id, RecordBatch& result) {
    std::string sql = "select * from ts_job_err where job_id = " + std::to_string(job_id);
    auto rb = connection->Query(sql.c_str());
    if (rb->GetRetCode() != GS_SUCCESS) {
        return GS_ERROR;
    }
    
    result = *rb;
    return GS_SUCCESS;
}