/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * job_stat.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/stream_agg/job_stat.cpp
 *
 * -------------------------------------------------------------------------
  */
#include "compute/ts/include/stream_agg/job_stat.h"

// 获取任务当前最新状态，方法： 查表
TsJobStat TsJobStatManager::TsJobStatRead(Connection *connection) {
    std::string sql;
    TsJobStat result;

    sql = "select * from ts_job_stat order by update_time desc limit 1;";
    GS_LOG_DEBUG_INF("select ts_job_stat sql=%s", sql.c_str());

    auto rb = connection->Query(sql.c_str());
    size_t row_count = rb->RowCount();
    if(row_count == 0) return result;

    auto status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_INF("Failed to select ts_job_stat tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_INF("Failed to select ts_job_stat tables, ignoring...(sql=%s)", sql.c_str());
        cm_reset_error();
        return result;
    }

    int i = 0;
    result.job_id = rb->Row(i).FieldRef(0).GetCastAs<uint32_t>();
    result.last_start = rb->Row(i).FieldRef(1).GetCastAs<timestamp_stor_t>();
    result.last_finish = rb->Row(i).FieldRef(2).GetCastAs<timestamp_stor_t>();
    result.next_start = rb->Row(i).FieldRef(3).GetCastAs<timestamp_stor_t>();
    result.last_succuss_finish = rb->Row(i).FieldRef(4).GetCastAs<timestamp_stor_t>();
    result.total_runs = rb->Row(i).FieldRef(5).GetCastAs<uint64_t>();
    result.total_failed = rb->Row(i).FieldRef(6).GetCastAs<uint64_t>();
    result.consecutive_failures = rb->Row(i).FieldRef(7).GetCastAs<uint32_t>();

    return result;
}