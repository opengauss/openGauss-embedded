/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * thread_pool.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/stream_agg/thread_pool.cpp
 *
 * -------------------------------------------------------------------------
  */
#include "thread_pool.h"

ThreadPool::ThreadPool(size_t num_threads) :stop_(false) {
    for (size_t i = 0; i < num_threads; ++i) {
        workers_.emplace_back(&ThreadPool::ThreadFunction, this);
        pthread_setname_np(workers_.back().native_handle(), "stream_agg_job");
    }
}

ThreadPool::~ThreadPool() {
    {
        std::unique_lock<std::mutex> lock(queue_mutex_);
        stop_ = true;
    }

    condition_.notify_all();

    for (std::thread& worker : workers_) {
        if (worker.joinable()) {
            worker.join(); // 等待线程执行完毕
        }
    }
}

void ThreadPool::ThreadFunction(){ // 线程执行函数
    while (true) {
        TsScheduledJob* sjob;
        std::unique_lock<std::mutex> lock(queue_mutex_);
        condition_.wait(lock, [this] { return stop_ || !job_queue_.empty(); });

        if (stop_ && job_queue_.empty()) {
            break;
        }

        sjob = job_queue_.front();
        job_queue_.pop_front();
        
        sjob->task(sjob);
    }
}

void ThreadPool::AddJob(TsScheduledJob* sjob) {
    {
        GS_LOG_DEBUG_INF("add scheduled job %d", sjob->job.ts_job.job_id);
        std::lock_guard<std::mutex> lock(queue_mutex_);
        job_queue_.emplace(job_queue_.end(), sjob);
    }
    
    condition_.notify_one();
}