/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * scheduler.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/ts/stream_agg/scheduler.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "compute/ts/include/stream_agg/cleaner.h"
#include <chrono>
#include <thread>

TsCleaner::TsCleaner(std::shared_ptr<IntarkDB> db) : db_(db) {
}

TsCleaner::~TsCleaner() {
}

void TsCleaner::TsCleanInvalidInfo() {
    Connection conn(db_);
    conn.Init();

    TsTable ts_table;
    TsJob ts_job;
    std::vector<TsJob> results;

    ts_job.state = JOB_STATE_INVALID;
    auto status = ts_table.Select(&conn, ts_job, results);
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_INF("Failed to ts_table.Select(&conn, ts_job, results)");
        GS_LOG_RUN_INF("Failed to ts_table.Select(&conn, ts_job, results)");
        cm_reset_error();
        return;
    }
    
    TsStreamAggJobManage ts;
    for (int i = 0; i < results.size(); i++){
        status = ts.DeletetTsJobInfo(&conn, results[i].job_id);
        if (status != GS_SUCCESS) {
            GS_LOG_RUN_INF("Failed to DeletetTsJobInfo, i = %d", i);
            GS_LOG_RUN_INF("Failed to DeletetTsJobInfo, i = %d", i);
            cm_reset_error();
            return;
        }
    }

    std::string sql;
    // 7 * 24  * 60 * 60 * 1000 * 1000
    long long outdate_time = 604800000000;
    // sql = "update ts_job_err set err_data = '' where err_data != '' and unix_timestamp(now())-unix_timestamp(update_time) > 604800000000;";
    sql = "update ts_job_err set err_data = '' where err_data != '' and unix_timestamp(now())-unix_timestamp(update_time) > " + std::to_string(outdate_time);
    GS_LOG_DEBUG_INF("delete ts_job_err sql=%s", sql.c_str());

    auto rb = conn.Query(sql.c_str());
    status = rb->GetRetCode();
    if (status != GS_SUCCESS) {
        GS_LOG_RUN_INF("Failed to delete ts_job_err tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
        GS_LOG_RUN_INF("Failed to delete ts_job_err tables, ignoring...(sql=%s)", sql.c_str());
        cm_reset_error();
        return;
    }
}

void TsCleaner::TsCleanProc() {
    GS_LOG_RUN_INF("Clean invalid jobs.");
    while(1){
        // 失效流计算任务
        TsCleanInvalidInfo();

        // 等待30秒
        int wait_time = 30;
        std::this_thread::sleep_for(std::chrono::seconds(wait_time));
    }
}

void* TsClean(void *dbstoragewrap) {
    int *result = nullptr;
    // instance_t storage_instance_
    DbStorageWrapper* instance = static_cast<DbStorageWrapper*>(dbstoragewrap);
    auto db = std::make_shared<IntarkDB>(instance->db_storage);

    TsCleaner ts_cleaner(db);
    ts_cleaner.TsCleanProc();

    delete instance;

    return result;
}