/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* ts_job_call_test.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/ts/test/ts_job_call_test.cpp
*
* -------------------------------------------------------------------------
*/

#include <gtest/gtest.h>

#include "compute/sql/include/main/connection.h"

class StreamJobManageTest : public ::testing::Test {
   protected:
    StreamJobManageTest() {}
    ~StreamJobManageTest() {}
    static void SetUpTestSuite() {
        db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance("./"));
        // 启动db
        db_instance->Init();
        // conn = std::make_unique<Connection>(db_instance);
        // conn->Init();

        // sleep for a while to wait for db to start
        sleep(1);
    }

    // Per-test-suite tear-down.
    // Called after the last test in this test suite.
    // Can be omitted if not needed.
    // static void TearDownTestSuite() { conn.reset(); }
    static void TearDownTestSuite() { }

    void SetUp() override {}

    // void TearDown() override {}

    static std::shared_ptr<IntarkDB> db_instance;
    static std::unique_ptr<Connection> conn;
};

std::shared_ptr<IntarkDB> StreamJobManageTest::db_instance = nullptr;
std::unique_ptr<Connection> StreamJobManageTest::conn = nullptr;

TEST_F(StreamJobManageTest, StreamJobManageTest) {
    Connection conn_s(db_instance);
    conn_s.Init();


    printf("create table test_\n");
    std::string create_sql = "CREATE TABLE test_(date datetime default now(),a int, b int);";
    auto r = conn_s.Query(create_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    printf("call ts_add_cagg_policy ts_job_\n");
    std::string add_sql = "call ts_add_cagg_policy('ts_job_','des_table_',1,'select sum(a) as sum_a, sum(b) as bb from test_ where date >= __START_TIME__ and date <= __END_TIME__',5,0,0,'date_time','');";
    r = conn_s.Query(add_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() != GS_SUCCESS);


    printf("create timescale table\n");
    create_sql = "CREATE TABLE test(date datetime default now(),a int, b int) PARTITION BY RANGE(date) timescale interval '1h' retention '30d' autopart crosspart;";
    r = conn_s.Query(create_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    std::string insert_sql = "insert into test(a,b) values(1,1);";
    r = conn_s.Query(insert_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    printf("call ts_add_cagg_policy\n");
    add_sql = "call ts_add_cagg_policy('ts_job','des_table',1,'select sum(a) as sum_a, sum(b) as bb from test where date >= __START_TIME__ and date <= __END_TIME__',5,0,0,'date_time','');";
    r = conn_s.Query(add_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    int times = 50;
    int i = 0;
    while(i < times){
        r = conn_s.Query(insert_sql.c_str());
        ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);
        sleep(1);
        i++;
    }

    std::string select_sql = "select count(*) from des_table";
    r = conn_s.Query(select_sql.c_str());
    printf("des_table row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());

    // printf("call ts_sel_cagg_stats\n");
    std::string sel_stats_sql = "call ts_sel_cagg_stats('ts_job');";
    r = conn_s.Query(sel_stats_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    // printf("call ts_sel_cagg_err\n");
    std::string sel_err_sql = "call ts_sel_cagg_err('ts_job');";
    r = conn_s.Query(sel_err_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    printf("call ts_pause_cagg_policy\n");
    std::string pause_sql = "call ts_pause_cagg_policy('ts_job');";
    r = conn_s.Query(pause_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);
    sleep(30);
    r = conn_s.Query(select_sql.c_str());
    printf("des_table row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());


    printf("call ts_refresh_cagg_policy\n");
    std::string refresh_sql = "call ts_refresh_cagg_policy('ts_job',0,0);";
    r = conn_s.Query(refresh_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);
    r = conn_s.Query(select_sql.c_str());
    printf("des_table row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());


    printf("call ts_del_cagg_policy\n");
    std::string del_sql = "call ts_del_cagg_policy('ts_job');";
    r = conn_s.Query(del_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    std::string select_sql1 = "select count(*) from ts_job;";
    r = conn_s.Query(select_sql1.c_str());
    printf("ts_job row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());

    std::string select_sql2 = "select count(*) from ts_job_err;";
    r = conn_s.Query(select_sql2.c_str());
    printf("ts_job_err row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());

    std::string select_sql3 = "select count(*) from ts_job_stat;";
    r = conn_s.Query(select_sql3.c_str());
    printf("ts_job_stat row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());

    std::string select_sql4 = "select count(*) from ts_job;";
    r = conn_s.Query(select_sql4.c_str());
    printf("ts_job row counts:%d\n", r->Row(0).FieldRef(0).GetCastAs<int32_t>());
}

int main(int argc, char** argv) {
    system("rm -rf intarkdb/");
    ::testing::GTEST_FLAG(output) = "xml";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}