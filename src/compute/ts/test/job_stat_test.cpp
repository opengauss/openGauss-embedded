/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* job_stat_test.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/ts/test/job_stat_test.cpp
*
* -------------------------------------------------------------------------
*/

#include <gtest/gtest.h>
#include "compute/sql/include/main/connection.h"
#include "compute/ts/include/stream_agg/job_stat.h"
#include <string>
#include <chrono>
#include <thread>

int main(){
    system("rm -rf intarkdb/");
    std::string path = "./";
    auto db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance(path.c_str()));
    db_instance->Init();

    Connection conn(db_instance);
    conn.Init();

    std::this_thread::sleep_for(std::chrono::seconds(1));

    TsTable ts_table;
    TsJobStat ts_job_stat;
    ts_job_stat.job_id = 1;
    ts_job_stat.last_start.ts = 1;
    ts_job_stat.last_finish.ts = 1;
    ts_job_stat.next_start.ts = 1;
    ts_job_stat.last_succuss_finish.ts = 1;
    EXPECT_TRUE(ts_table.Insert(&conn, ts_job_stat) == GS_SUCCESS);

    ts_job_stat.job_id = 2;
    ts_job_stat.last_start.ts = 2;
    ts_job_stat.last_finish.ts = 2;
    ts_job_stat.next_start.ts = 2;
    ts_job_stat.last_succuss_finish.ts = 2;
    EXPECT_TRUE(ts_table.Insert(&conn, ts_job_stat) == GS_SUCCESS);
    
    TsJobStatManager ts_stat;
    TsJobStat result;
    result = ts_stat.TsJobStatRead(&conn);
    EXPECT_TRUE(result.job_id == 2);
    EXPECT_TRUE(result.last_start.ts == 2);
    EXPECT_TRUE(result.last_finish.ts == 2);
    EXPECT_TRUE(result.next_start.ts == 2);
    EXPECT_TRUE(result.last_succuss_finish.ts == 2);
}