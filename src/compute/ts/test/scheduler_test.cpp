/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* scheduler_test.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/ts/test/scheduler_test.cpp
*
* -------------------------------------------------------------------------
*/
#include <gtest/gtest.h>
#include <thread>
#include <random>

#include "catalog/catalog.h"
#include "catalog/table_info.h"
#include "common/default_value.h"
#include "main/connection.h"
#include "main/database.h"
#include "interface/c/intarkdb_sql.h"

struct DatabaseWrapper {
    std::shared_ptr<IntarkDB> instance;
};

std::atomic<bool> terminateFlag(false);

class SchedulerTest : public ::testing::Test {
   protected:
    SchedulerTest() {}
    ~SchedulerTest() {}
    static void SetUpTestSuite() {
        db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance("./"));
        // 启动db
        db_instance->Init();
        conn = std::make_unique<Connection>(db_instance);
        conn->Init();

        DataPrepare(db_instance);
    }

    // Per-test-suite tear-down.
    // Called after the last test in this test suite.
    // Can be omitted if not needed.
    static void TearDownTestSuite() { 
        conn.reset(); 
    }

    static void batch_insert_example(Connection *conn_s);
    static void * DataInsertThread(void *db_wrap);
    static void DataPrepare(std::shared_ptr<IntarkDB> db_instance);

    void SetUp() override {}

    // void TearDown() override {}

    static std::shared_ptr<IntarkDB> db_instance;
    static std::unique_ptr<Connection> conn;
};

std::shared_ptr<IntarkDB> SchedulerTest::db_instance = nullptr;
std::unique_ptr<Connection> SchedulerTest::conn = nullptr;

void SchedulerTest::batch_insert_example(Connection *conn_s) {
    uint32_t id = 0;
    try {
        std::string sql = "INSERT INTO src_table_ts_agg (id, name, score, age, date_time) VALUES (";
        char buf_sql[64];

        // 创建一个随机数引擎
        std::default_random_engine rd;
        rd.seed(time(0));
        // 创建一个均匀分布的随机数分布
        std::uniform_int_distribution<int> score_dis(0, 100);
        std::uniform_int_distribution<int> age_dis(10, 15);

        while(!terminateFlag) {
            std::string date;
            if (!TryCast::Operation<timestamp_stor_t, std::string>(TimestampNow(), date)) {
                throw std::runtime_error("[batch_insert_example] now time convert to string failed!");
            }
            sprintf(buf_sql, "%d, '%s', %d, %d, '%s'", id, ("name_"+std::to_string(id)).c_str(), score_dis(rd), age_dis(rd), date.c_str());
            std::string  tmpSql = sql + std::string(buf_sql) + ");";

            auto result = conn_s->Query(tmpSql.c_str());
            ASSERT_TRUE(result->GetRetCode()==GS_SUCCESS);
            
            id++;

            sleep(1);
        }
    } catch (...) {
        //free
    }
}

void * SchedulerTest::DataInsertThread(void *db_wrap){
    auto db_wraper = static_cast<DatabaseWrapper*>(db_wrap);
    Connection conn_s(db_wraper->instance);
    conn_s.Init();
    batch_insert_example(&conn_s);
    return nullptr;
}

void SchedulerTest::DataPrepare(std::shared_ptr<IntarkDB> db_instance) {
    std::string src_table_create_sql = "create table src_table_ts_agg (id integer, name varchar(20), score smallint, age tinyint, date_time timestamp) PARTITION BY RANGE(date_time) timescale interval '1h' retention '30d' autopart crosspart;;";
    std::string dst_table_create_sql = "create table dst_test_job_1 (sum_score double, start_time timestamp, end_time timestamp);";
    std::string ts_job_data_prepare_sql = "insert into ts_job (job_id, job_name, schedule_time, schedule_interval, max_runtime, retry_period, partiton_type, \
        offset_start, offset_end) values (100, 'test_job_1', 1, 3, 3, 3, 20007, 6, 0);"; // 20007 = GS_TYPE_TIMESTAMP
    std::string ts_cagg_policy_data_prepare_sql = "insert into ts_cagg_policy (job_id, db_name, table_name, exec_sql, bucket_type, bucket_field_name, bucket_width) values \
        (100, 'intarkdb', 'dst_test_job_1', \
        'select sum(score) as sum_score, min(date_time) as start_time, max(date_time) as end_time from src_table_ts_agg where date_time >= __START_TIME__ and date_time < __END_TIME__;', \
        1, 'end_time', 3);"; // bucket_width 的单位设定为 秒
    std::string ts_job_stat_data_prepare_sql = "insert into ts_job_stat (job_id, last_start, last_finish, next_start, last_succuss_finish) values (100, 0, 0, 0, 0);";
    std::string ts_job_err_data_prepare_sql = "insert into ts_job_err (job_id, pid, start_time, finish_time, err_data) \
        values (100, 0, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '');";

    auto r1 = conn->Query(src_table_create_sql.c_str());
    ASSERT_TRUE(r1->GetRetCode() == GS_SUCCESS);

    auto r2 = conn->Query(dst_table_create_sql.c_str());
    ASSERT_TRUE(r2->GetRetCode() == GS_SUCCESS);

    auto r3 = conn->Query(ts_job_data_prepare_sql.c_str());
    ASSERT_TRUE(r3->GetRetCode() == GS_SUCCESS);
    
    auto r4 = conn->Query(ts_cagg_policy_data_prepare_sql.c_str());
    ASSERT_TRUE(r4->GetRetCode() == GS_SUCCESS);
            
    auto r5 = conn->Query(ts_job_stat_data_prepare_sql.c_str());
    ASSERT_TRUE(r5->GetRetCode() == GS_SUCCESS);

    auto r6 = conn->Query(ts_job_err_data_prepare_sql.c_str());
    ASSERT_TRUE(r6->GetRetCode() == GS_SUCCESS);

    auto db_wrap = new DatabaseWrapper();
    db_wrap->instance = db_instance;
    pthread_t id;
    pthread_create(&id, nullptr, DataInsertThread, db_wrap);
    pthread_setname_np(id, "src_tb_data_insert");
}

TEST_F(SchedulerTest, BackgroundAutomaticExecSchedulerJob) {
    // add column ,type integer, primary ke
    timestamp_stor_t start_test = TimestampNow();
    timestamp_stor_t quit_time = start_test + 30*MICROSECS_PER_SECOND;
    std::string dst_query = "select * from dst_test_job_1;";
    std::string src_query = "select * from src_table_ts_agg;";

    while(TimestampNow() < quit_time) {
        auto result = conn->Query(dst_query.c_str());
        ASSERT_TRUE(result->GetRetCode()==GS_SUCCESS);
        // printf("dst_test_job_1 row counts:%d\n", result->Row(0).Field(0).GetCastAs<int32_t>());
        result->Print();

        auto result1 = conn->Query(src_query.c_str());
        ASSERT_TRUE(result1->GetRetCode()==GS_SUCCESS);
        // printf("src_table_ts_agg row counts:%d\n", result1->Row(0).Field(0).GetCastAs<int32_t>());
        result1->Print();
        sleep(1);
    }

    terminateFlag = true;
    auto r1 = conn->Query("CALL TS_PAUSE_CAGG_POLICY('test_job_1');");
    ASSERT_TRUE(r1->GetRetCode()==GS_SUCCESS);
}

TEST_F(SchedulerTest, BGAutoExecNoneTimeBucketSJob) {
    // add column ,type integer, primary ke
    // std::string src_table_create_sql = "create table src_table_none_time as select * from src_table_ts_agg;";
    std::string dst_table_create_sql = "create table dst_table_none_time (sum_score double, start_time timestamp, end_time timestamp);";
    std::string ts_job_data_prepare_sql = "insert into ts_job (job_id, job_name, schedule_time, schedule_interval, max_runtime, retry_period, \
        partiton_type, offset_start, offset_end) values (101, 'test_job_none_time_bucket', 1, 3, 3, 3, 20007, 10, 0);"; // 20007 = GS_TYPE_TIMESTAMP
    std::string ts_cagg_policy_data_prepare_sql = "insert into ts_cagg_policy (job_id, db_name, table_name, exec_sql, bucket_type, bucket_field_name, bucket_width) values \
        (101, 'intarkdb', 'dst_table_none_time', \
        'select sum(score) as sum_score, min(date_time) as start_time, max(date_time) as end_time from src_table_ts_agg where date_time >= __START_TIME__ and date_time < __END_TIME__;', \
        0, 'end_time', 3);"; // bucket_width 的单位设定为 秒
    std::string ts_job_stat_data_prepare_sql = "insert into ts_job_stat (job_id, last_start, last_finish, next_start, last_succuss_finish) values (101, 0, 0, 0, 0);";
    std::string ts_job_err_data_prepare_sql = "insert into ts_job_err (job_id, pid, start_time, finish_time, err_data) values (101, 0, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '');";

    auto r1 = conn->Query("begin;");
    ASSERT_TRUE(r1->GetRetCode()==GS_SUCCESS);
    // auto r1 = conn->Query(src_table_create_sql.c_str());
    // ASSERT_TRUE(r1->GetRetCode()==GS_SUCCESS);

    auto r2 = conn->Query(dst_table_create_sql.c_str());
    ASSERT_TRUE(r2->GetRetCode()==GS_SUCCESS);

    auto r3 = conn->Query(ts_job_data_prepare_sql.c_str());
    ASSERT_TRUE(r3->GetRetCode() == GS_SUCCESS);
    
    auto r4 = conn->Query(ts_cagg_policy_data_prepare_sql.c_str());
    ASSERT_TRUE(r4->GetRetCode() == GS_SUCCESS);
            
    auto r5 = conn->Query(ts_job_stat_data_prepare_sql.c_str());
    ASSERT_TRUE(r5->GetRetCode() == GS_SUCCESS);

    auto r6 = conn->Query(ts_job_err_data_prepare_sql.c_str());
    ASSERT_TRUE(r6->GetRetCode() == GS_SUCCESS);

    auto r7 = conn->Query("commit;");
    ASSERT_TRUE(r7->GetRetCode()==GS_SUCCESS);

    sleep(8);

    std::string dst_query = "select count(*) from dst_table_none_time;";
    std::string src_query = "select count(*) from src_table_ts_agg;";

    auto result = conn->Query(dst_query.c_str());
    ASSERT_TRUE(result->GetRetCode()==GS_SUCCESS);
    ASSERT_TRUE(result->Row(0).Field(0).GetCastAs<int32_t>() > 0);

    auto result1 = conn->Query(src_query.c_str());
    ASSERT_TRUE(result1->GetRetCode()==GS_SUCCESS);
    ASSERT_TRUE(result1->Row(0).Field(0).GetCastAs<int32_t>() > 0);
}

// bug #710220387
TEST_F(SchedulerTest, BGAutoExecZeroIntervalSJob) {
    std::string add_sql = "call ts_add_cagg_policy('test_job_zero_interval','dst_table_zero_interval',1,\
        'select sum(score) as sum_score from src_table_ts_agg where date_time >= __START_TIME__ and date_time <= __END_TIME__',\
        0,0,0,'end_time','');";
    auto r = conn->Query(add_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    sleep(8);

    std::string err_query = "select err_data from ts_job_err where job_id = 102;";

    auto result = conn->Query(err_query.c_str());
    ASSERT_TRUE(result->GetRetCode()==GS_SUCCESS);
    ASSERT_STREQ(result->Row(0).Field(0).GetCastAs<std::string>().c_str(), "[StreamAgg] job 102-test_job_zero_interval start time is greater than end time! please check job schedule_interval(0) must be greater than 0!");
}

// 聚合的列类型不支持该种聚合函数
TEST_F(SchedulerTest, BGAutoExecSJobAggFuncParamTypeMisMatching) {
    std::string add_sql = "call ts_add_cagg_policy('ts_agg_time1','des_table_agg_time1',1,'select sum(date_time) as avg_a \
    from src_table_ts_agg where date_time >= __START_TIME__ and date_time <= __END_TIME__',60,0,0,'date_time','1h');";
    auto r = conn->Query(add_sql.c_str());
    ASSERT_TRUE(r->GetRetCode() == GS_SUCCESS);

    sleep(8);

    std::string err_query = "select err_data from ts_job_err where job_id = 103;";

    auto result = conn->Query(err_query.c_str());
    ASSERT_TRUE(result->GetRetCode()==GS_SUCCESS);
    ASSERT_TRUE(result->Row(0).Field(0).GetCastAs<std::string>().c_str() != nullptr); // sum only support int or float type
    printf("%s\n", result->Row(0).Field(0).GetCastAs<std::string>().c_str());
}

int main(int argc, char** argv) {
    system("rm -rf intarkdb/");
    ::testing::GTEST_FLAG(output) = "xml";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
