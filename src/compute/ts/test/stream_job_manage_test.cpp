#include <gtest/gtest.h>

#include "compute/sql/include/main/connection.h"
#include "compute/ts/include/stream_agg/ts_stream_agg_job_manage.h"

class StreamJobManageTest : public ::testing::Test {
   protected:
    StreamJobManageTest() {}
    ~StreamJobManageTest() {}
    static void SetUpTestSuite() {
        db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance("./"));
        // 启动db
        db_instance->Init();
        // conn = std::make_unique<Connection>(db_instance);
        // conn->Init();

        // sleep for a while to wait for db to start
        sleep(1);
    }

    // Per-test-suite tear-down.
    // Called after the last test in this test suite.
    // Can be omitted if not needed.
    // static void TearDownTestSuite() { conn.reset(); }
    static void TearDownTestSuite() { }

    void SetUp() override {}

    // void TearDown() override {}

    static std::shared_ptr<IntarkDB> db_instance;
    static std::unique_ptr<Connection> conn;
};

std::shared_ptr<IntarkDB> StreamJobManageTest::db_instance = nullptr;
std::unique_ptr<Connection> StreamJobManageTest::conn = nullptr;

TEST_F(StreamJobManageTest, StreamJobManageTest) {
    Connection conn_s(db_instance);
    conn_s.Init();

    TsStreamAggJobManage ts;
    TsJobInfo ts_job_info;

    auto infos = ts.GetAllValidJob(&conn_s);
    EXPECT_EQ(infos.size(), 0);

    ts_job_info.ts_job.name = "test1";
    ts_job_info.ts_job_err.pid = 1;
    ts_job_info.ts_job_stat.last_start.ts = 0;
    ts_job_info.ts_job_stat.last_finish.ts = 0;
    ts_job_info.ts_job_stat.next_start.ts = 0;
    ts_job_info.ts_job_stat.last_succuss_finish.ts = 0;
    ts_job_info.ts_cagg_policy.table_name = "test1";
    ts_job_info.ts_cagg_policy.exec_sql = "test1";
    ts_job_info.ts_cagg_policy.bucket_width = 1;
    ts_job_info.ts_cagg_policy.bucket_field_name = "key";
    std::string des_sql = "create table test1(i int)";
    EXPECT_TRUE(ts.CreateTsjob(&conn_s, ts_job_info, des_sql) == GS_SUCCESS);

    ts_job_info.ts_job.name = "test2";
    ts_job_info.ts_job_err.pid = 2;
    ts_job_info.ts_cagg_policy.table_name = "test2";
    ts_job_info.ts_cagg_policy.exec_sql = "test2";
    des_sql = "create table test2(i int)";
    EXPECT_TRUE(ts.CreateTsjob(&conn_s, ts_job_info, des_sql) == GS_SUCCESS);

    EXPECT_TRUE(ts.CountTsJob(&conn_s) == 2);

    auto ts_job_infos = ts.GetAllValidJob(&conn_s);
    EXPECT_EQ(ts_job_infos.size(), 2);

    EXPECT_TRUE(ts.StartJob(&conn_s, "test1", 123, 123) == GS_SUCCESS);

    EXPECT_TRUE(ts.PauseJob(&conn_s, "test2") == GS_SUCCESS);
    auto results = ts.GetAllValidJob(&conn_s);
    EXPECT_EQ(results.size(), 1);

    auto job_names = ts.GetAllJobName(&conn_s);
    EXPECT_EQ(job_names.size(), 2);

    if (results.size() > 0) {
        auto ts_job_info = results[0];

        ts_job_info.ts_job_err.err_data = ts_job_info.ts_job.name;
        EXPECT_TRUE(ts.UpdateTsJobErr(&conn_s, ts_job_info.ts_job_err) == GS_SUCCESS);

        ts_job_info.ts_job_stat.total_runs = 10;
        ts_job_info.ts_job_stat.total_failed = 1;
        ts_job_info.ts_job_stat.last_succuss_finish = timestamp_stor_t{12300000000};
        EXPECT_TRUE(ts.UpdateTsJobStat(&conn_s, ts_job_info.ts_job_stat) == GS_SUCCESS);

        auto temp = ts.GetAllValidJob(&conn_s);
        EXPECT_EQ(temp.size(), 1);
        if (temp.size() == 1) {
            auto info = temp[0];
            EXPECT_EQ(info.ts_job.name, "test1");
            EXPECT_EQ(info.ts_job.state, JOB_STATE_VALID);
            EXPECT_EQ(info.ts_job.offset_start, 123);
            EXPECT_EQ(info.ts_job.offset_end, 123);

            EXPECT_EQ(info.ts_job_err.err_data, info.ts_job.name);

            EXPECT_EQ(info.ts_job_stat.total_runs, 10);
            EXPECT_EQ(info.ts_job_stat.total_failed, 1);
            EXPECT_EQ(info.ts_job_stat.last_succuss_finish, timestamp_stor_t{12300000000});
        }

        uint32_t job_id;
        EXPECT_TRUE(ts.GetJobId(&conn_s, ts_job_info.ts_job.name, job_id) == GS_SUCCESS);
        EXPECT_EQ(job_id, ts_job_info.ts_job.job_id);
        EXPECT_EQ(job_id, ts_job_info.ts_job_err.job_id);
        EXPECT_EQ(job_id, ts_job_info.ts_job_stat.job_id);
        EXPECT_EQ(job_id, ts_job_info.ts_cagg_policy.job_id);

        EXPECT_TRUE(ts.DeletetTsJobInfo(&conn_s, job_id) == GS_SUCCESS);
    }
}

int main(int argc, char** argv) {
    system("rm -rf intarkdb/");
    ::testing::GTEST_FLAG(output) = "xml";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}