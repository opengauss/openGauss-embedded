/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* ts_clean_test.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/ts/test/ts_clean_test.cpp
*
* -------------------------------------------------------------------------
*/

#include <gtest/gtest.h>
#include "compute/sql/include/main/connection.h"
#include "compute/ts/include/stream_agg/job_stat.h"
#include <string>
#include <chrono>
#include <thread>

int main(){
    system("rm -rf intarkdb/");
    std::string path = "./";
    auto db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance(path.c_str()));
    db_instance->Init();

    Connection conn(db_instance);
    conn.Init();
    TsTable ts_table;

    std::this_thread::sleep_for(std::chrono::seconds(1));

    TsJob ts_job;
    ts_job.job_id = 1;
    ts_job.name = "ts_job_insert_test1";
    ts_job.state = JOB_STATE_VALID;
    EXPECT_TRUE(ts_table.Insert(&conn, ts_job) == GS_SUCCESS);
    std::vector<TsJob> results_job;
    std::cout<<"insert into ts_job success"<<std::endl;

    TsJobStat ts_job_stat;
    ts_job_stat.job_id = 1;
    ts_job_stat.last_start.ts = 1;
    ts_job_stat.last_finish.ts = 1;
    ts_job_stat.next_start.ts = 1;
    ts_job_stat.last_succuss_finish.ts = 1;
    EXPECT_TRUE(ts_table.Insert(&conn, ts_job_stat) == GS_SUCCESS);
    std::vector<TsJobStat> results_stat;
    std::cout<<"insert into ts_job_stat success"<<std::endl;

    TsCaggPolicy ts_cagg_policy;
    ts_cagg_policy.job_id = 1;
    ts_cagg_policy.table_name = "ts_cagg_policy_test1";
    ts_cagg_policy.exec_sql = "test";
    ts_cagg_policy.bucket_width = 1;
    ts_cagg_policy.bucket_type = TIME_BUCKET;
    EXPECT_TRUE(ts_table.Insert(&conn, ts_cagg_policy) == GS_SUCCESS);
    std::vector<TsCaggPolicy> results_policy;
    std::cout<<"insert into ts_cagg_policy success"<<std::endl;

    TsJobErr ts_job_err;
    ts_job_err.job_id = 1;
    ts_job_err.pid = 1;
    EXPECT_TRUE(ts_table.Insert(&conn, ts_job_err) == GS_SUCCESS);
    std::vector<TsJobErr> results_err;  
    std::cout<<"insert into ts_job_err success"<<std::endl;

    TsJob from, to;
    from = ts_job;
    to = ts_job;
    to.state = JOB_STATE_INVALID;
    EXPECT_TRUE(ts_table.Update(&conn, from, to) == GS_SUCCESS);
    std::cout<<"now update state of ts_job which becomes invalid\n-wait 30s "<<std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(30));

    EXPECT_TRUE(ts_table.Select(&conn, ts_job, results_job) == GS_SUCCESS);
    EXPECT_TRUE(results_job.size() == 0);
    std::cout<<"delete invalid ts_job success "<<std::endl;

    EXPECT_TRUE(ts_table.Select(&conn, ts_job_stat, results_stat) == GS_SUCCESS);
    EXPECT_TRUE(results_stat.size() == 0);
    std::cout<<"delete invalid ts_job_stat success "<<std::endl;

    EXPECT_TRUE(ts_table.Select(&conn, ts_cagg_policy, results_policy) == GS_SUCCESS);
    EXPECT_TRUE(results_policy.size() == 0);
    std::cout<<"delete invalid ts_cagg_policy success "<<std::endl;
    
    EXPECT_TRUE(ts_table.Select(&conn, ts_job_err, results_err) == GS_SUCCESS);
    EXPECT_TRUE(results_err.size() == 0);
    std::cout<<"delete invalid ts_job_err success "<<std::endl;
  
    auto result = conn.Query("insert into ts_job_err(id,job_id,err_data,update_time) values(1,1,'test','2024-03-21');");
    EXPECT_TRUE(result->GetRetCode()==GS_SUCCESS);
    result = conn.Query("insert into ts_job_err(id,job_id,err_data,update_time) values(2,2,'test', now());");
    EXPECT_TRUE(result->GetRetCode()==GS_SUCCESS);
    std::cout<<"insert into outdate error success"<<std::endl;

    std::cout<<"now delete outdate error\n-wait 30s "<<std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(30));
    EXPECT_TRUE(ts_table.Select(&conn, 1, ts_job_err) == GS_SUCCESS);
    EXPECT_EQ(ts_job_err.err_data , "");
    EXPECT_TRUE(ts_table.Select(&conn, 2, ts_job_err) == GS_SUCCESS);
    EXPECT_EQ(ts_job_err.err_data , "test");
    std::cout<<"delete outdate error success"<<std::endl;
}