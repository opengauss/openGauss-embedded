#include <gtest/gtest.h>

#include "compute/sql/include/main/connection.h"
#include "compute/ts/include/table/ts_table.h"

class TsTableTest : public ::testing::Test {
   protected:
    TsTableTest() {}
    ~TsTableTest() {}
    static void SetUpTestSuite() {
        db_instance = std::shared_ptr<IntarkDB>(IntarkDB::GetInstance("./"));
        // 启动db
        db_instance->Init();
        conn = new Connection(db_instance);
        conn->Init();

        // sleep for a while to wait for db to start
        sleep(1);
    }

    // Per-test-suite tear-down.
    // Called after the last test in this test suite.
    // Can be omitted if not needed.
    // static void TearDownTestSuite() { conn.reset(); }
    static void TearDownTestSuite() { delete conn; }

    void SetUp() override {}

    // void TearDown() override {}

    static std::shared_ptr<IntarkDB> db_instance;
    static Connection* conn;

    TsTable ts_table;
};

std::shared_ptr<IntarkDB> TsTableTest::db_instance = nullptr;
Connection* TsTableTest::conn = nullptr;

TEST_F(TsTableTest, TsJobTest) {
    TsJob ts_job;
    ts_job.name = "ts_job_insert_test1";
    ts_job.initial_start.ts = 12;
    ts_job.state = JOB_STATE_VALID;
    EXPECT_TRUE(ts_table.Insert(conn, ts_job) == GS_SUCCESS);

    ts_job.name = "ts_job_insert_test2";
    ts_job.initial_start.ts = 123;
    EXPECT_TRUE(ts_table.Insert(conn, ts_job) == GS_SUCCESS);

    std::vector<TsJob> results;
    EXPECT_TRUE(ts_table.Select(conn, ts_job, results) == GS_SUCCESS);
    EXPECT_EQ(results.size(), 1);
    results.clear();

    TsJob result;
    result.state = JOB_STATE_VALID;
    EXPECT_TRUE(ts_table.Select(conn, result, results) == GS_SUCCESS);
    EXPECT_EQ(results.size(), 2);

    if (results.size() == 2) {
        EXPECT_EQ(results[0].initial_start.ts, 12);
        EXPECT_EQ(results[1].initial_start.ts, 123);

        EXPECT_TRUE(ts_table.Select(conn, results[1].job_id, result) == GS_SUCCESS);

        TsJob from, to;
        from = result;
        to = result;
        to.state = JOB_STATE_PAUSE;
        EXPECT_TRUE(ts_table.Update(conn, from, to) == GS_SUCCESS);

        std::vector<TsJob> temp_results;
        EXPECT_TRUE(ts_table.Select(conn, temp_results) == GS_SUCCESS);
        EXPECT_EQ(temp_results.size(), 2);

        TsJob temp;
        EXPECT_TRUE(ts_table.Select(conn, to.job_id, temp) == GS_SUCCESS);
        EXPECT_EQ(temp.state, JOB_STATE_PAUSE);

        EXPECT_TRUE(ts_table.Delete(conn, temp) == GS_SUCCESS);

        EXPECT_TRUE(ts_table.Select(conn, temp.job_id, temp) == GS_ERROR);
    }
}

TEST_F(TsTableTest, TsCaggPolicyTest) {
    TsCaggPolicy ts_cagg_policy;
    ts_cagg_policy.job_id = 1;
    ts_cagg_policy.table_name = "ts_cagg_policy_test1";
    ts_cagg_policy.exec_sql = "test";
    ts_cagg_policy.bucket_width = 1;
    ts_cagg_policy.bucket_type = TIME_BUCKET;
    EXPECT_TRUE(ts_table.Insert(conn, ts_cagg_policy) == GS_SUCCESS);

    ts_cagg_policy.job_id = 2;
    ts_cagg_policy.table_name = "ts_cagg_policy_test2";
    EXPECT_TRUE(ts_table.Insert(conn, ts_cagg_policy) == GS_SUCCESS);

    std::vector<TsCaggPolicy> results;
    TsCaggPolicy result;
    result.bucket_type = TIME_BUCKET;
    EXPECT_TRUE(ts_table.Select(conn, result, results) == GS_SUCCESS);
    EXPECT_EQ(results.size(), 2);

    if (results.size() == 2) {
        EXPECT_EQ(results[0].table_name, "ts_cagg_policy_test1");
        EXPECT_EQ(results[1].table_name, "ts_cagg_policy_test2");

        EXPECT_TRUE(ts_table.Select(conn, results[1].job_id, result) == GS_SUCCESS);

        TsCaggPolicy from, to;
        from = result;
        to = result;
        to.db_name = "update";
        EXPECT_TRUE(ts_table.Update(conn, from, to) == GS_SUCCESS);

        TsCaggPolicy temp;
        EXPECT_TRUE(ts_table.Select(conn, to.job_id, temp) == GS_SUCCESS);
        EXPECT_EQ(temp.db_name, "update");

        EXPECT_TRUE(ts_table.Delete(conn, temp) == GS_SUCCESS);

        EXPECT_TRUE(ts_table.Select(conn, temp.job_id, temp) == GS_ERROR);
    }
}

TEST_F(TsTableTest, TsJobStatTest) {
    TsJobStat ts_job_stat;
    ts_job_stat.job_id = 1;
    ts_job_stat.last_start.ts = 1;
    ts_job_stat.last_finish.ts = 1;
    ts_job_stat.next_start.ts = 1;
    ts_job_stat.last_succuss_finish.ts = 1;
    EXPECT_TRUE(ts_table.Insert(conn, ts_job_stat) == GS_SUCCESS);

    ts_job_stat.job_id = 2;
    ts_job_stat.last_start.ts = 2;
    ts_job_stat.last_finish.ts = 2;
    ts_job_stat.next_start.ts = 2;
    ts_job_stat.last_succuss_finish.ts = 2;
    EXPECT_TRUE(ts_table.Insert(conn, ts_job_stat) == GS_SUCCESS);

    std::vector<TsJobStat> results;
    TsJobStat result;
    result.total_runs = 0;
    EXPECT_TRUE(ts_table.Select(conn, result, results) == GS_SUCCESS);
    EXPECT_EQ(results.size(), 2);

    if (results.size() == 2) {
        EXPECT_EQ(results[0].job_id, 1);
        EXPECT_EQ(results[0].last_succuss_finish.ts, 1);
        EXPECT_EQ(results[1].job_id, 2);
        EXPECT_EQ(results[1].last_succuss_finish.ts, 2);

        EXPECT_TRUE(ts_table.Select(conn, results[1].job_id, result) == GS_SUCCESS);

        TsJobStat from, to;
        from = result;
        to = result;
        to.total_failed = 3;
        EXPECT_TRUE(ts_table.Update(conn, from, to) == GS_SUCCESS);

        TsJobStat temp;
        EXPECT_TRUE(ts_table.Select(conn, to.job_id, temp) == GS_SUCCESS);
        EXPECT_EQ(temp.total_failed, 3);

        EXPECT_TRUE(ts_table.Delete(conn, temp) == GS_SUCCESS);

        EXPECT_TRUE(ts_table.Select(conn, temp.job_id, temp) == GS_ERROR);
    }
}

TEST_F(TsTableTest, TsJobErrTest) {
    TsJobErr ts_job_err;
    ts_job_err.job_id = 1;
    ts_job_err.pid = 1;
    ts_job_err.err_data = "test";
    EXPECT_TRUE(ts_table.Insert(conn, ts_job_err) == GS_SUCCESS);

    ts_job_err.job_id = 2;
    ts_job_err.pid = 2;
    EXPECT_TRUE(ts_table.Insert(conn, ts_job_err) == GS_SUCCESS);

    std::vector<TsJobErr> results;
    TsJobErr result;
    result.err_data = "test";
    // result.err_data = "";
    EXPECT_TRUE(ts_table.Select(conn, result, results) == GS_SUCCESS);
    EXPECT_EQ(results.size(), 2);

    if (results.size() == 2) {
        EXPECT_EQ(results[0].job_id, 1);
        EXPECT_EQ(results[0].pid, 1);
        EXPECT_EQ(results[1].job_id, 2);
        EXPECT_EQ(results[1].pid, 2);

        EXPECT_TRUE(ts_table.Select(conn, results[1].job_id, result) == GS_SUCCESS);

        TsJobErr from, to;
        from = result;
        to = result;
        to.err_data = "ts_job_err";
        EXPECT_TRUE(ts_table.Update(conn, from, to) == GS_SUCCESS);

        TsJobErr temp;
        EXPECT_TRUE(ts_table.Select(conn, to.job_id, temp) == GS_SUCCESS);
        EXPECT_EQ(temp.err_data, "ts_job_err");

        EXPECT_TRUE(ts_table.Delete(conn, temp) == GS_SUCCESS);

        EXPECT_TRUE(ts_table.Select(conn, temp.job_id, temp) == GS_ERROR);
    }

    ts_job_err.job_id = 3;
    ts_job_err.pid = 3;
    ts_job_err.err_data = "[TimeBucket] job id:100, agg_sql_bucket(INSERT INTO dst_test_job_1 select sum(score) as sum_score, min(date_time) as start_time, max(date_time) as end_time from src_table_ts_agg where date_time >= '2024-04-07 05:45:01.602860' and date_time < '2024-04-07 05:45:01.675459';) exec failed!, err:column ( sum_score ) not nullable!";
    EXPECT_TRUE(ts_table.Insert(conn, ts_job_err) == GS_SUCCESS);
}    

int main(int argc, char** argv) {
    system("rm -rf intarkdb/");
    ::testing::GTEST_FLAG(output) = "xml";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
