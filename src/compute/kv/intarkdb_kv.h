/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* intarkdb_kv.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/kv/intarkdb_kv.h
*
* -------------------------------------------------------------------------
*/

#pragma once

#ifdef _WIN32
#define EXP_SQL_API __declspec(dllexport)
#else
#define EXP_SQL_API __attribute__((visibility("default")))
#endif

#include "interface/c/intarkdb_sql.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------*/
// Type define
/*--------------------------------------------------------------------*/

typedef struct st_intarkdb_connection_kv {
    void *conn;
} *intarkdb_connection_kv;

/* This is the reply object */
typedef struct KvReply_t {
    int type;   /* return type */
    size_t len; /* Length of string */
    char *str;  /* err or value*/
} KvReply;

// --------------------------------------------------------------------------------------------------

EXP_SQL_API int intarkdb_connect_kv(intarkdb_database database, intarkdb_connection_kv *kvconn);

EXP_SQL_API void intarkdb_disconnect_kv(intarkdb_connection_kv *kvconn);

EXP_SQL_API void * intarkdb_set(intarkdb_connection_kv kvconn, const char *key, const char *val);

EXP_SQL_API void * intarkdb_get(intarkdb_connection_kv kvconn, const char *key);

EXP_SQL_API void * intarkdb_del(intarkdb_connection_kv kvconn, const char *key);

EXP_SQL_API void intarkdb_begin(intarkdb_connection_kv kvconn);

EXP_SQL_API void intarkdb_commit(intarkdb_connection_kv kvconn);

EXP_SQL_API void intarkdb_rollback(intarkdb_connection_kv kvconn);

EXP_SQL_API void intarkdb_multi(intarkdb_connection_kv kvconn);

EXP_SQL_API void intarkdb_exec(intarkdb_connection_kv kvconn);

EXP_SQL_API void intarkdb_discard(intarkdb_connection_kv kvconn);

#ifdef __cplusplus
}
#endif
