/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * intarkdb_kv-c.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/kv/intarkdb_kv-c.cpp
 *
 * -------------------------------------------------------------------------
 */
#include <vector> 

#include "intarkdb_kv.h"

#include "compute/kv/kv_connection.h"

struct DatabaseWrapper_kv {
    std::shared_ptr<IntarkDB> instance;
};

KvReply default_reply = { GS_ERROR, 20, (char *)"KvConnection is NULL" };

int intarkdb_connect_kv(intarkdb_database database, intarkdb_connection_kv *kvconn) {
    if (!database || !kvconn) {
        return -1;
    }
    auto wrapper = (DatabaseWrapper_kv *)database;
    KvConnection *connection = nullptr;
    try {
        connection = new KvConnection(wrapper->instance);

        connection->Init();
    } catch (...) {
        if (connection) {
            delete connection;
            connection = nullptr;
        }
        return -1;
    }
    *kvconn = (intarkdb_connection_kv)connection;
    return 0;
}

void intarkdb_disconnect_kv(intarkdb_connection_kv *kvconn) {
    if (kvconn && *kvconn) {
        KvConnection *connection = (KvConnection *)*kvconn;
        delete connection;
        *kvconn = nullptr;
    }
}

void * intarkdb_set(intarkdb_connection_kv kvconn, const char *key, const char *val) {
    if (!kvconn) {
        return &default_reply;
    }

    KvConnection *conn = (KvConnection *)kvconn;
    return conn->Set(key, val);
}

void * intarkdb_get(intarkdb_connection_kv kvconn, const char *key) {
    if (!kvconn) {
        return &default_reply;
    }

    KvConnection *conn = (KvConnection *)kvconn;
    return conn->Get(key);
}

void * intarkdb_del(intarkdb_connection_kv kvconn, const char *key) {
    if (!kvconn) {
        return &default_reply;
    }

    KvConnection *conn = (KvConnection *)kvconn;
    return conn->Del(key);
}

void intarkdb_begin(intarkdb_connection_kv kvconn) {
    if (!kvconn) {
        return;
    }

    KvConnection *conn = (KvConnection *)kvconn;
    return conn->Begin();
}

void intarkdb_commit(intarkdb_connection_kv kvconn) {
    if (!kvconn) {
        return;
    }

    KvConnection *conn = (KvConnection *)kvconn;
    return conn->Commit();
}

void intarkdb_rollback(intarkdb_connection_kv kvconn) {
    if (!kvconn) {
        return;
    }

    KvConnection *conn = (KvConnection *)kvconn;
    return conn->Rollback();
}

void intarkdb_multi(intarkdb_connection_kv kvconn) {
    intarkdb_begin(kvconn);
}

void intarkdb_exec(intarkdb_connection_kv kvconn) {
    intarkdb_commit(kvconn);
}

void intarkdb_discard(intarkdb_connection_kv kvconn) {
    intarkdb_rollback(kvconn);
}
