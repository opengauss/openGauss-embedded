/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * nested_inner_join_exec.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/planner/physical_plan/nested_inner_join_exec.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once
#include "binder/table_ref/bound_join.h"
#include "planner/physical_plan/physical_plan.h"
#include "planner/physical_plan/seq_scan_exec.h"

class InnerJoinExec : public PhysicalPlan {
   public:
    InnerJoinExec(const Schema& schema, PhysicalPlanPtr left, PhysicalPlanPtr right)
        : schema_(schema), left_(left), right_(right) {}

    virtual Schema GetSchema() const override { return schema_; }

    virtual auto Execute() const -> RecordBatch override { return RecordBatch({}); }

    virtual std::vector<PhysicalPlanPtr> Children() const override { return {left_, right_}; }

    virtual std::string ToString() const override { return "InnerJoinExec"; }

    virtual auto Next() -> std::tuple<Record, knl_cursor_t*, bool> override;

    virtual void ResetNext() override;

   private:
    Schema schema_;
    PhysicalPlanPtr left_;  // probe_table
    PhysicalPlanPtr right_;
    bool left_eof_{false};
    bool right_eof_{true};
    Record curr_record_;
    uint32_t idx_{0};
};
