/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * aggregate_exec.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/planner/physical_plan/aggregate_exec.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once

#include <map>
#include <set>
#include <vector>

#include "common/hash_util.h"
#include "planner/expressions/column_value_expression.h"
#include "planner/physical_plan/physical_plan.h"
#include "planner/physical_plan/distinct_exec.h"

struct AggKey {
    bool operator<(const AggKey& rhs) const {
        if (keys_.size() != rhs.keys_.size()) {
            return keys_.size() < rhs.keys_.size();
        }

        for (size_t i = 0; i < keys_.size(); ++i) {
            if (keys_[i].IsNull() || rhs.keys_[i].IsNull()) {
                if (keys_[i].IsNull() && !rhs.keys_[i].IsNull()) {
                    return false;
                }
                if (!keys_[i].IsNull() && rhs.keys_[i].IsNull()) {
                    return true;
                }
                continue;
            }
            if (keys_[i].GetType() < rhs.keys_[i].GetType()) {
                return false;
            }

            auto res = keys_[i].LessThan(rhs.keys_[i]);
            if (res == Trivalent::TRI_FALSE || res == Trivalent::UNKNOWN) {
                if (keys_[i].Equal(rhs.keys_[i]) == Trivalent::TRI_TRUE) {
                    continue;
                }
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    void Clear() { return keys_.clear(); }
    void AddItem(const Value& value) { keys_.push_back(value); }
    size_t ItemSize() const { return keys_.size(); }

    friend class std::hash<AggKey>;

   private:
    std::vector<Value> keys_;
};

namespace std {
template <>
struct hash<AggKey> {
    auto operator()(const AggKey& key) const -> size_t {
        size_t hash = 0;
        for (const auto& k : key.keys_) {
            if (!k.IsNull()) {
                if (k.IsDecimal()) {
                    // dec4_t 特殊处理
                    auto decimal = k.GetCastAs<dec4_t>();
                    hash = HashUtil::CombineHash(
                        hash, HashUtil::HashBytes(reinterpret_cast<const char*>(&decimal), cm_dec4_stor_sz(&decimal)));
                } else {
                    hash = HashUtil::CombineHash(hash, HashUtil::HashBytes(k.GetRawBuff(), k.Size()));
                }
            }
        }
        return hash;
    }
};
}  // namespace std

class AggregateExec : public PhysicalPlan {
   public:
    AggregateExec(std::vector<std::unique_ptr<Expression>> groupby, std::vector<std::string> ops,
                  std::vector<std::unique_ptr<Expression>> be_group, const std::vector<bool>& distincts, Schema schema,
                  PhysicalPlanPtr child);

    virtual Schema GetSchema() const override;

    virtual std::vector<PhysicalPlanPtr> Children() const override { return {child_}; }

    virtual std::string ToString() const override { return "AggreateExec"; }

    void Init();
    virtual auto Next() -> std::tuple<Record, knl_cursor_t*, bool> override;

    // depercated
    virtual auto Execute() const -> RecordBatch override { return RecordBatch({}); }

    virtual void ResetNext() override {
        init_ = false;
        child_->ResetNext();
        for (auto& group : groups_) {
            if (group) {
                group->Reset();
            }
        }
        for (auto& be_group : be_groups_) {
            if (be_group) {
                be_group->Reset();
            }
        }
    }

   private:
    Schema schema_;
    PhysicalPlanPtr child_;
    std::vector<std::unique_ptr<Expression>> groups_;     // 聚合条件
    std::vector<std::string> ops_;                        // 聚合函数字符串
    std::vector<std::unique_ptr<Expression>> be_groups_;  // 聚合函数表达式
    std::map<AggKey, Record> groups_map_;
    std::map<AggKey, Record>::iterator groups_map_iter_;
    std::unordered_map<DistinctKey, long> count_map_;
    std::map<int, std::map<AggKey, int>> avg_idx_count;  // for AVG，记录每个分组的非空行数
    bool init_{false};
    std::vector<bool> distincts;
    std::vector<std::set<AggKey>> be_groups_distinct_set;
};
