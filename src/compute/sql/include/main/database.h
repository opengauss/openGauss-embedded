/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * database.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/main/database.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once

#include <cstring>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>

#include "binder/binder.h"
#include "binder/statement/create_index_statement.h"
#include "binder/statement/create_statement.h"
#include "catalog/catalog.h"
#include "planner/planner.h"

const uint32_t MAX_DB_INSTANCE_COUNT = 32;

class IntarkDB : std::enable_shared_from_this<IntarkDB> {
   private:
    struct PassKey {
        explicit PassKey() {}
        ~PassKey() {}
    };

   public:
    // passkey idom , 避免外部直接构造
    explicit IntarkDB(PassKey passkey, std::shared_ptr<BaseStorage> storage, std::string path);
    explicit IntarkDB(std::shared_ptr<BaseStorage> storage);
    EXPORT_API ~IntarkDB();

    // TODO: remove
    EXPORT_API void Init() {}
    EXPORT_API bool HasInit() { return true; }

    EXPORT_API static std::shared_ptr<IntarkDB> GetInstance(const char* path);

   public:
    EXPORT_API static std::shared_ptr<BaseStorage> GetStorage(const std::string& in_path);
    EXPORT_API void DestoryHandle(void* handle);
    EXPORT_API status_t AllocHandle(void** handle);
    EXPORT_API bool IsDBAvailable();
    static thread_t * db_get_streamagg_main_thread(std::shared_ptr<BaseStorage> storage);
    uint32_t db_get_streamagg_threadpool_num();

    std::string GetDbPath() { return path_; }

   private:
    IntarkDB() = delete;
    IntarkDB(const IntarkDB& db) = delete;
    IntarkDB& operator=(IntarkDB& db) = delete;

   private:
    std::shared_ptr<BaseStorage> storage_;
    std::string path_;

    // static
    static std::mutex mutex_;
    static std::unordered_map<std::string, std::weak_ptr<BaseStorage>> instance_map_;
};
