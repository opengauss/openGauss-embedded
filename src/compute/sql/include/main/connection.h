/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * connection.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/main/connection.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once

#include <iostream>
#include <memory>

#include "common/record_batch.h"
#include "common/record_streaming.h"
#include "main/database.h"
#include "main/prepare_statement.h"

class Connection {
   public:
    EXPORT_API explicit Connection(std::shared_ptr<IntarkDB> instance);
    EXPORT_API ~Connection();

    EXPORT_API void Init();

    EXPORT_API std::unique_ptr<RecordBatch> Query(const char* query);
    EXPORT_API std::unique_ptr<RecordIterator> QueryIterator(const char* query);

    EXPORT_API std::unique_ptr<PreparedStatement> Prepare(const std::string& query);
    std::vector<std::unique_ptr<BoundStatement>> ParseStatementsInternal(const std::string& query);
    std::unique_ptr<PreparedStatement> CreatePreparedStatement(std::unique_ptr<BoundStatement> statement);
    std::unique_ptr<RecordBatch> ExecuteStatement(const std::string& query, std::unique_ptr<BoundStatement> statement);
    std::unique_ptr<RecordStreaming> ExecuteStatementStreaming(std::unique_ptr<BoundStatement> statement);
    auto CreateBinder() -> Binder;

    Planner CreatePlanner();

    int CreateTable(const CreateStatement& stmt);

    int CreateIndex(const CreateIndexStatement& stmt);

    int CreateSequence(const CreateSequenceStatement& stmt);

    int AlterTable(const AlterStatement& stmt);

    bool TruncateTable(const std::string& query, const DeleteStatement& stmt, bool is_prepare);

    int CtasTable(CtasStatement& stmt, RecordBatch& result);

    EXPORT_API std::unique_ptr<TableInfo> GetTableInfo(const std::string& table_name);

    int CreateView(const std::string& viewName, const std::vector<Column>& columns, const std::string& query,
                   bool ignoreConflict);

    EXPORT_API std::string ShowCreateTable(const std::string& table_name, std::vector<std::string>& index_sqls);

    auto CopyFrom(CopyStatement& stmt) -> int;
    auto CopyTo(CopyStatement& stmt, int64_t& effect_row) -> int;
    auto CopyFromInsertStatement(CopyStatement& stmt) -> std::unique_ptr<PreparedStatement>;

    int CallTsFunc(CallStatement& stmt, RecordBatch& result);

   public:
    EXPORT_API void* GetStorageHandle() { return handle_; }
    std::weak_ptr<IntarkDB> GetStorageInstance() { return instance_; }
    bool IsAutoCommit();

    void SetNeedResultSetEx(bool need) { is_need_result_ex = need; }

    void SetLimitRowsEx(uint64_t limit) { limit_rows_ex = limit; }

   private:
    void SetBeginTransaction(TransactionType type);

    std::weak_ptr<IntarkDB> instance_;
    void* handle_{NULL};
    duckdb::PostgresParser parser_;
    std::unique_ptr<Catalog> catalog_;
    bool is_autocommit_param = true;
    bool is_begin_transaction = false;

    // if need insert resultset
    bool is_need_result_ex = false;
    // if need limit rows
    uint64_t limit_rows_ex = 0;
};
