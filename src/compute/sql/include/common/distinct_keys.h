/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* distinct_keys.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/include/common/distinct_keys.h
*
* -------------------------------------------------------------------------
*/
#pragma once

#include <unordered_set>
#include <utility>
#include <vector>

#include "common/hash_util.h"
#include "type/troolean.h"
#include "type/value.h"

// mulit key map
struct DistinctKey {
    bool operator==(const DistinctKey& rhs) const {
        if (keys_.size() != rhs.keys_.size()) {
            return false;
        }
        for (size_t i = 0; i < keys_.size(); ++i) {
            if (keys_[i].IsNull() || rhs.keys_[i].IsNull()) {
                if (keys_[i].IsNull() != rhs.keys_[i].IsNull()) {
                    return false;
                }
                continue;  // 都是null ，跳过后续比较
            }
            if (keys_[i].GetType() != rhs.keys_[i].GetType()) {
                return false;
            }
            // 除了两个都是null之外，FALSE 和 UNKNOWN  都认为不相等
            auto res = keys_[i].Equal(rhs.keys_[i]);
            if (res == Trivalent::TRI_FALSE || res == Trivalent::UNKNOWN) {
                return false;
            }
        }
        return true;
    }

    void AddItem(const Value& key) { keys_.push_back(key); }
    void AddItem(Value&& key) { keys_.push_back(std::move(key)); }
    void Clear() { keys_.clear(); }
    const std::vector<Value>& Keys() const { return keys_; }

    friend class std::hash<DistinctKey>;

   private:
    std::vector<Value> keys_;
};

namespace std {
template <>
struct hash<DistinctKey> {
    // hash function for DistinctKey
    auto operator()(const DistinctKey& key) const -> size_t {
        size_t hash = 0;
        for (const auto& k : key.keys_) {
            if (!k.IsNull()) {
                if (k.IsDecimal()) {
                    // dec4_t 特殊处理
                    auto decimal = k.GetCastAs<dec4_t>();
                    hash = HashUtil::CombineHash(
                        hash, HashUtil::HashBytes(reinterpret_cast<const char*>(&decimal), cm_dec4_stor_sz(&decimal)));
                } else {
                    hash = HashUtil::CombineHash(hash, HashUtil::HashBytes(k.GetRawBuff(), k.Size()));
                }
            }
        }
        return hash;
    }
};
}  // namespace std

using DistinctKeySet = std::unordered_set<DistinctKey>;

template<typename T>
using DistinctKeyMap = std::unordered_map<DistinctKey, T, std::hash<DistinctKey>>;
