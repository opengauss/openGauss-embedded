/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * record_batch.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/common/record_batch.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once

#include <fmt/color.h>
#include <fmt/core.h>
#include <fmt/format.h>

#include <iostream>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include "binder/statement_type.h"
#include "catalog/schema.h"
#include "type/value.h"

enum class RecordBatchType : uint8_t {
    Invalid,
    Select,
    Delete,
    Update,
};

class Record {
   public:
    EXPORT_API Record() = default;
    EXPORT_API Record(std::unordered_map<uint16_t, Value>&& v);
    EXPORT_API Record(const std::vector<Value>& v);
    EXPORT_API Record(std::vector<Value>&& v);
    EXPORT_API Record(const Record& other) : values(other.values) {}
    EXPORT_API Record(Record&& other) noexcept : values(std::move(other.values)) {}
    EXPORT_API Record& operator=(const Record& other) {
        if (this != &other) {
            values = other.values;
        }
        return *this;
    }
    EXPORT_API Record& operator=(Record&& other) noexcept {
        if (this != &other) {
            values = std::move(other.values);
        }
        return *this;
    }

    EXPORT_API uint32_t ColumnCount() const;

    // slot 为下标
    // TODO: 返回引用？
    EXPORT_API Value Field(uint16_t slot) const;
    EXPORT_API const Value& FieldRef(uint16_t slot) const;
    EXPORT_API Value&& FieldMove(uint16_t slot);

    EXPORT_API void SetField(uint16_t slot, const Value& v);
    EXPORT_API Value& FieldMutable(uint16_t slot);

    EXPORT_API Record Concat(const Record& other);

   private:
    std::vector<Value> values;
};

auto RecordToVector(const Record& record) -> std::vector<Value>;

class RecordBatch {
   public:
    EXPORT_API RecordBatch(const Schema& schema) : schema_(schema) {}
    EXPORT_API uint32_t RowCount() { return records.size(); }
    EXPORT_API uint32_t ColumnCount() const { return schema_.GetColumnInfos().size(); }

    EXPORT_API Record Row(uint32_t row_idx) { return records[row_idx]; }
    EXPORT_API Record& RowMutable(uint32_t row_idx) { return records[row_idx]; }
    EXPORT_API const Record& RowRef(uint32_t row_idx) const { return records[row_idx]; }

    EXPORT_API void Clear() { records.clear(); }
    EXPORT_API bool AddRecord(const Record& rec) {
        // TODO check schema
        records.push_back(rec);
        return true;
    }
    EXPORT_API bool AddRecord(Record&& rec) {
        records.push_back(std::move(rec));
        return true;
    }

    EXPORT_API const Schema& GetSchema() const { return schema_; }
    EXPORT_API Schema& GetSchemaMutable() { return schema_; }

    EXPORT_API void SetRecordBatchType(RecordBatchType type) { rb_type = type; }
    EXPORT_API RecordBatchType GetRecordBatchType() const { return rb_type; }

   public:
    EXPORT_API void SetPragmaAttr(bool is_pragma) { is_pragma_ = is_pragma; }
    EXPORT_API bool GetPragmaAttr() const { return is_pragma_; }

    EXPORT_API void SetRetCode(int32_t code) { ret_code = code; }
    EXPORT_API int32_t GetRetCode() { return ret_code; }

    EXPORT_API void SetRetMsg(std::string msg) { ret_msg = msg; }
    EXPORT_API const std::string& GetRetMsg() { return ret_msg; }

    EXPORT_API int GetEffectRow() const { return effect_row; }
    EXPORT_API StatementType GetStmtType() const { return stmt_type; }

    EXPORT_API std::vector<std::vector<std::string>> GetRecords(std::string null_str = "") const;
    EXPORT_API void Print();
    int status;

    StatementType stmt_type;
    int64_t effect_row{0};

   private:
    std::vector<Record> records;
    Schema schema_;

    RecordBatchType rb_type = RecordBatchType::Invalid;
    bool is_pragma_{false};

    int32_t ret_code{0};
    std::string ret_msg{"Sql success!"};
};
