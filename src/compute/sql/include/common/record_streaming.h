/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * record_streaming.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/common/record_streaming.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once

#include "common/record_batch.h"
#include "planner/physical_plan/physical_plan.h"

class RecordStreaming {
   public:
    EXPORT_API RecordStreaming(const Schema& schema, PhysicalPlanPtr plan) : schema_(schema), plan_{plan} {}
    EXPORT_API uint32_t ColumnCount() const { return schema_.GetColumnInfos().size(); }

    EXPORT_API const Schema& GetSchema() const { return schema_; }

   public:
    EXPORT_API void SetPragmaAttr(bool is_pragma) { is_pragma_ = is_pragma; }
    EXPORT_API bool GetPragmaAttr() const { return is_pragma_; }

    EXPORT_API void SetRetCode(int32_t code) { ret_code = code; }
    EXPORT_API int32_t GetRetCode() { return ret_code; }

    EXPORT_API void SetRetMsg(std::string msg) { ret_msg = msg; }
    EXPORT_API const std::string& GetRetMsg() { return ret_msg; }

    EXPORT_API StatementType GetStmtType() const { return stmt_type; }
    EXPORT_API void SetStmtType(StatementType type) { stmt_type = type; }

    EXPORT_API std::tuple<Record, bool> Next() const {
        auto&& [r, _, eof] = plan_->Next();
        return {std::move(r), eof};
    }

   private:
    Schema schema_;
    PhysicalPlanPtr plan_;

    StatementType stmt_type;
    bool is_pragma_{false};

    int32_t ret_code{0};
    std::string ret_msg{"Sql success!"};
};

class RecordIterator {
   public:
    EXPORT_API RecordIterator(std::unique_ptr<RecordStreaming> rs) : rs_(std::move(rs)) {}
    EXPORT_API RecordIterator(std::unique_ptr<RecordBatch> rb) : rb_(std::move(rb)) {}
    EXPORT_API std::tuple<Record, bool> Next() {
        if (rb_ && index_ < rb_->RowCount()) {
            return {rb_->Row(index_++), false};
        }
        if (rs_) {
            return rs_->Next();
        }
        return {Record{}, true};
    }

    EXPORT_API int32_t GetRetCode() { return rb_ ? rb_->GetRetCode() : rs_->GetRetCode(); }

    EXPORT_API const std::string& GetRetMsg() { return rb_ ? rb_->GetRetMsg() : rs_->GetRetMsg(); }

    EXPORT_API StatementType GetStmtType() const { return rb_ ? rb_->GetStmtType() : rs_->GetStmtType(); }

    EXPORT_API RecordBatchType GetBatchType() const {return rb_->GetRecordBatchType(); }

    EXPORT_API const Schema& GetSchema() const { return rb_ ? rb_->GetSchema() : rs_->GetSchema(); }

    EXPORT_API uint64_t GetEffectRow() const { return rb_ ? rb_->GetEffectRow() : 0; }

    EXPORT_API auto GetHeader() -> std::vector<std::string> const;

   private:
    size_t index_{0};
    std::unique_ptr<RecordBatch> rb_{nullptr};
    std::unique_ptr<RecordStreaming> rs_{nullptr};
};
