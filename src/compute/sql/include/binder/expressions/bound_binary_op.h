/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * bound_binary_op.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/binder/expressions/bound_binary_op.h
 *
 * -------------------------------------------------------------------------
 */

#pragma once

#include <fmt/core.h>
#include <fmt/format.h>

#include <memory>
#include <string>

#include "binder/bound_expression.h"
#include "common/string_util.h"
#include "type/type_id.h"

// 判断是否比较操作符
static bool IsCompareOp(const std::string& op_name) {
    return op_name == "=" || op_name == "==" || op_name == "!=" || op_name == "<>" || op_name == "<" ||
           op_name == ">" || op_name == "<=" || op_name == ">=";
}

const char* const OR_FUNC_NAME = "or";
const char* const AND_FUNC_NAME = "and";

// 判断是否逻辑操作符
static bool IsLogicalOp(const std::string& op_name) {
    return intarkdb::StringUtil::IsEqualIgnoreCase(op_name, AND_FUNC_NAME) ||
           intarkdb::StringUtil::IsEqualIgnoreCase(op_name, OR_FUNC_NAME);
}

// 二元操作符
class BoundBinaryOp : public BoundExpression {
   public:
    explicit BoundBinaryOp(std::string op_name, std::unique_ptr<BoundExpression> larg,
                           std::unique_ptr<BoundExpression> rarg)
        : BoundExpression(ExpressionType::BINARY_OP),
          opname_(std::move(op_name)),
          left_arg_(std::move(larg)),
          right_arg_(std::move(rarg)) {}

    auto ToString() const -> std::string override { return fmt::format("({}{}{})", left_arg_, opname_, right_arg_); }

    auto HasAggregation() const -> bool override { return left_arg_->HasAggregation() || right_arg_->HasAggregation(); }

    virtual auto HasSubQuery() const -> bool override { return left_arg_->HasSubQuery() || right_arg_->HasSubQuery(); }

    virtual auto HasParameter() const -> bool override {
        return left_arg_->HasParameter() || right_arg_->HasParameter();
    }

    BoundExpression& Left() { return *left_arg_; }
    BoundExpression& Right() { return *right_arg_; }

    std::unique_ptr<BoundExpression>& LeftPtr() { return left_arg_; }
    std::unique_ptr<BoundExpression>& RightPtr() { return right_arg_; }

    const std::string OpName() const { return opname_; }

    virtual auto ToColumn(LogicalPlanPtr plan) const -> Column override {
        auto def = exp_column_def_t{};
        def.col_type = ColType();
        def.nullable = true;
        return Column(ToString(), def);
    }

    virtual hash_t Hash() const override {
        auto h = HashUtil::HashBytes(opname_.c_str(), opname_.length());
        h = HashUtil::CombineHash(h, left_arg_->Hash());
        return HashUtil::CombineHash(h, right_arg_->Hash());
    }

    virtual std::unique_ptr<BoundExpression> Copy() const override {
        return std::make_unique<BoundBinaryOp>(opname_, left_arg_->Copy(), right_arg_->Copy());
    }

    virtual GStorDataType ColType() const override {
        auto result_type = GStorDataType::GS_TYPE_UNKNOWN;
        if (IsCompareOp(opname_) || IsLogicalOp(opname_)) {
            result_type = GStorDataType::GS_TYPE_BOOLEAN;
        } else {
            result_type = intarkdb::GetCompatibleType(left_arg_->ColType(), right_arg_->ColType()).TypeId();
        }
        if (opname_ == "/") {
            result_type = (result_type == GStorDataType::GS_TYPE_DECIMAL) ? GStorDataType::GS_TYPE_DECIMAL
                                                                          : GStorDataType::GS_TYPE_REAL;
        }
        return result_type;
    }

    virtual auto Equals(const BoundExpression& other) const -> bool override {
        if (other.Type() != ExpressionType::BINARY_OP) {
            return false;
        }
        auto& other_binary = static_cast<const BoundBinaryOp&>(other);
        return opname_ == other_binary.opname_ && left_arg_->Equals(*other_binary.left_arg_) &&
               right_arg_->Equals(*other_binary.right_arg_);
    }

   private:
    std::string opname_;

    std::unique_ptr<BoundExpression> left_arg_;

    std::unique_ptr<BoundExpression> right_arg_;
};
