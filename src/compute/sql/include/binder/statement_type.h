/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* statement_type.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/include/binder/statement_type.h
*
* -------------------------------------------------------------------------
*/
#pragma once

#include <fmt/format.h>

#include <cstdint>
#include <string_view>

enum class StatementType : uint8_t {
    INVALID_STATEMENT,        // invalid statement type
    SELECT_STATEMENT,         // select statement type
    INSERT_STATEMENT,         // insert statement type
    UPDATE_STATEMENT,         // update statement type
    CREATE_STATEMENT,         // create statement type
    DELETE_STATEMENT,         // delete statement type
    EXPLAIN_STATEMENT,        // explain statement type
    DROP_STATEMENT,           // drop statement type
    INDEX_STATEMENT,          // index statement type
    PREPARE_STATEMENT,        // prepare statement type
    EXECUTE_STATEMENT,        // execute statement type
    ALTER_STATEMENT,          // alter statement type
    TRANSACTION_STATEMENT,    // transaction statement type,
    COPY_STATEMENT,           // copy type
    ANALYZE_STATEMENT,        // analyze type
    VARIABLE_SET_STATEMENT,   // variable set statement type
    VARIABLE_SHOW_STATEMENT,  // show variable statement type
    CREATE_FUNC_STATEMENT,    // create func statement type
    EXPORT_STATEMENT,         // EXPORT statement type
    PRAGMA_STATEMENT,         // PRAGMA statement type
    SHOW_STATEMENT,           // SHOW statement type
    VACUUM_STATEMENT,         // VACUUM statement type
    CALL_STATEMENT,           // CALL statement type
    SET_STATEMENT,            // SET statement type
    LOAD_STATEMENT,           // LOAD statement type
    RELATION_STATEMENT,
    EXTENSION_STATEMENT,
    LOGICAL_PLAN_STATEMENT,
    ATTACH_STATEMENT,
    DETACH_STATEMENT,
    MULTI_STATEMENT,
    CTAS_STATEMENT,     // create table as select statement type
    SEQUENCE_STATEMENT,  // sequence statement type
    CREATE_VIEW_STATEMENT,             // CREATE VIEW statement type
    CHECKPOINT_STATEMENT,
    COMMENT_STATEMENT,
    CAGG_POLICY_STATEMENT
};

template <>
struct fmt::formatter<StatementType> : formatter<std::string_view> {
    template <typename FormatContext>
    auto format(StatementType c, FormatContext &ctx) const {
        std::string_view name;
        switch (c) {
            case StatementType::INVALID_STATEMENT:
                name = "Invalid";
                break;
            case StatementType::SELECT_STATEMENT:
                name = "Select";
                break;
            case StatementType::INSERT_STATEMENT:
                name = "Insert";
                break;
            case StatementType::UPDATE_STATEMENT:
                name = "Update";
                break;
            case StatementType::CREATE_STATEMENT:
                name = "Create";
                break;
            case StatementType::DELETE_STATEMENT:
                name = "Delete";
                break;
            case StatementType::EXPLAIN_STATEMENT:
                name = "Explain";
                break;
            case StatementType::DROP_STATEMENT:
                name = "Drop";
                break;
            case StatementType::INDEX_STATEMENT:
                name = "Index";
                break;
            case StatementType::VARIABLE_SHOW_STATEMENT:
                name = "VariableShow";
                break;
            case StatementType::VARIABLE_SET_STATEMENT:
                name = "VariableSet";
                break;
            case StatementType::TRANSACTION_STATEMENT:
                name = "Transaction";
                break;
            case StatementType::SET_STATEMENT:
                name = "Set";
                break;
            case StatementType::SEQUENCE_STATEMENT:
                name = "Sequence";
                break;
        }
        return formatter<std::string_view>::format(name, ctx);
    }
};