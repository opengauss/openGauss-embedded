/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* column_alias_binder.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/include/binder/column_alias_binder.h
*
* -------------------------------------------------------------------------
*/
#pragma once

#include <stdint.h>

#include <unordered_set>

// helper binder which support alias as columnref
// class ColumnAliasBinder {
//    public:
//     ColumnAliasBinder();

//     bool BindAlias();

//    private:
//     std::unordered_set<int32_t> visited_select_indexes;
// };
