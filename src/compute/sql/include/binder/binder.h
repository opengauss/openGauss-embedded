/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * binder.h
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/include/binder/binder.h
 *
 * -------------------------------------------------------------------------
 */
#pragma once

#include <assert.h>

#include <memory>
#include <optional>
#include <set>
#include <vector>

#include "binder/bound_expression.h"
#include "binder/bound_sort.h"
#include "binder/bound_statement.h"
#include "binder/expressions/bound_alias.h"
#include "binder/expressions/bound_column_def.h"
#include "binder/expressions/bound_parameter.h"
#include "binder/statement/alter_statement.h"
#include "binder/statement/cagg_policy_statement.h"
#include "binder/statement/checkpoint_statement.h"
#include "binder/statement/comment_statement.h"
#include "binder/statement/constraint.h"
#include "binder/statement/copy_statement.h"
#include "binder/statement/create_index_statement.h"
#include "binder/statement/create_sequence_statement.h"
#include "binder/statement/create_statement.h"
#include "binder/statement/create_view.h"
#include "binder/statement/ctas_statement.h"
#include "binder/statement/delete_statement.h"
#include "binder/statement/drop_statement.h"
#include "binder/statement/explain_statement.h"
#include "binder/statement/insert_statement.h"
#include "binder/statement/select_statement.h"
#include "binder/statement/set_statement.h"
#include "binder/statement/show_statement.h"
#include "binder/statement/transaction_statement.h"
#include "binder/statement/update_statement.h"
#include "binder/statement/call_statement.h"
#include "binder/table_ref/bound_base_table.h"
#include "binder/table_ref/bound_join.h"
#include "binder/table_ref/bound_subquery.h"
#include "catalog/catalog.h"
#include "nodes/parsenodes.hpp"
#include "nodes/pg_list.hpp"
#include "pg_definitions.hpp"
#include "postgres_parser.hpp"

static const std::string RETENTION_DEFAULT = "7d";

constexpr int32_t INVALID_IDX = INT32_MAX;

// 实体表的列绑定
class TableBinding {
    std::string table_name_or_alias_;
    std::vector<std::string> col_names_;
    std::vector<LogicalType> col_types_;
    // std::unordered_map<std::string, int> col_name_to_idx_;

   public:
    TableBinding(const std::string &table_name_or_alias, std::vector<std::string> &&col_names,
                 std::vector<LogicalType> &&col_types)
        : table_name_or_alias_(table_name_or_alias),
          col_names_(std::move(col_names)),
          col_types_(std::move(col_types)) {}
    const std::string &GetTableName() const { return table_name_or_alias_; }
    const std::vector<std::string> &GetColNames() const { return col_names_; }
    const std::vector<LogicalType> &GetColTypes() const { return col_types_; }
};

struct AliasIdx {
    int idx;
    LogicalType type;
};

struct BinderContext {
    std::unordered_map<std::string, AliasIdx> alias_binding;  // alias -> idx
    std::vector<std::unique_ptr<BoundExpression>> correlated_columns;
    std::unordered_map<std::string, int> table_bindings;  // table_name -> table_binding
    std::vector<TableBinding> table_bindings_vec;

    auto AddTableBinding(const std::string &table_name_or_alias, const std::vector<Column> &cols) -> void {
        auto binding_iter = table_bindings.find(table_name_or_alias);
        if (binding_iter != table_bindings.end()) {
            throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("Duplicate table {} ", table_name_or_alias));
        }
        std::vector<std::string> col_names;
        std::vector<LogicalType> col_types;
        for (auto &col : cols) {
            col_names.push_back(col.NameWithoutPrefix());
            col_types.push_back(col.GetLogicalType());
        }
        table_bindings_vec.emplace_back(table_name_or_alias, std::move(col_names), std::move(col_types));
        table_bindings[table_name_or_alias] = table_bindings_vec.size() - 1;
    }

    auto AddTableBinding(const std::string &table_name_or_alias, const std::vector<SchemaColumnInfo> &col_infos)
        -> void {
        auto binding_iter = table_bindings.find(table_name_or_alias);
        if (binding_iter != table_bindings.end()) {
            throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("Duplicate table {} ", table_name_or_alias));
        }
        std::vector<std::string> col_names;
        std::vector<LogicalType> col_types;
        for (auto &col_info : col_infos) {
            col_names.push_back(col_info.col_name.back());  // 只保留列名
            col_types.push_back(col_info.col_type);
        }
        table_bindings_vec.emplace_back(table_name_or_alias, std::move(col_names), std::move(col_types));
        table_bindings[table_name_or_alias] = table_bindings_vec.size() - 1;
    }

    auto AddTableBinding(const std::string &table_name_or_alias, std::vector<std::string> &&col_name,
                         std::vector<LogicalType> &&col_type) -> void {
        auto binding_iter = table_bindings.find(table_name_or_alias);
        if (binding_iter != table_bindings.end()) {
            throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("Duplicate table {} ", table_name_or_alias));
        }
        table_bindings_vec.emplace_back(table_name_or_alias, std::move(col_name), std::move(col_type));
        table_bindings[table_name_or_alias] = table_bindings_vec.size() - 1;
    }

    auto MergeTableBinding(BinderContext &other) -> void {
        for (auto &binding : other.table_bindings_vec) {
            std::string table_name = binding.GetTableName();
            auto binding_iter = table_bindings.find(table_name);
            if (binding_iter != table_bindings.end()) {
                throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("Duplicate table {} ", table_name));
            }
            table_bindings_vec.emplace_back(std::move(binding));
            table_bindings[table_name] = table_bindings_vec.size() - 1;
        }
    }

    auto GetTableBinding(const std::string &table_name) const -> const TableBinding * {
        auto it = table_bindings.find(table_name);
        if (it == table_bindings.end() || (size_t)it->second >= table_bindings_vec.size()) {
            return nullptr;
        }
        return &table_bindings_vec[it->second];
    }

    auto GetAllTableBindings() const -> const std::vector<TableBinding> & { return table_bindings_vec; }

    auto BuildProjectionAndAlias(const std::vector<std::unique_ptr<BoundExpression>> &select_list) -> void {
        for (size_t i = 0; i < select_list.size(); i++) {
            auto &expr = select_list[i];
            if (expr->Type() == ExpressionType::ALIAS) {
                const auto &name = expr->GetName();
                const auto &alias = name.back();
                auto alias_iter = alias_binding.find(alias);
                if (alias_iter == alias_binding.end()) {
                    alias_binding[alias] = {(int)i, expr->ReturnType()};
                }
            }
        }
    }
};

class Binder {
   public:
    Binder(const Catalog &catalog, duckdb::PostgresParser &parse)
        : catalog_(catalog), parser_(parse), parent_binder_(nullptr) {}
    Binder(Binder *parent_binder)
        : catalog_(parent_binder->catalog_), parser_(parent_binder->parser_), parent_binder_(parent_binder) {}
    ~Binder() {}

    // 解析 sql query 语句 为 statement序列
    void ParseAndSave(const std::string &query);

    void SaveParseResult(duckdb_libpgquery::PGList *tree);

    // bind statement
    auto BindStatement(duckdb_libpgquery::PGNode *stmt) -> std::unique_ptr<BoundStatement>;
    auto BindExpression(duckdb_libpgquery::PGNode *node) -> std::unique_ptr<BoundExpression>;
    auto BindColumnRef(duckdb_libpgquery::PGColumnRef *node) -> std::unique_ptr<BoundExpression>;

    auto BindColumnDefinition(duckdb_libpgquery::PGColumnDef &cdef, uint16_t slot, std::vector<Constraint> &constraints)
        -> Column;
    auto BindCreateColumnList(duckdb_libpgquery::PGList *tableElts, const std::string &table_name,
                              std::vector<Column> &columns, std::vector<Constraint> &constraints) -> void;

    auto BindCreate(duckdb_libpgquery::PGCreateStmt *pg_stmt) -> std::unique_ptr<CreateStatement>;
    auto BindCreateIndex(duckdb_libpgquery::PGIndexStmt *pg_stmt) -> std::unique_ptr<CreateIndexStatement>;
    auto BindSelect(duckdb_libpgquery::PGSelectStmt *pg_stmt) -> std::unique_ptr<SelectStatement>;
    auto BindSelectNoSetOp(duckdb_libpgquery::PGSelectStmt *pg_stmt) -> std::unique_ptr<SelectStatement>;
    auto BindSelectSetOp(duckdb_libpgquery::PGSelectStmt *pg_stmt) -> std::unique_ptr<SelectStatement>;

    auto BindDistinctOnList(SelectStatement &stmt, duckdb_libpgquery::PGList *distinct_clause) -> void;
    auto BindFrom(duckdb_libpgquery::PGList *list) -> std::unique_ptr<BoundTableRef>;
    auto BindTableRef(const duckdb_libpgquery::PGNode &node) -> std::unique_ptr<BoundTableRef>;
    auto BindRangeVar(const duckdb_libpgquery::PGRangeVar &table_ref, bool is_select = true)
        -> std::unique_ptr<BoundTableRef>;
    auto BindJoin(const duckdb_libpgquery::PGJoinExpr &node) -> std::unique_ptr<BoundTableRef>;
    auto BindRangeSubselect(const duckdb_libpgquery::PGRangeSubselect &node) -> std::unique_ptr<BoundTableRef>;
    auto BindBaseTableRef(const std::string &table_name, std::optional<std::string> &&alias, bool if_exists = false)
        -> std::unique_ptr<BoundBaseTable>;
    auto BindSubquery(duckdb_libpgquery::PGSelectStmt *node, const std::string &alias)
        -> std::unique_ptr<BoundSubquery>;

    auto BindTableAllColumns(const std::string &table_name) -> std::vector<std::unique_ptr<BoundExpression>>;
    auto BindSelectList(duckdb_libpgquery::PGList *list) -> std::vector<std::unique_ptr<BoundExpression>>;
    auto BindAllColumns(const char *expect_relation_name) -> std::vector<std::unique_ptr<BoundExpression>>;
    auto BindResTarget(duckdb_libpgquery::PGResTarget *root) -> std::unique_ptr<BoundExpression>;
    auto BindStar(duckdb_libpgquery::PGAStar *node) -> std::unique_ptr<BoundExpression>;
    auto BindColumnRefFromTableBinding(const std::vector<std::string> &col_name) -> std::unique_ptr<BoundExpression>;

    auto BindWhere(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression>;
    auto BindAExpr(duckdb_libpgquery::PGAExpr *root) -> std::unique_ptr<BoundExpression>;

    auto BindValue(duckdb_libpgquery::PGValue *node) -> std::unique_ptr<BoundExpression>;
    auto BindConstant(duckdb_libpgquery::PGAConst *node) -> std::unique_ptr<BoundExpression>;
    auto BindGroupBy(duckdb_libpgquery::PGList *list) -> std::vector<std::unique_ptr<BoundExpression>>;
    auto BindGroupByExpression(duckdb_libpgquery::PGNode *node) -> std::unique_ptr<BoundExpression>;
    auto BindHaving(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression>;
    auto BindLimit(duckdb_libpgquery::PGNode *limit, duckdb_libpgquery::PGNode *offset) -> std::unique_ptr<LimitClause>;
    auto BindLimitOffset(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression>;
    auto BindLimitCount(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression>;
    auto BindSortItems(duckdb_libpgquery::PGList *list, const std::vector<std::unique_ptr<BoundExpression>> &)
        -> std::vector<std::unique_ptr<BoundSortItem>>;
    auto BindSortExpression(duckdb_libpgquery::PGNode *node,
                            const std::vector<std::unique_ptr<BoundExpression>> &select_list)
        -> std::unique_ptr<BoundExpression>;
    auto BindFuncCall(duckdb_libpgquery::PGFuncCall *root) -> std::unique_ptr<BoundExpression>;

    auto BindBoolExpr(duckdb_libpgquery::PGBoolExpr *root) -> std::unique_ptr<BoundExpression>;
    auto BindExpressionList(duckdb_libpgquery::PGList *list) -> std::vector<std::unique_ptr<BoundExpression>>;
    auto BindNullTest(duckdb_libpgquery::PGNullTest *root) -> std::unique_ptr<BoundExpression>;
    auto BindTypeCast(duckdb_libpgquery::PGTypeCast *root) -> std::unique_ptr<BoundExpression>;

    auto BindSubQueryExpr(duckdb_libpgquery::PGSubLink *root) -> std::unique_ptr<BoundExpression>;

    auto BindInsert(duckdb_libpgquery::PGInsertStmt *pg_stmt) -> std::unique_ptr<InsertStatement>;
    auto BindValueList(duckdb_libpgquery::PGList *list) -> ValueClaluse;

    auto BindTransaction(duckdb_libpgquery::PGTransactionStmt *pg_stmt) -> std::unique_ptr<TransactionStatement>;

    auto BindDrop(duckdb_libpgquery::PGDropStmt *stmt) -> std::unique_ptr<DropStatement>;

    auto BindSequence(duckdb_libpgquery::PGCreateSeqStmt *stmt) -> std::unique_ptr<CreateSequenceStatement>;

    auto BindDelete(duckdb_libpgquery::PGDeleteStmt *stmt) -> std::unique_ptr<DeleteStatement>;

    auto BindUpdate(duckdb_libpgquery::PGUpdateStmt *stmt) -> std::unique_ptr<UpdateStatement>;
    auto BindUpdateItems(duckdb_libpgquery::PGList *target_list, const BoundBaseTable &table)
        -> std::vector<UpdateSetItem>;

    auto BindVariableSet(duckdb_libpgquery::PGVariableSetStmt *stmt) -> std::unique_ptr<SetStatement>;

    auto NodeTagToString(duckdb_libpgquery::PGNodeTag type) -> std::string;

    const std::vector<duckdb_libpgquery::PGNode *> &GetStatementNodes() const { return pg_statements_; }

    // 绑定单列约束
    auto BindSingleColConstraint(duckdb_libpgquery::PGListCell *cell, Column &col) -> std::unique_ptr<Constraint>;
    // 绑定多列约束
    auto BindMultiColConstraint(const duckdb_libpgquery::PGConstraint &constraint) -> Constraint;
    // 绑定约束列表
    auto BindSingleColConstraintList(duckdb_libpgquery::PGList *list, Column &column,
                                     std::vector<Constraint> &constraints) -> void;
    auto BoundExpressionToDefaultValue(BoundExpression &expr, Column &column) -> std::vector<uint8_t>;

    auto BindAlter(duckdb_libpgquery::PGAlterTableStmt *stmt) -> std::unique_ptr<AlterStatement>;
    auto BindRename(duckdb_libpgquery::PGRenameStmt *stmt) -> std::unique_ptr<AlterStatement>;
    auto BindDefault(duckdb_libpgquery::PGNode *node) -> std::unique_ptr<BoundExpression>;
    auto BindComment(duckdb_libpgquery::PGNode *node) -> std::string;
    auto BindVariableShow(duckdb_libpgquery::PGVariableShowStmt *stmt) -> std::unique_ptr<ShowStatement>;
    auto BindShowDataBase() -> std::unique_ptr<ShowStatement>;
    auto BindShowTables() -> std::unique_ptr<ShowStatement>;
    auto BindShowSpecificTable(const std::string &lname) -> std::unique_ptr<ShowStatement>;
    auto BindShowAll(const std::string &lname) -> std::unique_ptr<ShowStatement>;

    auto BindCtas(duckdb_libpgquery::PGCreateTableAsStmt *stmt) -> std::unique_ptr<CtasStatement>;
    auto BindCreateView(duckdb_libpgquery::PGViewStmt *stmt) -> std::unique_ptr<CreateViewStatement>;

    auto BindCopyOptions(CopyInfo &info, duckdb_libpgquery::PGList *options) -> void;
    auto BindCopy(duckdb_libpgquery::PGCopyStmt *stmt) -> std::unique_ptr<CopyStatement>;
    auto BindCopyFrom(const duckdb_libpgquery::PGCopyStmt &stmt, CopyInfo &info) -> void;
    auto BindCopyTo(const duckdb_libpgquery::PGCopyStmt &stmt, CopyInfo &info) -> std::unique_ptr<SelectStatement>;
    auto BindCopySelectList(const duckdb_libpgquery::PGCopyStmt &stmt, const BoundBaseTable &table)
        -> std::vector<std::unique_ptr<BoundExpression>>;
    auto BindCopyOptionWithArg(const std::string &option_name, CopyInfo &info, duckdb_libpgquery::PGDefElem *def_elem)
        -> void;
    auto BindCopyOptionWithNoArg(const std::string &option_name, CopyInfo &info) -> void;

    auto BindCheckPoint(duckdb_libpgquery::PGCheckPointStmt *stmt) -> std::unique_ptr<CheckpointStatement>;
    auto BindExplain(duckdb_libpgquery::PGExplainStmt *stmt) -> std::unique_ptr<ExplainStatement>;
    auto BindCommentOn(duckdb_libpgquery::PGCommentStmt *stmt) -> std::unique_ptr<CommentStatement>;
    auto BindCaggPolicy(duckdb_libpgquery::PGCaggPolicyStmt *stmt) -> std::unique_ptr<CaggPolicyStatement>;

    auto BindCall(duckdb_libpgquery::PGCallStmt *stmt) -> std::unique_ptr<CallStatement>;

    auto GetNextUniversalID() -> int {
        auto root_binder = RootBinder();
        return root_binder->universal_id_++;
    }

    auto ParamCount() -> uint32_t {
        auto root_binder = RootBinder();
        return root_binder->n_param_;
    }

    auto BindParamRef(duckdb_libpgquery::PGParamRef *node) -> std::unique_ptr<BoundExpression>;

   private:
    auto IsLikeFunction(const std::string &name) -> bool;
    auto IsLikeExpression(duckdb_libpgquery::PGAExpr_Kind kind) -> bool;

    auto RootBinder() -> Binder * {
        if (parent_binder_ == nullptr) {
            return this;
        }
        return parent_binder_->RootBinder();
    }

   private:
    const Catalog &catalog_;
    duckdb::PostgresParser &parser_;

    std::vector<duckdb_libpgquery::PGNode *> pg_statements_;  // save parse result
    int universal_id_{0};
    std::unordered_map<std::string, std::unique_ptr<BoundExpression>> alias_map_;

    uint32_t n_param_ = 0;

    std::map<int, BoundParameter *> params_cols_map_;
    Binder *parent_binder_ = nullptr;

    bool check_column_exist_{true};  // FIXME: 应该显示在BindExpression函数中指定
   public:
    BinderContext ctx;
};

inline auto Binder::IsLikeFunction(const std::string &funcname) -> bool {
    // function_name is low case
    return funcname == "like_escape" || funcname == "ilike_escape" || funcname == "not_like_escape" ||
           funcname == "not_ilike_escape";
}

inline auto Binder::IsLikeExpression(duckdb_libpgquery::PGAExpr_Kind kind) -> bool {
    return kind == duckdb_libpgquery::PG_AEXPR_LIKE || kind == duckdb_libpgquery::PG_AEXPR_ILIKE;
}
