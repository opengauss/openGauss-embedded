/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* cagg_policy_statement.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/include/binder/statement/cagg_policy_statement.h
*
* -------------------------------------------------------------------------
*/
#pragma once

#include <memory>
#include <vector>

#include "binder/bound_statement.h"
#include "binder/bound_table_ref.h"
#include "catalog/column.h"

// CREATE CAGGPOLICY policy_name INTO des_table_name AS subquery 
// [REFREASH_INTERVAL refreash_interval] [OFFSET start_offset end_offset]
// [BUCKET_WIDTH bucket_field bucket_width][INIT_TIME init_time]
class CaggPolicyStatement : public BoundStatement {
   public:
    explicit CaggPolicyStatement()
        : BoundStatement(StatementType::CAGG_POLICY_STATEMENT) {}

    std::unique_ptr<BoundTableRef> table_;

    std::string policy_name;
    std::string table_name;
    std::string subs_query;

    std::string refreash_interval;
    std::string start_offset;
    std::string end_offset;
    std::string bucket_field;
    std::string bucket_width;
    std::string init_time;

    auto ToString() const -> std::string override { return "CaggPolicyStatement"; };
};
