/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* call_statement.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/include/binder/statement/call_statement.h
*
* -------------------------------------------------------------------------
*/
#pragma once

#include <string>
#include <cstdint>

#include "binder/bound_statement.h"

const uint64_t MAX_SINTERVAL_NUMS = 10;
const uint64_t MAX_TS_JOB_NUMS = 60;
const uint64_t MAX_POLICY_NAME_LENGTH = 256;
const uint64_t MAX_DES_TABLE_NAME_LENGTH = 64;

#define TS_PAUSE_PARAM_COUNT            1
#define TS_DEL_PARAM_COUNT              1
#define TS_SEL_STATS_PARAM_COUNT        1
#define TS_SEL_ERR_PARAM_COUNT          1
#define TS_REFRESH_PARAM_COUNT          3
#define TS_ADD_PARAM_COUNT              9

enum class TsType : uint8_t {
    TS_ADD_CAGG_POLICY,
    TS_DEL_CAGG_POLICY,
    TS_PAUSE_CAGG_POLICY,
    TS_REFRESH_CAGG_POLICY,
    TS_SEL_CAGG_STATS,
    TS_SEL_CAGG_ERR
};

class CallStatement : public BoundStatement {
    public:
        explicit CallStatement() : BoundStatement(StatementType::CALL_STATEMENT) {}
        ~CallStatement() {}

    public:
        TsType type;
        std::string function_name;
        std::string policy_name;
        std::string des_table_name;
        std::string agg_sql;
        uint32_t schedule_time;
        uint32_t refresh_interval;
        uint32_t start_offset;
        uint32_t end_offset;
        std::string bucket_field;
        std::string bucket_width;
};