/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * bind_select.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/binder/bind_select.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "binder/binder.h"
#include "binder/bound_sort.h"
#include "binder/expressions/bound_alias.h"
#include "binder/expressions/bound_binary_op.h"
#include "binder/expressions/bound_conjunctive.h"
#include "binder/expressions/bound_constant.h"
#include "binder/expressions/bound_func_call.h"
#include "binder/expressions/bound_like_op.h"
#include "binder/expressions/bound_position_ref_expr.h"
#include "binder/expressions/bound_unary_op.h"
#include "common/aexpr_type.h"
#include "common/null_check_ptr.h"
#include "nodes/parsenodes.hpp"
#include "planner/expression_iterator.h"

static void checkSelectStatement(duckdb_libpgquery::PGSelectStmt *pg_stmt) {
    if (pg_stmt->intoClause) {
        // 不支持 select info , e.g: select * into new_table from old_table;
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported select into");
    }
    if (pg_stmt->lockingClause) {
        // 不支持select for update , e.g: select * from t2 for update
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported select ... for update");
    }
    if (pg_stmt->withClause) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported with sytax");
    }
    if (pg_stmt->windowClause) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported windows function");
    }
    if (pg_stmt->pivot) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported pivot");
    }
    if (pg_stmt->sampleOptions) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported sample");
    }
    if (pg_stmt->qualifyClause) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported qualify");
    }
}

auto Binder::BindDistinctOnList(SelectStatement &stmt, duckdb_libpgquery::PGList *distinct_clause) -> void {
    stmt.is_distinct = false;
    if (distinct_clause != nullptr && distinct_clause->head != nullptr) {
        auto target = NullCheckPtrCast<duckdb_libpgquery::PGNode>(distinct_clause->head->data.ptr_value);
        if (target != nullptr) {
            // 支持 Distinct ON
            stmt.distinct_on_list = BindExpressionList(distinct_clause);
        }
        stmt.is_distinct = true;
    }
}

auto Binder::BindSelectNoSetOp(duckdb_libpgquery::PGSelectStmt *pg_stmt) -> std::unique_ptr<SelectStatement> {
    auto select_stmt = std::make_unique<SelectStatement>();

    if (pg_stmt->valuesLists) {
        // value list
        select_stmt->value_clause = BindValueList(pg_stmt->valuesLists);
        // init return list
        for (size_t i = 0; i < select_stmt->value_clause.columns.size(); ++i) {
            ReturnItem item;
            item.col_name = select_stmt->value_clause.columns[i].col_name;
            item.col_type = select_stmt->value_clause.columns[i].col_type;
            select_stmt->return_list.push_back(item);
        }
        return select_stmt;
    }

    // 绑定 from 子句
    select_stmt->table_ref = BindFrom(pg_stmt->fromClause);

    if (!pg_stmt->targetList) {
        throw intarkdb::Exception(ExceptionType::BINDER, "no select list");
    }

    BindDistinctOnList(*select_stmt, pg_stmt->distinctClause);

    // 绑定 select 列
    select_stmt->select_expr_list = BindSelectList(pg_stmt->targetList);

    // init return list
    for (auto &expr : select_stmt->select_expr_list) {
        ReturnItem item;
        item.col_name = expr->GetName();
        item.col_type = expr->ReturnType();
        select_stmt->return_list.push_back(item);
    }

    auto &select_list = select_stmt->select_expr_list;

    // build alias map
    ctx.BuildProjectionAndAlias(select_list);

    // 绑定 where 子句
    select_stmt->where_clause = BindWhere(pg_stmt->whereClause);
    // group by
    select_stmt->group_by_clause = BindGroupBy(pg_stmt->groupClause);
    // Bind HAVING clause.
    select_stmt->having_clause = BindHaving(pg_stmt->havingClause);
    // Bind LIMIT clause.
    select_stmt->limit_clause = BindLimit(pg_stmt->limitCount, pg_stmt->limitOffset);
    // Bind ORDER BY clause.
    select_stmt->sort_items = BindSortItems(pg_stmt->sortClause, select_stmt->select_expr_list);
    return select_stmt;
}

static auto MergeAliasBinding(std::unordered_map<std::string, AliasIdx> &src,
                              std::unordered_map<std::string, AliasIdx> &dst,
                              const std::vector<std::unique_ptr<BoundExpression>> &result_list) -> void {
    for (auto &item : src) {
        auto iter = dst.find(item.first);
        if (iter == dst.end()) {  // 只有第一个命中的别名会被加入
            item.second.type = result_list[item.second.idx]->ReturnType();
            dst.insert(item);
        }
    }
}

static auto MergeReturnListToAliasBinding(const std::vector<ReturnItem> &return_list,
                                          std::unordered_map<std::string, AliasIdx> &alias_binding,
                                          const std::vector<std::unique_ptr<BoundExpression>> &result_list) -> void {
    int return_size = return_list.size();
    for (int i = 0; i < return_size; ++i) {
        const auto &item = return_list[i];
        const auto &result_type = result_list[i]->ReturnType();
        auto iter = alias_binding.find(item.col_name.back());
        if (iter == alias_binding.end()) {
            alias_binding.insert(std::make_pair(item.col_name.back(), AliasIdx{i, result_type}));
        }
    }
}

static auto AddSelectOriginName(const std::vector<std::unique_ptr<BoundExpression>> &select_expr_list,
                                std::unordered_map<std::string, AliasIdx> &alias_binding,
                                const std::vector<std::unique_ptr<BoundExpression>> &result_list) -> void {
    int list_size = select_expr_list.size();
    for (int i = 0; i < list_size; ++i) {
        const auto &expr = select_expr_list[i];
        if (expr->Type() == ExpressionType::ALIAS) {
            const auto &result_type = result_list[i]->ReturnType();
            const auto &alias_expr = static_cast<const BoundAlias &>(*expr);
            auto origin_name = alias_expr.child_->GetName().back();
            auto iter = alias_binding.find(origin_name);
            if (iter == alias_binding.end()) {
                alias_binding.insert(std::make_pair(origin_name, AliasIdx{i, result_type}));
            }
        }
    }
}

static auto BuildUnionAliasMap(SelectStatement &left_stmt, SelectStatement &right_stmt, BinderContext &ctx,
                               const std::vector<std::unique_ptr<BoundExpression>> &result_list) -> void {
    MergeReturnListToAliasBinding(left_stmt.return_list, ctx.alias_binding, result_list);
    MergeReturnListToAliasBinding(right_stmt.return_list, ctx.alias_binding, result_list);
    AddSelectOriginName(left_stmt.select_expr_list, ctx.alias_binding, result_list);
    AddSelectOriginName(right_stmt.select_expr_list, ctx.alias_binding, result_list);
}

auto Binder::BindSelectSetOp(duckdb_libpgquery::PGSelectStmt *pg_stmt) -> std::unique_ptr<SelectStatement> {
    auto select_stmt = std::make_unique<SelectStatement>();
    Binder left_binder(this);
    Binder right_binder(this);
    select_stmt->larg = left_binder.BindSelect(pg_stmt->larg);
    select_stmt->rarg = right_binder.BindSelect(pg_stmt->rarg);

    if (select_stmt->larg->select_expr_list.size() != select_stmt->rarg->select_expr_list.size()) {
        throw intarkdb::Exception(ExceptionType::BINDER,
                                  "set operation must have an equal number of expressions in target lists");
    }

    for (size_t i = 0; i < select_stmt->larg->select_expr_list.size(); ++i) {
        auto type = intarkdb::GetCompatibleType(select_stmt->larg->select_expr_list[i]->ReturnType(),
                                                select_stmt->rarg->select_expr_list[i]->ReturnType());
        if (type.TypeId() == GS_TYPE_NULL) {
            type = LogicalType::Integer();
        }
        select_stmt->select_expr_list.push_back(
            std::make_unique<BoundColumnRef>(select_stmt->larg->select_expr_list[i]->GetName(), type));
    }

    BuildUnionAliasMap(*select_stmt->larg, *select_stmt->rarg, ctx, select_stmt->select_expr_list);
    MergeAliasBinding(left_binder.ctx.alias_binding, ctx.alias_binding, select_stmt->select_expr_list);
    MergeAliasBinding(right_binder.ctx.alias_binding, ctx.alias_binding, select_stmt->select_expr_list);

    for (size_t i = 0; i < select_stmt->select_expr_list.size(); ++i) {
        select_stmt->return_list.emplace_back(
            ReturnItem{select_stmt->select_expr_list[i]->GetName(), select_stmt->select_expr_list[i]->ReturnType()});
    }

    select_stmt->table_ref = std::make_unique<BoundTableRef>(DataSourceType::DUAL);

    // 关闭检查列存在性
    check_column_exist_ = false;
    select_stmt->sort_items = BindSortItems(pg_stmt->sortClause, select_stmt->select_expr_list);
    check_column_exist_ = true;

    // check sort items show in select list ?
    for (size_t i = 0; i < select_stmt->sort_items.size(); ++i) {
        auto &sort_item = select_stmt->sort_items[i];
        if (sort_item->sort_expr->Type() != ExpressionType::POSITION_REF) {
            auto iter = ctx.alias_binding.find(sort_item->sort_expr->GetName().back());
            if (iter != ctx.alias_binding.end()) {
                sort_item->sort_expr = std::make_unique<BoundPositionRef>(iter->second.idx, iter->second.type);
            } else {
                throw intarkdb::Exception(ExceptionType::BINDER, "sort item not in select list");
            }
        }
    }

    select_stmt->limit_clause = BindLimit(pg_stmt->limitCount, pg_stmt->limitOffset);

    switch (pg_stmt->op) {
        case duckdb_libpgquery::PG_SETOP_UNION: {
            select_stmt->set_operation_type = SetOperationType::UNION;
            break;
        }
        case duckdb_libpgquery::PG_SETOP_EXCEPT: {
            select_stmt->set_operation_type = SetOperationType::EXCEPT;
            break;
        }
        case duckdb_libpgquery::PG_SETOP_INTERSECT: {
            select_stmt->set_operation_type = SetOperationType::INTERSECT;
            break;
        }
        // ignore union all by name
        default:
            throw intarkdb::Exception(ExceptionType::BINDER, "invalid set operation");
    }
    select_stmt->is_distinct = !pg_stmt->all;
    return select_stmt;
}

auto Binder::BindSelect(duckdb_libpgquery::PGSelectStmt *pg_stmt) -> std::unique_ptr<SelectStatement> {
    // 检查不支持的语法元素
    checkSelectStatement(pg_stmt);

    switch (pg_stmt->op) {
        case duckdb_libpgquery::PG_SETOP_NONE: {
            return BindSelectNoSetOp(pg_stmt);
        }
        case duckdb_libpgquery::PG_SETOP_UNION:
        case duckdb_libpgquery::PG_SETOP_EXCEPT:
        case duckdb_libpgquery::PG_SETOP_INTERSECT: {
            return BindSelectSetOp(pg_stmt);
        }
        default:
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported select type");
    }
}

static auto TransformSortType(duckdb_libpgquery::PGSortByDir sortby_dir) -> SortType {
    switch (sortby_dir) {
        case duckdb_libpgquery::PG_SORTBY_DEFAULT:
        case duckdb_libpgquery::PG_SORTBY_ASC:
            return SortType::ASC;
        case duckdb_libpgquery::PG_SORTBY_DESC:
            return SortType::DESC;
        default:
            throw intarkdb::Exception(ExceptionType::BINDER, "unsupported sort type");
    }
}

auto Binder::BindSortExpression(duckdb_libpgquery::PGNode *node,
                                const std::vector<std::unique_ptr<BoundExpression>> &select_list)
    -> std::unique_ptr<BoundExpression> {
    auto expr = BindExpression(node);
    if (expr) {
        if (expr->Type() == ExpressionType::COLUMN_REF) {
            // 再从alias map中查找，如果有别名命中，会优先使用别名[区别于where，orderby 会优先使用别名]
            // 如果已经是别名，不会走到这里
            const auto &column_ref_expr = static_cast<const BoundColumnRef &>(*expr);
            // 从OrderByClause获取的列名都是带有表名的，解析器会自动加上表名
            const auto &expr_name = column_ref_expr.GetName();
            const auto &alias = expr_name.back();  // 去掉表名 ， 只保留列名
            auto alias_expr = ctx.alias_binding.find(alias);
            if (alias_expr != ctx.alias_binding.end()) {
                expr = std::make_unique<BoundPositionRef>(alias_expr->second.idx, alias_expr->second.type);
            }
        } else if (expr->Type() == ExpressionType::LITERAL) {
            const auto &constant_expr = static_cast<const BoundConstant &>(*expr);
            const auto &val = constant_expr.ValRef();
            if (val.IsInteger()) {
                auto idx = val.GetCastAs<int32_t>();
                if (idx <= 0 || idx > (int32_t)select_list.size()) {
                    throw intarkdb::Exception(
                        ExceptionType::BINDER,
                        fmt::format("ORDER term out of range - should be between 1 and {}", select_list.size()));
                }
                expr = select_list[idx - 1]->Copy();
            } else {
                expr = nullptr;  // 常量非整数
            }
        } else if (expr->Type() == ExpressionType::BOUND_PARAM) {
            // 暂不支持 param表达式
            throw intarkdb::Exception(ExceptionType::BINDER, "not supported param expression in order by");
        }
    }
    return expr;
}

auto Binder::BindSortItems(duckdb_libpgquery::PGList *list,
                           const std::vector<std::unique_ptr<BoundExpression>> &select_list)
    -> std::vector<std::unique_ptr<BoundSortItem>> {
    auto sort_items = std::vector<std::unique_ptr<BoundSortItem>>{};
    if (list) {
        for (auto node = list->head; node != nullptr; node = node->next) {
            auto item = NullCheckPtrCast<duckdb_libpgquery::PGNode>(node->data.ptr_value);
            if (item->type == duckdb_libpgquery::T_PGSortBy) {
                auto sort_item = NullCheckPtrCast<duckdb_libpgquery::PGSortBy>(item.get());
                auto type = TransformSortType(sort_item->sortby_dir);
                auto is_null_first = sort_item->sortby_nulls == duckdb_libpgquery::PG_SORTBY_NULLS_FIRST;
                auto order_expr = BindSortExpression(sort_item->node, select_list);
                if (order_expr) {
                    sort_items.emplace_back(
                        std::make_unique<BoundSortItem>(type, is_null_first, std::move(order_expr)));
                }
            } else {
                throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "unsupported order by node");
            }
        }
    }
    return sort_items;
}

auto Binder::BindLimit(duckdb_libpgquery::PGNode *limit, duckdb_libpgquery::PGNode *offset)
    -> std::unique_ptr<LimitClause> {
    std::unique_ptr<LimitClause> limit_clause = nullptr;
    if (limit || offset) {
        limit_clause = std::make_unique<LimitClause>();
    }
    if (limit) {
        limit_clause->limit = BindLimitCount(limit);
    }
    if (offset) {
        limit_clause->offset = BindLimitOffset(offset);
    }
    return limit_clause;
}

auto Binder::BindLimitCount(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression> {
    auto expr = BindExpression(root);
    if (expr && expr->Type() != ExpressionType::LITERAL && expr->Type() != ExpressionType::BOUND_PARAM) {
        throw intarkdb::Exception(ExceptionType::BINDER, "limit count must be constant");
    }
    return expr;
}

auto Binder::BindLimitOffset(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression> {
    auto expr = BindExpression(root);
    if (expr && expr->Type() != ExpressionType::LITERAL && expr->Type() != ExpressionType::BOUND_PARAM) {
        throw intarkdb::Exception(ExceptionType::BINDER, "limit count must be constant");
    }
    return expr;
}

auto Binder::BindHaving(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression> {
    return BindExpression(root);
}

auto Binder::BindGroupBy(duckdb_libpgquery::PGList *list) -> std::vector<std::unique_ptr<BoundExpression>> {
    std::vector<std::unique_ptr<BoundExpression>> group_by_list;
    if (list) {
        for (auto node = list->head; node != nullptr; node = lnext(node)) {
            auto target = reinterpret_cast<duckdb_libpgquery::PGNode *>(node->data.ptr_value);
            auto expr = BindGroupByExpression(target);
            if (expr) {
                group_by_list.emplace_back(std::move(expr));
            }
        }
    }
    return group_by_list;
}

auto Binder::BindGroupByExpression(duckdb_libpgquery::PGNode *node) -> std::unique_ptr<BoundExpression> {
    auto expr = BindExpression(node);
    if (expr && expr->Type() == ExpressionType::LITERAL) {
        auto &constant_expr = static_cast<BoundConstant &>(*expr);
        const auto &val = constant_expr.ValRef();
        if (val.IsInteger()) {
            auto idx = val.GetCastAs<int32_t>();
            expr = std::make_unique<BoundPositionRef>(idx - 1, GS_TYPE_UNKNOWN);
        } else {
            expr = nullptr;  // groupby 非整数常量，等于无效groupby 忽略
        }
    }
    return expr;
}

auto Binder::BindWhere(duckdb_libpgquery::PGNode *root) -> std::unique_ptr<BoundExpression> {
    auto expr = BindExpression(root);
    if (expr) {
        // where 表达式中含有*，这种情况pg的语法解释不能过滤掉
        ExpressionIterator::EnumerateChildren(*expr, [&](std::unique_ptr<BoundExpression> &child) {
            if (child->Type() == ExpressionType::STAR) {
                throw intarkdb::Exception(ExceptionType::BINDER, "not supported * expression in whereClause");
            }
        });
    }
    return expr;
}

constexpr size_t BOOL_EXPR_ARG_NUM = 2;

static auto MakeORExpr(BoundConjunctive &conjunctive_left, BoundConjunctive &conjunctive_right)
    -> std::unique_ptr<BoundConjunctive> {
    std::vector<std::unique_ptr<BoundExpression>> items;
    items.reserve(conjunctive_left.items.size() * conjunctive_right.items.size());
    for (size_t i = 0; i < conjunctive_left.items.size(); ++i) {
        for (size_t j = 0; j < conjunctive_right.items.size(); ++j) {
            items.push_back(std::make_unique<BoundBinaryOp>("or", conjunctive_left.items[i]->Copy(),
                                                            conjunctive_right.items[j]->Copy()));
        }
    }
    return std::make_unique<BoundConjunctive>(std::move(items));
}

static auto MakeORExpr(BoundConjunctive &conjunctive, std::unique_ptr<BoundExpression> &&expr)
    -> std::unique_ptr<BoundConjunctive> {
    std::vector<std::unique_ptr<BoundExpression>> items;
    items.reserve(conjunctive.items.size());
    for (auto &item : conjunctive.items) {
        items.push_back(std::make_unique<BoundBinaryOp>("or", item->Copy(), expr->Copy()));
    }
    return std::make_unique<BoundConjunctive>(std::move(items));
}

static auto MakeORExpr(std::unique_ptr<BoundExpression> &&left, std::unique_ptr<BoundExpression> &&right)
    -> std::unique_ptr<BoundExpression> {
    if (left->Type() == ExpressionType::CONJUNCTIVE && right->Type() == ExpressionType::CONJUNCTIVE) {
        return MakeORExpr(static_cast<BoundConjunctive &>(*left), static_cast<BoundConjunctive &>(*right));
    } else if (left->Type() == ExpressionType::CONJUNCTIVE) {
        return MakeORExpr(static_cast<BoundConjunctive &>(*left), std::move(right));
    } else if (right->Type() == ExpressionType::CONJUNCTIVE) {
        return MakeORExpr(static_cast<BoundConjunctive &>(*right), std::move(left));
    } else {
        return std::make_unique<BoundBinaryOp>("or", std::move(left), std::move(right));
    }
}

static auto MakeORExpr(std::vector<std::unique_ptr<BoundExpression>> &&exprs) -> std::unique_ptr<BoundExpression> {
    auto expr = MakeORExpr(std::move(exprs[0]), std::move(exprs[1]));
    for (size_t i = 2; i < exprs.size(); ++i) {
        expr = MakeORExpr(std::move(expr), std::move(exprs[i]));
    }
    return expr;
}

static auto MakeANDExpr(std::unique_ptr<BoundExpression> &&left, std::unique_ptr<BoundExpression> &&right)
    -> std::unique_ptr<BoundConjunctive> {
    std::vector<std::unique_ptr<BoundExpression>> items;
    if (left->Type() == ExpressionType::CONJUNCTIVE) {
        auto &conjunctive = static_cast<BoundConjunctive &>(*left);
        items = std::move(conjunctive.items);
    } else {
        items.push_back(std::move(left));
    }
    if (right->Type() == ExpressionType::CONJUNCTIVE) {
        auto &conjunctive = static_cast<BoundConjunctive &>(*right);
        std::copy(std::make_move_iterator(conjunctive.items.begin()), std::make_move_iterator(conjunctive.items.end()),
                  std::back_inserter(items));
    } else {
        items.push_back(std::move(right));
    }
    return std::make_unique<BoundConjunctive>(std::move(items));
}

static auto MakeANDExpr(std::vector<std::unique_ptr<BoundExpression>> &&exprs) -> std::unique_ptr<BoundConjunctive> {
    std::vector<std::unique_ptr<BoundExpression>> items;
    for (auto &expr : exprs) {
        if (expr->Type() == ExpressionType::CONJUNCTIVE) {
            auto &conjunctive = static_cast<BoundConjunctive &>(*expr);
            std::copy(std::make_move_iterator(conjunctive.items.begin()),
                      std::make_move_iterator(conjunctive.items.end()), std::back_inserter(items));
        } else {
            items.push_back(std::move(expr));
        }
    }
    return std::make_unique<BoundConjunctive>(std::move(items));
}

constexpr int BETWEEN_ARG_NUM = 2;

auto Binder::BindAExpr(duckdb_libpgquery::PGAExpr *root) -> std::unique_ptr<BoundExpression> {
    if (root->kind == duckdb_libpgquery::PG_AEXPR_BETWEEN || root->kind == duckdb_libpgquery::PG_AEXPR_NOT_BETWEEN) {
        // 处理 between 表达式
        auto between_exprs = reinterpret_cast<duckdb_libpgquery::PGList *>(root->rexpr);
        if (between_exprs->length != BETWEEN_ARG_NUM) {
            throw intarkdb::Exception(ExceptionType::BINDER, "between expression need two args");
        }

        auto between_left =
            BindExpression(reinterpret_cast<duckdb_libpgquery::PGNode *>(between_exprs->head->data.ptr_value));
        auto between_right =
            BindExpression(reinterpret_cast<duckdb_libpgquery::PGNode *>(between_exprs->tail->data.ptr_value));

        auto col_ref1 = BindExpression(root->lexpr);
        auto col_ref2 = col_ref1->Copy();

        auto left_half = std::make_unique<BoundBinaryOp>(">=", std::move(col_ref1), std::move(between_left));
        auto right_half = std::make_unique<BoundBinaryOp>("<=", std::move(col_ref2), std::move(between_right));

        auto result = MakeANDExpr(std::move(left_half), std::move(right_half));
        if (root->kind == duckdb_libpgquery::PG_AEXPR_NOT_BETWEEN) {
            return std::make_unique<BoundUnaryOp>("not", std::move(result));
        }
        return result;
    }

    // op name
    std::string_view name = NullCheckPtrCast<duckdb_libpgquery::PGValue>(root->name->head->data.ptr_value)->val.str;

    if (root->kind == duckdb_libpgquery::PG_AEXPR_IN) {
        auto exprs = BindExpressionList(reinterpret_cast<duckdb_libpgquery::PGList *>(root->rexpr));
        if (exprs.size() < 0) {
            throw intarkdb::Exception(ExceptionType::BINDER, "argument list after IN expresion can't be empty");
        }
        auto col_ref = BindExpression(root->lexpr);
        std::unique_ptr<BoundExpression> in_expr =
            std::make_unique<BoundBinaryOp>("=", std::move(col_ref), std::move(exprs[0]));
        for (size_t i = 1; i < exprs.size(); ++i) {
            // FIXME: 优化多次绑定列
            auto col_ref_tmp = BindExpression(root->lexpr);
            auto tmp = std::make_unique<BoundBinaryOp>("=", std::move(col_ref_tmp), std::move(exprs[i]));
            in_expr = std::make_unique<BoundBinaryOp>("or", std::move(in_expr), std::move(tmp));
        }
        if (name == "<>") {
            in_expr = std::make_unique<BoundUnaryOp>("not", std::move(in_expr));
        }
        return in_expr;
    }

    if (IsLikeExpression(root->kind)) {
        auto left_expr = BindExpression(root->lexpr);
        auto right_expr = BindExpression(root->rexpr);
        if (!left_expr || !right_expr) {
            throw intarkdb::Exception(ExceptionType::BINDER, "argument with LIKE expresion can't be empty");
        }
        std::vector<std::unique_ptr<BoundExpression>> arguments;
        arguments.push_back(std::move(left_expr));
        arguments.push_back(std::move(right_expr));
        return std::make_unique<BoundLikeOp>(std::string(name), std::move(arguments));
    }

    if (root->kind != duckdb_libpgquery::PG_AEXPR_OP) {
        throw intarkdb::Exception(ExceptionType::BINDER,
                                  fmt::format("not supported this kind of expression {}", root->kind));
    }

    std::unique_ptr<BoundExpression> left_expr = nullptr;
    std::unique_ptr<BoundExpression> right_expr = nullptr;

    if (root->lexpr != nullptr) {
        left_expr = BindExpression(root->lexpr);
    }
    if (root->rexpr != nullptr) {
        right_expr = BindExpression(root->rexpr);
    }

    if (left_expr && right_expr) {
        // 双操作符
        if (name == "and") {
            return MakeANDExpr(std::move(left_expr), std::move(right_expr));
        } else if (name == "or") {
            return MakeORExpr(std::move(left_expr), std::move(right_expr));
        } else {
            return std::make_unique<BoundBinaryOp>(std::string(name), std::move(left_expr), std::move(right_expr));
        }
    }
    if (!left_expr && right_expr) {
        if (name == "+") {  // + 单目操作符，忽略
            return right_expr;
        }
        return std::make_unique<BoundUnaryOp>(std::string(name), std::move(right_expr));
    }
    throw intarkdb::Exception(ExceptionType::BINDER, "unsupported AExpr");
}

auto Binder::BindBoolExpr(duckdb_libpgquery::PGBoolExpr *expr_root) -> std::unique_ptr<BoundExpression> {
    if (expr_root->boolop == duckdb_libpgquery::PG_NOT_EXPR) {
        auto exprs = BindExpressionList(expr_root->args);
        if (exprs.size() != 1) {
            throw std::invalid_argument("NOT need only one arg");
        }
        // NOTE: expr为合取式时，是否需要改写表达式
        return std::make_unique<BoundUnaryOp>("not", std::move(exprs[0]));
    }

    std::string op_name;
    switch (expr_root->boolop) {
        case duckdb_libpgquery::PG_AND_EXPR: {
            op_name = "and";
            break;
        }
        case duckdb_libpgquery::PG_OR_EXPR: {
            op_name = "or";
            break;
        }
        default:
            throw intarkdb::Exception(ExceptionType::BINDER, "invalid bool expr");
    }

    auto exprs = BindExpressionList(expr_root->args);
    if (exprs.size() < BOOL_EXPR_ARG_NUM) {  // NOTE: 是否有三值逻辑
        throw intarkdb::Exception(ExceptionType::BINDER,
                                  fmt::format("{} need two arguments size={}", op_name, exprs.size()));
    }

    std::unique_ptr<BoundExpression> expr;
    if (op_name == "and") {
        expr = MakeANDExpr(std::move(exprs));
    } else {
        expr = MakeORExpr(std::move(exprs));
    }
    return expr;
}

auto Binder::BindExpressionList(duckdb_libpgquery::PGList *list) -> std::vector<std::unique_ptr<BoundExpression>> {
    auto select_list = std::vector<std::unique_ptr<BoundExpression>>{};
    for (auto node = list->head; node != nullptr; node = lnext(node)) {
        auto target = reinterpret_cast<duckdb_libpgquery::PGNode *>(node->data.ptr_value);
        auto expr = BindExpression(target);
        select_list.emplace_back(std::move(expr));
    }
    return select_list;
}
