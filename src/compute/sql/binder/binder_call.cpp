/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * binder_call.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/binder/binder_call.cpp
 *
 * -------------------------------------------------------------------------
 */

#include "binder/binder.h"
#include "common/string_util.h"

auto Binder::BindCall(duckdb_libpgquery::PGCallStmt *stmt) -> std::unique_ptr<CallStatement> {
    auto result = std::make_unique<CallStatement>();
    auto func = reinterpret_cast<duckdb_libpgquery::PGFuncCall *>(stmt->func);
    auto name = func->funcname;
    std::string function_name = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->data.ptr_value)->val.str;
    function_name = intarkdb::StringUtil::Lower(function_name);
    result->function_name = function_name;
    
    // arguements
    std::vector<Value> values;
    if (func->args != nullptr) {
        for (auto root = func->args->head; root != nullptr; root = root->next) {
            // Constant
            auto node = static_cast<duckdb_libpgquery::PGAConst *>(root->data.ptr_value) -> val;
            auto val = node.val;
            // only int or string
            switch (node.type) {
                case duckdb_libpgquery::T_PGInteger:
                    if (val.ival > INT32_MAX) {
                        throw std::invalid_argument("integer out of range!");
                    }
                    values.push_back(ValueFactory::ValueInt(val.ival));
                    break;
                case duckdb_libpgquery::T_PGBitString:
                case duckdb_libpgquery::T_PGString:
                    values.push_back(ValueFactory::ValueVarchar(val.str));
                    break;
                default:
                    throw std::invalid_argument("the type of arguements error");
                    break;
            }
        }
    }

    if (function_name == "ts_add_cagg_policy") {
        if (values.size() != TS_ADD_PARAM_COUNT)
            throw std::invalid_argument("the number of arguements don't match!");
        result->type = TsType::TS_ADD_CAGG_POLICY;
        result->policy_name = values[0].GetCastAs<std::string>();
        result->des_table_name = values[1].GetCastAs<std::string>();
        result->schedule_time = values[2].GetCastAs<uint32_t>();
        result->agg_sql = values[3].GetCastAs<std::string>();
        result->refresh_interval = values[4].GetCastAs<uint32_t>();
        result->start_offset = values[5].GetCastAs<uint32_t>();
        result->end_offset = values[6].GetCastAs<uint32_t>();
        result->bucket_field = values[7].GetCastAs<std::string>();
        result->bucket_width = values[8].GetCastAs<std::string>();
    }
    else if (function_name == "ts_pause_cagg_policy") {
        if (values.size() != TS_PAUSE_PARAM_COUNT)
            throw std::invalid_argument("the number of arguements don't match!");
        result->type = TsType::TS_PAUSE_CAGG_POLICY;
        result->policy_name = values[0].GetCastAs<std::string>();
    }
    else if (function_name == "ts_del_cagg_policy") {
        if (values.size() != TS_DEL_PARAM_COUNT)
            throw std::invalid_argument("the number of arguements don't match!");
        result->type = TsType::TS_DEL_CAGG_POLICY;
        result->policy_name = values[0].GetCastAs<std::string>();
    }
    else if (function_name == "ts_refresh_cagg_policy") {
        if (values.size() != TS_REFRESH_PARAM_COUNT)
            throw std::invalid_argument("the number of arguements don't match!");
        result->type = TsType::TS_REFRESH_CAGG_POLICY;
        result->policy_name = values[0].GetCastAs<std::string>();
        result->start_offset = values[1].GetCastAs<uint32_t>();
        result->end_offset = values[2].GetCastAs<uint32_t>();
    }
    else if (function_name == "ts_sel_cagg_stats") {
        if (values.size() != TS_SEL_STATS_PARAM_COUNT)
            throw std::invalid_argument("the number of arguements don't match!");
        result->type = TsType::TS_SEL_CAGG_STATS;
        result->policy_name = values[0].GetCastAs<std::string>();
    }
    else if (function_name == "ts_sel_cagg_err") {
        if (values.size() != TS_SEL_ERR_PARAM_COUNT)
            throw std::invalid_argument("the number of arguements don't match!");
        result->type = TsType::TS_SEL_CAGG_ERR;
        result->policy_name = values[0].GetCastAs<std::string>();
    }
    else {
        throw std::runtime_error("unspproted ts job function: " + function_name);
    }

    if (result->policy_name.size() > MAX_POLICY_NAME_LENGTH) {
        throw std::runtime_error(fmt::format("the size of policy name is greater than {}!", MAX_POLICY_NAME_LENGTH));
    }
    if (result->des_table_name.size() > MAX_DES_TABLE_NAME_LENGTH) {
        throw std::runtime_error(fmt::format("the size of destination table name is greater than {}!", MAX_DES_TABLE_NAME_LENGTH));
    }

    return result;
}