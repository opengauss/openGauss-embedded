/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * binder.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/binder/binder.cpp
 *
 * -------------------------------------------------------------------------
 */

#include "binder/binder.h"

#include <fmt/core.h>
#include <fmt/format.h>

#include <stdexcept>
#include <unordered_set>
#include <vector>

#include "binder/bound_expression.h"
#include "binder/expressions/bound_agg_call.h"
#include "binder/expressions/bound_alias.h"
#include "binder/expressions/bound_binary_op.h"
#include "binder/expressions/bound_cast.h"
#include "binder/expressions/bound_column_def.h"
#include "binder/expressions/bound_constant.h"
#include "binder/expressions/bound_func_call.h"
#include "binder/expressions/bound_like_op.h"
#include "binder/expressions/bound_null_test.h"
#include "binder/expressions/bound_parameter.h"
#include "binder/expressions/bound_position_ref_expr.h"
#include "binder/expressions/bound_seq_func.h"
#include "binder/expressions/bound_star.h"
#include "binder/expressions/bound_sub_query.h"
#include "binder/expressions/bound_unary_op.h"
#include "binder/table_ref/bound_join.h"
#include "binder/transform_typename.h"
#include "common/compare_type.h"
#include "common/constrain_type.h"
#include "common/default_value.h"
#include "common/exception.h"
#include "common/gstor_exception.h"
#include "common/null_check_ptr.h"
#include "function/sql_function.h"
#include "nodes/nodes.hpp"
#include "nodes/pg_list.hpp"
#include "pg_functions.hpp"
#include "planner/expression_iterator.h"
#include "type/operator/cast_operators.h"
#include "type/type_system.h"

void Binder::ParseAndSave(const std::string &query) {
    parser_.Parse(query);
    if (!parser_.success) {
        throw intarkdb::Exception(ExceptionType::SYNTAX, "[query fail to parse]" + parser_.error_message);
    }
    SaveParseResult(parser_.parse_tree);
}

auto Binder::SaveParseResult(duckdb_libpgquery::PGList *tree) -> void {
    if (tree != nullptr) {
        for (auto entry = tree->head; entry != nullptr; entry = entry->next) {
            pg_statements_.push_back(reinterpret_cast<duckdb_libpgquery::PGNode *>(entry->data.ptr_value));
        }
    }
}

auto Binder::BindStatement(duckdb_libpgquery::PGNode *stmt) -> std::unique_ptr<BoundStatement> {
    std::unique_ptr<BoundStatement> result;
    if (stmt == nullptr) {
        throw intarkdb::Exception(ExceptionType::BINDER, "statement is nullptr");
    }
    switch (stmt->type) {
        case duckdb_libpgquery::T_PGRawStmt:
            result = BindStatement(NullCheckPtrCast<duckdb_libpgquery::PGRawStmt>(stmt)->stmt);
            break;
        case duckdb_libpgquery::T_PGCreateStmt:
            result = BindCreate(reinterpret_cast<duckdb_libpgquery::PGCreateStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGInsertStmt:
            result = BindInsert(reinterpret_cast<duckdb_libpgquery::PGInsertStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGSelectStmt:
            result = BindSelect(reinterpret_cast<duckdb_libpgquery::PGSelectStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGExplainStmt:
            result = BindExplain(reinterpret_cast<duckdb_libpgquery::PGExplainStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCheckPointStmt:
            result = BindCheckPoint(reinterpret_cast<duckdb_libpgquery::PGCheckPointStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGDeleteStmt:
            result = BindDelete(reinterpret_cast<duckdb_libpgquery::PGDeleteStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGUpdateStmt:
            result = BindUpdate(reinterpret_cast<duckdb_libpgquery::PGUpdateStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGIndexStmt:
            result = BindCreateIndex(reinterpret_cast<duckdb_libpgquery::PGIndexStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGVariableSetStmt:
            result = BindVariableSet(reinterpret_cast<duckdb_libpgquery::PGVariableSetStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGVariableShowStmt:
            result = BindVariableShow(reinterpret_cast<duckdb_libpgquery::PGVariableShowStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGTransactionStmt:
            result = BindTransaction(reinterpret_cast<duckdb_libpgquery::PGTransactionStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGAlterTableStmt:
            result = BindAlter(reinterpret_cast<duckdb_libpgquery::PGAlterTableStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGRenameStmt:
            result = BindRename(reinterpret_cast<duckdb_libpgquery::PGRenameStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGDropStmt:
            result = BindDrop(reinterpret_cast<duckdb_libpgquery::PGDropStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCreateTableAsStmt:
            result = BindCtas(reinterpret_cast<duckdb_libpgquery::PGCreateTableAsStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGViewStmt:
            result = BindCreateView(reinterpret_cast<duckdb_libpgquery::PGViewStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCreateSeqStmt:
            result = BindSequence(reinterpret_cast<duckdb_libpgquery::PGCreateSeqStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCopyStmt:
            result = BindCopy(reinterpret_cast<duckdb_libpgquery::PGCopyStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCommentStmt:
            result = BindCommentOn(reinterpret_cast<duckdb_libpgquery::PGCommentStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCallStmt:
            result = BindCall(reinterpret_cast<duckdb_libpgquery::PGCallStmt *>(stmt));
            break;
        case duckdb_libpgquery::T_PGCaggPolicyStmt:
            result = BindCaggPolicy(reinterpret_cast<duckdb_libpgquery::PGCaggPolicyStmt *>(stmt));
            break;
        default:
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED,
                                      "[not support statement]" + NodeTagToString(stmt->type));
    }

    if (params_cols_map_.size() > 0) {
        int count = 0;
        for (auto &param : params_cols_map_) {  // 为每个参数设置序号
            param.second->slot = count++;
        }
    }
    return result;
}

// PGColumnDef -> Column , 列描述 -> 列实体
auto Binder::BindColumnDefinition(duckdb_libpgquery::PGColumnDef &cdef, uint16_t slot,
                                  std::vector<Constraint> &constraints) -> Column {
    if (cdef.collClause != nullptr) {
        // 表示列的排序规则，先不支持
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "col clause on column is not supported");
    }

    // 列名检查
    std::string colname = cdef.colname;
    if (colname.length() >= GS_NAME_BUFFER_SIZE) {
        throw intarkdb::Exception(ExceptionType::BINDER,
                                  fmt::format("column name is too long, max length:{}", GS_NAME_BUFFER_SIZE - 1));
    }
    colname = intarkdb::StringUtil::Lower(colname);
    Column col(colname, {});
    // TransformTypeName 会返回匹配的类型，如果没有匹配的类型，会抛出异常
    LogicalType gstor_type = TransformTypeName(*cdef.typeName);
    col.SetColType(gstor_type);
    col.SetSlot(slot);
    col.SetNullable(true);  // 默认为可空
    BindSingleColConstraintList(cdef.constraints, col, constraints);
    return col;
}

auto Binder::BoundExpressionToDefaultValue(BoundExpression &expr, Column &column) -> std::vector<uint8_t> {
    std::vector<uint8_t> default_value;
    switch (expr.Type()) {
        case ExpressionType::LITERAL: {
            auto const_expr = static_cast<BoundConstant &>(expr);
            if (const_expr.ValRef().IsNull()) {  // NULL
                column.SetNullable(true);
            } else {
                auto &d_val = const_cast<Value &>(const_expr.ValRef());
                auto col_type = column.GetLogicalType();
                if (col_type.TypeId() != d_val.GetLogicalType().TypeId()) {
                    d_val = DataType::GetTypeInstance(col_type.TypeId())->CastValue(d_val);
                }
                auto default_value_ptr =
                    CreateDefaultValue(DefaultValueType::DEFAULT_VALUE_TYPE_LITERAL, d_val.Size(), d_val.GetRawBuff());
                default_value = std::vector<uint8_t>(
                    (uint8_t *)default_value_ptr.get(),
                    (uint8_t *)default_value_ptr.get() + DefaultValueSize(default_value_ptr.get()));
            }
            break;
        }
        case ExpressionType::UNARY_OP: {  // NOT NULL
            column.SetNullable(false);
            break;
        }
        case ExpressionType::SEQ_FUNC: {
            auto &seq_func_expr = static_cast<BoundSequenceFunction &>(expr);
            const std::string &func_name = seq_func_expr.ToString();
            const std::string &seq_name = seq_func_expr.arg->ToString();
            if (!catalog_.IsSequenceExists(seq_name)) {
                throw intarkdb::Exception(ExceptionType::CATALOG,
                                          fmt::format("Sequence with name {} does not exist!", seq_name));
            }
            auto default_value_ptr =
                CreateDefaultValue(DefaultValueType::DEFAULT_VALUE_TYPE_FUNC, func_name.length(), func_name.c_str());
            default_value =
                std::vector<uint8_t>((uint8_t *)default_value_ptr.get(),
                                     (uint8_t *)default_value_ptr.get() + DefaultValueSize(default_value_ptr.get()));
            break;
        }
        case ExpressionType::FUNC_CALL: {
            auto &func_call_expr = static_cast<BoundFuncCall &>(expr);
            const std::string &func_name = func_call_expr.ToString();
            if (func_call_expr.func_name_ == "now" || func_call_expr.func_name_ == "current_date" ||
                func_call_expr.func_name_ == "random") {
                // 当前只支持上述可变值的函数
                auto default_value_ptr = CreateDefaultValue(DefaultValueType::DEFAULT_VALUE_TYPE_FUNC,
                                                            func_name.length(), func_name.c_str());
                default_value = std::vector<uint8_t>(
                    (uint8_t *)default_value_ptr.get(),
                    (uint8_t *)default_value_ptr.get() + DefaultValueSize(default_value_ptr.get()));
            } else {
                throw intarkdb::Exception(
                    ExceptionType::CATALOG,
                    fmt::format("Column's default expression type {} func_name {} is not supported ", expr.Type(),
                                func_call_expr.func_name_));
            }
            break;
        }
        default:
            throw intarkdb::Exception(ExceptionType::CATALOG,
                                      fmt::format("Column's default expression type {} is not supported", expr.Type()));
    }
    return default_value;
}

static auto CheckValidAutoIncrement(const Column &col) -> void {
    if (col.IsAutoIncrement()) {
        if (!intarkdb::IsInteger(col.GetLogicalType().TypeId())) {
            throw intarkdb::Exception(ExceptionType::BINDER, "autoincrement column must be integer type!");
        }
        if (col.HasDefault()) {
            throw intarkdb::Exception(ExceptionType::BINDER, "autoincrement column must not have default value!");
        }
    }
}

auto Binder::BindSingleColConstraint(duckdb_libpgquery::PGListCell *cell, Column &col) -> std::unique_ptr<Constraint> {
    auto constraint = NullCheckPtrCast<duckdb_libpgquery::PGConstraint>(cell->data.ptr_value);
    switch (constraint->contype) {
        case duckdb_libpgquery::PG_CONSTR_NOTNULL: {
            col.SetNullable(false);
            return nullptr;
        }
        case duckdb_libpgquery::PG_CONSTR_NULL: {
            col.SetNullable(true);
            return nullptr;
        }
        case duckdb_libpgquery::PG_CONSTR_DEFAULT: {
            if (col.IsAutoIncrement()) {
                throw intarkdb::Exception(ExceptionType::BINDER, "autoincrement column must not have default value!");
            }
            auto expr = BindDefault(constraint->raw_expr);
            if (expr) {
                auto default_value = BoundExpressionToDefaultValue(*expr, col);
                if (default_value.size() > 0) {
                    col.SetDefault(default_value);
                }
            }
            return nullptr;
        }
        case duckdb_libpgquery::PG_CONSTR_PRIMARY: {
            col.SetIsPrimaryKey(true);
            col.SetNullable(false);
            return std::make_unique<Constraint>(CONS_TYPE_PRIMARY, std::vector<std::string>{col.Name()});
        }
        case duckdb_libpgquery::PG_CONSTR_UNIQUE: {
            col.SetIsUnique(true);
            return std::make_unique<Constraint>(CONS_TYPE_UNIQUE, std::vector<std::string>{col.Name()});
        }
        case duckdb_libpgquery::PG_CONSTR_GENERATED_VIRTUAL:
        case duckdb_libpgquery::PG_CONSTR_GENERATED_STORED:
        case duckdb_libpgquery::PG_CONSTR_CHECK:
        case duckdb_libpgquery::PG_CONSTR_COMPRESSION:
        case duckdb_libpgquery::PG_CONSTR_FOREIGN: {
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED,
                                      fmt::format("not support {} constraint yet!", constraint->contype));
        }
        case duckdb_libpgquery::PG_CONSTR_EXTENSION_COMMENT: {
            const std::string &comment = BindComment(constraint->raw_expr);
            if (!comment.empty()) {
                col.SetComment(comment);
            }
            return nullptr;
        }
        case duckdb_libpgquery::PG_CONSTR_EXTENSION_AUTOINCREMENT: {
            col.SetAutoIncrement(true);
            CheckValidAutoIncrement(col);
            return nullptr;
        }
        default:
            throw intarkdb::Exception(ExceptionType::BINDER, "Constraint type not handled yet!");
    }
}

auto Binder::BindMultiColConstraint(const duckdb_libpgquery::PGConstraint &constraint) -> Constraint {
    switch (constraint.contype) {
        case duckdb_libpgquery::PG_CONSTR_UNIQUE:
        case duckdb_libpgquery::PG_CONSTR_PRIMARY: {
            bool is_primary_key = constraint.contype == duckdb_libpgquery::PG_CONSTR_PRIMARY;
            std::vector<std::string> columns;
            for (auto kc = constraint.keys->head; kc != nullptr; kc = kc->next) {
                columns.emplace_back(intarkdb::StringUtil::Lower(
                    reinterpret_cast<duckdb_libpgquery::PGValue *>(kc->data.ptr_value)->val.str));
            }
            return Constraint(is_primary_key ? CONS_TYPE_PRIMARY : CONS_TYPE_UNIQUE, columns, constraint.conname);
        }
        case duckdb_libpgquery::PG_CONSTR_CHECK: {
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "not support check constraint yet!");
        }
        case duckdb_libpgquery::PG_CONSTR_FOREIGN: {
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "not support foreign key constraint yet!");
        }
        default:
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "Constraint type not handled yet!");
    }
}

static void CheckUnSupportedFeature(duckdb_libpgquery::PGCreateStmt *pg_stmt) {
    if (!pg_stmt) {
        throw intarkdb::Exception(ExceptionType::BINDER, "create table statement is nullptr");
    }
    if (pg_stmt->relation == nullptr) {
        throw intarkdb::Exception(ExceptionType::BINDER, "create table relation is nullptr");
    }
    std::string_view table_name = pg_stmt->relation->relname ? pg_stmt->relation->relname : "";
    if (table_name.length() >= GS_NAME_BUFFER_SIZE) {
        throw intarkdb::Exception(ExceptionType::BINDER,
                                  fmt::format("table name is too long, max length:{}", GS_NAME_BUFFER_SIZE - 1));
    }
    if (pg_stmt->onconflict == duckdb_libpgquery::PG_REPLACE_ON_CONFLICT) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "replace expr is not supported in create table sql!");
    }
    if (pg_stmt->inhRelations) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "not support inheritance relations");
    }

    if (!pg_stmt->tableElts || !pg_stmt->tableElts->head) {
        throw intarkdb::Exception(ExceptionType::BINDER, "should have at least 1 column");
    }
    if (pg_stmt->relation->relpersistence == duckdb_libpgquery::PG_RELPERSISTENCE_TEMP) {  // create temp table ....
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "not support temp table");
    }
}

auto Binder::BindSingleColConstraintList(duckdb_libpgquery::PGList *constraints_node, Column &column,
                                         std::vector<Constraint> &constraints) -> void {
    if (constraints_node) {
        for (auto constr = constraints_node->head; constr != nullptr; constr = constr->next) {
            auto constraint = BindSingleColConstraint(constr, column);
            if (constraint) {
                constraints.push_back(std::move(*constraint));
            }
        }
    }
}

auto Binder::BindCreateColumnList(duckdb_libpgquery::PGList *tableElts, const std::string &table_name,
                                  std::vector<Column> &columns, std::vector<Constraint> &constraints) -> void {
    if (tableElts == nullptr) {
        throw intarkdb::Exception(ExceptionType::BINDER, "tableElts is nullptr");
    }

    size_t column_slots = 0;
    intarkdb::CaseInsensitiveSet lower_case_col_set;
    bool has_auto_increment = false;
    for (auto c = tableElts->head; c != nullptr; c = lnext(c)) {
        auto node = NullCheckPtrCast<duckdb_libpgquery::PGNode>(c->data.ptr_value);
        switch (node->type) {
            case duckdb_libpgquery::T_PGColumnDef: {
                auto cdef = NullCheckPtrCast<duckdb_libpgquery::PGColumnDef>(c->data.ptr_value);
                // create column
                auto col = BindColumnDefinition(*cdef, column_slots, constraints);
                // 检查列名是否重复
                if (lower_case_col_set.find(col.Name()) != lower_case_col_set.end()) {
                    throw intarkdb::Exception(ExceptionType::BINDER,
                                              fmt::format("Column {} already exists!", col.Name()));
                }
                lower_case_col_set.insert(col.Name());
                if (col.IsAutoIncrement()) {
                    if (has_auto_increment) {
                        throw intarkdb::Exception(ExceptionType::BINDER,
                                                  "not support more than one autoincrement column!");
                    }
                    has_auto_increment = true;
                }
                columns.push_back(std::move(col));
                ++column_slots;
                break;
            }
            case duckdb_libpgquery::T_PGConstraint: {
                // node 已经检查过了，不会为空
                auto constr = reinterpret_cast<duckdb_libpgquery::PGConstraint *>(c->data.ptr_value);
                // 绑定多列约束
                constraints.emplace_back(BindMultiColConstraint(*constr));  // 多列约束
                break;
            }
            default:
                throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "ColumnDef type not handled yet");
        }
    }
}

static auto HandleCreatePartionAndTimescale(duckdb_libpgquery::PGCreateStmt *pg_stmt,
                                            std::unique_ptr<CreateStatement> &result) -> void {
    if (pg_stmt->partspec != NULL) {
        if (pg_stmt->partspec->strategy != duckdb_libpgquery::PARTITION_STRATEGY_RANGE) {
            throw std::runtime_error("part table do not support list or hash strategy yet!");
        }
        if (!pg_stmt->timescale) {
            throw std::runtime_error("we do not support NON-TIMESCALE part table yet!");
        }
        GS_LOG_RUN_INF("partition:strategy-range");
        result->SetPartition(GS_TRUE);
        auto &part_obj = result->GetPartObjDefMutable();
        part_obj.part_type = PART_TYPE_RANGE;
        uint32_t part_col_idx = 0;
        for (auto c = pg_stmt->partspec->partParams->head; c != nullptr; c = lnext(c)) {
            auto part_elem = reinterpret_cast<duckdb_libpgquery::PGPartitionElem *>(c->data.ptr_value);
            GS_LOG_RUN_INF("partition:column name-%s", part_elem->name);
            if (part_elem->name == NULL) {
                throw std::runtime_error("part column name is NULL!");
            }
            bool found = GS_FALSE;
            for (const auto &col : result->GetColumns()) {
                if (col.Name() == part_elem->name) {
                    if (col.GetRaw().col_type != GS_TYPE_TIMESTAMP) {
                        GS_LOG_RUN_ERR("part key column type(%d) is wrong, must be timestamp!", col.GetRaw().col_type);
                        throw std::runtime_error("part key column type is wrong, must be timestamp!");
                    }
                    part_obj.part_keys[part_col_idx].column_id = col.GetRaw().col_slot;
                    part_obj.part_keys[part_col_idx].datatype = col.GetRaw().col_type;
                    part_obj.part_keys[part_col_idx].size = col.GetRaw().size;
                    part_obj.part_keys[part_col_idx].precision = col.GetRaw().precision;
                    part_obj.part_keys[part_col_idx].scale = col.GetRaw().scale;
                    part_col_idx++;
                    found = GS_TRUE;
                    break;
                }
            }
            if (!found) {
                throw std::runtime_error(fmt::format("part column name {} is not found!", part_elem->name));
            }
        }
        part_obj.part_col_count = part_col_idx;
        part_obj.auto_addpart = pg_stmt->autopart;
        part_obj.is_crosspart = pg_stmt->crosspart;
        part_obj.is_interval = GS_FALSE;
        if (pg_stmt->interval) {
            GS_LOG_RUN_INF("partition interval:%s", pg_stmt->interval);
            std::string interv(pg_stmt->interval);
            std::regex pattern("[1-9][0-9]*[hdHD]");
            if (!std::regex_match(interv, pattern)) {
                throw std::runtime_error(
                    fmt::format("the number prefix of interval {} must be a positive integer, and suffix must be h/d!",
                                pg_stmt->interval));
            }
            part_obj.is_interval = GS_TRUE;
            part_obj.interval.str = pg_stmt->interval;
            part_obj.interval.len = strlen(pg_stmt->interval);
        } else {
            if (pg_stmt->timescale) {
                throw std::runtime_error("timescale table missing interval!");
            }
        }
    }
    if (pg_stmt->timescale) {
        GS_LOG_RUN_INF("timescale table");
        result->SetTimescale(GS_TRUE);
        if (!result->Partition()) {
            throw std::runtime_error("timescale table must be parted!");
        }
    }
    if (pg_stmt->retention) {
        GS_LOG_RUN_INF("partition retention time:%s", pg_stmt->retention);
        std::string retent(pg_stmt->retention);
        std::regex pattern("[1-9][0-9]*[hdHD]");
        if (!std::regex_match(retent, pattern)) {
            throw std::runtime_error(
                fmt::format("the number prefix of retention {} must be a positive integer, and suffix must be h/d!",
                            pg_stmt->retention));
        }
        result->SetRetention(pg_stmt->retention);
    } else {
        if (pg_stmt->timescale) {
            GS_LOG_RUN_INF("You do not set the retention for timescale, default 7 days!");
            result->SetRetention((char *)RETENTION_DEFAULT.c_str());
        }
    }
}

auto Binder::BindCreate(duckdb_libpgquery::PGCreateStmt *pg_stmt) -> std::unique_ptr<CreateStatement> {
    // 检查是否有不支持的特性
    CheckUnSupportedFeature(pg_stmt);

    auto columns = std::vector<Column>{};
    auto constraints = std::vector<Constraint>{};
    std::string table_name = pg_stmt->relation->relname ? pg_stmt->relation->relname : "";
    table_name = intarkdb::StringUtil::Lower(table_name);
    auto result = std::make_unique<CreateStatement>(table_name);

    // tableElts 为列定义，一般为 PGColumnDef(列定义) 或 PGConstraint(索引定义)
    BindCreateColumnList(pg_stmt->tableElts, table_name, columns, constraints);

    result->SetColumns(std::move(columns));
    result->SetConstraints(std::move(constraints));

    // 处理分区表
    HandleCreatePartionAndTimescale(pg_stmt, result);

    // TABLE COMMENT
    result->SetComment(pg_stmt->comment);

    result->SetConflictOpt(pg_stmt->onconflict);

    return result;
}

auto Binder::BindBaseTableRef(const std::string &table_name, std::optional<std::string> &&alias, bool if_exists)
    -> std::unique_ptr<BoundBaseTable> {
    // 查找表是否存
    auto table_info = catalog_.GetTable(table_name);
    if (!table_info) {
        if (if_exists) {
            GS_LOG_RUN_WAR("%s", fmt::format("table {} not exists!", table_name).c_str());
            return nullptr;
        }
        // no table
        GS_LOG_RUN_WAR("%s", fmt::format("table {} not exists!", table_name).c_str());
        throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("table {} not exists!", table_name));
    }
    return std::make_unique<BoundBaseTable>(table_name, std::move(alias), std::move(table_info));
}

static auto CheckBindRangVar(const duckdb_libpgquery::PGRangeVar &table_ref) -> void {
    if (table_ref.schemaname != nullptr) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "schema name is not supported yet");
    }
    if (table_ref.catalogname != nullptr) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "catalog name is not supported yet");
    }
    if (table_ref.sample != nullptr) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "sample is not supported yet");
    }
    if (table_ref.alias != nullptr && table_ref.alias->colnames != nullptr) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "alias(colnames) is not supported yet");
    }
    if (table_ref.relname == nullptr) {
        throw intarkdb::Exception(ExceptionType::BINDER, "table name is nullptr");
    }
}

auto Binder::BindRangeVar(const duckdb_libpgquery::PGRangeVar &table_ref, bool is_select)
    -> std::unique_ptr<BoundTableRef> {
    CheckBindRangVar(table_ref);

    std::unique_ptr<BoundBaseTable> base_table = BindBaseTableRef(
        table_ref.relname, table_ref.alias ? std::make_optional(table_ref.alias->aliasname) : std::nullopt);
    if (!base_table) {
        throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("table {} not exists!", table_ref.relname));
    }
    const auto &table_info = base_table->GetTableInfo();
    switch (table_info.GetObjectType()) {
        case DIC_TYPE_TABLE:
            base_table->SetDictType(DIC_TYPE_TABLE);
            if (is_select) {
                ctx.AddTableBinding(base_table->GetTableNameOrAlias(), table_info.columns);
            }
            return base_table;
        case DIC_TYPE_VIEW: {
            auto query_sql = table_info.GetTableMetaInfo().sql;
            Binder view_binder(this);
            view_binder.ParseAndSave(query_sql.str);
            const auto &stmts = view_binder.GetStatementNodes();
            auto node = reinterpret_cast<duckdb_libpgquery::PGNode *>(stmts[0]);
            if (node->type == duckdb_libpgquery::T_PGRawStmt) {
                node = reinterpret_cast<duckdb_libpgquery::PGRawStmt *>(node)->stmt;
            }
            if (node->type != duckdb_libpgquery::T_PGViewStmt && node->type != duckdb_libpgquery::T_PGRawStmt) {
                throw intarkdb::Exception(ExceptionType::BINDER,
                                          fmt::format("invalid view statement {}", NodeTagToString(node->type)));
            }
            auto view_node = reinterpret_cast<duckdb_libpgquery::PGViewStmt *>(node);
            return BindSubquery(reinterpret_cast<duckdb_libpgquery::PGSelectStmt *>(view_node->query),
                                view_node->view->relname);
        }
        default:
            throw std::invalid_argument(
                fmt::format("unsupported dictionary type: {}", (int)table_info.GetObjectType()));
    }
}

auto Binder::BindTableRef(const duckdb_libpgquery::PGNode &node) -> std::unique_ptr<BoundTableRef> {
    switch (node.type) {
        case duckdb_libpgquery::T_PGRangeVar: {
            // 表 或 视图 类型，比如 select * from student;
            return BindRangeVar((const duckdb_libpgquery::PGRangeVar &)node);
        }
        case duckdb_libpgquery::T_PGJoinExpr: {
            // 连接表达式类型，比如SELECT * FROM employees JOIN departments ON employees.department_id = departments.id;
            return BindJoin((const duckdb_libpgquery::PGJoinExpr &)node);
        }
        case duckdb_libpgquery::T_PGRangeSubselect: {
            // 子查询类型，比如SELECT department_id FROM (SELECCT * FROM emplyees ) WHERE salary > 10;
            // 就是T_PGRangeSubselect类型的节点
            return BindRangeSubselect((const duckdb_libpgquery::PGRangeSubselect &)node);
        }
        default:
            throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED,
                                      fmt::format("unsupported node type: {}", Binder::NodeTagToString(node.type)));
    }
}

static auto TransformJoinType(duckdb_libpgquery::PGJoinType type) -> JoinType {
    JoinType join_type = JoinType::Invalid;
    switch (type) {
        case duckdb_libpgquery::PG_JOIN_INNER:
            join_type = JoinType::CrossJoin;
            break;
        case duckdb_libpgquery::PG_JOIN_LEFT:
            join_type = JoinType::LeftJoin;
            break;
        default:
            throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("Join type {} not supported", type));
    }
    return join_type;
}

auto Binder::BindJoin(const duckdb_libpgquery::PGJoinExpr &node) -> std::unique_ptr<BoundTableRef> {
    if (node.usingClause) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "USING clause is not supported yet");
    }
    if (node.isNatural) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "NATURAL JOIN is not supported yet");
    }

    if (node.alias && node.alias->colnames) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "join with alias(colnames) is not supported yet");
    }

    JoinType join_type = TransformJoinType(node.jointype);

    Binder left_table_binder(this);
    Binder right_table_binder(this);
    auto left_table = left_table_binder.BindTableRef(*NullCheckPtrCast<duckdb_libpgquery::PGNode>(node.larg));
    auto right_table = right_table_binder.BindTableRef(*NullCheckPtrCast<duckdb_libpgquery::PGNode>(node.rarg));

    ctx.MergeTableBinding(left_table_binder.ctx);
    ctx.MergeTableBinding(right_table_binder.ctx);

    // TODO 子查询correlated_columns 处理

    auto join = std::make_unique<BoundJoin>(join_type, std::move(left_table), std::move(right_table), nullptr);
    if (node.quals != nullptr) {
        join->on_condition = BindExpression(node.quals);
    }
    return join;
}

auto Binder::BindSubquery(duckdb_libpgquery::PGSelectStmt *node, const std::string &subquery_name)
    -> std::unique_ptr<BoundSubquery> {
    // 绑定select statement
    auto subquery_binder = Binder(this);
    auto subquery = subquery_binder.BindSelect(node);
    // 查询列表
    std::vector<std::string> col_names;
    std::vector<LogicalType> col_types;
    for (const auto &item : subquery->return_list) {
        col_names.push_back(item.col_name.back());  // 只保留列名
        col_types.push_back(item.col_type);
    }
    if (!col_names.empty()) {
        ctx.AddTableBinding(subquery_name, std::move(col_names), std::move(col_types));
    }
    // TODO: handle correlated_columns
    return std::make_unique<BoundSubquery>(std::move(subquery), subquery_name);
}

auto Binder::BindRangeSubselect(const duckdb_libpgquery::PGRangeSubselect &node) -> std::unique_ptr<BoundTableRef> {
    if (node.lateral) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "LATERNAL in subquery is not supported yet");
    }
    if (node.sample) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "SAMPLE in subquery is not supported yet");
    }
    // 暂不支持如下语法 e.g: select * from (select 1) as t(a);
    if (node.alias != nullptr && node.alias->colnames != nullptr) {
        throw intarkdb::Exception(ExceptionType::NOT_IMPLEMENTED, "subquery with alias(colnames) is not supported yet");
    }

    // 子查询别名
    std::string subquery_name = node.alias ? node.alias->aliasname : fmt::format("__subquery_{}", GetNextUniversalID());
    // 将子查询的列绑定到当前上下文
    return BindSubquery(reinterpret_cast<duckdb_libpgquery::PGSelectStmt *>(node.subquery), subquery_name);
}

static auto CreateCrossJoin(std::unique_ptr<BoundTableRef> left, std::unique_ptr<BoundTableRef> right)
    -> std::unique_ptr<BoundJoin> {
    return std::make_unique<BoundJoin>(JoinType::CrossJoin, std::move(left), std::move(right), nullptr);
}

auto Binder::BindFrom(duckdb_libpgquery::PGList *list) -> std::unique_ptr<BoundTableRef> {
    if (list == nullptr) {
        // not table
        return std::make_unique<BoundTableRef>(DataSourceType::DUAL);
    }

    if (list->length == 1) {
        return BindTableRef(*NullCheckPtrCast<duckdb_libpgquery::PGNode>(list->head->data.ptr_value));
    }

    auto curr = list->head;
    auto left_table = BindTableRef(*NullCheckPtrCast<duckdb_libpgquery::PGNode>(curr->data.ptr_value));
    curr = lnext(curr);

    auto right_table = BindTableRef(*NullCheckPtrCast<duckdb_libpgquery::PGNode>(curr->data.ptr_value));
    curr = lnext(curr);

    auto result = CreateCrossJoin(std::move(left_table), std::move(right_table));

    for (; curr != nullptr; curr = lnext(curr)) {
        result = CreateCrossJoin(std::move(result),
                                 BindTableRef(*NullCheckPtrCast<duckdb_libpgquery::PGNode>(curr->data.ptr_value)));
    }
    return result;
}

auto Binder::BindTableAllColumns(const std::string &table_name) -> std::vector<std::unique_ptr<BoundExpression>> {
    auto columns = std::vector<std::unique_ptr<BoundExpression>>{};
    auto binding = ctx.GetTableBinding(table_name);
    if (!binding) {
        throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("reference table {} not found", table_name));
    }
    const auto &col_names = binding->GetColNames();
    const auto &col_types = binding->GetColTypes();

    columns.reserve(col_names.size());
    for (size_t i = 0; i < col_names.size(); ++i) {
        columns.emplace_back(std::make_unique<BoundColumnRef>(std::vector{table_name, col_names[i]}, col_types[i]));
    }
    return columns;
}

auto Binder::BindAllColumns(const char *expect_relation_name) -> std::vector<std::unique_ptr<BoundExpression>> {
    if (expect_relation_name) {
        auto columns = BindTableAllColumns(expect_relation_name);
        if (columns.empty()) {
            throw intarkdb::Exception(ExceptionType::BINDER,
                                      fmt::format("reference table {} not found", expect_relation_name));
        }
        return columns;
    } else {
        const auto &table_bindings_vec = ctx.GetAllTableBindings();
        std::vector<std::unique_ptr<BoundExpression>> columns;
        for (const auto &table_bindings : table_bindings_vec) {
            const auto &table_name = table_bindings.GetTableName();
            auto table_columns = BindTableAllColumns(table_name);
            std::copy(std::make_move_iterator(table_columns.begin()), std::make_move_iterator(table_columns.end()),
                      std::back_inserter(columns));
        }
        return columns;
    }
}

// 从表达式中查找 star 表达式
static auto SearchAndHandleStarExpression(BoundExpression &expr, BoundStar **star_expr, bool is_root_expr) -> bool {
    bool found_star_expr = false;
    if (expr.Type() == ExpressionType::STAR) {
        // star 表达式只能是根表达式
        if (is_root_expr) {
            *star_expr = static_cast<BoundStar *>(&expr);
            return true;
        }
        // 比如 * + 1 ，这种情况下 star 表达式不是根表达式
        throw intarkdb::Exception(ExceptionType::BINDER, "star expression must be root expression");
    }
    if (expr.Type() == ExpressionType::ALIAS) {  // fix for: select * as a from test
        auto &alias_expr = static_cast<BoundAlias &>(expr);
        return SearchAndHandleStarExpression(*alias_expr.child_, star_expr, true);  // 别名特殊处理，root状态不变
    }
    ExpressionIterator::EnumerateChildren(expr, [&](BoundExpression &child) {
        if (SearchAndHandleStarExpression(child, star_expr, false)) {
            found_star_expr = true;
        }
    });
    return found_star_expr;
}

/** 绑定 select 子句 */
auto Binder::BindSelectList(duckdb_libpgquery::PGList *list) -> std::vector<std::unique_ptr<BoundExpression>> {
    auto select_list = std::vector<std::unique_ptr<BoundExpression>>{};
    for (auto node = list->head; node != nullptr; node = lnext(node)) {
        auto target = reinterpret_cast<duckdb_libpgquery::PGNode *>(node->data.ptr_value);
        // 表达式解释
        BoundStar *star_expr = nullptr;
        auto expr = BindExpression(target);
        if (SearchAndHandleStarExpression(*expr, &star_expr, true)) {
            auto all_select_list = BindAllColumns(star_expr->GetRelationName());
            if (all_select_list.empty()) {
                // select *
                throw intarkdb::Exception(ExceptionType::SYNTAX, "no valid table name for star expression");
            }
            std::copy(std::make_move_iterator(all_select_list.begin()), std::make_move_iterator(all_select_list.end()),
                      std::back_inserter(select_list));
        } else {
            select_list.emplace_back(std::move(expr));
        }
    }
    return select_list;
}

auto Binder::BindExpression(duckdb_libpgquery::PGNode *node) -> std::unique_ptr<BoundExpression> {
    if (!node) {
        return nullptr;
    }
    switch (node->type) {
        case duckdb_libpgquery::T_PGResTarget:
            // typeT_ResTarget表示一个列,ResTarget 再细分为其他类型(ColumnRef,Star等)
            return BindResTarget(reinterpret_cast<duckdb_libpgquery::PGResTarget *>(node));
        case duckdb_libpgquery::T_PGColumnRef:
            return BindColumnRef(reinterpret_cast<duckdb_libpgquery::PGColumnRef *>(node));
        case duckdb_libpgquery::T_PGAConst:
            return BindConstant(reinterpret_cast<duckdb_libpgquery::PGAConst *>(node));
        case duckdb_libpgquery::T_PGAStar:
            return BindStar(reinterpret_cast<duckdb_libpgquery::PGAStar *>(node));
        case duckdb_libpgquery::T_PGFuncCall:
            return BindFuncCall(reinterpret_cast<duckdb_libpgquery::PGFuncCall *>(node));
        case duckdb_libpgquery::T_PGAExpr:
            return BindAExpr(reinterpret_cast<duckdb_libpgquery::PGAExpr *>(node));
        case duckdb_libpgquery::T_PGBoolExpr: {
            return BindBoolExpr(reinterpret_cast<duckdb_libpgquery::PGBoolExpr *>(node));
        }
        case duckdb_libpgquery::T_PGNullTest: {
            return BindNullTest(reinterpret_cast<duckdb_libpgquery::PGNullTest *>(node));
        }
        case duckdb_libpgquery::T_PGSubLink: {
            return BindSubQueryExpr(reinterpret_cast<duckdb_libpgquery::PGSubLink *>(node));
        }
        case duckdb_libpgquery::T_PGParamRef: {
            return BindParamRef(reinterpret_cast<duckdb_libpgquery::PGParamRef *>(node));
        }
        case duckdb_libpgquery::T_PGTypeCast: {
            return BindTypeCast(reinterpret_cast<duckdb_libpgquery::PGTypeCast *>(node));
        }
        default:
            break;
    }
    throw intarkdb::Exception(ExceptionType::BINDER,
                              fmt::format("type {} expr  not supported", Binder::NodeTagToString(node->type)));
}

auto Binder::BindParamRef(duckdb_libpgquery::PGParamRef *node) -> std::unique_ptr<BoundExpression> {
    auto expr = std::make_unique<BoundParameter>();
    if (node->number < 0) {
        throw std::invalid_argument("Parameter numbers cannot be negative");
    }

    if (node->name) {
        // This is a named parameter, try to find an entry for it
        throw std::invalid_argument("Named parameter not support");
    }
    if (node->number == 0) {
        expr->parameter_nr = ParamCount() + 1;
    } else {
        expr->parameter_nr = node->number;
    }
    auto root_binder = RootBinder();
    root_binder->n_param_ = ParamCount() > expr->parameter_nr ? ParamCount() : expr->parameter_nr;
    expr->slot = 0;

    root_binder->params_cols_map_.insert(std::make_pair(node->location, expr.get()));
    return expr;
}

static auto CheckSubqueryColumns(size_t size, duckdb_libpgquery::PGSubLink *root) -> void {
    if (size == 0) {
        throw intarkdb::Exception(ExceptionType::BINDER, "subquery should have select list");
    }
    if ((root->subLinkType == duckdb_libpgquery::PG_ANY_SUBLINK ||
         root->subLinkType == duckdb_libpgquery::PG_ALL_SUBLINK) &&
        size > 1) {
        throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("subquery returns {} columns - expected 1", size));
    }
}

auto Binder::BindSubQueryExpr(duckdb_libpgquery::PGSubLink *root) -> std::unique_ptr<BoundExpression> {
    // testexpr是外部查询的表达式，先绑定
    std::unique_ptr<BoundExpression> child = nullptr;
    if (root->subLinkType == duckdb_libpgquery::PG_ALL_SUBLINK ||
        root->subLinkType == duckdb_libpgquery::PG_ANY_SUBLINK) {
        child = BindExpression(root->testexpr);
    }

    Binder subquery_binder(this);
    // 特殊需求，这里使用shated_ptr
    auto select_statment = std::shared_ptr<SelectStatement>(
        subquery_binder.BindSelect(reinterpret_cast<duckdb_libpgquery::PGSelectStmt *>(root->subselect)));

    if (select_statment->select_expr_list.size() == 0) {
        throw intarkdb::Exception(ExceptionType::BINDER, "subquery should have select list");
    }

    // ALL or ANY 子查询只能返回一个列
    CheckSubqueryColumns(select_statment->select_expr_list.size(), root);

    std::unique_ptr<BoundExpression> subquery;
    // note: 外部可能不存在其他 table , 比如 : insert into t3 values ((select b from t3));
    std::vector<std::unique_ptr<BoundExpression>> correlated_columns =
        std::move(subquery_binder.ctx.correlated_columns);

    switch (root->subLinkType) {
        case duckdb_libpgquery::PG_EXISTS_SUBLINK: {
            // eg select * from table1 where exists (select * from table2 where table1.id = table2.id);
            subquery =
                std::make_unique<BoundSubqueryExpr>(SubqueryType::EXISTS, std::move(select_statment),
                                                    std::move(correlated_columns), nullptr, "", GetNextUniversalID());
            break;
        }
        case duckdb_libpgquery::PG_EXPR_SUBLINK: {
            // e.g: select * from table1 where id = (select id from table2 where id is not null);
            subquery =
                std::make_unique<BoundSubqueryExpr>(SubqueryType::SCALAR, std::move(select_statment),
                                                    std::move(correlated_columns), nullptr, "", GetNextUniversalID());
            break;
        }
        case duckdb_libpgquery::PG_ANY_SUBLINK:
        case duckdb_libpgquery::PG_ALL_SUBLINK: {
            // ANY e.g select * from table1 where id = ANY (select id from table2 where id is not null);
            // ALL e.g select * from table1 where id > ALL (select id from table2)
            // IN  e.g select * from table1 where id in (select id from table2)  == ANY
            std::string op_name = "";
            if (!root->operName) {
                // default is equal
                op_name = "=";
            } else {
                op_name = std::string(
                    (reinterpret_cast<duckdb_libpgquery::PGValue *>(root->operName->head->data.ptr_value))->val.str);
            }
            // check op_name is valid
            if (!intarkdb::IsValidComparisonOperation(op_name)) {
                throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("op_name {} not supported", op_name));
            }

            subquery = std::make_unique<BoundSubqueryExpr>(
                root->subLinkType == duckdb_libpgquery::PG_ANY_SUBLINK ? SubqueryType::ANY : SubqueryType::ALL,
                std::move(select_statment), std::move(correlated_columns), std::move(child), op_name,
                GetNextUniversalID());
            break;
        }
        default:
            throw intarkdb::Exception(ExceptionType::BINDER,
                                      fmt::format("subquery type {} not supported", (int)root->subLinkType));
    }
    return subquery;
}

auto Binder::BindNullTest(duckdb_libpgquery::PGNullTest *root) -> std::unique_ptr<BoundExpression> {
    auto target = reinterpret_cast<duckdb_libpgquery::PGNode *>(root->arg);
    if (target == nullptr) {
        throw intarkdb::Exception(ExceptionType::BINDER, "NullTest Expression should not be null argument");
    }
    auto expr = BindExpression(target);
    auto null_test_type =
        root->nulltesttype == duckdb_libpgquery::IS_NOT_NULL ? NullTest::IS_NOT_NULL : NullTest::IS_NULL;
    return std::make_unique<BoundNullTest>(std::move(expr), null_test_type);
}

auto Binder::BindTypeCast(duckdb_libpgquery::PGTypeCast *root) -> std::unique_ptr<BoundExpression> {
    auto type = TransformTypeName(*root->typeName);
    return std::make_unique<BoundCast>(type, BindExpression(root->arg), root->tryCast);
}

static auto IsValidFunction(const std::string &funcname) -> bool {
    if (SQLFunction::FUNC_MAP.find(funcname) != SQLFunction::FUNC_MAP.end()) {
        return true;
    }
    return false;
}

static auto IsValidParamType(const std::string &funcname, const std::vector<std::unique_ptr<BoundExpression>> &children)
    -> bool {
    if(funcname == "sum" || funcname == "avg") {
        if (children.size() != 1) {
            return false;
        }
        const auto& param = children[0];
        auto type = param->ReturnType().TypeId();
        if(intarkdb::IsNumeric(type)) {
            return true;
        }
        return false;
    }
    return true;
}

auto Binder::BindFuncCall(duckdb_libpgquery::PGFuncCall *root) -> std::unique_ptr<BoundExpression> {
    auto name = root->funcname;
    std::string catalog, schema, function_name;
    if (name->length == 3) {
        // catalog + schema + name
        catalog = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->data.ptr_value)->val.str;
        schema = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->next->data.ptr_value)->val.str;
        function_name = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->next->next->data.ptr_value)->val.str;
    } else if (name->length == 2) {
        // schema + name
        schema = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->data.ptr_value)->val.str;
        function_name = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->next->data.ptr_value)->val.str;
    } else if (name->length == 1) {
        // unqualified name
        function_name = reinterpret_cast<duckdb_libpgquery::PGValue *>(name->head->data.ptr_value)->val.str;
    } else {
        throw intarkdb::Exception(ExceptionType::BINDER, "BindFuncCall - Expected 1, 2 or 3 qualifications");
    }
    // transfor function_name into lower
    function_name = intarkdb::StringUtil::Lower(function_name);

    std::vector<std::unique_ptr<BoundExpression>> children;
    if (root->args != nullptr) {
        for (auto node = root->args->head; node != nullptr; node = node->next) {
            auto child_expr = BindExpression(static_cast<duckdb_libpgquery::PGNode *>(node->data.ptr_value));
            children.push_back(std::move(child_expr));
        }
    }

    if (function_name == "min" || function_name == "max" || function_name == "sum" || function_name == "count" ||
        function_name == "avg" || function_name == "mode") {
        // Rewrite count(*) to count_star().
        if (function_name == "count") {
            if (children.size() == 0 || (children.size() > 0 && children[0]->Type() == ExpressionType::STAR)) {
                function_name = "count_star";
                children.clear();
            }
        }
        // Bind function as agg call.
        if (IsValidParamType(function_name, children)) {
            return std::make_unique<BoundAggCall>(function_name, root->agg_distinct, std::move(children));
        } else {
            throw intarkdb::Exception(ExceptionType::BINDER,
                                      fmt::format("Invalid param type for function {}", function_name));
        }
    }

    if (IsLikeFunction(function_name)) {
        return std::make_unique<BoundLikeOp>(std::move(function_name), std::move(children));
    }

    if (function_name == "nextval" || function_name == "currval") {
        if (children.size() != 1) {
            throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("{} should have one argument", function_name));
        }
        return std::make_unique<BoundSequenceFunction>(function_name, std::move(children[0]), catalog_);
    }

    if (IsValidFunction(function_name)) {
        return std::make_unique<BoundFuncCall>(function_name, std::move(children));
    }
    throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("unspport func name {}", function_name));
}

auto Binder::BindResTarget(duckdb_libpgquery::PGResTarget *root) -> std::unique_ptr<BoundExpression> {
    auto expr = BindExpression(root->val);
    if (!expr) {
        return nullptr;
    }
    if (root->name != nullptr && strlen(root->name) > 0) {
        return std::make_unique<BoundAlias>(root->name, std::move(expr));
    }
    return expr;
}

auto Binder::BindStar(duckdb_libpgquery::PGAStar *node) -> std::unique_ptr<BoundExpression> {
    if (node->except_list) {
        // e.g SELECT * EXCLUDE(y) FROM integers;
        throw intarkdb::Exception(ExceptionType::BINDER, "exclude list is not supported yet");
    }
    if (node->replace_list) {
        // e.g SELECT * REPLACE (y as z) FROM integers;
        throw intarkdb::Exception(ExceptionType::BINDER, "replace list is not supported yet");
    }
    if (node->expr || node->columns) {
        // e.g SELECT COLUMNS([x for x in (*) if x <> 'integers.i']) FROM integers;
        throw intarkdb::Exception(ExceptionType::BINDER, "star expr in column is not supported yet");
    }
    // SELECT * FROM tbl; 时 relation 为空
    // SELECT tbl.* from tbl; 时 relation 为 tbl
    return std::make_unique<BoundStar>(node->relation);
}

auto Binder::BindValue(duckdb_libpgquery::PGValue *node) -> std::unique_ptr<BoundExpression> {
    auto val = node->val;
    switch (node->type) {
        case duckdb_libpgquery::T_PGInteger: {
            if (val.ival > INT32_MAX) {
                throw std::invalid_argument("integer out of range");
            }
            return std::make_unique<BoundConstant>(ValueFactory::ValueInt(val.ival));
        }
        case duckdb_libpgquery::T_PGBitString:
        case duckdb_libpgquery::T_PGString: {
            return std::make_unique<BoundConstant>(ValueFactory::ValueVarchar(val.str));
        }
        case duckdb_libpgquery::T_PGFloat: {
            auto len = strlen(val.str);
            if (len >= GS_MAX_DEC_OUTPUT_ALL_PREC) {
                len = GS_MAX_DEC_OUTPUT_ALL_PREC - 1;
            }
            std::string str_val(val.str, len);
            bool try_cast_as_integer = true;
            bool try_cast_as_decimal = true;
            int decimal_position = -1;
            for (size_t i = 0; i < str_val.length(); ++i) {
                if (str_val[i] == '.') {
                    try_cast_as_integer = false;
                    decimal_position = i;
                }
                if (str_val[i] == 'e' || str_val[i] == 'E') {
                    // 科学计数法，只能转换为double
                    try_cast_as_integer = false;
                    try_cast_as_decimal = false;
                }
            }
            if (try_cast_as_integer) {
                int64_t bigint_value;
                if (TryCast::Operation<std::string, int64_t>(str_val, bigint_value)) {
                    return std::make_unique<BoundConstant>(ValueFactory::ValueBigInt(bigint_value));
                }
                hugeint_t hugeint_value;
                if (TryCast::Operation<std::string, hugeint_t>(str_val, hugeint_value)) {
                    return std::make_unique<BoundConstant>(ValueFactory::ValueHugeInt(hugeint_value));
                }
            }
            // 计算decimal 的精度
            size_t decimal_offset = str_val[0] == '-' ? 3 : 2;
            if (try_cast_as_decimal && decimal_position >= 0 &&
                str_val.length() < DecimalPrecision::max + decimal_offset) {
                // figure out the width/scale based on the decimal position
                auto width = static_cast<uint8_t>(str_val.length() - 1);
                auto scale = static_cast<uint8_t>(width - decimal_position);
                if (str_val[0] == '-') {
                    width--;
                }
                if (width <= DecimalPrecision::max) {
                    // we can cast the value as a decimal
                    auto dec_value = ValueFactory::ValueDecimal(Cast::Operation<std::string, dec4_t>(str_val));
                    dec_value.SetScaleAndPrecision(scale, width);
                    return std::make_unique<BoundConstant>(dec_value);
                }
            }
            return std::make_unique<BoundConstant>(
                ValueFactory::ValueDouble(Cast::Operation<std::string, double>(str_val)));
        }
        case duckdb_libpgquery::T_PGNull: {
            return std::make_unique<BoundConstant>(ValueFactory::ValueNull());
        }
        default:
            break;
    }
    throw intarkdb::Exception(ExceptionType::BINDER,
                              fmt::format("unsupported value type: {}", Binder::NodeTagToString(node->type)));
}

auto Binder::BindConstant(duckdb_libpgquery::PGAConst *node) -> std::unique_ptr<BoundExpression> {
    auto &val = node->val;
    return BindValue(&val);
}

auto Binder::BindColumnRefFromTableBinding(const std::vector<std::string> &col_name)
    -> std::unique_ptr<BoundExpression> {
    if (col_name.size() > 1) {
        const std::string &table_name = col_name[0];
        const std::string &name = col_name[1];
        auto binding = ctx.GetTableBinding(table_name);
        if (binding) {
            const auto &col_names = binding->GetColNames();
            const auto &col_types = binding->GetColTypes();
            for (size_t i = 0; i < col_names.size(); i++) {
                if (intarkdb::StringUtil::IsEqualIgnoreCase(col_names[i], name)) {
                    return std::make_unique<BoundColumnRef>(std::vector<std::string>{table_name, col_names[i]},
                                                            col_types[i]);
                }
            }
        }
    } else {
        const std::string &name = col_name[0];
        std::unique_ptr<BoundExpression> expr = nullptr;
        for (const auto &binding : ctx.table_bindings_vec) {
            const std::string &table_name = binding.GetTableName();
            const auto &col_names = binding.GetColNames();
            const auto &col_types = binding.GetColTypes();
            for (size_t i = 0; i < col_names.size(); i++) {
                if (intarkdb::StringUtil::IsEqualIgnoreCase(col_names[i], name)) {
                    if (!expr) {
                        expr = std::make_unique<BoundColumnRef>(std::vector<std::string>{table_name, col_names[i]},
                                                                col_types[i]);
                    } else {
                        throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("ambiguous column {}", name));
                    }
                }
            }
        }
        if (expr) {
            return expr;
        }
        // 从别名中查找
        for (const auto &alias : ctx.alias_binding) {
            if (intarkdb::StringUtil::IsEqualIgnoreCase(alias.first, name)) {
                if (alias.second.idx != INVALID_IDX) {
                    return std::make_unique<BoundPositionRef>(alias.second.idx, alias.second.type);
                }
                throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("ambiguous alias {}", name));
            }
        }
    }

    // 从上层中查找列信息
    if (parent_binder_) {
        auto expr = parent_binder_->BindColumnRefFromTableBinding(col_name);
        // 考虑expr的类型 PositionRef or ColumnRef
        if (expr != nullptr) {
            ctx.correlated_columns.push_back(expr->Copy());
            if (expr->Type() == ExpressionType::COLUMN_REF) {
                auto &column_ref = static_cast<BoundColumnRef &>(*expr);
                column_ref.SetOuterFlag(true);
                return expr;
            }
        }
    }
    if (check_column_exist_) {
        throw intarkdb::Exception(ExceptionType::BINDER, fmt::format("column {} not found", fmt::join(col_name, ".")));
    }
    return std::make_unique<BoundColumnRef>(col_name, GS_TYPE_UNKNOWN);
}

constexpr int MAX_COLUMN_ITEM_NUM = 2;
static auto TransformColumName(duckdb_libpgquery::PGList *fields) -> std::vector<std::string> {
    if (fields->length < 1) {
        throw intarkdb::Exception(ExceptionType::SYNTAX, "unexpected field length");
    }
    // fields 会有多段，比如 表前缀
    // e.g: select a.sid from student as a; columns_names wiil be ["a","sid"]
    // 如果没有表前缀，那么columns_names只有一段
    std::vector<std::string> column_names;
    for (auto node = fields->head; node != nullptr; node = node->next) {
        // TODO: 列名不可直接转为小写，会引起其他问题
        column_names.emplace_back(NullCheckPtrCast<duckdb_libpgquery::PGValue>(node->data.ptr_value)->val.str);
    }
    // 列名最多只能有两段
    if (column_names.size() > MAX_COLUMN_ITEM_NUM) {
        throw intarkdb::Exception(ExceptionType::BINDER, "column reference must have at most two identifiers");
    }
    return column_names;
}

auto Binder::BindColumnRef(duckdb_libpgquery::PGColumnRef *node) -> std::unique_ptr<BoundExpression> {
    auto column_fields = NullCheckPtrCast<duckdb_libpgquery::PGNode>(node->fields->head->data.ptr_value);
    if (column_fields->type == duckdb_libpgquery::T_PGString) {
        return BindColumnRefFromTableBinding(TransformColumName(node->fields));
    }
    throw intarkdb::Exception(
        ExceptionType::NOT_IMPLEMENTED,
        fmt::format("ColumnRef type {} not implemented!", Binder::NodeTagToString(column_fields->type)));
}

auto Binder::BindTransaction(duckdb_libpgquery::PGTransactionStmt *pg_stmt) -> std::unique_ptr<TransactionStatement> {
    switch (pg_stmt->kind) {
        case duckdb_libpgquery::PG_TRANS_STMT_BEGIN:
        case duckdb_libpgquery::PG_TRANS_STMT_START:
            return std::make_unique<TransactionStatement>(TransactionType::BEGIN_TRANSACTION);
        case duckdb_libpgquery::PG_TRANS_STMT_COMMIT:
            return std::make_unique<TransactionStatement>(TransactionType::COMMIT);
        case duckdb_libpgquery::PG_TRANS_STMT_ROLLBACK:
            return std::make_unique<TransactionStatement>(TransactionType::ROLLBACK);
        default:
            throw intarkdb::Exception(
                ExceptionType::NOT_IMPLEMENTED,
                fmt::format("Transaction type {} not implemented yet", static_cast<int>(pg_stmt->kind)));
    }
}

auto Binder::BindVariableSet(duckdb_libpgquery::PGVariableSetStmt *stmt) -> std::unique_ptr<SetStatement> {
    if (stmt->kind != duckdb_libpgquery::VariableSetKind::VAR_SET_VALUE) {
        throw std::invalid_argument(fmt::format("VariableSetKind is not implemented."));
    }

    if (stmt->scope == duckdb_libpgquery::VariableSetScope::VAR_SET_SCOPE_LOCAL) {
        throw std::invalid_argument(fmt::format("VariableSetScope is not implemented."));
    }

    std::string name = std::string(stmt->name);
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    if (name.empty()) {
        throw std::invalid_argument(fmt::format("set key is empty."));
    }
    if (name != "auto_commit") {
        throw std::invalid_argument(fmt::format("SET name not support."));
    }
    if (stmt->args->length != 1) {
        throw std::invalid_argument(fmt::format("SET needs a single scalar value parameter"));
    }
    if (!stmt->args->head || !stmt->args->head->data.ptr_value) {
        throw std::invalid_argument(fmt::format("args is empty."));
    }
    if (((duckdb_libpgquery::PGNode *)stmt->args->head->data.ptr_value)->type != duckdb_libpgquery::T_PGAConst) {
        throw std::invalid_argument(fmt::format("only support T_PGAConst."));
    }

    if (((duckdb_libpgquery::PGAConst *)stmt->args->head->data.ptr_value)->val.type != duckdb_libpgquery::T_PGString) {
        throw std::invalid_argument(fmt::format("SET value type not support."));
    }

    std::string value = std::string(((duckdb_libpgquery::PGAConst *)stmt->args->head->data.ptr_value)->val.val.str);
    if (value == "true") {
        return std::make_unique<SetStatement>(name, SetType::SET, true);
    } else if (value == "false") {
        return std::make_unique<SetStatement>(name, SetType::SET, false);
    } else {
        throw std::invalid_argument(fmt::format("The value set must be true or false."));
    }
}

auto Binder::BindDefault(duckdb_libpgquery::PGNode *node) -> std::unique_ptr<BoundExpression> {
    // TODO: 检查默认值表达式，支持 函数 & 常量 ，其他类型要支持吗?
    return node ? BindExpression(node) : nullptr;
}

auto Binder::BindComment(duckdb_libpgquery::PGNode *node) -> std::string {
    if (node) {
        auto expr = BindExpression(node);
        if (expr && expr->Type() == ExpressionType::LITERAL) {
            return expr->ToString();
        }
    }
    return "";
}

auto Binder::BindDrop(duckdb_libpgquery::PGDropStmt *stmt) -> std::unique_ptr<DropStatement> {
    if (stmt->objects->length != 1) {
        throw std::invalid_argument(fmt::format("Can only drop one object at a time."));
    }
    ObjectType type;
    switch (stmt->removeType) {
        case duckdb_libpgquery::PG_OBJECT_TABLE:
            type = ObjectType::TABLE_ENTRY;
            break;
        case duckdb_libpgquery::PG_OBJECT_INDEX:
            type = ObjectType::INDEX_ENTRY;
            break;
        case duckdb_libpgquery::PG_OBJECT_SEQUENCE:
            type = ObjectType::SEQUENCE_ENTRY;
            break;
        case duckdb_libpgquery::PG_OBJECT_VIEW:
            type = ObjectType::VIEW_ENTRY;
            break;
        default:
            throw std::invalid_argument(fmt::format("Cannot drop this type yet."));
    }

    auto view_list = (duckdb_libpgquery::PGList *)stmt->objects->head->data.ptr_value;
    std::string name;
    if (view_list->length == 1) {
        // TODO: 将所有名称相关都强制为小写之后，才能修改这里，否则会drop不掉
        name = ((duckdb_libpgquery::PGValue *)view_list->head->data.ptr_value)->val.str;
    } else {
        throw std::invalid_argument(fmt::format("This format is not currently supported."));
    }

    if (type == ObjectType::TABLE_ENTRY) {
        auto table = BindBaseTableRef(name, std::nullopt, true);
        if (table) {
            auto metaInfo = table->GetTableInfo().GetTableMetaInfo();
            if (metaInfo.space_id != SQL_SPACE_TYPE_USERS) {
                throw std::runtime_error("Cannot drop system table : " + std::string(metaInfo.name));
            }
        }
    }

    return std::make_unique<DropStatement>(name, stmt->missing_ok, type);
}

auto Binder::BindSequence(duckdb_libpgquery::PGCreateSeqStmt *stmt) -> std::unique_ptr<CreateSequenceStatement> {
    auto result = std::make_unique<CreateSequenceStatement>();
    result->ignore_conflict = stmt->onconflict == duckdb_libpgquery::PG_IGNORE_ON_CONFLICT;
    if (stmt->sequence->relname) {
        result->name = stmt->sequence->relname;
    } else {
        throw intarkdb::Exception(ExceptionType::SYNTAX, "Missing Sequence name.");
    }

    if (stmt->sequence->relpersistence == duckdb_libpgquery::PG_RELPERSISTENCE_TEMP) {
        throw intarkdb::Exception(ExceptionType::SYNTAX, "Temporary Sequence is not supported.");
    }

    if (stmt->options) {
        std::unordered_set<SequenceInfo> used;
        duckdb_libpgquery::PGListCell *cell = nullptr;
        for_each_cell(cell, stmt->options->head) {
            auto *def_elem = reinterpret_cast<duckdb_libpgquery::PGDefElem *>(cell->data.ptr_value);
            std::string opt_name = std::string(def_elem->defname);
            auto val = (duckdb_libpgquery::PGValue *)def_elem->arg;
            bool nodef = def_elem->defaction == duckdb_libpgquery::PG_DEFELEM_UNSPEC && !val;  // e.g. NO MINVALUE
            int64_t opt_value = 0;

            if (val) {
                if (val->type == duckdb_libpgquery::T_PGInteger) {
                    opt_value = val->val.ival;
                } else if (val->type == duckdb_libpgquery::T_PGFloat) {
                    if (!TryCast::Operation<std::string, int64_t>(std::string(val->val.str), opt_value)) {
                        throw std::invalid_argument(
                            fmt::format("Expected an integer argument for option {}", opt_name));
                    }
                } else {
                    throw std::invalid_argument(fmt::format("Expected an integer argument for option {}", opt_name));
                }
            }
            if (opt_name == "increment") {
                if (used.find(SequenceInfo::SEQ_INC) != used.end()) {
                    throw std::invalid_argument(fmt::format("Increment value should be passed as most once"));
                }
                used.insert(SequenceInfo::SEQ_INC);
                if (nodef) {
                    continue;
                }

                result->increment = opt_value;
                if (result->increment == 0) {
                    throw std::invalid_argument(fmt::format("Increment must not be zero"));
                }
                if (result->increment < 0) {
                    result->start_value = result->max_value = -1;
                    result->min_value = NumericLimits<int64_t>::Minimum();
                } else {
                    result->start_value = result->min_value = 1;
                    result->max_value = NumericLimits<int64_t>::Maximum();
                }
            } else if (opt_name == "minvalue") {
                if (used.find(SequenceInfo::SEQ_MIN) != used.end()) {
                    throw std::invalid_argument(fmt::format("Minvalue should be passed as most once"));
                }
                used.insert(SequenceInfo::SEQ_MIN);
                if (nodef) {
                    continue;
                }

                result->min_value = opt_value;
                if (result->increment > 0) {
                    result->start_value = result->min_value;
                }
            } else if (opt_name == "maxvalue") {
                if (used.find(SequenceInfo::SEQ_MAX) != used.end()) {
                    throw std::invalid_argument(fmt::format("Maxvalue should be passed as most once"));
                }
                used.insert(SequenceInfo::SEQ_MAX);
                if (nodef) {
                    continue;
                }

                result->max_value = opt_value;
                if (result->increment < 0) {
                    result->start_value = result->max_value;
                }
            } else if (opt_name == "start") {
                if (used.find(SequenceInfo::SEQ_START) != used.end()) {
                    throw std::invalid_argument(fmt::format("Start value should be passed as most once"));
                }
                used.insert(SequenceInfo::SEQ_START);
                if (nodef) {
                    continue;
                }

                result->start_value = opt_value;
            } else if (opt_name == "cycle") {
                if (used.find(SequenceInfo::SEQ_CYCLE) != used.end()) {
                    throw std::invalid_argument(fmt::format("Cycle value should be passed as most once"));
                }
                used.insert(SequenceInfo::SEQ_CYCLE);
                if (nodef) {
                    continue;
                }

                result->cycle = opt_value > 0;
            } else {
                throw std::invalid_argument(fmt::format("Unrecognized option {} for CREATE SEQUENCE.", opt_name));
            }
        }
    }

    if (result->max_value <= result->min_value) {
        throw std::invalid_argument(
            fmt::format("MINVALUE {} must be less than MAXVALUE {}.", result->min_value, result->max_value));
    }
    if (result->start_value < result->min_value) {
        throw std::invalid_argument(
            fmt::format("START value {} cannot be less than MINVALUE {}.", result->start_value, result->min_value));
    }
    if (result->start_value > result->max_value) {
        throw std::invalid_argument(
            fmt::format("START value {} cannot be greater than MAXVALUE {}.", result->start_value, result->max_value));
    }
    return result;
}

auto Binder::BindCheckPoint(duckdb_libpgquery::PGCheckPointStmt *stmt) -> std::unique_ptr<CheckpointStatement> {
    auto result = std::make_unique<CheckpointStatement>();
    return result;
}

auto Binder::BindExplain(duckdb_libpgquery::PGExplainStmt *stmt) -> std::unique_ptr<ExplainStatement> {
    auto result = std::make_unique<ExplainStatement>();
    auto explain_type = ExplainType::EXPLAIN_STANDARD;

    if (stmt->options) {
        for (auto n = stmt->options->head; n; n = n->next) {
            auto def_elem = reinterpret_cast<duckdb_libpgquery::PGDefElem *>(n->data.ptr_value)->defname;
            std::string elem(def_elem);
            if (elem == "analyze") {
                explain_type = ExplainType::EXPLAIN_ANALYZE;
            } else {
                throw std::invalid_argument("unspported explain statement defname:" + elem);
            }
        }
    }
    result->explain_type = explain_type;
    result->stmt = BindStatement(stmt->query);
    return result;
}

// COMMENT ON COLUMN <object> IS <text>
// COMMENT ON TABLE <object> IS <text>
auto Binder::BindCommentOn(duckdb_libpgquery::PGCommentStmt *stmt) -> std::unique_ptr<CommentStatement> {
    auto result = std::make_unique<CommentStatement>();

    std::vector<std::string> obj_decode_list;
    if (stmt->object->type == duckdb_libpgquery::T_PGList) {
        auto obj_list = reinterpret_cast<duckdb_libpgquery::PGList *>(stmt->object);
        for (auto c = obj_list->head; c != nullptr; c = lnext(c)) {
            auto target = reinterpret_cast<duckdb_libpgquery::PGResTarget *>(c->data.ptr_value);
            obj_decode_list.push_back(std::string(target->name));
        }
    }
    result->comment = stmt->comment;

    if (stmt->objtype == duckdb_libpgquery::PG_OBJECT_COLUMN) {
        result->object_type_ = PG_OBJECT_COLUMN;
        // support <object> format : [user_name.]table_name.column_name
        if (obj_decode_list.size() == OBJ_LIST_2ARG) {
            result->table_name = obj_decode_list[0];
            result->column_name = obj_decode_list[1];

        } else if (obj_decode_list.size() == OBJ_LIST_3ARG) {
            result->user_name = obj_decode_list[0];
            result->table_name = obj_decode_list[1];
            result->column_name = obj_decode_list[2];
        } else {
            throw std::invalid_argument("Bad format of object input!!, must be : [user_name.]table_name.column_name");
        }
    } else if (stmt->objtype == duckdb_libpgquery::PG_OBJECT_TABLE) {
        result->object_type_ = PG_OBJECT_TABLE;
        // support <object> format : [user_name.]table_name
        if (obj_decode_list.size() == OBJ_LIST_1ARG) {
            result->table_name = obj_decode_list[0];

        } else if (obj_decode_list.size() == OBJ_LIST_2ARG) {
            result->user_name = obj_decode_list[0];
            result->table_name = obj_decode_list[1];
        } else {
            throw std::invalid_argument("Bad format of object input!!, must be : [user_name.]table_name");
        }
    } else {
        throw std::invalid_argument("unspported comment statement objtype:" + std::to_string(stmt->objtype));
    }
    return result;
}
