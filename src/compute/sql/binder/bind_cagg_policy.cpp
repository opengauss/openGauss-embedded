/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * bind_cagg_policy.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/binder/bind_cagg_policy.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "binder/binder.h"

auto Binder::BindCaggPolicy(duckdb_libpgquery::PGCaggPolicyStmt *pg_stmt) -> std::unique_ptr<CaggPolicyStatement> {
    auto result = std::make_unique<CaggPolicyStatement>();

    result->policy_name = pg_stmt->policyname;
    throw std::invalid_argument("CaggPolicyStatement not support!");

    return result;
}
