/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * record_batch.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/common/record_batch.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "common/record_batch.h"

#include <fmt/color.h>
#include <fmt/core.h>
#include <fmt/format.h>

#include <stdexcept>
#include <unordered_map>
#include <vector>

#include "binder/statement_type.h"
#include "catalog/schema.h"
#include "common/string_util.h"
#include "common/util.h"
#include "type/value.h"

Record::Record(std::unordered_map<uint16_t, Value>&& v) {
    values.resize(v.size());
    for (auto& [slot, value] : v) {
        values[slot] = value;
    }
}

Record::Record(const std::vector<Value>& v) : values(v) {
}

Record::Record(std::vector<Value>&& v) : values(std::move(v)) {
}

uint32_t Record::ColumnCount() const { return values.size(); }

// slot 为下标
Value Record::Field(uint16_t slot) const { return FieldRef(slot); }

const Value& Record::FieldRef(uint16_t slot) const {
    // 此处slot是record中的顺序,应该是连续的，和列定义中的slot不是同样的东西
    // 列定义中的slot，因为存在删除列的情况，可能是不连续的
    if (slot < values.size()) {
        return values[slot];
    }
    throw std::runtime_error(fmt::format("unfound slot={} values size={}", slot, values.size()));
}

Value&& Record::FieldMove(uint16_t slot) {
    if (slot < values.size()) {
        return std::move(values[slot]);
    }
    throw std::runtime_error(fmt::format("unfound slot={} values size={}", slot, values.size()));
}

void Record::SetField(uint16_t slot, const Value& v) {
    if (slot < values.size()) {
        values[slot] = v;
    } else {
        throw std::runtime_error(fmt::format("unfound slot={} values size={}", slot, values.size()));
    }
}

Value& Record::FieldMutable(uint16_t slot) {
    if (slot < values.size()) {
        return values[slot];
    }
    throw std::runtime_error(fmt::format("unfound slot={} values size={}", slot, values.size()));
}

Record Record::Concat(const Record& other) {
    std::vector<Value> new_values;
    new_values.reserve(values.size() + other.values.size());
    for (const auto& v : values) {
        new_values.push_back(v);
    }
    for (const auto& v : other.values) {
        new_values.push_back(v);
    }
    return Record(std::move(new_values));
}

auto RecordToVector(const Record& record) -> std::vector<Value> {
    std::vector<Value> values;
    size_t col_num = record.ColumnCount();
    values.reserve(col_num);
    for (size_t i = 0; i < col_num; ++i) {
        values.push_back(record.FieldRef(i));
    }
    return values;
}

std::vector<std::vector<std::string>> RecordBatch::GetRecords(std::string null_str) const {
    std::vector<std::vector<std::string>> result;
    std::vector<std::string> headers_row;
    const auto& headers = schema_.GetColumnInfos();
    for (const auto& header : headers) {
        headers_row.emplace_back(header.GetColNameWithoutTableName());
    }
    result.push_back(headers_row);
    for (const auto& rec : records) {
        std::vector<std::string> row;
        for (size_t i = 0; i < headers.size(); ++i) {
            const auto& v = rec.FieldRef(i);
            std::string col_value(v.IsNull() ? null_str : v.ToString());
            row.push_back(col_value);
        }
        result.push_back(row);
    }
    return result;
}

void RecordBatch::Print() {
    for (auto& ss : GetRecords()) {
        for (auto& s : ss) {
            std::cout << s << "|        |";
        }
        std::cout << std::endl;
    }
}
