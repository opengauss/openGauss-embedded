/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * connection.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/main/connection.cpp
 *
 * -------------------------------------------------------------------------
 */

#include "main/connection.h"

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <iostream>
#include <random>
#include <set>

#include "binder/binder.h"
#include "binder/expressions/bound_func_call.h"
#include "binder/expressions/bound_seq_func.h"
#include "binder/statement_type.h"
#include "catalog/default_view.h"
#include "common/csv_util.h"
#include "common/default_value.h"
#include "common/gstor_exception.h"
#include "common/record_batch.h"
#include "common/string_util.h"
#include "function/pragma/pragma_queries.h"
#include "planner/optimizer/optimizer.h"
#include "planner/planner.h"
#include "storage/gstor/zekernel/common/cm_log.h"
#include "storage/storage.h"
#include "type/type_str.h"
#include "catalog/default_table.h"
#include "stream_agg/ts_stream_agg_job_manage.h"
#include "binder/expressions/bound_alias.h"

Connection::Connection(std::shared_ptr<IntarkDB> instance) : instance_(instance) {}

Connection::~Connection() {
    if (handle_) {
        auto instance_ptr = instance_.lock();
        if (instance_ptr != nullptr) {
            // 回收handle
            instance_ptr->DestoryHandle(handle_);
        }
        handle_ = nullptr;
    }
}

void Connection::Init() {
    if (instance_.lock() == nullptr || !instance_.lock()->HasInit()) {
        throw std::runtime_error("Connection init error, The database instance must be initialized first!");
    }

    // get the database operate handle
    auto instance_ptr = instance_.lock();
    if (instance_ptr == nullptr) {
        throw std::runtime_error("db_handle_alloc error, the database has closed!");
    }

    auto ret = instance_ptr->AllocHandle(&handle_);
    if (ret != GS_SUCCESS) {
        if (ret == GS_FULL_CONN) {
            throw std::runtime_error("The number of connections has reached the upper limit!");
        }
        throw std::runtime_error("get database handle fail");
    }
    // init catalog
    catalog_ = std::make_unique<Catalog>(handle_);

    // create default views
    DefaultViewGenerator::CreateDefaultEntry(this);

    // create default tables
    DefaultTableGenerator::CreateDefaultEntry(this);
}

std::unique_ptr<PreparedStatement> Connection::Prepare(const std::string& query) {
    GS_LOG_RUN_INF("[DB:%s][Prepare SQL]:%s", instance_.lock()->GetDbPath().c_str(), query.c_str());
    try {
        auto statements = ParseStatementsInternal(query);
        if (statements.empty()) {
            throw std::runtime_error("No statement to prepare!");
        }
        if (statements.size() > 1) {
            throw std::runtime_error("Cannot prepare multiple statements at once!");
        }
        return CreatePreparedStatement(std::move(statements[0]));
    } catch (std::exception& ex) {
        return std::make_unique<PreparedStatement>(true, std::string(ex.what()));
    }
}

std::vector<std::unique_ptr<BoundStatement>> Connection::ParseStatementsInternal(const std::string& query) {
    std::vector<std::unique_ptr<BoundStatement>> statements;
    Binder binder = CreateBinder();
    binder.ParseAndSave(query);

    const auto& stmts = binder.GetStatementNodes();
    for (auto stmt : stmts) {
        auto statement = binder.BindStatement(stmt);
        statement->n_param = binder.ParamCount();
        statement->query = query;
        statements.push_back(std::move(statement));
    }
    return statements;
}

std::unique_ptr<PreparedStatement> Connection::CreatePreparedStatement(std::unique_ptr<BoundStatement> stmt) {
    Planner planner = CreatePlanner();
    intarkdb::Optimizer optimizer;
    PhysicalPlanPtr physical_plan;
    auto& statement = *stmt;
    switch (statement.Type()) {
        case StatementType::SELECT_STATEMENT: {
            auto& select_stmt = dynamic_cast<SelectStatement&>(statement);
            auto logical_plan = planner.PlanSelect(select_stmt);
            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);
            physical_plan = planner.CreatePhysicalPlan(logical_plan);
            break;
        }
        case StatementType::INSERT_STATEMENT: {
            auto& insert_stmt = dynamic_cast<InsertStatement&>(statement);
            auto logical_plan = planner.PlanInsert(insert_stmt);
            physical_plan = planner.CreatePhysicalPlan(logical_plan);
            break;
        }
        case StatementType::DELETE_STATEMENT: {
            auto& delete_stmt = dynamic_cast<DeleteStatement&>(statement);
            if (TruncateTable(statement.query, delete_stmt, true)) {
                return nullptr;
            }
            auto logical_plan = planner.PlanDelete(delete_stmt);
            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);
            physical_plan = planner.CreatePhysicalPlan(logical_plan);
            break;
        }
        case StatementType::UPDATE_STATEMENT: {
            auto& update_stmt = dynamic_cast<UpdateStatement&>(statement);
            auto logical_plan = planner.PlanUpdate(update_stmt);
            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);
            physical_plan = planner.CreatePhysicalPlan(logical_plan);
            break;
        }
        case StatementType::CREATE_STATEMENT:
        case StatementType::INDEX_STATEMENT:
        case StatementType::SEQUENCE_STATEMENT:
        case StatementType::TRANSACTION_STATEMENT:
        case StatementType::SET_STATEMENT:
        case StatementType::ALTER_STATEMENT:
        case StatementType::SHOW_STATEMENT:
        case StatementType::DROP_STATEMENT:
        case StatementType::CTAS_STATEMENT:
        case StatementType::CREATE_VIEW_STATEMENT:
        case StatementType::COPY_STATEMENT:
        case StatementType::CHECKPOINT_STATEMENT:
        case StatementType::EXPLAIN_STATEMENT:
        case StatementType::COMMENT_STATEMENT:
            break;
        default:
            std::invalid_argument("unspported statement type");
    }

    auto r = std::make_unique<PreparedStatement>(statement.query, statement.n_param, this, std::move(stmt),
                                                 std::move(physical_plan));
    r->params = planner.GetPrepareParams();
    r->SetStatementType(statement.Type());
    return r;
}

std::unique_ptr<RecordBatch> Connection::Query(const char* query) {
    // for test
    std::string sql(query);
    GS_LOG_RUN_INF("[DB:%s][Query SQL]:%s", instance_.lock()->GetDbPath().c_str(), query);

    std::unique_ptr<RecordBatch> result = std::make_unique<RecordBatch>(Schema());

    try {
        auto statements = ParseStatementsInternal(query);
        if (statements.empty()) {
            throw std::runtime_error("No statement to execute!");
        }
        for (auto& statement : statements) {
            result = ExecuteStatement(query, std::move(statement));
        }
        parser_.Clear();
    } catch (intarkdb::Exception& instar_ex) {
        if (IsAutoCommit()) {
            gstor_rollback(((db_handle_t*)handle_)->handle);
        }
        result->SetRetCode(-1);
        result->SetRetMsg(instar_ex.what());
        GS_LOG_RUN_INF("%s", instar_ex.what());
    } catch (const std::exception& e) {
        if (IsAutoCommit()) {
            gstor_rollback(((db_handle_t*)handle_)->handle);
        }
        result->SetRetCode(-1);
        result->SetRetMsg(e.what());
        GS_LOG_RUN_INF("%s", e.what());
    } catch (...) {
        if (IsAutoCommit()) {
            gstor_rollback(((db_handle_t*)handle_)->handle);
        }
        result->SetRetCode(-1);
        result->SetRetMsg("unknown exception!");
    }
    return result;
}

std::unique_ptr<RecordIterator> Connection::QueryIterator(const char* query) {
    GS_LOG_RUN_INF("[DB:%s][Query SQL]:%s", instance_.lock()->GetDbPath().c_str(), query);
    std::unique_ptr<RecordIterator> result = nullptr;
    try {
        auto stmts = ParseStatementsInternal(query);
        if (stmts.empty()) {
            throw std::runtime_error("No statement to execute!");
        }
        for (auto& stmt : stmts) {
            if (stmt->Type() == StatementType::SELECT_STATEMENT) {
                result = std::make_unique<RecordIterator>(ExecuteStatementStreaming(std::move(stmt)));
            } else {
                result = std::make_unique<RecordIterator>(ExecuteStatement(query, std::move(stmt)));
            }
            break;  // not support multi statement
        }
        parser_.Clear();
    } catch (const std::exception& e) {
        if (IsAutoCommit()) {
            gstor_rollback(((db_handle_t*)handle_)->handle);
        }
        auto fail_record_batch = std::make_unique<RecordBatch>(Schema());
        fail_record_batch->SetRetCode(-1);
        fail_record_batch->SetRetMsg(e.what());
        result = std::make_unique<RecordIterator>(std::move(fail_record_batch));
    }
    return result;
}

std::unique_ptr<RecordStreaming> Connection::ExecuteStatementStreaming(std::unique_ptr<BoundStatement> statement) {
    if (statement && statement->Type() == StatementType::SELECT_STATEMENT) {
        try {
            auto& select_stmt = dynamic_cast<SelectStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanSelect(select_stmt);
            intarkdb::Optimizer optimizer;
            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            auto record_streaming = std::make_unique<RecordStreaming>(physical_plan->GetSchema(), physical_plan);
            record_streaming->SetStmtType(select_stmt.Type());
            return record_streaming;
        } catch (const std::exception& e) {
            auto result = std::make_unique<RecordStreaming>(Schema(), nullptr);
            result->SetRetCode(-1);
            result->SetRetMsg(e.what());
            return result;
        }
    }
    throw std::runtime_error("ExecuteStatementStreaming not support non select statement");
}

std::unique_ptr<RecordBatch> Connection::ExecuteStatement(const std::string& query,
                                                          std::unique_ptr<BoundStatement> statement) {
    std::unique_ptr<RecordBatch> result = std::make_unique<RecordBatch>(Schema());
    intarkdb::Optimizer optimizer;
    switch (statement->Type()) {
        case StatementType::CREATE_STATEMENT: {
            auto& create_stmt = dynamic_cast<CreateStatement&>(*statement);
            // handle create table statement
            auto ret = CreateTable(create_stmt);
            if (ret != GS_SUCCESS && ret != GS_IGNORE_OBJECT_EXISTS) {
                throw std::runtime_error("CreateTable err");
            }
            break;
        }
        case StatementType::INDEX_STATEMENT: {
            auto& index_stmt = dynamic_cast<CreateIndexStatement&>(*statement);
            // handle create index statement
            if (CreateIndex(index_stmt) != GS_SUCCESS) {
                throw std::runtime_error("CreateIndex err");
            }
            break;
        }
        case StatementType::SEQUENCE_STATEMENT: {
            auto& sequence_stmt = dynamic_cast<CreateSequenceStatement&>(*statement);
            // handle create index statement
            if (CreateSequence(sequence_stmt) != GS_SUCCESS) {
                throw std::runtime_error("CreateSequence err");
            }
            break;
        }
        case StatementType::SELECT_STATEMENT: {
            auto& select_stmt = dynamic_cast<SelectStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanSelect(select_stmt);

            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);

            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            result = std::make_unique<RecordBatch>(physical_plan->GetSchema());
            result->SetRecordBatchType(RecordBatchType::Select);
            uint64_t row_count = 0;
            while (true) {
                if (limit_rows_ex > 0 && row_count >= limit_rows_ex) {
                    break;
                }
                auto&& [r, _, eof] = physical_plan->Next();
                if (eof) {
                    break;
                }
                result->AddRecord(std::move(r));
                row_count++;
            }
            break;
        }
        case StatementType::INSERT_STATEMENT: {
            auto& insert_stmt = dynamic_cast<InsertStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanInsert(insert_stmt);
            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            {
                physical_plan->SetNeedResultSetEx(is_need_result_ex);
                physical_plan->Execute(*result);
            }
            if (IsAutoCommit()) {
                gstor_commit(((db_handle_t*)handle_)->handle);
            }
            break;
        }
        case StatementType::DELETE_STATEMENT: {
            auto& delete_stmt = dynamic_cast<DeleteStatement&>(*statement);
            // check if truncate statement
            if (TruncateTable(query, delete_stmt, false)) {
                break;
            }

            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanDelete(delete_stmt);

            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);

            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            result = std::make_unique<RecordBatch>(physical_plan->GetSchema());
            result->SetRecordBatchType(RecordBatchType::Delete);
            {
                auto r = physical_plan->Execute();
                result->effect_row = r.effect_row;
            }
            if (IsAutoCommit()) {
                gstor_commit(((db_handle_t*)handle_)->handle);
            }
            break;
        }
        case StatementType::TRANSACTION_STATEMENT: {
            auto& transaction_stmt = dynamic_cast<TransactionStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanTransaction(handle_, transaction_stmt);
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            auto r = physical_plan->Execute();
            if (r.status != GS_SUCCESS) {
                throw std::runtime_error("transaction err");
            }
            SetBeginTransaction(transaction_stmt.type);
            break;
        }
        case StatementType::SET_STATEMENT: {
            auto& set_stmt = dynamic_cast<SetStatement&>(*statement);
            if (set_stmt.name == "auto_commit") {
                is_autocommit_param = set_stmt.value;
            }
            break;
        }
        case StatementType::UPDATE_STATEMENT: {
            auto& update_stmt = dynamic_cast<UpdateStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanUpdate(update_stmt);

            logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);

            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            *result = RecordBatch(physical_plan->GetSchema());
            {
                auto r = physical_plan->Execute();
                result->effect_row = r.effect_row;
            }
            if (IsAutoCommit()) {
                gstor_commit(((db_handle_t*)handle_)->handle);
            }
            break;
        }
        case StatementType::ALTER_STATEMENT: {
            auto& alter_stmt = dynamic_cast<AlterStatement&>(*statement);
            // handle alter table statement
            if (AlterTable(alter_stmt) != GS_SUCCESS) {
                throw std::runtime_error("AlterTable err");
            }
            break;
        }
        case StatementType::SHOW_STATEMENT: {
            auto& show_stmt = dynamic_cast<ShowStatement&>(*statement);
            show_stmt.db_path = instance_.lock()->GetDbPath();
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanShow(show_stmt);
            if (show_stmt.show_type != ShowType::SHOW_TYPE_ALL) {
                logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);
            }
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            physical_plan->Execute(*result);
            result->SetRecordBatchType(RecordBatchType::Select);
            break;
        }
        case StatementType::DROP_STATEMENT: {
            auto& drop_stmt = dynamic_cast<DropStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanDrop(handle_, drop_stmt);
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            auto r = physical_plan->Execute();
            if (r.status != GS_SUCCESS) {
                throw std::runtime_error("drop err");
            }
            break;
        }
        case StatementType::CTAS_STATEMENT: {
            auto& ctas_stmt = dynamic_cast<CtasStatement&>(*statement);
            // handle create table as select statement
            if (CtasTable(ctas_stmt, *result) != GS_SUCCESS) {
                throw std::runtime_error("CtasTable err");
            }
            break;
        }
        case StatementType::CREATE_VIEW_STATEMENT: {
            auto& view_stmt = dynamic_cast<CreateViewStatement&>(*statement);
            auto queryStmt = view_stmt.getBoundSTMT();
            auto& select_stmt = dynamic_cast<SelectStatement&>(*queryStmt);

            // TODO: 有必要Planner吗?
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanSelect(select_stmt);
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            // FIXME: 这个columns有什么必要？
            const auto& schema = physical_plan->GetSchema();
            auto columns = schema.GetColumns();
            if (CreateView(view_stmt.getViewName(), columns, query, view_stmt.ignore_conflict) != GS_SUCCESS) {
                throw std::runtime_error("CreateView err");
            }
            break;
        }
        case StatementType::COPY_STATEMENT: {
            auto& copy_stmt = static_cast<CopyStatement&>(*statement);
            if (copy_stmt.info->is_from) {
                CopyFrom(copy_stmt);
            } else {
                CopyTo(copy_stmt, result->effect_row);
            }
            break;
        }
        case StatementType::CHECKPOINT_STATEMENT: {
            auto result = catalog_->forceCheckpoint();
            if (result != 0) {
                printf("force checkpoint return error %d !\n", result);
            } else {
                printf("force checkpoint success \n");
            }
            break;
        }
        case StatementType::EXPLAIN_STATEMENT: {
            break;
        }
        case StatementType::COMMENT_STATEMENT: {
            auto& comment_stmt = dynamic_cast<CommentStatement&>(*statement);
            Planner planner = CreatePlanner();
            auto logical_plan = planner.PlanCommentOn(catalog_.get(), comment_stmt);
            auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
            auto r = physical_plan->Execute();
            break;
        }
        case StatementType::CALL_STATEMENT: {
            auto& call_stmt = dynamic_cast<CallStatement&>(*statement);
            if (CallTsFunc(call_stmt, *result) != GS_SUCCESS) {
                throw std::runtime_error("Call ts function err");
            }
            break;
        }
        default:
            throw intarkdb::Exception(ExceptionType::EXECUTOR, "unspported statement type");
    }
    result->stmt_type = statement->Type();
    return result;
}

auto Connection::CreateBinder() -> Binder { return Binder(*catalog_, parser_); }

Planner Connection::CreatePlanner() { return Planner(*catalog_); }

int Connection::CreateTable(const CreateStatement& stmt) {
    auto table_info = catalog_->GetTable(stmt.GetTableName());
    if (table_info != nullptr &&
        table_info->GetObjectType() != DIC_TYPE_TABLE) {  // 此处只处理非表名重复的情况，表名重复的情况在catalog中处理
        throw intarkdb::Exception(ExceptionType::CATALOG,
                                  fmt::format("object {} is not a table ", stmt.GetTableName()));
    }
    return catalog_->CreateTable(stmt.GetTableName(), stmt.GetColumns(), stmt);
}

int Connection::CreateIndex(const CreateIndexStatement& stmt) {
    auto ret = catalog_->CreateIndex(stmt.GetTableName(), stmt);
    return ret;
}

int Connection::CreateSequence(const CreateSequenceStatement& stmt) {
    auto ret = catalog_->CreateSequence(stmt);
    return ret;
}

bool Connection::TruncateTable(const std::string& query, const DeleteStatement& stmt, bool is_prepare) {
    std::string sql = query;
    for (size_t i = 0; i < sql.size(); i++) {
        if (sql[i] >= 'a' && sql[i] <= 'z') {
            sql[i] -= 32;
        }
    }

    if (sql.find("TRUNCATE") != std::string::npos) {
        if (is_prepare) {
            return true;
        }
        auto table_name = stmt.target_table->GetBoundTableName();

        // TRUNCATE
        int32 ret = gstor_truncate_table(((db_handle_t*)handle_)->handle, nullptr, (char*)table_name.c_str());
        if (ret != GS_SUCCESS) {
            printf("truncate table %s error!!\n", table_name.c_str());
        } else {
            printf("truncate table %s success!\n", table_name.c_str());
        }
        return true;
    }
    return false;
}

bool Connection::IsAutoCommit() {
    if (is_begin_transaction) {
        return false;
    } else {
        return is_autocommit_param;
    }
    // 兜底自动提交
    return true;
}

void Connection::SetBeginTransaction(TransactionType type) {
    if (type == TransactionType::BEGIN_TRANSACTION) {
        is_begin_transaction = true;
        int32 ret = gstor_set_is_begin_transaction(((db_handle_t*)handle_)->handle, GS_TRUE);
        if (ret != GS_SUCCESS) {
            printf("set session is_begin_transaction error!\n");
        }
    } else if (type == TransactionType::COMMIT || type == TransactionType::ROLLBACK) {
        is_begin_transaction = false;
        int32 ret = gstor_set_is_begin_transaction(((db_handle_t*)handle_)->handle, GS_FALSE);
        if (ret != GS_SUCCESS) {
            printf("set session is_begin_transaction error!\n");
        }
    }
}

int Connection::AlterTable(const AlterStatement& stmt) {
    auto& stmt_ref = const_cast<AlterStatement&>(stmt);

    if (stmt_ref.AlterType() == GsAlterTableType::ALTABLE_ADD_PARTITION) {
        stmt_ref.GetAlterTableInfoDefMutable().part_opt.hiboundval.str =
            const_cast<char*>(stmt_ref.hpartbound_.c_str());
        stmt_ref.GetAlterTableInfoDefMutable().part_opt.hiboundval.len = stmt_ref.hpartbound_.length();
    }

    // TODO: 分析这里的作用
    if (stmt_ref.AlterType() == GsAlterTableType::ALTABLE_MODIFY_TABLE_COMMENT) {
        // just go to catalog_->AlterTable
    }

    return catalog_->AlterTable(stmt.GetTableName(), stmt);
}

// rename column name if has same name
static auto RenameSchema(const Schema& schema) -> Schema {
    intarkdb::CaseInsensitiveMap<int> column_names;
    std::vector<SchemaColumnInfo> column_defs;
    const auto& columns = schema.GetColumnInfos();
    size_t col_num = columns.size();
    column_defs.reserve(col_num);
    for (size_t i = 0; i < col_num; ++i) {
        const auto& col = columns[i];
        SchemaColumnInfo new_col = col;
        std::string name = "";
        if (!new_col.alias.empty()) {
            name = new_col.alias;
            new_col.alias.clear();
        } else {
            name = new_col.col_name.back();
        }
        auto iter = column_names.find(name);
        if (iter != column_names.end()) {
            name = fmt::format("{}_{}", name, iter->second);
            iter->second++;
        } else {
            column_names.insert({name, 1});
        }
        new_col.col_name = std::vector<std::string>{name};  // 移除旧表的前缀 or 重新设置列名
        new_col.slot = i;                                   // 重新设置 slot
        column_defs.emplace_back(std::move(new_col));
    }
    return Schema(std::move(column_defs));
}

int Connection::CtasTable(CtasStatement& stmt, RecordBatch& result) {
    Planner planner = CreatePlanner();

    auto select_plan = planner.PlanSelect(*stmt.as_select_);
    auto new_schema = RenameSchema(select_plan->GetSchema());  // 如果有重名的列，会重命名列名
    std::vector<Column> columns = new_schema.GetColumns();
    stmt.create_stmt_->SetColumns(std::move(columns));
    // create table
    auto ret = CreateTable(*stmt.create_stmt_);
    if (ret != GS_SUCCESS) {
        if (ret == GS_IGNORE_OBJECT_EXISTS) {
            return GS_SUCCESS;
        }
        throw std::runtime_error("Ctas-CreateTable err");
    }

    try {
        // insert data to new table : insert into select
        auto table_info = catalog_->GetTable(stmt.tablename_);
        if (!table_info) {
            throw std::invalid_argument(fmt::format("invalid table {}", stmt.tablename_));
        }
        auto table_ref = std::make_unique<BoundBaseTable>(stmt.tablename_, std::nullopt, std::move(table_info));

        auto logical_plan = planner.PlanInsert(std::move(table_ref), std::move(stmt.create_stmt_), select_plan);
        auto physical_plan = planner.CreatePhysicalPlan(logical_plan);
        physical_plan->Execute(result);
    } catch (const std::runtime_error& e) {
        drop_def_t drop_info = {0};
        drop_info.name = (char*)stmt.tablename_.c_str();
        drop_info.if_exists = (int)false;
        drop_info.type = DROP_TYPE_TABLE;
        if (gstor_drop(((db_handle_t*)handle_)->handle, nullptr, &drop_info) != GS_SUCCESS) {
            GS_LOG_RUN_ERR("CtasTable runtime_error, drop_table failed");
        }
        gstor_rollback(((db_handle_t*)handle_)->handle);
        throw std::runtime_error(e.what());
    } catch (...) {
        drop_def_t drop_info = {0};
        drop_info.name = (char*)stmt.tablename_.c_str();
        drop_info.if_exists = (int)false;
        drop_info.type = DROP_TYPE_TABLE;
        if (gstor_drop(((db_handle_t*)handle_)->handle, nullptr, &drop_info) != GS_SUCCESS) {
            GS_LOG_RUN_ERR("CtasTable exception, drop_table failed");
        }
        gstor_rollback(((db_handle_t*)handle_)->handle);
        throw std::runtime_error("unknown error.");
    }
    if (IsAutoCommit()) {
        gstor_commit(((db_handle_t*)handle_)->handle);
    }

    return GS_SUCCESS;
}

std::unique_ptr<TableInfo> Connection::GetTableInfo(const std::string& table_name) {
    return catalog_->GetTable(table_name);
}

int Connection::CreateView(const std::string& viewName, const std::vector<Column>& columns, const std::string& query,
                           bool ignoreConflict) {
    return catalog_->CreateView(viewName, columns, query, ignoreConflict);
}

std::string Connection::ShowCreateTable(const std::string& table_name, std::vector<std::string>& index_sqls) {
    auto table = GetTableInfo(table_name);
    if (table == nullptr) throw std::runtime_error("table:" + table_name + " not exist");
    auto table_meta = table->GetTableMetaInfo();
    std::stringstream sschema;
    sschema << "CREATE TABLE " << table_meta.name << "(\n";
    std::vector<uint16> primary_col_ids;
    for (size_t i = 0; i < table_meta.index_count; i++) {
        auto index = table_meta.indexes[i];
        bool bIndexSQL = true;

        if (index.is_primary) {
            for (size_t j = 0; j < index.col_count; j++) {
                primary_col_ids.push_back(index.col_ids[j]);
            }
            bIndexSQL = false;
        }

        if (bIndexSQL) {
            std::string index_sql = "CREATE ";
            if (index.is_unique) index_sql += "UNIQUE ";

            index_sql += "INDEX " + std::string(index.name.str) + " on " + std::string(table_meta.name) + "(";
            for (size_t j = 0; j < index.col_count; j++) {
                index_sql += table_meta.columns[index.col_ids[j]].name.str;
                if (j != index.col_count - 1) index_sql += ", ";
            }
            index_sql += ");";
            index_sqls.push_back(index_sql);
        }
    }

    // COMMENT
    std::string table_comment;
    bool32 is_table_comment = GS_FALSE;
    std::string table_id = std::to_string(table_meta.id);
    std::string sql = "select \"COLUMN#\",\"COMMENT#\" from \"SYS_COMMENT\" where \"TABLE#\" = " + table_id;
    auto rb = Query(sql.c_str());

    for (size_t i = 0; i < table_meta.column_count; i++) {
        auto column = table_meta.columns[i];
        sschema << column.name.str << " ";
        std::string col_type_name = fmt::format("{}", column.col_type);
        if (col_type_name == UNKNOWN_TYPE_NAME) {
            std::stringstream sErr;
            sErr << "not support " << column.name.str << " type:" << column.col_type << std::endl;
            return sErr.str();
        }
        sschema << col_type_name;
        if (column.precision > 0)
            sschema << "(" << column.precision << ", " << column.scale << ")";
        else if (column.col_type == gs_type_t::GS_TYPE_VARCHAR && column.size != COLUMN_VARCHAR_SIZE_DEFAULT)
            sschema << "(" << column.size << ")";

        if (!column.nullable) sschema << " NOT NULL";

        if (column.is_default) {
            column.crud_value = column.default_val;
            Value value(column);
            sschema << " DEFAULT " << value.ToSQLString();
        }

        if (primary_col_ids.size() == 1 && primary_col_ids[0] == i) sschema << " PRIMARY KEY";

        // COMMENT
        auto col_slot = std::to_string(column.col_slot);
        for (size_t i = 0; i < rb->RowCount(); ++i) {
            auto& row = rb->RowMutable(i);
            auto& val_col_slot = row.FieldMutable(0);
            // COLUMN COMMENT
            if (val_col_slot.ToString() == col_slot) {
                auto val_col_comment = row.FieldMutable(1).ToSQLString();
                sschema << " COMMENT " << val_col_comment;
                if (is_table_comment) break;
            }
            // TABLE COMMENT
            if (val_col_slot.IsNull()) {
                is_table_comment = GS_TRUE;
                table_comment = row.FieldMutable(1).ToSQLString();
            }
        }

        if (i != table_meta.column_count - 1) sschema << ",\n";
    }
    if (primary_col_ids.size() > 1) {
        sschema << ", PRIMARY KEY (";
        auto iter = primary_col_ids.begin();
        while (iter != primary_col_ids.end()) {
            sschema << std::string(table_meta.columns[*iter].name.str);
            if (++iter != primary_col_ids.end()) sschema << ",";
        }
        sschema << ")";
    }
    if (is_table_comment) {
        sschema << ") COMMENT " << table_comment;
        sschema << ";";
    } else {
        sschema << ");";
    }
    return sschema.str();
}

static auto GetDelimiter(const std::unordered_map<std::string, std::vector<Value>>& options) -> char {
    char delimiter = intarkdb::kDefaultCSVDelimiter;
    auto delimiter_it = options.find("delimiter");
    if (delimiter_it != options.end()) {
        if (delimiter_it->second.size() == 0) {
            throw intarkdb::Exception(ExceptionType::PARSER, "'delimiter' expects a single argument as a string value");
        }
        const auto& tmp_delimiter = delimiter_it->second[0].GetCastAs<std::string>();
        if (tmp_delimiter.size() > 1 || tmp_delimiter.size() == 0) {
            throw intarkdb::Exception(ExceptionType::PARSER,
                                      "'delimiter' expects a single argument as a string value with signle character");
        }
        delimiter = tmp_delimiter[0];
    }
    return delimiter;
}

// 构造 insert PreparedStatement for copy from
auto Connection::CopyFromInsertStatement(CopyStatement& stmt) -> std::unique_ptr<PreparedStatement> {
    if (!stmt.info->table_ref) {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "insert table is null");
    }
    if (stmt.info->select_list.size() <= 0) {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "insert table field is null");
    }
    std::string sql = "insert into " + stmt.info->table_ref->GetBoundTableName() + "(";
    std::string values = "(";
    auto item_num = stmt.info->select_list.size();
    for (size_t i = 0; i < item_num; ++i) {
        const auto& item = stmt.info->select_list[i];
        if (item->Type() != ExpressionType::COLUMN_REF) {
            throw intarkdb::Exception(ExceptionType::BINDER, "not support non column name in insert values list");
        }
        BoundColumnRef& column_ref = static_cast<BoundColumnRef&>(*item);
        const auto& colname = column_ref.GetName().back();
        if (i + 1 < item_num) {
            sql += fmt::format("{}", colname) + ",";
            values += "?,";
        } else {
            sql += fmt::format("{}", colname) + ")";
            values += "?)";
        }
    }
    sql += "values " + values;
    return Prepare(sql);
}

auto Connection::CopyTo(CopyStatement& stmt, int64_t& effect_row) -> int {
    auto planner = CreatePlanner();
    auto logical_plan = planner.PlanSelect(*stmt.select_statement);

    intarkdb::Optimizer optimizer;

    logical_plan = optimizer.OptimizeLogicalPlan(logical_plan);

    auto physical_plan = planner.CreatePhysicalPlan(logical_plan);

    auto delimiter = GetDelimiter(stmt.info->options);
    intarkdb::CSVWriter writer(stmt.info->file_path, delimiter);
    if (!writer.Open()) {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "open file failed");
    }
    effect_row = 0;
    while (true) {
        const auto& [r, _, eof] = physical_plan->Next();
        if (eof) {
            break;
        }
        writer.WriteRecord(r);
        effect_row++;
    }
    if (IsAutoCommit()) {
        gstor_commit(((db_handle_t*)handle_)->handle);
    }
    return GS_SUCCESS;
};

auto Connection::CopyFrom(CopyStatement& stmt) -> int {
    char delimiter = GetDelimiter(stmt.info->options);
    intarkdb::CSVReader reader(
        stmt.info->file_path,
        Schema(stmt.info->table_ref->GetTableNameOrAlias(), stmt.info->table_ref->GetTableInfo().columns), delimiter);
    if (!reader.Open()) {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "open file failed");
    }
    auto prepare_insert_stmt = CopyFromInsertStatement(stmt);
    while (true) {
        const auto& [r, eof] = reader.ReadRecord();
        if (eof) {
            break;
        }
        prepare_insert_stmt->Execute(RecordToVector(r));
    }
    if (IsAutoCommit()) {
        gstor_commit(((db_handle_t*)handle_)->handle);
    }
    return GS_SUCCESS;
}

std::string GetColString(std::vector<std::string> col_names) {
    std::string result = " ";
    for (size_t i = 0; i < col_names.size(); i++) {
        result += col_names[i];
        result += " real, "; 
    }

    return result;
}

static void ReplaceSubstring(std::string& str, const std::string& oldSubstr, const std::string& newSubstr) {
    size_t pos = 0;
    bool replaced = false;
    while ((pos = str.find(oldSubstr, pos)) != std::string::npos) {
        str.replace(pos, oldSubstr.length(), newSubstr);
        pos += newSubstr.length();
        replaced = true;
    }
    if (!replaced)
        throw std::runtime_error("agg sql replace " + oldSubstr + " falied!");
}

std::string GetSubstring(std::string str) {
    std::string result;
    size_t pos = str.find("where") != std::string::npos ? str.find("where") : str.find("WHERE");
    result = str.substr(0, pos);
    return result;
}

std::string GetAggSql(std::string sql, std::string bucket_field) {
    std::string result;
    size_t pos = sql.find("from") != std::string::npos ? sql.find("from") : sql.find("FROM");
    size_t pos_start = sql.find("where") != std::string::npos ? sql.find("where") : sql.find("WHERE");
    size_t pos_end = sql.find(">");
    if ((pos == std::string::npos) || (pos_start == std::string::npos) || (pos_end == std::string::npos)) {
        throw std::runtime_error("illegal aggregation sql!");
    }

    std::regex pattern(R"(^\s+|\s+$)");
    result = sql.substr(0, pos - 1) + ", max(" + std::regex_replace(sql.substr(pos_start + 5, pos_end-pos_start - 6), pattern, "") + ") as " + bucket_field + " " + sql.substr(pos);
    return result;
}

std::string GetTableName(std::string sql) {
    std::string table_name;
    size_t pos_start = sql.find("from") != std::string::npos ? sql.find("from") + 4 : sql.find("FROM") + 4;
    size_t pos_end = sql.find("where") != std::string::npos ? sql.find("where") -1 : sql.find("WHERE") -1;
    std::regex pattern(R"(^\s+|\s+$)");
    table_name = std::regex_replace(sql.substr(pos_start, pos_end - pos_start), pattern, "");
    return table_name;
}

int Connection::CallTsFunc(CallStatement& stmt, RecordBatch& result) {
    TsStreamAggJobManage *job_manager;
    TsJobInfo jobinfo;
    int32_t hours_per_day = 24;

    if (stmt.type == TsType::TS_ADD_CAGG_POLICY) {
        timestamp_stor_t now_time = TimestampNow();
        timestamp_stor_t start_time;
        timestamp_stor_t end_time = now_time - stmt.end_offset * MICROSECS_PER_SECOND;
        if (stmt.start_offset == 0) {
            start_time.ts = 0;
        }
        else {
            start_time = now_time - stmt.start_offset * MICROSECS_PER_SECOND;
        }

        if (start_time >= end_time) {
            throw std::runtime_error("start time is greater than or equal end time!");
        }

        if (job_manager->CountTsJob(this) >= MAX_TS_JOB_NUMS) {
            GS_LOG_RUN_ERR("[StreamAgg] just support no more than 60 ts job!");
            throw std::runtime_error("just support no more than 60 ts job!");
        }

        jobinfo.ts_job.name = stmt.policy_name;
        jobinfo.ts_job.schedule_time = stmt.schedule_time;
        jobinfo.ts_job.schedule_interval = stmt.refresh_interval;
        jobinfo.ts_job.offset_start = stmt.start_offset;
        jobinfo.ts_job.offset_end = stmt.end_offset;

        jobinfo.ts_cagg_policy.table_name = stmt.des_table_name;
        jobinfo.ts_cagg_policy.exec_sql = GetAggSql(stmt.agg_sql, stmt.bucket_field);
        jobinfo.ts_cagg_policy.bucket_field_name = stmt.bucket_field;
        jobinfo.ts_cagg_policy.bucket_width = 0;

        std::string table_name = GetTableName(stmt.agg_sql);
        auto table_info = catalog_->GetTable(table_name);
        if (!table_info) {
            throw std::runtime_error(fmt::format("source data table {} does not exist!", table_name));
        }
        auto meta_info = table_info->GetTableMetaInfo();
        if (!meta_info.is_timescale){
            throw std::runtime_error("source data table must be a timescale table!");
        }
        auto partiton_type = meta_info.part_table.keycols->datatype;
        jobinfo.ts_job.partiton_type = partiton_type;

        // select lists
        std::string agg_sql = GetSubstring(stmt.agg_sql);
        auto stmts = ParseStatementsInternal(agg_sql);
        std::vector<std::string> col_names;
        for (auto& statement : stmts) {
            auto& select_stmt = dynamic_cast<SelectStatement&>(*std::move(statement));
            for (auto& sel : select_stmt.return_list) {
                col_names.push_back(sel.col_name.back());
            }
        }

        // destination table
        std::string bucketwidth = stmt.bucket_width;
        std::string des_sql;
        if (bucketwidth == "") {
            des_sql = " datetime);";
        }
        else {
            jobinfo.ts_cagg_policy.bucket_type = TIME_BUCKET;

            std::regex pattern("[1-9][0-9]*[hdHD]");
            if (!std::regex_match(bucketwidth, pattern)) {
                throw std::runtime_error("the number prefix of bucket_width must be a positive integer, and suffix must be h/d!");
            }

            auto prefix = std::stoi(bucketwidth.substr(0, bucketwidth.size()-1));
            auto suffix = bucketwidth.substr(bucketwidth.size()-1);
            if (suffix == "h" || suffix == "H") {
                if (hours_per_day % prefix != 0) {
                    throw std::runtime_error("24/bucket_width must be an integer when bucket_width is hour!");
                }
                jobinfo.ts_cagg_policy.bucket_width = prefix * SECONDS_PER_HOUR;
            }
            else if (suffix == "d" || suffix == "D") {
                jobinfo.ts_cagg_policy.bucket_width = prefix * SECONDS_PER_DAY;
            }

            des_sql = " datetime) PARTITION BY RANGE(" + stmt.bucket_field + ") timescale interval '" + bucketwidth +"' autopart crosspart;";
        }
        des_sql = "create table " + stmt.des_table_name + "(" + GetColString(col_names) + stmt.bucket_field + des_sql;

        // db name
        auto ins = GetStorageInstance();
        auto path = ins.lock()->GetDbPath();
        jobinfo.ts_cagg_policy.db_name = path;

        int32_t r = job_manager->CreateTsjob(this, jobinfo, des_sql);
        if (r != GS_SUCCESS) {
            GS_LOG_RUN_ERR("[StreamAgg] add ts job failed!");
            throw std::runtime_error("add ts job failed!");
        }
        
        return r;
    }
    else if (stmt.type == TsType::TS_PAUSE_CAGG_POLICY) {
        auto policy_names = job_manager->GetAllJobName(this);
        if (std::find(policy_names.begin(), policy_names.end(), stmt.policy_name) == policy_names.end()) {
            throw std::runtime_error(fmt::format("ts job {} does not exist!", stmt.policy_name));
        }

        int32_t r = job_manager->PauseJob(this, stmt.policy_name);
        if (r != GS_SUCCESS) {
            throw std::runtime_error("pause ts job failed!");
        }

        return r;
    }
    else if (stmt.type == TsType::TS_DEL_CAGG_POLICY) {
        uint32_t job_id;
        int32_t r = job_manager->GetJobId(this, stmt.policy_name, job_id);

        r = job_manager->DeletetTsJobInfo(this, job_id);
        if (r != GS_SUCCESS) {
            throw std::runtime_error("delete ts job failed!");
        }

        return r;
    }
    else if (stmt.type == TsType::TS_REFRESH_CAGG_POLICY) {
        TsJobInfo job;

        int32_t r = job_manager->GetJob(this, stmt.policy_name, job);
        if (r != GS_SUCCESS) {
            throw std::runtime_error("get job failed!");
        }

        try {
            std::string agg_sql = job.ts_cagg_policy.exec_sql;

            timestamp_stor_t now_time = TimestampNow();
            timestamp_stor_t start_time;
            timestamp_stor_t end_time = now_time - stmt.end_offset * MICROSECS_PER_SECOND;
            std::string start_date;
            std::string end_date;

            if (stmt.start_offset == 0) {
                start_time.ts = 0;
            }
            else {
                start_time = now_time - stmt.start_offset * MICROSECS_PER_SECOND;
            }

            if (start_time >= end_time) {
                throw std::runtime_error("refresh job start time is greater than or equal end time!");
            }

            if (end_time - start_time > MAX_SINTERVAL_NUMS * job.ts_job.schedule_interval * MICROSECS_PER_SECOND) {
                start_time = end_time - MAX_SINTERVAL_NUMS * job.ts_job.schedule_interval * MICROSECS_PER_SECOND;
            }

            if (!TryCast::Operation<timestamp_stor_t, std::string>(start_time, start_date)) {
                throw std::runtime_error("start_time convert to string failed!");
            }
            ReplaceSubstring(agg_sql, "__START_TIME__", "'"+start_date+"'");

            if (!TryCast::Operation<timestamp_stor_t, std::string>(end_time, end_date)) {
                throw std::runtime_error("end_time convert to string failed!");
            }
            ReplaceSubstring(agg_sql, "__END_TIME__", "'"+end_date+"'");

            std::string del_sql = "DELETE FROM " + job.ts_cagg_policy.table_name + " WHERE " + job.ts_cagg_policy.bucket_field_name + "='" + end_date + "'";
            auto ret = Query(del_sql.c_str());
            if (ret->GetRetCode() != GS_SUCCESS) {
                throw std::runtime_error(ret->GetRetMsg());
            }

            agg_sql = "INSERT INTO " + job.ts_cagg_policy.table_name + " " + agg_sql;
            ret = Query(agg_sql.c_str());
            if (ret->GetRetCode() != GS_SUCCESS) {
                throw std::runtime_error(ret->GetRetMsg());
            }
        }
        catch (const std::exception& e) {
            GS_LOG_RUN_ERR("[StreamAgg] refresh job exec failed: %s", e.what());
            throw std::runtime_error(e.what());
        }

        return GS_SUCCESS;
    }
    else {
        uint32_t job_id;
        int32_t r = job_manager->GetJobId(this, stmt.policy_name, job_id);

        if (stmt.type == TsType::TS_SEL_CAGG_STATS) {
            r = job_manager->SelectTsJobStat(this, job_id, result);
            if (r != GS_SUCCESS) {
                throw std::runtime_error("select ts job stat failed!");
            }  
        }
        else if (stmt.type == TsType::TS_SEL_CAGG_ERR) {
            r = job_manager->SelectTsJobErr(this, job_id, result);
            if (r != GS_SUCCESS) {
                throw std::runtime_error("select ts job err failed!");
            }  
        }

        result.SetRecordBatchType(RecordBatchType::Select);
        return r;
    }
}