/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * database.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/main/database.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "main/database.h"

#include <iostream>
#include <stdexcept>

#include "main/base_storage.h"
#include "storage/gstor/zekernel/common/cm_error.h"
#include "storage/gstor/zekernel/common/cm_file.h"
#include "storage/storage.h"
#include "function/function.h"
#include "compute/ts/include/stream_agg/scheduler.h"
#include "compute/ts/include/stream_agg/cleaner.h"

std::mutex IntarkDB::mutex_;
std::unordered_map<std::string, std::weak_ptr<BaseStorage>> IntarkDB::instance_map_ = {};

IntarkDB::IntarkDB(PassKey passkey, std::shared_ptr<BaseStorage> storage, std::string path)
        : storage_(storage), path_(path) {
    intarkdb::FunctionContext::Init();
}

IntarkDB::IntarkDB(std::shared_ptr<BaseStorage> storage) : storage_(storage) {
    intarkdb::FunctionContext::Init();
}

IntarkDB::~IntarkDB() {
    std::unordered_map<std::string, std::weak_ptr<BaseStorage>>::iterator it;
    it = instance_map_.find(path_);
    if (it != instance_map_.end()) {
        instance_map_.erase(it);
    }
}

std::shared_ptr<BaseStorage> IntarkDB::GetStorage(const std::string& in_path) {
    auto iter = instance_map_.find(in_path);
    if (iter != instance_map_.end()) {
        auto instance = iter->second.lock();
        if (instance != nullptr) {
            return instance;
        }
    }

    // double check
    std::lock_guard<std::mutex> lock(mutex_);
    iter = instance_map_.find(in_path);
    if (iter != instance_map_.end()) {
        auto instance = iter->second.lock();
        if (instance != nullptr) {
            return instance;
        }
    }
    // 限制同时打开的数据库实例的数量   MAX_DB_INSTANCE_COUNT
    if (instance_map_.size() >= MAX_DB_INSTANCE_COUNT) {
        GS_LOG_RUN_ERR("No more than 32 databases can be opened simultaneously!");
        throw std::runtime_error("No more than 32 databases can be opened simultaneously!");
    }
    auto instance = std::make_shared<BaseStorage>();
    instance->Open(const_cast<char*>(in_path.c_str()));
    instance_map_[in_path] = instance;

    if (instance->bs_get_ts_cagg_switch_on()) {
            // 创建一个新线程，并指定要执行的函数, 开启流计算任务执行线程
            auto dbstorage_wrap = new DbStorageWrapper();
            dbstorage_wrap->db_storage = instance;
            auto &thread_id = db_get_streamagg_main_thread(instance)->id;
            thread_t clean_thread;
    #ifdef _WIN32
            db_get_streamagg_main_thread(instance)->handle = CreateThread(NULL, 0, TsStreamAggSchedulerMain, dbstorage_wrap, 0, &thread_id);
            // SetThreadDescription(db_get_streamagg_main_thread(instance)->handle, L"stream_agg_proc");
        
            clean_thread.handle = CreateThread(NULL, 0, TsClean, dbstorage_wrap, 0, &clean_thread.id);
            // SetThreadDescription(clean_thread.handle, L"stream_clean");
    #else
            pthread_create(&thread_id, nullptr, TsStreamAggSchedulerMain, dbstorage_wrap);
            pthread_setname_np(thread_id, "stream_agg_proc");
        
            pthread_create(&clean_thread.id, nullptr, TsClean, dbstorage_wrap);
            pthread_setname_np(clean_thread.id, "stream_clean");
    #endif
        }
    return instance;
}

static bool IsAbsolutePath(const std::string& path) {
#ifdef __linux__
    return path.length() > 0 && path[0] == '/';
#else
    return true;  // windwos 平台暂都返回true
#endif
}

const char* DEFAULT_PATH = "./";
constexpr size_t DEFAULT_PATH_LEN = 2;

std::shared_ptr<IntarkDB> IntarkDB::GetInstance(const char* path) {
    // get current path
    char current_path[1024] = {0};
    getcwd(current_path, sizeof(current_path));
    // path is aoosolute path or relative path
    std::string in_path = "";
    if (path == nullptr || strcmp(path, DEFAULT_PATH) == 0) {
        in_path = current_path;
    } else {
        if (IsAbsolutePath(path)) {
            in_path = path;
        } else {
            size_t len = strlen(path);
            if (len >= DEFAULT_PATH_LEN && strncmp(path, DEFAULT_PATH, DEFAULT_PATH_LEN) == 0) {
                in_path = current_path;
                in_path += "/";
                in_path += path + DEFAULT_PATH_LEN;
            } else {
                in_path = current_path;
                in_path += "/";
                in_path += path;
            }
        }
    }
    auto storage = GetStorage(in_path);
    if (storage == nullptr) {
        GS_LOG_RUN_ERR("Get database instance err!");
        throw std::runtime_error("Get database instance err!");
    }
    return std::make_shared<IntarkDB>(IntarkDB::PassKey{}, storage, in_path);
}

void IntarkDB::DestoryHandle(void* handle) { storage_->db_handle_free(handle); }

status_t IntarkDB::AllocHandle(void** handle) { return storage_->db_handle_alloc(handle); }

bool IntarkDB::IsDBAvailable() { return storage_->db_is_available(); }

thread_t * IntarkDB::db_get_streamagg_main_thread(std::shared_ptr<BaseStorage> storage) {
    return storage->bs_get_streamagg_main_thread();
}

uint32_t IntarkDB::db_get_streamagg_threadpool_num() {
    return storage_->bs_get_streamagg_threadpool_num();
}