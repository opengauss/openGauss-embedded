/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* diff_date.h
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/include/function/diff_date.h
*
* -------------------------------------------------------------------------
*/
#pragma once

#include <vector>

#include "type/value.h"

// YEAR/QUARTER/MONTH/WEEK/DAY/HOUR/MINUTE/SECOND
#define DATEDIFF_TYPE_YEAR "YEAR"
#define DATEDIFF_TYPE_QUARTER "QUARTER"
#define DATEDIFF_TYPE_MONTH "MONTH"
#define DATEDIFF_TYPE_WEEK "WEEK"
#define DATEDIFF_TYPE_DAY "DAY"
#define DATEDIFF_TYPE_HOUR "HOUR"
#define DATEDIFF_TYPE_MINUTE "MINUTE"
#define DATEDIFF_TYPE_SECOND "SECOND"

constexpr int DATEDIFF_ARG_NUM2 = 2;
constexpr int DATEDIFF_ARG_NUM3 = 3;
auto timestamp_diff(const std::vector<Value>& values) -> Value {
    std::string diff_type;
    Value date_1, date_2;
    if (values.size() == DATEDIFF_ARG_NUM2) {
        diff_type = DATEDIFF_TYPE_DAY;
        date_1 = DataType::GetTypeInstance(GStorDataType::GS_TYPE_DATE)->CastValue(values[0]);
        date_2 = DataType::GetTypeInstance(GStorDataType::GS_TYPE_DATE)->CastValue(values[1]);
    } else if (values.size() == DATEDIFF_ARG_NUM3) {
        diff_type = intarkdb::StringUtil::Upper(values[0].ToString());
        date_1 = DataType::GetTypeInstance(GStorDataType::GS_TYPE_DATE)->CastValue(values[1]);
        date_2 = DataType::GetTypeInstance(GStorDataType::GS_TYPE_DATE)->CastValue(values[2]);
    } else {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "timestampdiff function parameter number error");
    }

    int64_t result = 0;
    auto ts_1 = date_1.GetCastAs<TimestampType::StorType>().ts;
    auto ts_2 = date_2.GetCastAs<TimestampType::StorType>().ts;
    if (diff_type == DATEDIFF_TYPE_DAY) {
        result = (ts_1 - ts_2) / UNITS_PER_DAY;
    } else if (diff_type == DATEDIFF_TYPE_HOUR) {
        result = (ts_1 - ts_2) / ((int64_t)SECONDS_PER_HOUR * (int64_t)MILLISECS_PER_SECOND * (int64_t)MICROSECS_PER_MILLISEC);
    } else if (diff_type == DATEDIFF_TYPE_MINUTE) {
        result = (ts_1 - ts_2) / ((int64_t)SECONDS_PER_MIN * (int64_t)MILLISECS_PER_SECOND * (int64_t)MICROSECS_PER_MILLISEC);
    } else if (diff_type == DATEDIFF_TYPE_SECOND) {
        result = (ts_1 - ts_2) / ((int64_t)MILLISECS_PER_SECOND * (int64_t)MICROSECS_PER_MILLISEC);
    } else {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "timestampdiff function parameter type error, support()");
    }

    return ValueFactory::ValueBigInt(result);
}

auto date_diff(const std::vector<Value>& values) -> Value {
    if (values.size() != DATEDIFF_ARG_NUM2) {
        throw intarkdb::Exception(ExceptionType::EXECUTOR, "datediff function parameter number error");
    }

    int64_t result = 0;
    auto date_1 = DataType::GetTypeInstance(GStorDataType::GS_TYPE_DATE)->CastValue(values[0]);
    auto date_2 = DataType::GetTypeInstance(GStorDataType::GS_TYPE_DATE)->CastValue(values[1]);
    auto ts_1 = date_1.GetCastAs<TimestampType::StorType>().ts;
    auto ts_2 = date_2.GetCastAs<TimestampType::StorType>().ts;
    result = (ts_1 - ts_2) / UNITS_PER_DAY;

    return ValueFactory::ValueBigInt(result);
}
