/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * multiply.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/function/math/multiply.cpp
 *
 * -------------------------------------------------------------------------
 */

#include "function/math/multiply.h"

#include "type/operator/decimal_cast.h"
#include "type/operator/mul_operator.h"
#include "type/type_id.h"
#include "type/value.h"

namespace intarkdb {

auto MultiplySet::Register(FunctionContext& context) -> void {
    context.RegisterFunction("*", {{GS_TYPE_TINYINT, GS_TYPE_TINYINT}, GS_TYPE_TINYINT},
                             [](const std::vector<Value>& args) {
                                 return ValueFactory::ValueTinyInt(MultiplyOp::Operation<int8_t, int8_t, int8_t>(
                                     args[0].GetCastAs<int8_t>(), args[1].GetCastAs<int8_t>()));
                             });
    context.RegisterFunction("*", {{GS_TYPE_SMALLINT, GS_TYPE_SMALLINT}, GS_TYPE_SMALLINT},
                             [](const std::vector<Value>& args) {
                                 return ValueFactory::ValueSmallInt(MultiplyOp::Operation<int16_t, int16_t, int16_t>(
                                     args[0].GetCastAs<int16_t>(), args[1].GetCastAs<int16_t>()));
                             });
    context.RegisterFunction("*", {{GS_TYPE_INTEGER, GS_TYPE_INTEGER}, GS_TYPE_INTEGER},
                             [](const std::vector<Value>& args) {
                                 return ValueFactory::ValueInt(MultiplyOp::Operation<int32_t, int32_t, int32_t>(
                                     args[0].GetCastAs<int32_t>(), args[1].GetCastAs<int32_t>()));
                             });
    context.RegisterFunction("*", {{GS_TYPE_BIGINT, GS_TYPE_BIGINT}, GS_TYPE_BIGINT},
                             [](const std::vector<Value>& args) {
                                 return ValueFactory::ValueBigInt(MultiplyOp::Operation<int64_t, int64_t, int64_t>(
                                     args[0].GetCastAs<int64_t>(), args[1].GetCastAs<int64_t>()));
                             });
    context.RegisterFunction(
        "*", {{GS_TYPE_UTINYINT, GS_TYPE_UTINYINT}, GS_TYPE_UTINYINT}, [](const std::vector<Value>& args) {
            return ValueFactory::ValueUnsignTinyInt(MultiplyOp::Operation<uint8_t, uint8_t, uint8_t>(
                args[0].GetCastAs<uint8_t>(), args[1].GetCastAs<uint8_t>()));
        });
    context.RegisterFunction(
        "*", {{GS_TYPE_USMALLINT, GS_TYPE_USMALLINT}, GS_TYPE_USMALLINT}, [](const std::vector<Value>& args) {
            return ValueFactory::ValueUnsignSmallInt(MultiplyOp::Operation<uint16_t, uint16_t, uint16_t>(
                args[0].GetCastAs<uint16_t>(), args[1].GetCastAs<uint16_t>()));
        });
    context.RegisterFunction(
        "*", {{GS_TYPE_UINT32, GS_TYPE_UINT32}, GS_TYPE_UINT32}, [](const std::vector<Value>& args) {
            return ValueFactory::ValueUnsignInt(MultiplyOp::Operation<uint32_t, uint32_t, uint32_t>(
                args[0].GetCastAs<uint32_t>(), args[1].GetCastAs<uint32_t>()));
        });
    context.RegisterFunction(
        "*", {{GS_TYPE_UINT64, GS_TYPE_UINT64}, GS_TYPE_UINT64}, [](const std::vector<Value>& args) {
            return ValueFactory::ValueUnsignBigInt(MultiplyOp::Operation<uint64_t, uint64_t, uint64_t>(
                args[0].GetCastAs<uint64_t>(), args[1].GetCastAs<uint64_t>()));
        });
    context.RegisterFunction(
        "*", {{GS_TYPE_DECIMAL, GS_TYPE_DECIMAL}, GS_TYPE_DECIMAL}, [](const std::vector<Value>& args) {
            auto result_type = GetCompatibleType(args[0].GetLogicalType(), args[1].GetLogicalType());
            auto result =
                MultiplyOp::Operation<dec4_t, dec4_t, dec4_t>(args[0].GetCastAs<dec4_t>(), args[1].GetCastAs<dec4_t>());
            result = DecimalCast::Operation<dec4_t>(result, result_type.Scale(), result_type.Precision());
            return ValueFactory::ValueDecimal(result, result_type.Precision(), result_type.Scale());
        });
    context.RegisterFunction("*", {{GS_TYPE_REAL, GS_TYPE_REAL}, GS_TYPE_REAL}, [](const std::vector<Value>& args) {
        return ValueFactory::ValueDouble(
            MultiplyOp::Operation<double, double, double>(args[0].GetCastAs<double>(), args[1].GetCastAs<double>()));
    });
}
}  // namespace intarkdb
