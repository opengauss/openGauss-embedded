/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* drop_exec.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/planner/physical_plan/drop_exec.cpp
*
* -------------------------------------------------------------------------
*/
#include "planner/physical_plan/drop_exec.h"

#include "storage/db_handle.h"
#include "storage/storage.h"

auto DropExec::Execute() const -> RecordBatch {
    RecordBatch rb(schema_);
    drop_def_t drop_info = {0};
    drop_info.name = (char*)name.c_str();
    drop_info.if_exists = (int)if_exists;
    switch (type) {
        case ObjectType::TABLE_ENTRY:
            drop_info.type = DROP_TYPE_TABLE;
            break;
        case ObjectType::INDEX_ENTRY:
            drop_info.type = DROP_TYPE_INDEX;
            break;
        case ObjectType::SEQUENCE_ENTRY:
            drop_info.type = DROP_TYPE_SEQUENCE;
            break;
        case ObjectType::VIEW_ENTRY:
            drop_info.type = DROP_TYPE_VIEW;
            break;
        default:
            throw std::invalid_argument(fmt::format("physical plan drop type not implemented yet"));
    }

    rb.status = gstor_drop(((db_handle_t*)handle)->handle, nullptr, &drop_info);
    return rb;
}