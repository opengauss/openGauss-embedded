/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * aggregate_exec.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/planner/physical_plan/aggregate_exec.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "planner/physical_plan/aggregate_exec.h"
#include "type/type_id.h"

constexpr int AGG_DEFAULT_DECIMAL_SCALE = 4;
constexpr int AGG_DEFAULT_DECIMAL_PRECISION = 18;

AggregateExec::AggregateExec(std::vector<std::unique_ptr<Expression>> groupby, std::vector<std::string> ops,
                             std::vector<std::unique_ptr<Expression>> be_group, const std::vector<bool>& distincts,
                             Schema schema, PhysicalPlanPtr child)
    : schema_(schema),
      child_(child),
      groups_(std::move(groupby)),
      ops_(std::move(ops)),
      be_groups_(std::move(be_group)),
      distincts(distincts) {
    be_groups_distinct_set.resize(distincts.size());
}

Schema AggregateExec::GetSchema() const {
    // 可以查询的字段包括 聚合字段 + groupb 字段
    return schema_;
}

void AggregateExec::Init() {
    AggKey key;
    groups_map_.clear();
    avg_idx_count.clear();
    count_map_.clear();
    DistinctKey globalMode;
    long globalCount = 0;
    int windowSize = 1024 / 2;

    while (true) {
        // clear , ready for next time, avoid allocate memory again
        key.Clear();
        auto [record, cursor, eof] = child_->Next();
        if (eof) {
            break;
        }
        for (size_t i = 0; i < groups_.size(); ++i) {
            key.AddItem(groups_[i]->Evaluate(record));
        }
        if (key.ItemSize() == 0) {
            key.AddItem(ValueFactory::ValueInt(1));
        }

        auto iter = groups_map_.find(key);
        if (iter != groups_map_.end()) {
            auto& r = iter->second;
            for (size_t i = 0; i < ops_.size(); ++i) {
                const auto& name = ops_[i];
                if (name == "count_star") {
                    r.SetField(i + groups_.size(),
                               ValueFactory::ValueInt(r.Field(i + groups_.size()).GetCastAs<int32_t>() + 1));
                } else if (name == "count") {
                    auto count_v = be_groups_[i]->Evaluate(record);
                    if (count_v.IsNull()) {
                        continue;
                    }
                    if (distincts[i]) {
                        AggKey k;
                        k.AddItem(count_v);
                        auto distinct_key = be_groups_distinct_set[i].find(k);
                        if (distinct_key == be_groups_distinct_set[i].end()) {
                            be_groups_distinct_set[i].insert(k);
                            r.SetField(i + groups_.size(),
                                       ValueFactory::ValueInt(r.Field(i + groups_.size()).GetCastAs<int32_t>() + 1));
                        }
                    } else {
                        r.SetField(i + groups_.size(),
                                   ValueFactory::ValueInt(r.Field(i + groups_.size()).GetCastAs<int32_t>() + 1));
                    }
                } else if (name == "max") {
                    auto new_value = be_groups_[i]->Evaluate(record);
                    if (new_value.IsNull()) {
                        continue;
                    }
                    if (r.Field(i + groups_.size()).IsNull()) {
                        r.SetField(i + groups_.size(), new_value);
                    } else if (r.Field(i + groups_.size()).LessThan(new_value) == Trivalent::TRI_TRUE) {
                        r.SetField(i + groups_.size(), new_value);
                    }
                } else if (name == "min") {
                    auto new_value = be_groups_[i]->Evaluate(record);
                    if (new_value.IsNull()) {
                        continue;
                    }
                    if (r.Field(i + groups_.size()).IsNull()) {
                        r.SetField(i + groups_.size(), new_value);
                    } else if (new_value.LessThan(r.Field(i + groups_.size())) == Trivalent::TRI_TRUE) {
                        r.SetField(i + groups_.size(), new_value);
                    }
                }else if (name == "mode") {
                    auto new_value = be_groups_[i]->Evaluate(record);
                    if (new_value.IsNull()) {
                        continue;
                    }
                    if (distincts[i]) {
                        AggKey k;
                        k.AddItem(new_value);
                        auto distinct_key = be_groups_distinct_set[i].find(k);
                        if (distinct_key == be_groups_distinct_set[i].end()) {
                            be_groups_distinct_set[i].insert(k);
                        }
                    } 

                    DistinctKey distinctKey;
                    distinctKey.AddItem(new_value);
                    count_map_[distinctKey]++;
                    if (count_map_[distinctKey] > globalCount) {
                        globalMode.Clear();
                        globalMode.AddItem(new_value);
                        globalCount = count_map_[distinctKey];
                    }
                    if(i >= windowSize){
                        auto old_value =  be_groups_[i - windowSize]->Evaluate(record);
                        DistinctKey old_distinctKey;
                        old_distinctKey.AddItem(old_value);
                        count_map_[old_distinctKey]--;
                        if (count_map_[old_distinctKey] == 0) {
                            count_map_.erase(old_distinctKey);
                        }
                    }
                    r.SetField(i + groups_.size(), globalMode.Keys()[0]);
                } else if (name == "sum") {
                    auto new_value = be_groups_[i]->Evaluate(record);
                    if (new_value.IsNull()) {
                        continue;
                    }
                    if (!new_value.IsFloat() && !new_value.IsInteger()) {
                        throw std::runtime_error("sum only support int or float type");
                    }
                    // 需要判断原来的值是不是null
                    if (distincts[i]) {
                        AggKey k;
                        k.AddItem(new_value);
                        auto distinct_key = be_groups_distinct_set[i].find(k);
                        if (distinct_key == be_groups_distinct_set[i].end()) {
                            be_groups_distinct_set[i].insert(k);
                            if (r.Field(i + groups_.size()).IsNull()) {
                                r.SetField(i + groups_.size(), new_value);
                            } else {
                                auto numeric_ptr =
                                    dynamic_cast<const NumberType*>(DataType::GetTypeInstance(new_value.GetType()));
                                r.SetField(i + groups_.size(),
                                           numeric_ptr->Add(r.Field(i + groups_.size()), new_value));
                            }
                        }
                    } else {
                        if (r.Field(i + groups_.size()).IsNull()) {
                            r.SetField(i + groups_.size(), new_value);
                        } else {
                            const NumberType* numeric_ptr = nullptr;
                            if (new_value.IsUnSigned()) {
                                numeric_ptr = dynamic_cast<const NumberType*>(
                                    DataType::GetTypeInstance(GStorDataType::GS_TYPE_UINT64));
                            } else if (new_value.IsInteger()) {
                                numeric_ptr = dynamic_cast<const NumberType*>(
                                    DataType::GetTypeInstance(GStorDataType::GS_TYPE_BIGINT));
                            } else {
                                numeric_ptr =
                                    dynamic_cast<const NumberType*>(DataType::GetTypeInstance(new_value.GetType()));
                            }
                            r.SetField(i + groups_.size(), numeric_ptr->Add(r.Field(i + groups_.size()), new_value));
                        }
                    }
                } else if (name == "avg") {
                    auto new_value = be_groups_[i]->Evaluate(record);
                    if (new_value.IsNull()) {
                        continue;
                    }
                    if (!new_value.IsFloat() && !new_value.IsInteger()) {
                        throw std::runtime_error("avg only support int or float type");
                    }
                    if (distincts[i]) {
                        AggKey k;
                        k.AddItem(new_value);
                        auto distinct_key = be_groups_distinct_set[i].find(k);
                        if (distinct_key == be_groups_distinct_set[i].end()) {
                            be_groups_distinct_set[i].insert(k);
                            if (r.Field(i + groups_.size()).IsNull()) {
                                avg_idx_count[i + groups_.size()][key] = 1;
                                r.SetField(i + groups_.size(), new_value);
                            } else {
                                avg_idx_count[i + groups_.size()][key] += 1;

                                const NumberType* numeric_ptr = nullptr;
                                if (new_value.IsUnSigned()) {
                                    numeric_ptr = dynamic_cast<const NumberType*>(
                                        DataType::GetTypeInstance(GStorDataType::GS_TYPE_UINT64));
                                } else if (new_value.IsInteger()) {
                                    numeric_ptr = dynamic_cast<const NumberType*>(
                                        DataType::GetTypeInstance(GStorDataType::GS_TYPE_BIGINT));
                                } else {
                                    numeric_ptr =
                                        dynamic_cast<const NumberType*>(DataType::GetTypeInstance(new_value.GetType()));
                                }

                                r.SetField(i + groups_.size(),
                                           numeric_ptr->Add(r.Field(i + groups_.size()), new_value));
                            }
                        }
                    } else {
                        if (r.Field(i + groups_.size()).IsNull()) {
                            avg_idx_count[i + groups_.size()][key] = 1;
                            r.SetField(i + groups_.size(), new_value);
                        } else {
                            avg_idx_count[i + groups_.size()][key] += 1;
                            const NumberType* numeric_ptr = nullptr;
                            if (new_value.IsUnSigned()) {
                                numeric_ptr = dynamic_cast<const NumberType*>(
                                    DataType::GetTypeInstance(GStorDataType::GS_TYPE_UINT64));
                            } else if (new_value.IsInteger()) {
                                numeric_ptr = dynamic_cast<const NumberType*>(
                                    DataType::GetTypeInstance(GStorDataType::GS_TYPE_BIGINT));
                            } else {
                                numeric_ptr =
                                    dynamic_cast<const NumberType*>(DataType::GetTypeInstance(new_value.GetType()));
                            }
                            // auto numeric_ptr =
                            //     dynamic_cast<const NumberType*>(DataType::GetTypeInstance(new_value.GetType()));
                            r.SetField(i + groups_.size(), numeric_ptr->Add(r.Field(i + groups_.size()), new_value));
                        }
                    }
                } else {
                    throw std::runtime_error(fmt::format("not supported func {}", name));
                }
            }
        } else {
            std::unordered_map<uint16_t, Value> values;
            int v_idx = 0;
            for (size_t i = 0; i < groups_.size(); ++i) {
                values[v_idx++] = groups_[i]->Evaluate(record);
            }
            for (size_t i = 0; i < ops_.size(); ++i) {
                const auto& name = ops_[i];
                if (name == "count_star") {
                    values[v_idx++] = ValueFactory::ValueInt(1);
                } else if (name == "count") {
                    auto v = be_groups_[i]->Evaluate(record);
                    values[v_idx++] = v.IsNull() ? ValueFactory::ValueInt(0) : ValueFactory::ValueInt(1);
                    if (distincts[i]) {
                        AggKey k;
                        k.AddItem(v);
                        be_groups_distinct_set[i].insert(k);
                    }
                } else if (name == "sum") {
                    auto v = be_groups_[i]->Evaluate(record);
                    if (v.IsNull()) {
                        values[v_idx++] = v;  // put null in it
                    } else {
                        if (!v.IsFloat() && !v.IsInteger()) {
                            throw std::runtime_error("sum only support int or float type");
                        }

                        values[v_idx++] = v;
                        if (distincts[i]) {
                            AggKey k;
                            k.AddItem(v);
                            be_groups_distinct_set[i].insert(k);
                        }
                    }
                } else if (name == "avg") {
                    auto v = be_groups_[i]->Evaluate(record);
                    if (v.IsNull()) {
                        values[v_idx++] = v;
                    } else {
                        if (!v.IsFloat() && !v.IsInteger()) {
                            throw std::runtime_error("sum only support int or float type");
                        }
                        if (distincts[i]) {
                            AggKey k;
                            k.AddItem(v);
                            be_groups_distinct_set[i].insert(k);
                        }
                        avg_idx_count[v_idx].insert({key, 1});  // 记录平均值的下标
                        values[v_idx++] = v;
                    }
                } else if (name == "mode"){
                    auto new_value = be_groups_[i]->Evaluate(record);
                    if (new_value.IsNull()) {
                        values[v_idx++] = new_value;
                    }
                    if (distincts[i]) {
                        AggKey k;
                        k.AddItem(new_value);
                        be_groups_distinct_set[i].insert(k);
                    } 

                    DistinctKey distinctKey;
                    distinctKey.AddItem(new_value);
                    count_map_[distinctKey]++;
                    if (count_map_[distinctKey] > globalCount) {
                        globalMode.Clear();
                        globalMode.AddItem(new_value);
                        globalCount = count_map_[distinctKey];
                    }
                    if(i >= windowSize){
                        auto old_value =  be_groups_[i - windowSize]->Evaluate(record);
                        DistinctKey old_distinctKey;
                        old_distinctKey.AddItem(old_value);
                        count_map_[old_distinctKey]--;
                        if (count_map_[old_distinctKey] == 0) {
                            count_map_.erase(old_distinctKey);
                        }
                    }
                    values[v_idx++] = globalMode.Keys()[0];    
                } else {
                    auto v = be_groups_[i]->Evaluate(record);
                    if (v.IsNull()) {
                        values[v_idx++] = v;
                    } else {
                        values[v_idx++] = be_groups_[i]->Evaluate(record);
                    }
                }
            }
            groups_map_[key] = Record(std::move(values));
        }
    }

    // 计算平均值
    for (auto& idx_iter : avg_idx_count) {
        auto idx = idx_iter.first;
        auto& group_row_count = idx_iter.second;
        for (auto& r : groups_map_) {
            dec4_t num;
            auto iter = group_row_count.find(r.first);
            if (iter == group_row_count.end()) {
                throw std::runtime_error("AVG Caluate Error");
            }
            if (iter->second == 0) {
                continue;
            }
            cm_real_to_dec4((double)iter->second, &num);
            auto& record = r.second;
            auto v = record.Field(idx);
            auto numeric_ptr =
                dynamic_cast<const NumberType*>(DataType::GetTypeInstance(GStorDataType::GS_TYPE_DECIMAL));
            v = numeric_ptr->Div(v, ValueFactory::ValueDecimal(num));
            // FIXME: 如何更好显示？这里设置了默认规格
            v.SetScaleAndPrecision(AGG_DEFAULT_DECIMAL_SCALE, AGG_DEFAULT_DECIMAL_PRECISION);
            record.SetField(idx, v);
        }
    }

    // 空表处理
    if (groups_map_.size() == 0 && groups_.size() == 0) {
        std::unordered_map<uint16_t, Value> values;
        int v_idx = 0;
        for (size_t i = 0; i < ops_.size(); ++i) {
            const auto& name = ops_[i];
            if (name == "count_star" || name == "count") {
                values[v_idx++] = ValueFactory::ValueInt(0);
            } else {
                values[v_idx++] = ValueFactory::ValueNull();
            }
        }
        groups_map_[key] = Record(std::move(values));
    }
    groups_map_iter_ = groups_map_.begin();
    init_ = true;
}

auto AggregateExec::Next() -> std::tuple<Record, knl_cursor_t*, bool> {
    if (!init_) {
        Init();
    }
    if (groups_map_iter_ != groups_map_.end()) {
        std::tuple<Record, knl_cursor_t*, bool> r = {groups_map_iter_->second, nullptr, false};
        groups_map_iter_++;
        return r;
    }
    init_ = false;
    return {Record{}, nullptr, true};
}
