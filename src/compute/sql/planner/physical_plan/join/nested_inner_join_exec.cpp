/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * nested_inner_join_exec.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/planner/physical_plan/nested_inner_join_exec.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "planner/physical_plan/join/nested_inner_join_exec.h"

auto InnerJoinExec::Next() -> std::tuple<Record, knl_cursor_t*, bool> {
    while (true) {
        if (right_eof_) {
            const auto& [record, _, eof] = left_->Next();
            if (eof) {
                left_eof_ = true;
                ResetNext();
                return {{}, nullptr, true};
            }
            curr_record_ = record;
        }
        auto&& [record, _, eof] = right_->Next();
        if (eof) {
            right_eof_ = true;
            right_->ResetNext();
            continue;
        }
        right_eof_ = false;
        return {curr_record_.Concat(std::move(record)), nullptr, false};
    }
}

auto InnerJoinExec::ResetNext() -> void {
    idx_ = 0;
    left_eof_ = false;
    right_eof_ = true;
    left_->ResetNext();
    right_->ResetNext();
}
