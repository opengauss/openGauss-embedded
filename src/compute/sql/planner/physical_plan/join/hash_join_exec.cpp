/*
 * Copyright (c) GBA-NCTI-ISDC. 2022-2024.
 *
 * openGauss embedded is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * hash_join_exec.cpp
 *
 * IDENTIFICATION
 * openGauss-embedded/src/compute/sql/planner/physical_plan/join/hash_join_exec.cpp
 *
 * -------------------------------------------------------------------------
 */
#include "planner/physical_plan/join/hash_join_exec.h"

auto HashJoinExec::Init() -> void {
    while (true) {
        auto&& [record, _, eof] = right_->Next();
        if (eof) {
            break;
        }
        bool include_null_field = false;
        DistinctKey key;
        for (auto idx : inner_key_idxs_) {
            auto& field = record.FieldRef(idx);
            if (field.IsNull()) {
                include_null_field = true;
                break;
            }
            key.AddItem(record.FieldRef(idx));
        }
        if (include_null_field) {  // 含义null字段的记录，必不会相等
            continue;
        }
        auto iter = hash_table_.find(key);
        if (iter == hash_table_.end()) {
            hash_table_.emplace(std::move(key), std::vector<Record>{std::move(record)});
        } else {
            iter->second.push_back(record);
        }
    }
}

auto HashJoinExec::Next() -> std::tuple<Record, knl_cursor_t*, bool> {
    if (!init_) {
        init_ = true;
        Init();
    }
    if (hash_idx_.idx == -1 || hash_idx_.idx == static_cast<int>(hash_idx_.iter->second.size())) {
        while (true) {
            auto&& [record, cursor, eof] = left_->Next();
            if (eof) {
                ResetNext();
                return {Record(), nullptr, true};
            }
            bool include_null_field = false;
            DistinctKey key;
            for (auto idx : outer_key_idxs_) {
                if (record.FieldRef(idx).IsNull()) {
                    include_null_field = true;
                    break;
                }
                key.AddItem(record.FieldRef(idx));
            }
            if (include_null_field) {
                continue;
            }
            auto iter = hash_table_.find(key);
            if (iter != hash_table_.end()) {
                hash_idx_.idx = 0;
                hash_idx_.iter = iter;
                curr_record_ = std::move(record);
                break;
            }
        }
    }
    auto& records = hash_idx_.iter->second;
    return {curr_record_.Concat(records[hash_idx_.idx++]), nullptr, false};
}

auto HashJoinExec::ResetNext() -> void {
    left_->ResetNext();
    right_->ResetNext();
    hash_idx_.Clear();
}
