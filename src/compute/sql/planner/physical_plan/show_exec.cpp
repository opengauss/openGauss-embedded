/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* show_exec.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/planner/physical_plan/show_exec.cpp
*
* -------------------------------------------------------------------------
*/
#include "planner/physical_plan/show_exec.h"

#include <fmt/core.h>

#include "type/type_str.h"
#include "common/default_value.h"
#include "monitor/status.h"
#include "include/intarkdb.h"

auto ShowExec::Execute() const -> RecordBatch {
    return RecordBatch(Schema());
}

void ShowExec::Execute(RecordBatch &rb_out) {
    switch (show_type_) {
        case ShowType::SHOW_TYPE_DATABASES: {
            throw intarkdb::Exception(ExceptionType::CATALOG,
                                      fmt::format("show type {} is not supported", show_type_));
            break;
        }
        case ShowType::SHOW_TYPE_TABLES: {
            ShowTables(rb_out);
            break;
        }
        case ShowType::SHOW_TYPE_SPECIFIC_TABLE: {
            DescribeTable(rb_out);
            break;
        }
        case ShowType::SHOW_TYPE_ALL: {
            ShowAll(rb_out);
            break;
        }
        default: {
            throw intarkdb::Exception(ExceptionType::CATALOG,
                                      fmt::format("show type {} is not supported", int(show_type_)));
        }
    }
}

void ShowExec::ShowTables(RecordBatch &rb_out) {
    auto schema = child_->GetSchema();
    rb_out = RecordBatch(schema);
    while (true) {
        auto [r, _, eof] = child_->Next();
        if (eof) {
            break;
        }
        rb_out.AddRecord(r);
    }
}

void ShowExec::DescribeTable(RecordBatch &rb_out) {
    auto schema = child_->GetSchema();
    rb_out = RecordBatch(schema);
    while (true) {
        auto [r, _, eof] = child_->Next();
        if (eof) {
            break;
        }
        rb_out.AddRecord(r);
    } 
}

void ShowExec::ShowAll(RecordBatch &rb_out) {
    std::vector<SchemaColumnInfo> columns = { 
            { {"__show_tables_expanded", "name"}, "", GS_TYPE_VARCHAR, 0}, 
            { {"__show_tables_expanded", "value"}, "", GS_TYPE_VARCHAR, 1}, 
            { {"__show_tables_expanded", "description"}, "", GS_TYPE_VARCHAR, 2}, };
    auto schema = Schema(std::move(columns));
    rb_out = RecordBatch(schema);
    
    // cpu
    {
        std::vector<Value> row_values;
        auto val_cpu = get_self_cpu(0);
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "cpu"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, std::to_string(val_cpu)));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "process cpu(persent)"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // memory
    {
        std::vector<Value> row_values;
        auto val_memory = get_memory_by_pid(getpid());
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "memory"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, std::to_string(val_memory)));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "process memory(k)"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // db size
    {
        std::vector<Value> row_values;
        auto val_dir_size = get_dir_size(db_path.c_str());
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "dir_size"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, std::to_string(val_dir_size)));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "the size of db dir(k)"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // db path
    {
        std::vector<Value> row_values;
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "db_path"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, db_path));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "the path of db"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // log path
    {
        std::vector<Value> row_values;
        std::string log_path = db_path + "/intarkdb/log/run/";
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "log_path"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, log_path));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "the path of db run log"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // db version
    {
        std::vector<Value> row_values;
        std::string db_version = get_version();
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "db_version"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, db_version));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "the version of db"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // db starttime
    {
        std::vector<Value> row_values;
        auto start_time = get_start_time(getpid());
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "start_time"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, std::to_string(start_time)));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "db start time"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }

    // os infomation
    {
        std::vector<Value> row_values;
        char osinfo[1024];
        get_os_info(osinfo, 1024);
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "osinfo"));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, osinfo));
        row_values.push_back(Value(GStorDataType::GS_TYPE_VARCHAR, "operating system infomation(sysname, release, machine)"));
        rb_out.AddRecord(Record(std::move(row_values)));
    }
}
