/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* compare_expression.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/planner/expressions/compare_expression.cpp
*
* -------------------------------------------------------------------------
*/

#include "planner/expressions/compare_expression.h"

#include "type/compare_operator.h"
#include "type/type_system.h"

auto ComparisonExpression::Evaluate(const Record& record) const -> Value {
    auto left_value = lexp_->Evaluate(record);
    auto right_value = rexp_->Evaluate(record);
    return CompareOperator(type_, left_value, right_value);
}

using intarkdb::ComparisonType;

Value CompareOperator(ComparisonType type, const Value& left, const Value& right) {
    Trivalent r = Trivalent::UNKNOWN;
    switch (type) {
        case ComparisonType::Equal: {
            r = left.Equal(right);
            break;
        }
        case ComparisonType::NotEqual: {
            r = TrivalentOper::Not(left.Equal(right));
            break;
        }
        case ComparisonType::LessThan: {
            r = left.LessThan(right);
            break;
        }
        case ComparisonType::GreaterThan: {
            r = right.LessThan(left);
            break;
        }
        case ComparisonType::LessThanOrEqual: {
            r = left.LessThanOrEqual(right);
            break;
        }
        case ComparisonType::GreaterThanOrEqual: {
            r = right.LessThanOrEqual(left);
            break;
        }
        default:
            throw std::runtime_error(fmt::format("this {} comparison type not impl", type));
    }
    return Value(GStorDataType::GS_TYPE_BOOLEAN, r);
}
