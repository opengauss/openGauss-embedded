/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* default_tables.cpp
*
* IDENTIFICATION
* openGauss-embedded/src/compute/sql/catalog/default_tables.cpp
*
* -------------------------------------------------------------------------
*/
#include "catalog/default_table.h"

#include <vector>

#include "main/connection.h"

static bool g_is_create_default_tables = false;

static std::vector<std::string> default_sqls = {
    "create table if not exists ts_job_stat ( \
    job_id int not null comment '流计算任务id', \
    last_start bigint not null default 0 comment '上一次启动时间', \
    last_finish bigint not null default 0 comment '上一次结束时间', \
    next_start bigint not null default 0 comment '下一次启动时间', \
    last_succuss_finish bigint not null default 0 comment '上一次成功时间', \
    total_runs bigint not null default 0 comment '任务执行次数', \
    total_failed bigint not null default 0 comment '任务执行失败次数', \
    consecutive_failures int not null default 0 comment '任务连续失败次数', \
    create_time datetime not null default now() comment '创建时间', \
    update_time datetime not null default now() comment '更新时间', \
    primary key(job_id) \
    );",

    "create table if not exists ts_job(  \
    job_id int not null autoincrement comment '流计算任务id', \
    job_name varchar(256) not null default '' comment '流计算任务名', \
    state tinyint not null default 1 comment '任务状态: 0无效,1有效, 2暂停', \
    schedule_time int not null default 0 comment '任务调度周期, 默认0 为一次性任务', \
    schedule_interval int not null default 300 comment '调度周期, 默认300 单位：秒', \
    max_runtime int not null default 10 comment '任务单次最长执行时间,默认10S', \
    failed_retry_num tinyint not null default 3 comment '失败重试次数, 默认3次', \
    retry_period int not null default 300 comment '失败重试间隔, 默认300S', \
    initial_start bigint not null default 0 comment '首次启动时间,默认立刻', \
    partiton_type bigint not null default 0 comment '时序表分区字段类型', \
    offset_start bigint not null default 0 comment '任务执行起始时间,默认0为数据最小时间', \
    offset_end bigint not null default 0 comment '任务执行结束时间,默认0为数据最大时间', \
    create_time datetime not null default now() comment '创建时间', \
    update_time datetime not null default now() comment '更新时间', \
    primary key(job_id), \
    unique(job_name) \
    );",

    "create table if not exists ts_cagg_policy( \
    job_id int not null comment '流计算任务id', \
    db_name varchar(256) not null default '' comment '存储数据库名', \
    table_name varchar(64) not null comment '存储表名', \
    exec_sql text not null comment '聚合sql语句', \
    bucket_type tinyint not null default 0 comment '分桶类型: 0无分桶; 1时间分桶', \
    bucket_field_name varchar(128) not null default '' comment '分桶字段名', \
    bucket_width int not null comment '分桶宽度', \
    create_time datetime not null default now() comment '创建时间', \
    update_time datetime not null default now() comment '更新时间', \
    primary key(job_id), \
    unique(db_name,table_name) \
    );",

    "create table if not exists ts_job_err( \
    id int not null autoincrement comment '错误id', \
    job_id int not null comment '流计算任务id', \
    pid bigint not null default 0 comment '执行任务线程ID', \
    start_time datetime not null default now() comment '任务开始时间', \
    finish_time datetime not null default now() comment '任务结束时间', \
    err_data text not null default '' comment '任务失败原因', \
    create_time datetime not null default now() comment '创建时间', \
    update_time datetime not null default now() comment '更新时间', \
    primary key(id), \
    unique(job_id) \
    );",
};

void DefaultTableGenerator::CreateDefaultEntry(Connection *conn) {
    if (g_is_create_default_tables) {
        return;
    }

    for (auto &sql : default_sqls) {
        if (!g_is_create_default_tables) {
            g_is_create_default_tables = true;
        }

        try {
            auto rb = conn->Query((char *)sql.c_str());
            if (rb->GetRetCode() != GS_SUCCESS) {
                GS_LOG_RUN_INF("Failed to create default tables, ignoring...(msg=%s)", rb->GetRetMsg().c_str());
                GS_LOG_RUN_INF("Failed to create default tables, ignoring...(sql=%s)", sql.c_str());
                cm_reset_error();
            }
        } catch (intarkdb::Exception& instar_ex) {
            GS_LOG_RUN_INF("Failed to create default tables, ignoring...(sql=%s)", sql.c_str());
            cm_reset_error();
        } catch (const std::exception& e) {
            GS_LOG_RUN_INF("Failed to create default tables, ignoring...(sql=%s)", sql.c_str());
            cm_reset_error();
        } catch (...) {
            GS_LOG_RUN_INF("Failed to create default tables, ignoring...(sql=%s)", sql.c_str());
            cm_reset_error();
        }
    }
    g_is_create_default_tables = true;
}
