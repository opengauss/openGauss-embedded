/*
 * Copyright (c) 2022 Huawei Technologies Co.,Ltd.
 *
 * openGauss is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * knl_slm.h
 *
 * IDENTIFICATION
 * src\storage\gstor\zekernel\kernel\tsl\knl_slm.h
 *
 * -------------------------------------------------------------------------
 */

#ifndef __KNL_SLM_H__
#define __KNL_SLM_H__

#include "knl_context.h"
#ifdef __cplusplus
extern "C"
{
#endif

status_t tsm_lsm(knl_session_t *session);

#ifdef __cplusplus
}
#endif


#endif
