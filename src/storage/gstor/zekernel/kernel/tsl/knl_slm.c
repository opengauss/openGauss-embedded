/*
 * Copyright (c) 2022 Huawei Technologies Co.,Ltd.
 *
 * openGauss is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 * http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * knl_slm.c
 *
 * IDENTIFICATION
 * src\storage\gstor\zekernel\kernel\tsl\knl_slm.c
 *
 * -------------------------------------------------------------------------
 */

#include "knl_dc.h"
#include "knl_slm.h"
#include "cm_timer.h"
#include "cm_date.h"
#include "cm_list.h"
#include "cm_defs.h"
#include "knl_database.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define HOUR_PRE_TIME 1800000000
#define DAY_PRE_TIME 3600000000

#define PART_TYPE_HOUR 0
#define PART_TYPE_DAY 1

#define PART_NAME_MAX_LEN 12
#define PART_HIBOUND_VAL_LEN 24
#define TIME_SCALE_PART_KEY_MAX_BUFF 256

#define MAX_PART_OPER_PRE 500
#define CYCLE_TIME 300000000

static date_t last_run_time = 0;

static text_t g_part_fomarts[2] = {
    {.str = (char *)"YYYYMMDDHH24",
     .len = 12},
    {.str = (char *)"YYYYMMDD",
     .len = 8}};

static text_t g_part_cycle[2] = {
    {.str = (char *)"1h",
     .len = 2},
    {.str = (char *)"1d",
     .len = 2}
};

// 计算表的存储时间
static  date_t get_tsdb_retention(table_t *user_table)
{
    date_t value = 0;
    if (user_table->desc.retention.day != 0)
        value = (date_t)user_table->desc.retention.day * SECONDS_PER_DAY * MICROSECS_PER_SECOND;
    else if (user_table->desc.retention.hour != 0)
        value = (date_t)user_table->desc.retention.hour * SECONDS_PER_HOUR * MICROSECS_PER_SECOND;
    return value;
}

//获取待创建新分区名
static  status_t get_new_partname(uint32 part_type, text_t *new_partname, knl_session_t *session)
{
    date_t calc_time = 0;
    date_detail_t detail;
    new_partname->str = (char *)cm_push(session->stack, PART_NAME_MAX_LEN);
    if (new_partname->str == NULL)
    {
        GS_LOG_RUN_ERR("new part failed. \n");
        return GS_ERROR;
    }
    memset_s(new_partname->str, PART_NAME_MAX_LEN, 0, PART_NAME_MAX_LEN);

    memset_s(&detail, sizeof(detail),  0, sizeof(detail));
    if (part_type == PART_TYPE_HOUR)
    {
        calc_time = g_timer()->now + HOUR_PRE_TIME;
        cm_decode_date(calc_time, &detail);
        sprintf_s(new_partname->str, PART_NAME_MAX_LEN, "%04d%02d%02d%02d", detail.year, detail.mon, detail.day,detail.hour);
        GS_LOG_RUN_INF("now = %lld, calc_time = %lld \n", g_timer()->now, calc_time);
    }
    else
    {
        calc_time = g_timer()->now + DAY_PRE_TIME;
        cm_decode_date(calc_time, &detail);
        sprintf_s(new_partname->str, PART_NAME_MAX_LEN, "%04d%02d%02d", detail.year, detail.mon, detail.day);
        GS_LOG_RUN_INF("now = %lld, calc_time = %lld \n", g_timer()->now, calc_time);
    }
    new_partname->len = strlen(new_partname->str);
    GS_LOG_RUN_INF("part_type = %d, new_partname = %s \n", part_type, new_partname->str);
    return GS_SUCCESS;
}

// 根据表信息获取分区
static bool8 check_partname_can_drop(text_t *partname, uint32 part_type, date_t savetime, text_t * newpart, bool8 * have_newpart)
{
    int32 linepos = -1;
    if ((linepos = cm_text_rchr(partname, '_')) == -1)
    {
        GS_LOG_RUN_ERR("partname = %s, part_type = %d \n", partname, part_type);
        return GS_FALSE;
    }

    text_t tmpstr;
    tmpstr.str = (char *)calloc(1, GS_NAME_BUFFER_SIZE);
    cm_substrb(partname, linepos + 2, partname->len - linepos -1, &tmpstr);
    if(cm_get_error_code() != 0)
    {
        GS_LOG_RUN_ERR("tmpstr = %s \n", tmpstr.str);
        CM_FREE_PTR(tmpstr.str);
        cm_reset_error();
    }

    GS_LOG_RUN_INF("old part = %s, len= %d, new part = %s, len = %d \n", tmpstr.str, tmpstr.len, newpart->str, newpart->len);
    if (cm_text_equal(&tmpstr, newpart))
    {
        *have_newpart = GS_TRUE;
    }

    date_t partdate = 0;
    if (tmpstr.len > 0 && cm_text2date_fixed(&tmpstr, &g_part_fomarts[part_type], &partdate) != GS_SUCCESS)
    {
        GS_LOG_RUN_ERR("tmpstr = %s \n", tmpstr.str);
        CM_FREE_PTR(tmpstr.str);
        cm_reset_error();
        return GS_FALSE;
    }
    CM_FREE_PTR(tmpstr.str);
    GS_LOG_RUN_INF("now = %lld, partdate = %lld ,savetime = %lld\n", g_timer()->now, partdate, savetime);
    if (g_timer()->now - partdate >= savetime)
    {
        return GS_TRUE;
    }

    return GS_FALSE;
}

// 增加分区
static status_t add_add_part_oper(ptlist_t *oper_list, dc_user_t *user, table_t *user_table, uint32 part_type,
    text_t *part_suffix, knl_session_t *session)
{
    if (part_suffix == NULL || part_suffix->len == 0)
    {
        GS_LOG_RUN_ERR("part_suffix is invalied \n");
        return GS_ERROR;
    }

    // 设置分区描述
    knl_altable_def_t *altable_def = (knl_altable_def_t *)cm_push(session->stack, sizeof(knl_altable_def_t));
    if (altable_def == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,partname = %s\n", user_table->desc.name, part_suffix->str);
        return GS_ERROR;
    }

    text_t new_part_name;
    new_part_name.str = (char *)cm_push(session->stack, GS_NAME_BUFFER_SIZE);
    if (new_part_name.str == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,partname = %s\n", user_table->desc.name, part_suffix->str);
        return GS_ERROR;
    }
    sprintf_s(new_part_name.str, GS_MAX_NAME_LEN, "%s_%s", user_table->desc.name, part_suffix->str);
    new_part_name.len = strlen(new_part_name.str);
    date_t partdate = 0;
    if (cm_text2date_fixed(part_suffix, &g_part_fomarts[part_type], &partdate) != GS_SUCCESS)
    {
        GS_LOG_RUN_ERR("part = %s \n", part_suffix->str);
        cm_reset_error();
        return GS_FALSE;
    }
    if (part_type == PART_TYPE_HOUR)
    {
        partdate = partdate + SECONDS_PER_DAY * MICROSECS_PER_SECOND;
    }
    else
    {
        partdate = partdate + SECONDS_PER_HOUR * MICROSECS_PER_SECOND;
    }
        
    altable_def->user.len = (uint32)strlen(user->desc.name);
    altable_def->user.str = cm_push(session->stack, GS_NAME_BUFFER_SIZE);
    MEMS_RETURN_IFERR(memcpy_sp(altable_def->user.str, GS_NAME_BUFFER_SIZE, user->desc.name, strlen(user->desc.name)));

    altable_def->name.len = (uint32)strlen(user_table->desc.name);
    altable_def->name.str = cm_push(session->stack, GS_NAME_BUFFER_SIZE);
    MEMS_RETURN_IFERR(memcpy_sp(altable_def->name.str, GS_NAME_BUFFER_SIZE, user_table->desc.name, strlen(user_table->desc.name)));

    altable_def->part_def.name.str = new_part_name.str;
    altable_def->part_def.name.len = new_part_name.len;
    
    altable_def->action = ALTABLE_ADD_PARTITION;
    altable_def->part_def.obj_def = (knl_part_obj_def_t *)cm_push(session->stack, sizeof(knl_part_obj_def_t));
    if (altable_def->part_def.obj_def == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,partname = %s\n", user_table->desc.name, new_part_name.str);
        return GS_ERROR;
    }
    
    knl_part_obj_def_t *part_obj = altable_def->part_def.obj_def;
    MEMS_RETURN_IFERR(memset_s(part_obj, sizeof(knl_part_obj_def_t), 0, sizeof(knl_part_obj_def_t)));
    part_obj->part_type = PART_TYPE_RANGE;
    knl_part_def_t *part = NULL;
    cm_galist_init(&(part_obj->parts), session->stack, cm_stack_alloc);
    GS_RETURN_IFERR(cm_galist_new(&(part_obj->parts), sizeof(knl_part_def_t), (pointer_t *)&part));
    MEMS_RETURN_IFERR(memset_s(part, sizeof(knl_part_def_t), 0, sizeof(knl_part_def_t)));
    
    part->name.str = new_part_name.str;
    part->name.len = new_part_name.len;
    part->initrans = 0;
    part->pctfree = GS_INVALID_ID32;
    part->is_csf = GS_INVALID_ID8;
    part->space.len = 0;
    part->space.str = NULL;
    part->compress_algo = COMPRESS_NONE;
    part->is_parent = GS_FALSE;
    part->storage_def.initial = 0;
    part->storage_def.maxsize = 0;
    part->hiboundval.str = (char *)cm_push(session->stack, PART_HIBOUND_VAL_LEN);
    if (part->hiboundval.str == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,partname = %s\n", user_table->desc.name, new_part_name.str);
        return GS_ERROR;
    }
    cm_bigint2text(partdate, &part->hiboundval);
    part->partkey = (part_key_t *)cm_push(session->stack, TIME_SCALE_PART_KEY_MAX_BUFF);
    if(part->partkey  == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,partname = %s\n", user_table->desc.name, new_part_name.str);
        return GS_ERROR;
    }
    MEMS_RETURN_IFERR(memset_s(part->partkey, TIME_SCALE_PART_KEY_MAX_BUFF, 0, TIME_SCALE_PART_KEY_MAX_BUFF));
    part_key_init(part->partkey, 1);
    part_put_data(part->partkey, &partdate, sizeof(date_t), user_table->part_table->keycols[0].datatype);
    GS_LOG_RUN_INF("table_name = %s ,action = %d, part_name=%s \n",
                   user_table->desc.name, altable_def->action, altable_def->part_def.name.str);
    return cm_ptlist_add(oper_list, (pointer_t)altable_def);
}

// 删除分区
static status_t add_drop_part_oper(ptlist_t *oper_list, dc_user_t * user, table_t *user_table, uint32 part_no, knl_session_t *session)
{
    knl_altable_def_t *altable_def = (knl_altable_def_t *)cm_push(session->stack, sizeof(knl_altable_def_t));
    if (altable_def == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,part_no = %d\n", user_table->desc.name, part_no);
        return GS_ERROR;
    }
    table_part_t *part_entity = PART_GET_ENTITY(user_table->part_table, part_no);
    altable_def->user.len = (uint32)strlen(user->desc.name);
    altable_def->user.str = cm_push(session->stack, GS_NAME_BUFFER_SIZE);
    MEMS_RETURN_IFERR(memcpy_sp(altable_def->user.str, GS_NAME_BUFFER_SIZE, user->desc.name, strlen(user->desc.name)));

    altable_def->name.len = (uint32)strlen(user_table->desc.name);
    altable_def->name.str = cm_push(session->stack, GS_NAME_BUFFER_SIZE);
    MEMS_RETURN_IFERR(memcpy_sp(altable_def->name.str, GS_NAME_BUFFER_SIZE, user_table->desc.name, strlen(user_table->desc.name)));

    altable_def->part_def.name.len = (uint32)strlen(part_entity->desc.name);
    altable_def->part_def.name.str = (char *)cm_push(session->stack, GS_NAME_BUFFER_SIZE);
    if(altable_def->part_def.name.str == NULL)
    {
        GS_LOG_RUN_ERR("table_name = %s ,part_no = %d\n", user_table->desc.name, part_no);
        return GS_ERROR;
    }
    memset_s(altable_def->part_def.name.str, GS_NAME_BUFFER_SIZE, 0, GS_NAME_BUFFER_SIZE);
    MEMS_RETURN_IFERR(memcpy_sp(altable_def->part_def.name.str, GS_NAME_BUFFER_SIZE, part_entity->desc.name, strlen(part_entity->desc.name)));

    altable_def->action = ALTABLE_DROP_PARTITION;
    GS_LOG_RUN_INF("table_name = %s ,action = %d, altable_def->part_def.name.str = %s \n",
                   user_table->desc.name, altable_def->action, altable_def->part_def.name.str);
    return cm_ptlist_add(oper_list, (pointer_t) altable_def);
}

static status_t get_part_type(table_t *user_table, uint32 * part_type)
{
    if (cm_text_equal( &(user_table->part_table->desc.interval), &g_part_cycle[0]))
    {
        *part_type = PART_TYPE_HOUR;
    }
    else
    {
        *part_type = PART_TYPE_DAY;
    }
    return GS_SUCCESS;
}

static status_t oper_part_list(ptlist_t *oper_list, knl_session_t *session)
{
    if (oper_list != NULL && oper_list->count > 0)
    {
        for (int index = 0; index < oper_list->count; index++)
        {
            knl_altable_def_t *tmp = (knl_altable_def_t *)(oper_list->items[index]);
            if (tmp != NULL)
            {
                status_t result = GS_SUCCESS;
                GS_LOG_RUN_INF("table_name = %s , action = %d, part_name= %s, user = %s \n",
                               tmp->name.str, tmp->action, tmp->part_def.name.str, tmp->user.str);
                CM_SAVE_STACK(session->stack);
                if (tmp->action == ALTABLE_DROP_PARTITION)
                {
                    result = knl_alter_table(session, session->stack, tmp);
                }
                else if (tmp->action == ALTABLE_ADD_PARTITION)
                {
                    result = knl_alter_table(session, session->stack, tmp);
                }

                if (result != GS_SUCCESS)
                {
                    GS_LOG_RUN_ERR("table_name = %s , action = %d, part_name= %s, user = %s \n",
                                   tmp->name.str, tmp->action, tmp->part_def.name.str, tmp->user.str);
                }
                CM_RESTORE_STACK(session->stack);
                cm_reset_error(); 
            }
        }
    }
    return GS_SUCCESS;
}

static status_t time_scale_check(knl_session_t *session, dc_user_t *user, dc_entry_t *entry, text_t *new_day_part,
    text_t *new_hour_part, ptlist_t *oper_list)
{
    if (entry != NULL && entry->type == DICT_TYPE_TABLE && !entry->recycled)
    {
        GS_LOG_RUN_INF("name = %s, table_id = %d, uid = %d \n", entry->name, entry->id, user->desc.id);
        knl_dictionary_t dc;
        KNL_RESET_DC(&dc);
        dc.oid = entry->id;
        dc.uid = user->desc.id;
        cm_spin_lock(&entry->lock, &session->stat_dc_entry);
        dc_init_knl_dictionary(&dc, entry);
        cm_spin_unlock(&entry->lock);
        if (dc_open_entry(session, user, entry, &dc, GS_FALSE) != GS_SUCCESS)
        {
            cm_reset_error();
            return GS_ERROR;
        }

        if (entry->entity->table.desc.is_timescale && entry->entity->table.part_table->desc.auto_addpart)
        {
            uint32 part_type = 0;
            date_t savetime = 0;
            bool8 have_newpart = GS_FALSE;
            get_part_type(&(entry->entity->table), &part_type);
            text_t *part = part_type == PART_TYPE_HOUR ? new_hour_part : new_day_part;
            savetime = get_tsdb_retention(&(entry->entity->table));
            GS_LOG_RUN_INF("name = %s, part_type = %d, savetime = %lld, part_suffix=%s \n", entry->name, part_type, savetime, part->str);
            for (uint32 part_index = 0; part_index < PART_NAME_HASH_SIZE; part_index++)
            {
                uint32 part_no = entry->entity->table.part_table->pbuckets[part_index].first;
                while (part_no != GS_INVALID_ID32)
                {
                    table_part_t *part_entity = PART_GET_ENTITY(entry->entity->table.part_table, part_no);
                    GS_LOG_RUN_INF("part_no=%d ,part name = %s \n", part_no, part_entity->desc.name);
                    part_no = part_entity->pnext;
                    text_t partname;
                    cm_str2text(part_entity->desc.name, &partname);
                    if (check_partname_can_drop(&partname, part_type, savetime, part, &have_newpart) 
                        && add_drop_part_oper(oper_list, user, &(entry->entity->table), part_entity->part_no, session) != GS_SUCCESS)
                    {
                        GS_LOG_RUN_ERR("part name = %s \n", part_entity->desc.name); 
                    }
                    cm_reset_error();
                }
            }

            if (!have_newpart)
            {
                if (add_add_part_oper(oper_list, user, &(entry->entity->table), part_type, part, session) != GS_SUCCESS)
                {
                    GS_LOG_RUN_ERR("table_name = %s ,part suffix = %s \n", entry->entity->table.desc.name, part->str);
                }
                cm_reset_error();
            }
        }
        dc_close(&dc);
    }
    return GS_SUCCESS;
}

status_t check_all_table(knl_session_t *session, text_t *new_day_part, text_t *new_hour_part, ptlist_t *oper_list)
{
    knl_cursor_t *cursor = NULL;
    knl_set_session_scn(session, GS_INVALID_ID64);
    cursor = knl_push_cursor(session);
    knl_open_sys_cursor(session, cursor, CURSOR_ACTION_SELECT, SYS_TABLE_ID, GS_INVALID_ID32);
    knl_table_desc_t desc;
    for (;;)
    {
        if (knl_fetch(session, cursor) != GS_SUCCESS)
        {
            GS_LOG_RUN_ERR("knl_fetch failed\n");
            return GS_ERROR;
        }

        if (cursor->eof)
        {
            break;
        }

        MEMS_RETURN_IFERR(memset_s(&desc, sizeof(knl_table_desc_t), 0, sizeof(knl_table_desc_t)));
        
        dc_convert_table_desc(cursor, &desc);
        if (desc.is_timescale && !desc.recycled)
        {
            GS_LOG_RUN_INF("name = %s \n", desc.name);
            dc_user_t *user = session->kernel->dc_ctx.users[desc.uid];
            if(user == NULL)
            {
                continue;
            }
            cm_latch_s(&user->user_latch, session->id, GS_FALSE, NULL);
            dc_entry_t *entry = DC_GET_ENTRY(user, desc.id);
            cm_unlatch(&user->user_latch, NULL);
            time_scale_check(session, user, entry, new_day_part, new_hour_part, oper_list);
            if (oper_list->count >= MAX_PART_OPER_PRE)
            {
                break;
            }
        }
    }
    return GS_SUCCESS;
}

status_t tsm_lsm(knl_session_t *session)
{
    if (g_timer()->now - last_run_time < CYCLE_TIME)
    {
        return GS_SUCCESS;
    }
    last_run_time = g_timer()->now;
    dc_user_t *user = NULL;

    text_t new_day_part;  
    text_t new_hour_part;
    ptlist_t oper_list;
    cm_ptlist_init(&oper_list);
    CM_SAVE_STACK(session->stack);

    if (get_new_partname(PART_TYPE_HOUR, &new_hour_part, session) == GS_ERROR 
        || get_new_partname(PART_TYPE_DAY, &new_day_part, session) == GS_ERROR)
    {
        CM_RESTORE_STACK(session->stack);
        GS_LOG_RUN_ERR("get_new_partname failed. \n");
        return GS_ERROR;
    }
    check_all_table(session, &new_day_part,  &new_hour_part, &oper_list);

    oper_part_list(&oper_list, session);
    cm_destroy_ptlist(&oper_list);
    CM_RESTORE_STACK(session->stack);
    return GS_SUCCESS;
}

#ifdef __cplusplus
}
#endif