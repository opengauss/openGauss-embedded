/*
 * Copyright (c) 2022 Huawei Technologies Co.,Ltd.
 *
 * openGauss is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *          http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * -------------------------------------------------------------------------
 *
 * gstor_param.c
 *    gstor param
 *
 * IDENTIFICATION
 *    src/storage/gstor/gstor_param.c
 *
 * -------------------------------------------------------------------------
 */

#include "cm_defs.h"
#include "gstor_param.h"

config_item_t g_parameters[] = {
    // name (30B)             isdefault  attr      defaultvalue value     runtime desc range datatype           write_ini
    // -------------          ---------  ----      ------------ -----     ------- ---- ----- --------           -------- 
#if defined(INTARK_LITE)
    {"DATA_BUFFER_SIZE",        GS_TRUE, ATTR_NONE, "4M",       "4M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"USE_LARGE_PAGES",         GS_TRUE, ATTR_NONE, "FALSE",    "FALSE",    NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"REDO_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "8M",       "8M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  }, // 直接影响启动时的最大内存消耗
    {"USER_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "2M",       "2M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"UNDO_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "2M",       "2M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SWAP_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "2M",       "2M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SYSTEM_SPACE_SIZE",       GS_TRUE, ATTR_NONE, "2M",       "2M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SHARED_AREA_SIZE",        GS_TRUE, ATTR_NONE, "3M",       "8M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"MAX_RMS",                 GS_TRUE, ATTR_NONE, "1024",     "1024",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"INIT_LOCKPOOL_PAGES",     GS_TRUE, ATTR_NONE, "32",       "32",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"DEFAULT_EXTENTS",         GS_TRUE, ATTR_NONE, "8",        "8",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
#else           
    {"DATA_BUFFER_SIZE",        GS_TRUE, ATTR_NONE, "64M",      "64M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"USE_LARGE_PAGES",         GS_TRUE, ATTR_NONE, "TRUE",     "TRUE",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"REDO_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "16M",      "16M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"USER_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "8M",       "8M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"UNDO_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "8M",       "8M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SWAP_SPACE_SIZE",         GS_TRUE, ATTR_NONE, "8M",       "8M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SYSTEM_SPACE_SIZE",       GS_TRUE, ATTR_NONE, "8M",       "8M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SHARED_AREA_SIZE",        GS_TRUE, ATTR_NONE, "65M",      "65M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"MAX_RMS",                 GS_TRUE, ATTR_NONE, "16320",    "16320",    NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"INIT_LOCKPOOL_PAGES",     GS_TRUE, ATTR_NONE, "1024",     "1024",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"DEFAULT_EXTENTS",         GS_TRUE, ATTR_NONE, "32",       "32",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
#endif      
#if defined(DCC_LITE) || defined(INTARK_LITE)       
    {"LOG_BUFFER_SIZE",         GS_TRUE, ATTR_NONE, "1M",       "1M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"LOG_BUFFER_COUNT",        GS_TRUE, ATTR_NONE, "1",        "1",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"UNDO_RESERVE_SIZE",       GS_TRUE, ATTR_NONE, "64",       "64",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_UNDO_RESERVER_SIZE
    {"UNDO_SEGMENTS",           GS_TRUE, ATTR_NONE, "2",        "2",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_UNDO_SEGMENTS
    {"INDEX_BUF_SIZE",          GS_TRUE, ATTR_NONE, "16K",      "16K",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_INDEX_BUF_SIZE
#else           
    {"LOG_BUFFER_SIZE",         GS_TRUE, ATTR_NONE, "16M",      "16M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"LOG_BUFFER_COUNT",        GS_TRUE, ATTR_NONE, "4",        "4",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE  },
    {"UNDO_RESERVE_SIZE",       GS_TRUE, ATTR_NONE, "1024",     "1024",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"UNDO_SEGMENTS",           GS_TRUE, ATTR_NONE, "32",       "32",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"INDEX_BUF_SIZE",          GS_TRUE, ATTR_NONE, "32M",      "32M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
#endif          
#if defined(INTARK_LITE)            
    {"VMA_SIZE",                GS_TRUE, ATTR_NONE, "132K",     "132K",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"LARGE_VMA_SIZE",          GS_TRUE, ATTR_NONE, "2100K",    "2100K",    NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"LARGE_POOL_SIZE",         GS_TRUE, ATTR_NONE, "320K",     "320K",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"TEMP_BUF_SIZE",           GS_TRUE, ATTR_NONE, "1M",       "1M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"CR_POOL_SIZE",            GS_TRUE, ATTR_NONE, "16K",      "16K",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
#elif defined(DCC_LITE)         
    {"VMA_SIZE",                GS_TRUE, ATTR_NONE, "4M",       "4M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_VMA_SIZE
    {"LARGE_VMA_SIZE",          GS_TRUE, ATTR_NONE, "1M",       "1M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_LARGE_VMA_SIZE
    {"LARGE_POOL_SIZE",         GS_TRUE, ATTR_NONE, "4M",       "4M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_LARGE_POOL_SIZE
    {"TEMP_BUF_SIZE",           GS_TRUE, ATTR_NONE, "4M",       "4M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_TEMP_BUF_SIZE
    {"CR_POOL_SIZE",            GS_TRUE, ATTR_NONE, "1M",       "1M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE }, // DEFAULT_CR_POOL_SIZE
#else           
    {"VMA_SIZE",                GS_TRUE, ATTR_NONE, "16M",      "16M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"LARGE_VMA_SIZE",          GS_TRUE, ATTR_NONE, "16M",      "16M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"LARGE_POOL_SIZE",         GS_TRUE, ATTR_NONE, "16M",      "16M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"TEMP_BUF_SIZE",           GS_TRUE, ATTR_NONE, "128M",     "128M",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"CR_POOL_SIZE",            GS_TRUE, ATTR_NONE, "64M",      "64M",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
#endif          
    {"BUF_POOL_NUM",            GS_TRUE, ATTR_NONE, "1",        "1",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"PAGE_SIZE",               GS_TRUE, ATTR_NONE, "8K",       "8K",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"SPACE_SIZE",              GS_TRUE, ATTR_NONE, "96M",     "96M",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"UNDO_TABLESPACE",         GS_TRUE, ATTR_NONE, "UNDO",     "UNDO",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"ARCHIVE_MODE",            GS_TRUE, ATTR_NONE, "1",        "1",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"REDO_SAVE_TIME",          GS_TRUE, ATTR_NONE, "1800",     "1800",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"ARCHIVE_DEST_1",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_2",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_3",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_4",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_5",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_6",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_7",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_8",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_9",          GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_10",         GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_1",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_2",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_3",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_4",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_5",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_6",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_7",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_8",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_9",    GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"ARCHIVE_DEST_STATE_10",   GS_TRUE, ATTR_NONE, "ENABLE",   "ENABLE",   NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"_SYS_PASSWORD",           GS_TRUE, ATTR_NONE, "",         NULL,       NULL, "-", "-", "GS_TYPE_VARCHAR",  GS_FALSE },
    {"LOG_LEVEL",               GS_TRUE, ATTR_NONE, "3",        "3",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"CTRL_LOG_BACK_LEVEL",     GS_TRUE, ATTR_NONE, "0",        "0",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"TEMP_POOL_NUM",           GS_TRUE, ATTR_NONE, "1",        "1",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"CR_POOL_COUNT",           GS_TRUE, ATTR_NONE, "1",        "1",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"CKPT_INTERVAL",           GS_TRUE, ATTR_NONE, "300",      "300",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"CKPT_IO_CAPACITY",        GS_TRUE, ATTR_NONE, "4096",     "4096",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"LOG_REPLAY_PROCESSES",    GS_TRUE, ATTR_NONE, "1",        "1",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"RCY_SLEEP_INTERVAL",      GS_TRUE, ATTR_NONE, "32",       "32",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"DBWR_PROCESSES",          GS_TRUE, ATTR_NONE, "4",        "4",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"UNDO_RETENTION_TIME",     GS_TRUE, ATTR_NONE, "20",       "20",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"TX_ROLLBACK_PROC_NUM",    GS_TRUE, ATTR_NONE, "2",        "2",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"MAX_COLUMN_COUNT",        GS_TRUE, ATTR_NONE, "128",      "128",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"MAX_TEMP_TABLES",         GS_TRUE, ATTR_NONE, "128",      "128",      NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"STACK_SIZE",              GS_TRUE, ATTR_NONE, "512K",     "512K",     NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"TS_CAGG_SWITCH_ITEM",     GS_TRUE, ATTR_NONE, "FALSE",    "FALSE",    NULL, "-", "-", "GS_TYPE_INTEGER",  GS_TRUE  },
    {"JOB_QUEUE_CAPACITY",      GS_TRUE, ATTR_NONE, "60",       "60",       NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"EXEC_AGG_THREAD_NUM",     GS_TRUE, ATTR_NONE, "5",        "5",        NULL, "-", "-", "GS_TYPE_INTEGER",  GS_FALSE },
    {"CONTROL_FILES",           GS_TRUE, ATTR_NONE, "(%s/data/ctrl1,%s/data/ctrl2,%s/data/ctrl3)",  "(%s/data/ctrl1,%s/data/ctrl2,%s/data/ctrl3)",  NULL, "-", "-", "GS_TYPE_VARCHAR", GS_FALSE },
    {"ARCHIVE_FORMAT",          GS_TRUE, ATTR_NONE, "arch_%r_%s.arc",   "arch_%r_%s.arc",   NULL, "-", "-", "GS_TYPE_VARCHAR", GS_FALSE },
    {"LOGIN_PASSWORD",        GS_TRUE, ATTR_NONE, "",   "9d062088d7779405895f9b4304f555cb921c2a72af5388d318da8834c6ff00ac",   NULL, "-", "-", "GS_TYPE_VARCHAR", GS_TRUE},
    {"LOCK_WAIT_TIMEOUT",       GS_TRUE, ATTR_NONE, "60000",   "60000",   NULL, "-", "-", "GS_TYPE_VARCHAR", GS_TRUE},
    //LOGIN_PASSWORD sha256 algorithm, default passworkd:IntarkDB
};

// copy from g_parameters
/*
    ITEM: *name;
    ITEM: is_default;
    ITEM: attr;
    ITEM: *default_value;
    ITEM: *value;
    ITEM: *runtime_value;
    ITEM: *description;
    ITEM: *range;
    ITEM: *datatype;
    ITEM: is_write_ini;
*/
int knl_param_get_config_info(config_item_t **params, uint32 *count)
{
    // *params = g_parameters;
    *count = sizeof(g_parameters) / sizeof(config_item_t);

    *params = (config_item_t *)malloc(sizeof(g_parameters));
    if (*params == NULL) {
        GS_LOG_RUN_ERR("alloc params object failed");
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
        return GS_ERROR;
    }
    memset_s((*params), sizeof(g_parameters), 0, sizeof(g_parameters));

    for (int32 i = 0; i < *count; i++) {
        config_item_t * item = g_parameters + i;
        config_item_t * param_item = (*params) + i;
        // name
        if (!item->name) {
            param_item->name = NULL;
        } else {
            param_item->name = (char*)malloc(strlen(item->name) + 1);
            memset_s(param_item->name, strlen(item->name) + 1, 0, strlen(item->name) + 1);
            if (param_item->name == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->name, strlen(item->name), item->name, strlen(item->name));
        }
        // is_default
        param_item->is_default = item->is_default;
        // attr
        param_item->attr = item->attr;
        // default_value
        if (!item->default_value) {
            param_item->default_value = NULL;
        } else {
            param_item->default_value = (char*)malloc(strlen(item->default_value) + 1);
            memset_s(param_item->default_value, strlen(item->default_value) + 1, 0, strlen(item->default_value) + 1);
            if (param_item->default_value == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->default_value, strlen(item->default_value), item->default_value, strlen(item->default_value));
        }
        // value
        if (!item->value) {
            param_item->value = NULL;
        } else {
            param_item->value = (char*)malloc(strlen(item->value) + 1);
            memset_s(param_item->value, strlen(item->value) + 1, 0, strlen(item->value) + 1);
            if (param_item->value == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->value, strlen(item->value), item->value, strlen(item->value));
        }
        // runtime_value
        if (!item->runtime_value) {
            param_item->runtime_value = NULL;
        } else {
            param_item->runtime_value = (char*)malloc(strlen(item->runtime_value) + 1);
            memset_s(param_item->runtime_value, strlen(item->runtime_value) + 1, 0, strlen(item->runtime_value) + 1);
            if (param_item->runtime_value == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->runtime_value, strlen(item->runtime_value), item->runtime_value, strlen(item->runtime_value));
        }
        // description
        if (!item->description) {
            param_item->description = NULL;
        } else {
            param_item->description = (char*)malloc(strlen(item->description) + 1);
            memset_s(param_item->description, strlen(item->description) + 1, 0, strlen(item->description) + 1);
            if ((*params)->description == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->description, strlen(item->description), item->description, strlen(item->description));
        }
        // range
        if (!item->range) {
            param_item->range = NULL;
        } else {
            param_item->range = (char*)malloc(strlen(item->range) + 1);
            memset_s(param_item->range, strlen(item->range) + 1, 0, strlen(item->range) + 1);
            if (param_item->range == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->range, strlen(item->range), item->range, strlen(item->range));
        }
        // datatype
        if (!item->datatype) {
            param_item->datatype = NULL;
        } else {
            param_item->datatype = (char*)malloc(strlen(item->datatype) + 1);
            memset_s(param_item->datatype, strlen(item->datatype) + 1, 0, strlen(item->datatype) + 1);
            if (param_item->datatype == NULL) {
                GS_LOG_RUN_ERR("alloc params item failed");
                GS_THROW_ERROR(ERR_INVALID_PARAMETER, "copy from g_parameters");
                return GS_ERROR;
            }
            memcpy_s(param_item->datatype, strlen(item->datatype), item->datatype, strlen(item->datatype));
        }
        // is_write_ini
        param_item->is_write_ini = item->is_write_ini;
    }
}

status_t knl_param_get_size_uint64(config_t *config, char *param_name, uint64 *param_value)
{
    char *value = cm_get_config_value(config, param_name);
    if (value == NULL || strlen(value) == 0) {
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, param_name);
        return GS_ERROR;
    }
    int64 val_int64 = 0;
    if (cm_str2size(value, &val_int64) != GS_SUCCESS || val_int64 < 0) {
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, param_name);
        return GS_ERROR;
    }
    *param_value = (uint64)val_int64;
    return GS_SUCCESS;
}

status_t knl_param_get_uint32(config_t *config, char *param_name, uint32 *param_value)
{
    char *value = cm_get_config_value(config, param_name);
    if (value == NULL || strlen(value) == 0) {
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, param_name);
        return GS_ERROR;
    }

    if (cm_str2uint32(value, param_value) != GS_SUCCESS) {
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, param_name);
        return GS_ERROR;
    }
    return GS_SUCCESS;
}

status_t knl_param_get_size_uint32(config_t *config, char *param_name, uint32 *param_value)
{
    char *value = cm_get_config_value(config, param_name);
    int64 val_int64 = 0;

    if (value == NULL || strlen(value) == 0) {
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, param_name);
        return GS_ERROR;
    }

    if (cm_str2size(value, &val_int64) != GS_SUCCESS || val_int64 < 0 || val_int64 > UINT_MAX) {
        GS_THROW_ERROR(ERR_INVALID_PARAMETER, param_name);
        return GS_ERROR;
    }

    *param_value = (uint32)val_int64;
    return GS_SUCCESS;
}
