/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* om_agent.c
*
* IDENTIFICATION
* openGauss-embedded/src/network/om_agent/om_agent.c
*
* -------------------------------------------------------------------------
*/

#include <stdio.h>
#include <string.h>
#ifndef _WIN32
#include <sys/resource.h>
#endif

#include <dirent.h>
#include <time.h>
#include "storage/gstor/zekernel/common/cm_log.h"
#include "storage/gstor/zekernel/common/cm_thread.h"
#include "cs_listener.h"
#include "storage/gstor/zekernel/kernel/common/knl_context.h"
#include "interface/c/intarkdb_sql.h"
#include "interface/c/intarkdb_kv.h"
#include "include/intarkdb.h"
#include "json/svr_json.h"
#include "hashmap/hashmap.h"
#include "om_agent.h"
#include "monitor/status.h"

static char HOME_PATH[MAX_DB_PATH_SIZE] = {0};
static intarkdb_database om_agent_db;
intarkdb_connection login_conn;



status_t empty_proto_handle(cs_pipe_t *pipe, const cJSON * proto);
status_t kv_proto_handle(cs_pipe_t *pipe, const cJSON * proto);
status_t sql_proto_handle(cs_pipe_t *pipe, const cJSON * proto);
status_t status_proto_handle(cs_pipe_t *pipe, const cJSON * proto);
//status_t user_proto_handle(cs_pipe_t *pipe, const cJSON * proto);
status_t srv_om_agent_connect_action(tcp_lsnr_t *lsnr, cs_pipe_t *pipe);
status_t tcp_send_set_end(cs_pipe_t *pipe, const char *buf);
status_t varify_fail(cs_pipe_t *pipe);


typedef status_t(*proto_handle)(cs_pipe_t *pipe, const cJSON * const item);
static tcp_lsnr_t om_agent_lsnr;


static HashMap proto_map = NULL;
//todo:等kv的handle获取和sql一致后，后合并成一个
static HashMap transaction_map = NULL;
static HashMap transaction_sql_map = NULL;

typedef struct st_TransactionCfg
{
    bool8 isTransaction;
    uint32 option;   //Transactionl
    uint64 sessionId;
} *TransactionCfg;

TransactionCfg createTransaction(bool8 isTransaction, bool8 option, uint64 sessionId)
{
    TransactionCfg transaction_cfg = (TransactionCfg)malloc(sizeof(struct st_TransactionCfg));
    transaction_cfg->isTransaction = isTransaction;
    transaction_cfg->option = option;
    transaction_cfg->sessionId = sessionId;
    return transaction_cfg;
}

TransactionCfg createTransactionByJson(const cJSON * proto)
{
    bool8 isTransaction = GS_FALSE; //default false if not exsist
    uint32 option = Transaction_Begin;
    uint64 sessionId = 0;
    do
    {
        GS_BREAK_IF_ERROR(json_get_bool(proto, "isTransaction", &isTransaction));
        double tmp = 0;
        GS_BREAK_IF_ERROR(json_get_number(proto, "option", &tmp));
        option = (uint32)tmp;
        GS_BREAK_IF_ERROR(json_get_number(proto, "sessionId", &tmp));
        sessionId = (uint64)tmp;
    } while (0);

    return createTransaction(isTransaction, option, sessionId);
}


void init_om_agent_config(st_om_agent_config *cfg, const char * db_path)
{
    cfg->is_start_om_agent = DEFAULT_OM_AGENT_START;
    sprintf(cfg->db_path, "%s/gstor", db_path);
    cfg->om_agent_port = DEFAULT_OM_AGENT_PORT;
    strcpy(cfg->om_agent_hosts[0], DEFAULT_HOST);
    for (int i = 1; i < GS_MAX_LSNR_HOST_COUNT; i++)
    {
        strcpy(cfg->om_agent_hosts[i], "\0");
    }
    return;
}

status_t srv_start_om_agent_by_config(const intarkdb_database database, const char* config_path, const char * db_path)
{
    FILE *fp = fopen(config_path, "r");
    st_om_agent_config cfg;
    init_om_agent_config(&cfg, db_path);
    if (fp == NULL) {
        GS_LOG_DEBUG_INF("config file not exist, use default config");
        return srv_start_om_agent(database, &cfg);
    }
    char buf[CONFIG_SIZE];
    int ch;
    int i = 0;
    while ((ch = fgetc(fp)) != EOF)
    {
        buf[i++] = (char)ch;
    }
    buf[i] = '\0';
    fclose(fp);
    GS_LOG_DEBUG_INF("config:%s len:%ld", buf, strlen(buf));


    printf("line %d\n", __LINE__);
    cJSON* j_config = cJSON_Parse(buf);

    json_get_bool(j_config, "is_start_om_agent", &cfg.is_start_om_agent);
    double port = 0;
    json_get_number(j_config, "om_agent_port", &port);
    cfg.om_agent_port = port;
    cJSON *host_array;
    int size = 0;
    json_get_array(j_config, "om_agent_hosts", &host_array, &size);
    if (size > GS_MAX_LSNR_HOST_COUNT)
    {
        GS_LOG_DEBUG_INF("om_agent_hosts out of range");
        return GS_FALSE;
    }
    for(int host_i = 0; host_i < size; host_i++)
    {
        strcpy(cfg.om_agent_hosts[host_i], cJSON_GetStringValue(cJSON_GetArrayItem(host_array, host_i)));
    }

    return srv_start_om_agent(database, &cfg);
}

void register_proto_handles(int32 protoId, void *func)
{
    HashMapPut(proto_map, &protoId, func);
}


status_t srv_start_om_agent(const intarkdb_database database, const st_om_agent_config *cfg)
{
    GS_LOG_DEBUG_INF("om_agent start:%d port:%d", cfg->is_start_om_agent, cfg->om_agent_port);
    om_agent_db = database;
    if (!cfg->is_start_om_agent)
        return GS_SUCCESS;
    
    if (intarkdb_connect(om_agent_db, &login_conn) != SQL_SUCCESS)
    {
        GS_LOG_DEBUG_INF("db connect fail");
    }
    
    if(transaction_map == NULL)
        transaction_map = createtHashMap(TYPE_NUM_64);
    if(transaction_sql_map == NULL)
        transaction_sql_map = createtHashMap(TYPE_NUM_64);
    if(proto_map == NULL)
    {
        proto_map = createtHashMap(TYPE_NUM_32);
        register_proto_handles(PROTO_ID_NULL, empty_proto_handle);
        register_proto_handles(PROTO_ID_KV, kv_proto_handle);
        register_proto_handles(PROTO_ID_SQL, sql_proto_handle);
        register_proto_handles(PROTO_ID_STATUS, status_proto_handle);
    }

    strcpy(HOME_PATH, cfg->db_path);
    memcpy(om_agent_lsnr.host, cfg->om_agent_hosts, sizeof(cfg->om_agent_hosts));
    uint32 hosts_len = 8 * (CM_MAX_IP_LEN + 1);
    char str_hosts[8 * (CM_MAX_IP_LEN + 1)] = {0};
    for(uint32 i_host_num = 0; i_host_num < sizeof(om_agent_lsnr.host)/sizeof(om_agent_lsnr.host[0]); i_host_num++)
    {
        if (strlen(om_agent_lsnr.host[i_host_num]) == 0)
            break;
        snprintf_s(str_hosts, hosts_len, hosts_len -1, "%s%s|", str_hosts, om_agent_lsnr.host[i_host_num]);
    }
    om_agent_lsnr.port = cfg->om_agent_port;
    om_agent_lsnr.type = LSNR_TYPE_SERVICE;
    om_agent_lsnr.status = LSNR_STATUS_RUNNING;

    status_t status = cs_start_tcp_lsnr(&om_agent_lsnr, *srv_om_agent_connect_action, GS_TRUE);
    GS_LOG_DEBUG_INF("host:%s, port:%d, status:%d log level:%d", str_hosts, om_agent_lsnr.port, status, cm_log_param_instance()->log_level);
    return status;
}

void srv_close_om_agent()
{
    cm_close_thread(&om_agent_lsnr.thread);
}


uint32 GetBodySize(const unsigned char * head)
{
    uint32 size = 0;
    for(int i = 0; i < HEAD_SIZE; i++)
    {
        size = size | (head[i] << ((HEAD_SIZE - i - 1) * 8));
    }
    return size;
}

status_t srv_om_agent_connect_action(tcp_lsnr_t *lsnr, cs_pipe_t *pipe)
{
    cm_reset_error();
    char buf[MAX_BUF_SIZE] = {0};
    char head[HEAD_SIZE] = {0};
    int size = 0;
    if (cs_read_stream(pipe, head, SVR_TIME_OUT, HEAD_SIZE, &size) == GS_ERROR || size == 0)
    {
        GS_LOG_DEBUG_INF("socket read head err, maybe time out");
        cs_disconnect(pipe);
        return GS_SUCCESS;
    }
    uint32 body_size = GetBodySize(head);
    GS_LOG_DEBUG_INF("red head:%s size:%d bufsize:%d", head, size, body_size);
    if (cs_read_stream(pipe, buf, SVR_TIME_OUT, body_size, &size) == GS_ERROR)
    {
        GS_LOG_DEBUG_INF("socket read body err, maybe time out");
        cs_disconnect(pipe);
        return GS_SUCCESS;
    }
    GS_LOG_DEBUG_INF("%s tcp read buf:%s size:%ld max_size:%d", __func__, buf, strlen(buf), MAX_BUF_SIZE);

    cJSON* j_proto = cJSON_Parse(buf);
    double d_protoId = 0, timStamp = 0, seqid = 0;
    json_get_number(j_proto, "protoId", &d_protoId);
    int protoId = (int)d_protoId;
    char user_name[MAX_KEY_SIZE];
    unsigned char verification[MAX_BUF_SIZE];
    do {
    /*
        if (json_get_string(j_proto, "userName", user_name) == GS_SUCCESS && 
            json_get_string(j_proto, "verification", verification) == GS_SUCCESS &&
            json_get_number(j_proto, "seqId", &seqid) == GS_SUCCESS &&
            json_get_number(j_proto, "timeStamp", &timStamp) == GS_SUCCESS)
        {
            time_t timenow;
            time(&timenow);
            GS_LOG_DEBUG_INF("time:%d", timenow);
            if (abs(timenow - timStamp) > 5) 
            {
                varify_fail(pipe);
                break;
            }

            char password[SM3_DIGEST_SIZE * 2 + 1];
            get_user_password_sm3(&login_conn, user_name, &password); //todo deal rescode
            char str[256], dgst[SM3_DIGEST_SIZE * 2 + 1];
            sprintf(str, "%d%d%s", (uint32)seqid, (uint32)timStamp, password);
            get_hash_str(str, dgst);
            GS_LOG_DEBUG_INF("str:%s dgst:%s-%s", str, dgst, verification); //todo del
            if (strcmp(dgst, verification) != 0)
            {
                varify_fail(pipe);
                break;
            }
        }
        else
        {
            GS_LOG_DEBUG_INF("get verification info err");
            varify_fail(pipe);
            break;
        } */

        status_t handle_stauts = GS_SUCCESS;
        proto_handle func = HashMapGet(proto_map, &protoId);
        if(func == NULL)
        {
            GS_LOG_DEBUG_INF("protoId:%d err, out of proto_map size:%d", protoId, proto_map->size);
            int tmp_proto_id = 0;
            handle_stauts = ((proto_handle)HashMapGet(proto_map, &tmp_proto_id))(pipe, j_proto);
        }
        else
        {
            handle_stauts = func(pipe, j_proto);
        }
        GS_LOG_DEBUG_INF("protoId:%d status = %d", protoId, handle_stauts);
    } while (0);
    cs_disconnect(pipe);
    cJSON_Delete(j_proto);
    
    return GS_SUCCESS;
}

status_t tcp_send_set_end(cs_pipe_t *pipe, const char *buf)
{
    char sendbuf[strlen(buf) + HEAD_SIZE + 1];
    uint32 size = strlen(buf);
    for(int i = 0; i < HEAD_SIZE; i++)
    {
        sendbuf[i] = size >> (8 * (HEAD_SIZE - i - 1)) & 0xff;
    }
    memcpy(sendbuf + HEAD_SIZE, buf, strlen(buf)+1);
    GS_LOG_DEBUG_INF("bytes size:%s, size:%d", sendbuf, size);
    GS_LOG_DEBUG_INF("tcp send buf:%d%d%d%d - %s, size:%ld", sendbuf[0],sendbuf[1],sendbuf[2],sendbuf[3], sendbuf + HEAD_SIZE, strlen(buf) + HEAD_SIZE);

    return cs_write_stream(pipe, sendbuf, strlen(buf) + HEAD_SIZE, MAX_BUF_SIZE);
}

status_t empty_proto_handle(cs_pipe_t *pipe, const cJSON * proto)
{
    //error proto or error protoId
    GS_LOG_DEBUG_INF("%s proto: %s", __func__, cJSON_PrintUnformatted(proto));
    cJSON *res_proto = cJSON_CreateObject();
    cJSON_AddItemToObjectCS(res_proto, "rescode", cJSON_CreateNumber(RES_FIELD_ERR));
    cJSON_AddItemToObjectCS(res_proto, "msg", cJSON_CreateString("proto or protoId error"));
    char * res_str = cJSON_PrintUnformatted(res_proto);
    status_t status = tcp_send_set_end(pipe, res_str);
    GS_LOG_DEBUG_INF("%s tcp send buf:%s, size:%ld status:%d", __func__, res_str, strlen(res_str) + 1, status);
    return GS_ERROR;
}

status_t varify_fail(cs_pipe_t *pipe)
{
    //error proto or error protoId
    GS_LOG_DEBUG_INF("%s", __func__);
    cJSON *res_proto = cJSON_CreateObject();
    cJSON_AddItemToObjectCS(res_proto, "rescode", cJSON_CreateNumber(RES_PASSWORLD_ERROR));
    cJSON_AddItemToObjectCS(res_proto, "msg", cJSON_CreateString("authentication failed"));
    char * res_str = cJSON_PrintUnformatted(res_proto);
    status_t status = tcp_send_set_end(pipe, res_str);
    GS_LOG_DEBUG_INF("%s tcp send buf:%s, size:%ld status:%d", __func__, res_str, strlen(res_str) + 1, status);
    return GS_ERROR;
}

uint32 get_kv_handle(void **kv_handle, const TransactionCfg  transaction_cfg, char* res_msg, const char* table)
{
    uint32 rescode = RES_SUCCESS;
    if (transaction_cfg->isTransaction)
    {
        *kv_handle = HashMapGet(transaction_map, &transaction_cfg->sessionId);
        if (transaction_cfg->option == Transaction_Begin && *kv_handle != NULL) // sessionID conflict
        {
            strcpy(res_msg, "sessionId conflict");
            rescode = RES_SID_CONFLICT;
            GS_LOG_DEBUG_INF("%d %s", rescode, res_msg);
        }
        else if (transaction_cfg->option != Transaction_Begin && *kv_handle == NULL) // not find sessionID
        {
            strcpy(res_msg, "sessionId not exist");
            rescode = RES_SID_NOT_EXIST;
            GS_LOG_DEBUG_INF("%d %s", rescode, res_msg);
        }
    }
    if (!transaction_cfg->isTransaction || transaction_cfg->option == Transaction_Begin)
    {
        if (alloc_kv_handle(kv_handle) != GS_SUCCESS)
        {
            GS_LOG_DEBUG_INF("alloc kv_handle fail");
            strcpy(res_msg, "alloc kv_handle fail");
            rescode = RES_SERVER_ERR;
        }
        if (create_or_open_kv_table(*kv_handle, table) != GS_SUCCESS)
        {
            GS_LOG_DEBUG_INF("open table failed");
            strcpy(res_msg, "open table failed");
            rescode = RES_SERVER_ERR;
        }
    }
    return rescode;
}

uint32 get_sql_conn(intarkdb_connection *conn, const TransactionCfg  transaction_cfg, char* res_msg, const char* table)
{
    uint32 rescode = RES_SUCCESS;
    if (transaction_cfg->isTransaction)
    {
        *conn = HashMapGet(transaction_sql_map, &transaction_cfg->sessionId);
        if (transaction_cfg->option == Transaction_Begin && *conn != NULL) // sessionID conflict
        {
            strcpy(res_msg, "sessionId conflict");
            rescode = RES_SID_CONFLICT;
            GS_LOG_DEBUG_INF("%d %s", rescode, res_msg);
        }
        else if (transaction_cfg->option != Transaction_Begin && *conn == NULL) // not find sessionID
        {
            strcpy(res_msg, "sessionId not exist");
            rescode = RES_SID_NOT_EXIST;
            GS_LOG_DEBUG_INF("%d %s", rescode, res_msg);
        }
    }
    if (!transaction_cfg->isTransaction || transaction_cfg->option == Transaction_Begin)
    {
        if (intarkdb_connect(om_agent_db, conn) != SQL_SUCCESS)
        {
            GS_LOG_DEBUG_INF("db connect fail");
            strcpy(res_msg, "db connect fail");
            rescode = RES_SERVER_ERR;
        }
    }
    return rescode;
}

uint32 kv_set_operator(void *kv_handle, const char *key, const char * value, bool8 isCommit)
{
    intarkdb_kv_begin(kv_handle);
    uint32 rescode = RES_SUCCESS;
    intarkdbReply *reply = (intarkdbReply *)intarkdb_command_set(kv_handle, key, value);
    if (reply->type != GS_SUCCESS)
    {
        rescode = RES_SERVER_ERR;
        GS_LOG_RUN_ERR("Failed  error:%d", cm_get_os_error());
    }
    else if (isCommit)
    {
        intarkdb_kv_commit(kv_handle);
    }
    GS_LOG_DEBUG_INF("%s rescode:%d ret:%d", __func__, rescode, reply->type);
    return rescode;
}

uint32 kv_get_operator(void *kv_handle, const char *key, char *value)
{
    intarkdb_kv_begin(kv_handle);
    uint32 rescode = RES_SUCCESS;
    intarkdbReply *reply = (intarkdbReply *)intarkdb_command_get(kv_handle, key);
    if (reply->type == GS_SUCCESS)
    {
        if (reply->str != NULL)
        {
            strcpy(value,reply->str);
        }
        else
        {
            rescode = RES_NOT_EXIST;
            strcpy(value,"key not exist");
        }
    }
    else
    {
        rescode = RES_SERVER_ERR;
    }
    GS_LOG_DEBUG_INF("%s rescode:%d ret:%d value:%s", __func__, rescode, reply->type, value);
    return rescode;
}

uint32 kv_del_operator(void *kv_handle, const char *key, bool8 isCommit)
{
    intarkdb_kv_begin(kv_handle);
    uint32 rescode = RES_SUCCESS;
    int count = 0;
    intarkdbReply *reply = (intarkdbReply *)intarkdb_command_del(kv_handle, key, 0, &count);
    if (reply->type == GS_SUCCESS)
    {
        intarkdb_kv_commit(kv_handle);
    }
    else if (isCommit)
    {
        rescode = RES_SERVER_ERR;
    }
    GS_LOG_DEBUG_INF("%s rescode:%d ret:%d count:%d", __func__, rescode, reply->type, count);
    return rescode;
}

status_t kv_proto_handle(cs_pipe_t *pipe, const cJSON * proto)
{
    GS_LOG_DEBUG_INF("%s proto: %s", __func__, cJSON_PrintUnformatted(proto));
    char buf_cmd[KV_CMD_SIZE];
    char buf_key[MAX_KEY_SIZE];
    char buf_value[MAX_BUF_SIZE];
    char buf_table[MAX_KEY_SIZE];
    double seqId = 0;
    uint32 rescode = RES_SUCCESS;
    char res_msg[RES_MSG_SIZE] = "";

    cJSON *res_proto = cJSON_CreateObject();
    do
    {
        rescode = RES_FIELD_ERR;
        GS_BREAK_IF_ERROR(json_get_number(proto, "seqId", &seqId));
        GS_BREAK_IF_ERROR(json_get_string(proto, "cmd", buf_cmd));
        GS_BREAK_IF_ERROR(json_get_string(proto, "key", buf_key));
        if (json_get_string(proto, "table", buf_table) != GS_SUCCESS)
        {
            strcpy(buf_table, EXC_DCC_KV_TABLE);
        }
        if (strcmp(buf_cmd,"set")==0)
        {
            GS_BREAK_IF_ERROR(json_get_string(proto, "value", buf_value));
        }
        rescode = RES_SUCCESS;
    } while(0);
    TransactionCfg  transaction_cfg = createTransactionByJson(proto);

    cJSON_AddItemToObjectCS(res_proto, "seqId", cJSON_CreateNumber(seqId));
    do
    {
        if (rescode == RES_FIELD_ERR)
        {
            strcpy(res_msg, "field err or missing");
            break;
        }

        void *kv_handle;
        rescode = get_kv_handle(&kv_handle, transaction_cfg, res_msg, buf_table);
        GS_BREAK_IF_ERROR(rescode);

        if (strcmp(buf_cmd, "set") == 0)
        {
            rescode = kv_set_operator(kv_handle, buf_key, buf_value, !transaction_cfg->isTransaction);
        }
        else if (strcmp(buf_cmd, "get") == 0)
        {
            rescode = kv_get_operator(kv_handle, buf_key, buf_value);
            if (rescode == RES_NOT_EXIST)
            {
                strcpy(res_msg, buf_value);
            }
            else
            {
                cJSON_AddItemToObjectCS(res_proto, "value", cJSON_CreateString(buf_value));
            }
        }
        else if (strcmp(buf_cmd, "del") == 0)
        {
            rescode = kv_del_operator(kv_handle, buf_key, !transaction_cfg->isTransaction);
        }
        else if (strcmp(buf_cmd, "commit") == 0) //事务的提交，不做任何操作
        {
            rescode = RES_SUCCESS;
        }
        else
        {
            rescode = RES_CMD_ERR;
            sprintf(res_msg, "cmd:|%s| err", buf_cmd);
            break;
        }
        if (rescode == RES_SUCCESS && transaction_cfg->isTransaction)
        {
            if (transaction_cfg->option == Transaction_Commit )
            {
                intarkdb_kv_commit(kv_handle);
                HashMapRemove(transaction_map, &transaction_cfg->sessionId);
                free_kv_handle(kv_handle);

            }
            else if (transaction_cfg->option == Transaction_Begin)
            {
                HashMapPut(transaction_map, &transaction_cfg->sessionId, kv_handle);
            }
            else if (transaction_cfg->option == Transaction_Rollback)
            {
                intarkdb_kv_rollback(kv_handle);
                HashMapRemove(transaction_map, &transaction_cfg->sessionId);
                free_kv_handle(kv_handle);
            }
        }
        else if (!transaction_cfg->isTransaction)
        {
            db_free(kv_handle);
        }
        if (rescode == RES_SERVER_ERR)
        {
            strcpy(res_msg, "server err");
        }
    } while (0);


    cJSON_AddItemToObjectCS(res_proto, "rescode", cJSON_CreateNumber(rescode));
    cJSON_AddItemToObjectCS(res_proto, "msg", cJSON_CreateString(res_msg));
    char * res_str = cJSON_PrintUnformatted(res_proto);

    status_t status = tcp_send_set_end(pipe, res_str);
    GS_LOG_DEBUG_INF("tcp send buf:%s, size:%ld status:%d", res_str, strlen(res_str), status);
    free(transaction_cfg);
    return GS_SUCCESS;
}

status_t sql_proto_handle(cs_pipe_t *pipe, const cJSON * proto)
{
    GS_LOG_DEBUG_INF("%s proto: %s", __func__, cJSON_PrintUnformatted(proto));
    char buf_cmd[MAX_BUF_SIZE];
    double seqId = 0;
    uint32 rescode = RES_SUCCESS;
    char res_msg[RES_MSG_SIZE] = "Success";

    cJSON *res_proto = cJSON_CreateObject();
    do
    {
        rescode = RES_FIELD_ERR;
        GS_BREAK_IF_ERROR(json_get_number(proto, "seqId", &seqId));
        GS_BREAK_IF_ERROR(json_get_string(proto, "cmd", buf_cmd));
        rescode = RES_SUCCESS;
    } while(0);
    TransactionCfg  transaction_cfg = createTransactionByJson(proto);

    do
    {
        if (rescode == RES_FIELD_ERR)
        {
            strcpy(res_msg, "field err or missing");
            break;
        }
        intarkdb_connection conn;
        rescode = get_sql_conn(&conn, transaction_cfg, res_msg, NULL);
        GS_BREAK_IF_ERROR(rescode);

        intarkdb_result result = intarkdb_init_result();
        if (!result)
        {
            GS_LOG_DEBUG_INF("init_intarkdb_result failed");
            rescode = RES_SERVER_ERR;
            strcpy(res_msg, "server err");
            break;
        }
        if (transaction_cfg->isTransaction && transaction_cfg->option == Transaction_Begin)
        {
            if (intarkdb_query(conn, "BEGIN;", result) != SQL_SUCCESS)
            {
                GS_LOG_DEBUG_INF("begin err");
                rescode = RES_SERVER_ERR;
                strcpy(res_msg, "begin err");
                break;
            }
            intarkdb_free_row(result);
        }

        if (intarkdb_query(conn, buf_cmd, result) != SQL_SUCCESS)
        {
            char *err_msg = intarkdb_result_msg(result);
            GS_LOG_DEBUG_INF("query Failed msg:%s", err_msg);
            rescode = RES_SERVER_ERR;
            sprintf(res_msg, "server err msg:%s", err_msg);
            break;
        }
        GS_LOG_DEBUG_INF("Success to query database");
        uint64 row_count = intarkdb_row_count(result);
        int32 effect_row_count =  intarkdb_result_effect_row(result);
        GS_LOG_DEBUG_INF("row count:%llu EffectRows:%d", row_count, effect_row_count);
        cJSON_AddItemToObjectCS(res_proto, "effect_row_count", cJSON_CreateNumber(effect_row_count));
        if(result ->is_select) //is select?
        {
            uint64 column_count = intarkdb_column_count(result);
            cJSON *json_columns = cJSON_CreateArray();
            cJSON *json_types = cJSON_CreateArray();
            for (uint64 col = 0; col < column_count; col++)
            {
                char* columns = intarkdb_column_name(result, col);
                cJSON_AddItemReferenceToArray(json_columns, cJSON_CreateString(columns));
                char *type = (char*)malloc(MAX_TYPE_NAME_SIXE * sizeof(char));
                intarkdb_column_typename(result, col, type, MAX_TYPE_NAME_SIXE);
                cJSON_AddItemReferenceToArray(json_types, cJSON_CreateString(type));
                GS_LOG_DEBUG_INF("type:%s name:%s", type, columns);
                free(type);
            }
            cJSON_AddItemToObjectCS(res_proto, "columns", json_columns);
            cJSON_AddItemToObjectCS(res_proto, "types", json_types);
            cJSON *json_rows = cJSON_CreateArray();
            for (uint64 row = 0; row < row_count; row++)
            {
                cJSON *json_row = cJSON_CreateArray();
                for (uint64 col = 0; col < column_count; col++)
                {
                    char *value = intarkdb_value_varchar(result, row, col);
                    GS_LOG_DEBUG_INF("%lu %lu:%s", row, col, value);
                    if(value == NULL)
                        cJSON_AddItemReferenceToArray(json_row, cJSON_CreateNull());
                    else
                        cJSON_AddItemReferenceToArray(json_row, cJSON_CreateString(value));
                }
                cJSON_AddItemReferenceToArray(json_rows, json_row);
            }
            cJSON_AddItemToObject(res_proto, "value", json_rows);
        }
        intarkdb_free_row(result);

        if(transaction_cfg->isTransaction)
        {
            if (transaction_cfg->option == Transaction_Commit)
            {
                if (intarkdb_query(conn, "commit;", result) != SQL_SUCCESS)
                {
                    GS_LOG_DEBUG_INF("commit err");
                    rescode = RES_SERVER_ERR;
                    strcpy(res_msg, "commit err");
                }
                intarkdb_free_row(result);
                HashMapRemove(transaction_sql_map, &transaction_cfg->sessionId);
                intarkdb_disconnect(&conn);
            }
            else if (transaction_cfg->option == Transaction_Begin)
            {
                HashMapPut(transaction_sql_map, &transaction_cfg->sessionId, conn);
            }
            else if (transaction_cfg->option == Transaction_Rollback)
            {
                if (intarkdb_query(conn, "rollback;", result) != SQL_SUCCESS)
                {
                    GS_LOG_DEBUG_INF("rollback err");
                    rescode = RES_SERVER_ERR;
                    strcpy(res_msg, "rollback err");
                }
                intarkdb_free_row(result);
                HashMapRemove(transaction_sql_map, &transaction_cfg->sessionId);
                intarkdb_disconnect(&conn);
            }
        }
        else
        {
            intarkdb_disconnect(&conn);
        }
        intarkdb_destroy_result(result);
    } while (0);
    cJSON_AddItemToObjectCS(res_proto, "rescode", cJSON_CreateNumber(rescode));
    cJSON_AddItemToObjectCS(res_proto, "seqId", cJSON_CreateNumber(seqId));
    cJSON_AddItemToObjectCS(res_proto, "msg", cJSON_CreateString(res_msg));
    char * res_str = cJSON_PrintUnformatted(res_proto);
    status_t status = tcp_send_set_end(pipe, res_str);
    GS_LOG_DEBUG_INF("tcp send buf:%s, size:%ld status:%d", res_str, strlen(res_str), status);
    return GS_SUCCESS;
}
#ifndef _WIN32
status_t get_sys_status(cJSON* j_value)
{

    //todo 统计放到单独的统计线程定时跑
    uint32 pid = cm_get_current_thread_id();
    //cpu
    struct rusage self_ru;
    getrusage(RUSAGE_SELF, &self_ru);
    uint64 cpu_time1 = get_cpu_total_occupy();

    uint32 time_internal = 100;
    cm_sleep(time_internal);
    struct rusage self_ru2;
    getrusage(RUSAGE_SELF, &self_ru2);
    uint64 cpu_time2 = get_cpu_total_occupy();
    long proc_cpu_time = (self_ru2.ru_stime.tv_sec + self_ru2.ru_utime.tv_sec - self_ru.ru_stime.tv_sec - self_ru.ru_utime.tv_sec) * THOUSAND * THOUSAND + (self_ru2.ru_stime.tv_usec + self_ru2.ru_utime.tv_usec - self_ru.ru_stime.tv_usec - self_ru.ru_utime.tv_usec);
    uint64 size = get_dir_size(HOME_PATH)/DATA_CONVERSION;   //k
    GS_LOG_DEBUG_INF("pid:%u proc_cpu_time = %ld cpu time:%lld - %ld = %ld dir:%s size:%llu",
                    pid, proc_cpu_time, cpu_time2, cpu_time1, cpu_time2 - cpu_time1, HOME_PATH, size);
    cJSON_AddItemToObjectCS(j_value, "pid", cJSON_CreateNumber(pid));
    cJSON_AddItemToObjectCS(j_value, "cpu", cJSON_CreateNumber((double)proc_cpu_time/((cpu_time2 - cpu_time1) * THOUSAND)));
    cJSON_AddItemToObjectCS(j_value, "memory", cJSON_CreateNumber(get_memory_by_pid(pid))); //k
    cJSON_AddItemToObjectCS(j_value, "storage", cJSON_CreateNumber(size));
    return GS_SUCCESS;
}

uint64 get_start_time(uint64 pid)
{
    FILE *fd;
    char *p;
	unsigned long btime, run_time;
    char line[1024] = {0};
    char btime_name[8] = {0};
	fd = fopen("/proc/stat", "r");
	if (fd == NULL)
    {
        GS_LOG_DEBUG_INF("open /proc/stat failed");
        return 0;
    }
    while (fgets(line, sizeof(line), fd) != NULL )
    {
        if (strstr(line, "btime") != NULL)
        {
            sscanf(line, "%5s %lu", btime_name, &btime);
            break;
        }
    }
	close(fd);

    GS_LOG_DEBUG_INF("btime:%ld", btime);
    char pid_stat[255];
    sprintf(pid_stat, "/proc/%d/stat", pid);
    fd = fopen(pid_stat, "r");
    if (fd == NULL) {
        GS_LOG_DEBUG_INF("open %s failed", pid_stat);
        return 0;
    }
    char char_stat[1024] = {0};
    for(int i = 0; i < 22; i++)
    {
        fscanf(fd, "%s", char_stat);
    }
    run_time = strtoul(char_stat, &p, 10);
	close(fd);
    return run_time/sysconf(_SC_CLK_TCK) + btime;
}

status_t get_db_info(cJSON* j_value)
{
    uint32 pid = cm_get_current_thread_id();
    char *pname = strrchr(cm_sys_program_name(), '/') + 1;
    int64 start_time = get_start_time(cm_sys_pid());

    char real_data_path[GS_FILE_NAME_BUFFER_SIZE] = {0};
    char real_log_path[GS_FILE_NAME_BUFFER_SIZE] = {0};
    GS_RETURN_IFERR(realpath_file(HOME_PATH, real_data_path, GS_FILE_NAME_BUFFER_SIZE));
    sprintf(real_log_path, "%s/log", real_data_path);
    //cpu 
    GS_LOG_DEBUG_INF("pid:%u pname:%s ppid:%d start_time:%ld db_path:%s log_path:%s", 
                     pid, pname, cm_sys_pid(), start_time, real_data_path, real_log_path);
    cJSON_AddItemToObjectCS(j_value, "pid", cJSON_CreateNumber(pid));
    cJSON_AddItemToObjectCS(j_value, "pname", cJSON_CreateString(pname));
    cJSON_AddItemToObjectCS(j_value, "start_time", cJSON_CreateNumber(get_start_time(cm_sys_pid())));
    cJSON_AddItemToObjectCS(j_value, "db_path", cJSON_CreateString(real_data_path));
    cJSON_AddItemToObjectCS(j_value, "log_path", cJSON_CreateString(real_log_path));
    cJSON_AddItemToObjectCS(j_value, "version", cJSON_CreateString(get_version()));
    return GS_SUCCESS;
}

#endif

status_t status_proto_handle(cs_pipe_t *pipe, const cJSON * proto)
{
    GS_LOG_DEBUG_INF("%s proto: %s", __func__, cJSON_PrintUnformatted(proto));
    char buf_cmd[KV_CMD_SIZE];
    double seqId = 0;
    uint32 rescode = RES_SUCCESS;
    char res_msg[RES_MSG_SIZE] = "";

    cJSON *res_proto = cJSON_CreateObject();
    do
    {
        rescode = RES_FIELD_ERR;
        GS_BREAK_IF_ERROR(json_get_number(proto, "seqId", &seqId));
        GS_BREAK_IF_ERROR(json_get_string(proto, "cmd", buf_cmd));
        rescode = RES_SUCCESS;
    } while(0);

    do
    {
        if (rescode == RES_FIELD_ERR)
        {
            strcpy_s(res_msg, RES_MSG_SIZE, "field err or missing");
            break;
        }

        if (strcmp(buf_cmd, "status") == 0)
        {
            cJSON* j_value = cJSON_CreateObject();
            #ifdef _WIN32
            strcpy(res_msg, "not support windows");
            #else
            rescode = get_sys_status(j_value);
            cJSON_AddItemToObjectCS(res_proto, "value", j_value);
            #endif
        }
    } while(0);

    cJSON_AddItemToObjectCS(res_proto, "rescode", cJSON_CreateNumber(rescode));
    cJSON_AddItemToObjectCS(res_proto, "msg", cJSON_CreateString(res_msg));
    char * res_str = cJSON_PrintUnformatted(res_proto);

    status_t status = tcp_send_set_end(pipe, res_str);
    GS_LOG_DEBUG_INF("tcp send buf:%s, size:%ld status:%d", res_str, strlen(res_str), status);
    return GS_SUCCESS;
}

/* status_t user_proto_handle(cs_pipe_t *pipe, const cJSON * proto)
{
    GS_LOG_DEBUG_INF("%s proto: %s", __func__, cJSON_PrintUnformatted(proto));
    char buf_cmd[KV_CMD_SIZE], user[USER_MAX_LEN], password[PASSWORLD_MAX_LEN];
    double seqId = 0;
    uint32 rescode = RES_SUCCESS;
    char res_msg[RES_MSG_SIZE] = "";

    cJSON *res_proto = cJSON_CreateObject();
    do 
    {
        rescode = RES_FIELD_ERR;
        GS_BREAK_IF_ERROR(json_get_number(proto, "seqId", &seqId));
        GS_BREAK_IF_ERROR(json_get_string(proto, "cmd", buf_cmd));
        GS_BREAK_IF_ERROR(json_get_string(proto, "userName", user));
        GS_BREAK_IF_ERROR(json_get_string(proto, "password", password));
        rescode = RES_SUCCESS;
    } while(0);

    do
    {
        if (rescode == RES_FIELD_ERR)
        {
            strcpy_s(res_msg, RES_MSG_SIZE, "field err or missing");
            break;
        }

        
        if (strcmp(buf_cmd, "update") == 0)
        {
            if (update_user_sm3(&login_conn, user, password) != GS_SUCCESS)
            {
                rescode = RES_SERVER_ERR;
                strcpy_s(res_msg, RES_MSG_SIZE, "update user fail");
                break;
            }
        }
        else
        {
            GS_LOG_DEBUG_INF("field cmd:%s not support", buf_cmd);
            rescode = RES_CMD_ERR;
            strcpy_s(res_msg, RES_MSG_SIZE, "field cmd err");
            break;
        }
    } while(0);

    cJSON_AddItemToObjectCS(res_proto, "rescode", cJSON_CreateNumber(rescode));
    cJSON_AddItemToObjectCS(res_proto, "msg", cJSON_CreateString(res_msg));
    char * res_str = cJSON_PrintUnformatted(res_proto);

    status_t status = tcp_send_set_end(pipe, res_str);
    GS_LOG_DEBUG_INF("tcp send buf:%s, size:%ld status:%d", res_str, strlen(res_str), status);
    return GS_SUCCESS;
} */