/*
* Copyright (c) GBA-NCTI-ISDC. 2022-2024.
*
* openGauss embedded is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*
* http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FITFOR A PARTICULAR PURPOSE.
* See the Mulan PSL v2 for more details.
* -------------------------------------------------------------------------
*
* om_agent.h
*
* IDENTIFICATION
* openGauss-embedded/src/network/om_agent/om_agent.h
*
* -------------------------------------------------------------------------
*/

#ifndef __OM_AGENT_H__
#define __OM_AGENT_H__

#include "srv_def.h"
#include "storage/gstor/zekernel/common/cm_defs.h"
#include "storage/gstor/zekernel/common/cm_base.h"
#include "interface/c/intarkdb_sql.h"
#include "json/svr_json.h"
#include "cs_pipe.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct st_om_agent_config
{
    bool8 is_start_om_agent;
    char om_agent_hosts[GS_MAX_LSNR_HOST_COUNT][CM_MAX_IP_LEN];
    uint16 om_agent_port;
    char db_path[PATH_SIZE];
} st_om_agent_config;

EXPORT_API void init_om_agent_config(st_om_agent_config *cfg, const char * db_path);


EXPORT_API status_t srv_start_om_agent(const intarkdb_database database, const st_om_agent_config *cfg);
EXPORT_API status_t srv_start_om_agent_by_config(const intarkdb_database database, const char* config_path, const char * db_path);
EXPORT_API void srv_close_om_agent();
EXPORT_API status_t tcp_send_set_end(cs_pipe_t *pipe, const char *buf);
EXPORT_API void register_proto_handles();



#ifdef __cplusplus
}
#endif
#endif