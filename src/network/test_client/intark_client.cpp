#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include "client.h"
using namespace std;

int main() {
    string tablename = "TEST_TABLE";

    IntarkClient client{};
    ResStat stat = client.Connect("IntarkDB", "127.0.0.1", 9000, "root", "IntarkDB");
    if(stat.rescode != RES_SUCCESS) {
        printf("db connection err \n");
        return 0;
    }
    string create_sql = "create TABLE  if not exists  " + tablename + "(date timestamp,now_time timestamp , db_name varchar(50), value double) ";
    client.Query(create_sql);

    string insert_sql = "INSERT INTO " + tablename + " (date, now_time, db_name, value) VALUES (now(), now(), \'IntarkDB\', 1)";
    client.Query(insert_sql, true); 
    printf("EffectRow %d\n",client.GetResult()->GetEffectRow());
    string select_sql = "select * from " + tablename + " ;";
    client.Query(select_sql);
    client.GetResult()->Print();
    client.Close();

    stat = client.Connect("db1", "127.0.0.1", 9000, "root", "IntarkDB");
    if(stat.rescode != RES_SUCCESS) {
        printf("db connection err \n");
        return 0;
    }
    client.Query(create_sql);
    string insert_sql2 = "INSERT INTO " + tablename + " (date, now_time, db_name, value) VALUES (now(), now(), 'db1', 1)";
    client.Query(insert_sql2);
    //string select_sql = "select * from " + tablename + " ;";
    client.Query(select_sql);
    client.GetResult()->Print();
    client.Close();
}