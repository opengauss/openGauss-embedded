#include "client.h"
#include "../common/srv_def.h"
#include "dependency/GmSSL/include/gmssl/sha2.h"
#include <time.h>
#include <sstream>

IntarkClient::IntarkClient() {
    result = std::make_unique<RecordBatch>(Schema());
}

IntarkClient::~IntarkClient() {
    if (isConncet)
        Close();

}

const RecordBatch* IntarkClient::GetResult(){
    return result.get();
}

std::string IntarkClient::getSeqId() {
    uint64_t myt = time(NULL);
    uint64_t seqid =  time(NULL) << 48 && ++seqid_idx;
    std::stringstream ss;
    ss << seqid;
    return ss.str();
}   

bool IntarkClient::call_timed(PackProto &req, PackProto &res) {
    bool32 ready = false;
    req.sendProto(&pipe);
    if (cs_wait(&pipe, CS_WAIT_FOR_READ, pipe.socket_timeout, &ready) != GS_SUCCESS) {
        return false;
    }

    if (!ready) {
        return false;
    }
    if (res.unpacket(&pipe) == GS_ERROR) {
        return false;
    }
    return true;
}

ResStat IntarkClient::Connect(std::string database, std::string host, int port, std::string user, std::string password) {
    ResStat stat;
    errno_t rc_memzero = memset_s(&pipe, sizeof(cs_pipe_t), 0, sizeof(cs_pipe_t));
    MEMS_RETVOID_IFERR(rc_memzero);
    pipe.type = CS_TYPE_TCP;
    pipe.version = CS_LOCAL_VERSION;
    pipe.connect_timeout = GS_CONNECT_TIMEOUT;
    pipe.socket_timeout = GS_CONNECT_TIMEOUT/100;
    this->database = database;
    this->user = user;
    char dest[SHA256_DIGEST_SIZE];
    sha256_digest((unsigned char *)password.c_str(), password.length() , dest);
    this->password.append(dest, 32);
    this->host = host;
    this->port = port;
    std::stringstream url;
    url << host << ":" << port;
    if (cs_connect(url.str().c_str(), &pipe, NULL, NULL, NULL) != GS_SUCCESS) {
        stat.rescode = -1;
        stat.msg << "[NET]connect to " << url.str()  << " failed code:" << cm_get_error_code() << " info:" 
                 << cm_get_errormsg(cm_get_error_code());
        return stat;
    }
    LoginProtoReq req;
    LoginProtoRes res;
    req.seq_id = getSeqId();
    req.min_proto_version = 1;
    req.max_proto_version = 1;
    req.database_name = database;
    req.user_name = user;
    req.user_passworld = this->password;
    
    call_timed(req, res);
    printf("Connect rescode %d, resmsg:%s \n", res.rescode, res.res_msg.c_str());
    if (res.rescode == RES_SUCCESS )
        return stat;
    isConncet = true;
    stat.rescode = res.rescode;
    stat.msg << res.res_msg;
    return stat;	
}

void IntarkClient::Close() {
    
    database.clear();
    user.clear();
    password.clear();
    port = 0;
    //todo del stmt prepare
    result->Clear();
    ResultCloseReq result_req;
    ResultCloseRes result_res;
    result_req.seq_id = getSeqId();
    call_timed(result_req, result_res);
    SessionCloseReq close_req;
    SessionCloseRes close_res;
    close_req.seq_id = getSeqId();
    call_timed(result_req, result_res);
    cs_disconnect(&pipe);
    isConncet = false;
    return;
}

ResStat IntarkClient::Query(std::string sql, bool need_result = false) { 
    ResStat stat;
    ExecuteProtoReq req;
    ExecuteProtoRes res;
    req.seq_id = getSeqId();
    req.sql = sql;
    req.key_mode = need_result;
    req.limit_rows = limit_rows;
    
    call_timed(req, res);
    printf("Query rescode %d, resmsg:%s \n", res.rescode, res.res_msg.c_str());

    if (res.rescode != RES_SUCCESS) {
        stat.rescode = res.rescode;
        stat.msg << res.res_msg;
        return stat;
    }

    if(res.has_result) {
        std::vector<SchemaColumnInfo> columns;
        for(auto &item : res.result.col_info) {
            SchemaColumnInfo col;
            col.col_name = {item.columnName};
            col.col_type = GStorDataType(item.type);
            columns.push_back(col);
        }
        result = std::make_unique<RecordBatch>(Schema(std::move(columns))); 
        result->SetRetCode(res.rescode);
        int i = 0;
        for(const auto &item : res.result.rows) {
            Record record(item.values);
            result->AddRecord(record);
        } 
    } else  {
        result = std::make_unique<RecordBatch>(Schema());
    }
    result->effect_row = res.effect_rows;
    return stat;
}