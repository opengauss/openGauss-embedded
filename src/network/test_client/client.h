#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include "../protocol/protocol.h"
#include "common/record_batch.h"


struct ResStat {
    uint32_t rescode = RES_SUCCESS;
    std::stringstream msg;
};

class IntarkClient{
public:
    IntarkClient();
    ~IntarkClient();
    ResStat Connect(std::string database, std::string host, int port, std::string user, std::string password);
    void Close();
    ResStat Query(std::string sql,  bool need_result = false);
    const RecordBatch* GetResult();

private:
    bool isConncet = false;
    std::string database;
    std::string user;
    std::string password;
    std::string host;
    int port;
    cs_pipe_t pipe;
    int seqid_idx = 0;

    uint32_t limit_rows = 0;
    std::unique_ptr<RecordBatch> result;


private:
    std::string getSeqId();
    bool call_timed(PackProto &req, PackProto &res);
};